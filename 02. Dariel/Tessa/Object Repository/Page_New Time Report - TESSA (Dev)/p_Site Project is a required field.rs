<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Site Project is a required field</name>
   <tag></tag>
   <elementGuidId>de32ae9e-efb9-4785-9a68-fe5c1a42e9d4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>p.jss6076.jss6077.jss6083</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[5]/div/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss6076 jss6077 jss6083</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Site Project is a required field</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1732&quot;]/main[@class=&quot;jss2248 jss2250&quot;]/form[1]/div[@class=&quot;jss5392&quot;]/div[@class=&quot;jss5393&quot;]/div[@class=&quot;jss5396&quot;]/div[@class=&quot;jss3511 jss3514 jss6016&quot;]/div[@class=&quot;jss6023&quot;]/div[@class=&quot;css-10nd86i&quot;]/div[@class=&quot;jss6024 jss6025 jss6027&quot;]/p[@class=&quot;jss6076 jss6077 jss6083&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[5]/div/p</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Site Project is a required field']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/p</value>
   </webElementXpaths>
</WebElementEntity>
