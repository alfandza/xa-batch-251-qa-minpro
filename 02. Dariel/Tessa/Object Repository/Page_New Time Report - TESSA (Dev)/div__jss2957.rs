<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div__jss2957</name>
   <tag></tag>
   <elementGuidId>448de7df-9dc2-4f42-8028-69ef75709bd1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.jss2957</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[2]/div/div/div[2]/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>menu</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>-1</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss2957</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;mui-fixed jss1210 jss1196&quot;]/div[@class=&quot;jss1199 jss1197&quot;]/div[@class=&quot;jss1228 jss1254 jss1229 jss1200 jss2080 jss1201 jss1204&quot;]/div[@class=&quot;jss2943 jss2081&quot;]/div[@class=&quot;jss2955&quot;]/div[@class=&quot;jss2956&quot;]/div[@class=&quot;jss2957&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div[2]/div/div</value>
   </webElementXpaths>
</WebElementEntity>
