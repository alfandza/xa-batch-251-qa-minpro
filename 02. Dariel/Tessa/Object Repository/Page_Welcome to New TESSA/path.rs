<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>path</name>
   <tag></tag>
   <elementGuidId>2803bfe2-2b8d-4d1a-ab98-9ea7953489c3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.jss1703.jss1697.jss1721 > span.jss1702 > svg.jss1681 > path</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>path</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>d</name>
      <type>Main</type>
      <value>M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss945&quot;]/div[@class=&quot;jss964&quot;]/div[@class=&quot;jss1177 jss1200 jss1194&quot;]/div[@class=&quot;jss1178 jss1217 jss1227 jss1239 jss1251&quot;]/div[@class=&quot;jss1632 jss1635 jss1713&quot;]/div[@class=&quot;jss1703 jss1716&quot;]/div[@class=&quot;jss1703 jss1697 jss1721&quot;]/span[@class=&quot;jss1702&quot;]/svg[@class=&quot;jss1681&quot;]/path[1]</value>
   </webElementProperties>
</WebElementEntity>
