<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>path_1</name>
   <tag></tag>
   <elementGuidId>f2c443e3-43bd-4234-8fdf-7ab44e9b8e51</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>li.jss4558 > div.jss5057 > button.jss2705.jss2699 > span.jss2704 > svg.jss2708 > path</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>path</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>d</name>
      <type>Main</type>
      <value>M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;mui-fixed jss3558 jss3544 jss5374&quot;]/div[@class=&quot;jss3547 jss3545&quot;]/div[@class=&quot;jss3511 jss3537 jss3512 jss3548 jss3549 jss3552 jss3557&quot;]/div[@class=&quot;jss6921 jss5416&quot;]/ul[@class=&quot;jss4553 jss4554&quot;]/li[@class=&quot;jss4558&quot;]/div[@class=&quot;jss5057&quot;]/button[@class=&quot;jss2705 jss2699&quot;]/span[@class=&quot;jss2704&quot;]/svg[@class=&quot;jss2708&quot;]/path[1]</value>
   </webElementProperties>
</WebElementEntity>
