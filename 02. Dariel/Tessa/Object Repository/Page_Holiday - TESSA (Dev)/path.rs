<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>path</name>
   <tag></tag>
   <elementGuidId>7ebbee0c-d915-4854-a42c-f0b38f926e56</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.jss2705.jss2699.jss9150 > span.jss2704 > svg.jss2708 > path</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>path</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>d</name>
      <type>Main</type>
      <value>M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1732&quot;]/main[@class=&quot;jss2248 jss2250&quot;]/div[@class=&quot;jss3511 jss3514 jss9142&quot;]/div[@class=&quot;jss2705 jss9145&quot;]/div[@class=&quot;jss2705 jss2699 jss9150&quot;]/span[@class=&quot;jss2704&quot;]/svg[@class=&quot;jss2708&quot;]/path[1]</value>
   </webElementProperties>
</WebElementEntity>
