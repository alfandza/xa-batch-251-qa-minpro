<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div__jss6504 jss8134 jss8138 jss6510 jss813_451092</name>
   <tag></tag>
   <elementGuidId>7e958dee-6178-4824-b7d8-efb7d996b999</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.jss6504.jss8134.jss8138.jss6510.jss8139.jss6513.jss8141.jss6506.jss8136.jss6505.jss8135.jss6512.jss8140</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[5]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss6504 jss8134 jss8138 jss6510 jss8139 jss6513 jss8141 jss6506 jss8136 jss6505 jss8135 jss6512 jss8140</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1732&quot;]/main[@class=&quot;jss2248 jss2250&quot;]/form[1]/div[@class=&quot;jss7480&quot;]/div[@class=&quot;jss7481&quot;]/div[@class=&quot;jss7484&quot;]/div[@class=&quot;jss3511 jss3514 jss8104&quot;]/div[@class=&quot;jss8111&quot;]/div[@class=&quot;jss8112 jss8113 jss8115&quot;]/div[@class=&quot;jss6504 jss8134 jss8138 jss6510 jss8139 jss6513 jss8141 jss6506 jss8136 jss6505 jss8135 jss6512 jss8140&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[5]/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='With Holiday'])[1]/preceding::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Leave Submission'])[1]/preceding::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[5]/div</value>
   </webElementXpaths>
</WebElementEntity>
