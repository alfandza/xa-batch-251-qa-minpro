<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Leave date is a required field</name>
   <tag></tag>
   <elementGuidId>8121c7df-eea1-4f27-826b-c8dfad4e7127</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.jss5061 > div.jss8112.jss8113.jss8115 > p.jss8147.jss8148.jss8154</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/form/div/div[2]/div/div/ul/div/div/div/div/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss8147 jss8148 jss8154</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Leave date is a required field</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1732&quot;]/main[@class=&quot;jss2248 jss2250&quot;]/form[1]/div[@class=&quot;jss7480&quot;]/div[@class=&quot;jss7481&quot;]/div[@class=&quot;jss7484&quot;]/div[@class=&quot;jss3511 jss3514 jss8104&quot;]/ul[@class=&quot;jss4553 jss4554&quot;]/div[@class=&quot;jss5058 jss5059 jss8305&quot;]/div[@class=&quot;jss5060&quot;]/div[@class=&quot;jss5061&quot;]/div[@class=&quot;jss8112 jss8113 jss8115&quot;]/p[@class=&quot;jss8147 jss8148 jss8154&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div/div[2]/div/div/ul/div/div/div/div/p</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Leave date is a required field']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//ul/div/div/div/div/p</value>
   </webElementXpaths>
</WebElementEntity>
