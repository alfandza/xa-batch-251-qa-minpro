<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div__jss13005 jss14625 jss14629 jss13011 js_85552f</name>
   <tag></tag>
   <elementGuidId>48b199b2-6e64-49a6-9745-9e59b46506d5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.jss13005.jss14625.jss14629.jss13011.jss14630.jss13014.jss14632.jss13006.jss14626.jss13013.jss14631</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[5]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss13005 jss14625 jss14629 jss13011 jss14630 jss13014 jss14632 jss13006 jss14626 jss13013 jss14631</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1732&quot;]/main[@class=&quot;jss2248 jss2250&quot;]/form[1]/div[@class=&quot;jss13971&quot;]/div[@class=&quot;jss13972&quot;]/div[@class=&quot;jss13975&quot;]/div[@class=&quot;jss3511 jss3514 jss14595&quot;]/div[@class=&quot;jss14602&quot;]/div[@class=&quot;jss14603 jss14604 jss14606&quot;]/div[@class=&quot;jss13005 jss14625 jss14629 jss13011 jss14630 jss13014 jss14632 jss13006 jss14626 jss13013 jss14631&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[5]/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='With Holiday'])[1]/preceding::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Leave Submission'])[1]/preceding::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[5]/div</value>
   </webElementXpaths>
</WebElementEntity>
