import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('TC_Login_Global'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Page_Dashboard - TESSA (Dev)/div_Time Report'))

WebUI.click(findTestObject('Object Repository/Page_Dashboard - TESSA (Dev)/div_Time Report Entry'))

WebUI.click(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/div__jss2073_1'))

WebUI.click(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/div_Presales'))

WebUI.click(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/div__jss2073'))

WebUI.click(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/div_Barima Handal R4jas'))

WebUI.click(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/div__jss6086'))

WebUI.click(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/div_project java'))

WebUI.click(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/div__jss6086_1'))

WebUI.click(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/div_Our Office'))

WebUI.click(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/input__date'))

WebUI.click(findTestObject('Page_New Time Report - TESSA (Dev)/span_15'))

WebUI.click(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/span_OK'))

WebUI.click(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/input__start'))

WebUI.mouseOver(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/div__jss2933'))

WebUI.click(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/span_OK'))

WebUI.click(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/input__end'))

WebUI.clickOffset(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/div__jss2957'), 100, 100)

WebUI.click(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/span_OK'))

WebUI.setText(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/input__description'), '!@#$%^&*()<>?')

WebUI.scrollToPosition(0, 2)

WebUI.click(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/span_Submit'))

WebUI.click(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/button_Continue'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/div_Some fields has invalid value, please c_a06eb9'), 
    0)

WebUI.verifyElementText(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/div_Some fields has invalid value, please c_a06eb9'), 
    'Some fields has invalid value, please correct it.')

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/div_Unable to process your submission'), 
    0)

WebUI.verifyElementText(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/textarea_Validation Failed'), 
    'Validation Failed')

