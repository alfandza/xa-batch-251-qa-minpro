import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('TC_Login_Global'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Page_Dashboard - TESSA (Dev)/div_Time Report'))

WebUI.click(findTestObject('Object Repository/Page_Dashboard - TESSA (Dev)/div_Time Report Entry'))

WebUI.click(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/input__date'))

WebUI.click(findTestObject('Page_New Time Report - TESSA (Dev)/span_15'))

WebUI.click(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/span_OK'))

WebUI.click(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/input__start'))

WebUI.mouseOver(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/div__jss2933'))

WebUI.click(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/span_OK'))

WebUI.click(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/input__end'))

WebUI.clickOffset(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/div__jss2957'), 100, 100)

WebUI.click(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/span_OK'))

WebUI.click(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/span_Submit'))

WebUI.click(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/button_Continue'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/div_Unable to process your submission'), 
    0)

WebUI.verifyElementText(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/div_Unable to process your submission'), 
    'Unable to process your submission')

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/p_Activity Type is a required field'), 
    0)

WebUI.verifyElementText(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/p_Activity Type is a required field'), 
    'Activity Type is a required field')

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/p_Customer is a required field'), 
    0)

WebUI.verifyElementText(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/p_Customer is a required field'), 
    'Customer is a required field')

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/p_Project is a required field'), 
    0)

WebUI.verifyElementText(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/p_Project is a required field'), 
    'Project is a required field')

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/p_Site Project is a required field'), 
    0)

WebUI.verifyElementText(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/p_Site Project is a required field'), 
    'Site Project is a required field')

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/p_Notes is a required field'), 
    0)

WebUI.verifyElementText(findTestObject('Object Repository/Page_New Time Report - TESSA (Dev)/p_Notes is a required field'), 
    'Notes is a required field')

