import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('TC_Login_Global'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Page_Dashboard - TESSA (Dev)/svg_Setup_jss2708 jss2711 jss4649 jss4664'))

WebUI.click(findTestObject('Object Repository/Page_Dashboard - TESSA (Dev)/div_Setup'))

WebUI.click(findTestObject('Object Repository/Page_Dashboard - TESSA (Dev)/div_Holiday'))

WebUI.click(findTestObject('Object Repository/Page_Holiday - TESSA (Dev)/svg_Holiday_jss2708'))

WebUI.click(findTestObject('Object Repository/Page_New Holiday - TESSA (Dev)/div__jss10077'))

WebUI.click(findTestObject('Object Repository/Page_New Holiday - TESSA (Dev)/div_Xsis Mitra Utama'))

WebUI.setText(findTestObject('Object Repository/Page_New Holiday - TESSA (Dev)/textarea__description'), ' Imlek Besar')

WebUI.click(findTestObject('Object Repository/Page_New Holiday - TESSA (Dev)/input__date'))

WebUI.click(findTestObject('Object Repository/Page_New Holiday - TESSA (Dev)/button_12'))

WebUI.click(findTestObject('Object Repository/Page_New Holiday - TESSA (Dev)/span_OK'))

WebUI.click(findTestObject('Object Repository/Page_New Holiday - TESSA (Dev)/span_Submit'))

WebUI.click(findTestObject('Object Repository/Page_New Holiday - TESSA (Dev)/span_Continue'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_New Holiday - TESSA (Dev)/p_holiday date already exists'), 
    0)

WebUI.verifyElementText(findTestObject('Object Repository/Page_New Holiday - TESSA (Dev)/p_holiday date already exists'), 
    'holiday date already exists')

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_New Holiday - TESSA (Dev)/textarea_Validation Failed'), 
    0)

WebUI.verifyElementText(findTestObject('Object Repository/Page_New Holiday - TESSA (Dev)/textarea_Validation Failed'), 'Validation Failed')

