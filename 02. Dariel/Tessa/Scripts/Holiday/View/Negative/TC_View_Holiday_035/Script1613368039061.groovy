import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('TC_Login_Global'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/View_Page_Holiday - TESSA (Dev)/svg_Setup_jss2708 jss2711 jss4649 jss4664'))

WebUI.click(findTestObject('Object Repository/View_Page_Holiday - TESSA (Dev)/div_Setup'))

WebUI.click(findTestObject('Object Repository/View_Page_Holiday - TESSA (Dev)/div_Holiday'))

WebUI.click(findTestObject('View_Page_Holiday - TESSA (Dev)/input_Holiday_jss6026 jss5814'))

WebUI.setText(findTestObject('Object Repository/View_Page_Holiday - TESSA (Dev)/input_Holiday_jss6026 jss5814'), 'Xsis Mitra Utama')

WebUI.sendKeys(findTestObject('Object Repository/View_Page_Holiday - TESSA (Dev)/input_Holiday_jss6026 jss5814'), Keys.chord(
        Keys.ENTER))

