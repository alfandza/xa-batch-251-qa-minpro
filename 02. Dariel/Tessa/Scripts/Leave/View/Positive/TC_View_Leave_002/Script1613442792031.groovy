import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('TC_Login_Global'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Page_Dashboard - TESSA (Dev)/svg_Setup_jss2708 jss2711 jss4649 jss4664'))

WebUI.click(findTestObject('Object Repository/Page_Dashboard - TESSA (Dev)/div_Setup'))

WebUI.click(findTestObject('Object Repository/Page_Dashboard - TESSA (Dev)/div_Leave'))

WebUI.click(findTestObject('Object Repository/Page_Leave - TESSA (Dev)/svg_Employee_jss2708'))

WebUI.click(findTestObject('Object Repository/Page_Leave - TESSA (Dev)/svg_Year_jss2708'))

WebUI.click(findTestObject('Object Repository/Page_Leave - TESSA (Dev)/input_None_jss7417'))

WebUI.click(findTestObject('Object Repository/Page_Leave - TESSA (Dev)/button_Company_jss2705 jss2699'))

WebUI.click(findTestObject('Object Repository/Page_Leave - TESSA (Dev)/input_None_jss7417'))

WebUI.click(findTestObject('Object Repository/Page_Leave - TESSA (Dev)/span_Apply'))

WebUI.click(findTestObject('Object Repository/Page_Leave - TESSA (Dev)/svg_Employee_jss2708_1'))

WebUI.click(findTestObject('Object Repository/Page_Leave - TESSA (Dev)/li_Leave ID'))

WebUI.click(findTestObject('Object Repository/Page_Leave - TESSA (Dev)/svg_Employee_jss2708_1_2'))

WebUI.click(findTestObject('Object Repository/Page_Leave - TESSA (Dev)/li_Descending'))

WebUI.click(findTestObject('Object Repository/Page_Leave - TESSA (Dev)/button_Employee_option-size'))

WebUI.click(findTestObject('Object Repository/Page_Leave - TESSA (Dev)/li_10'))

WebUI.click(findTestObject('Object Repository/Page_Leave - TESSA (Dev)/svg_Employee_jss2708_1_2_3'))

