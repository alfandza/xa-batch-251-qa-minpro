import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('TC_Login_Global'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Page_Dashboard - TESSA (Dev)/svg_Setup_jss2708 jss2711 jss4649 jss4664'))

WebUI.click(findTestObject('Object Repository/Page_Dashboard - TESSA (Dev)/div_Setup'))

WebUI.click(findTestObject('Object Repository/Page_Dashboard - TESSA (Dev)/div_Leave'))

WebUI.click(findTestObject('Object Repository/Page_Leave - TESSA (Dev)/div_Details_jss2705 jss2699 jss7235'))

WebUI.click(findTestObject('Object Repository/Page_Leave - TESSA (Dev)/button_Modify'))

WebUI.click(findTestObject('Object Repository/Page_Modify Leave - TESSA (Dev)/span_February 01, 2021'))

WebUI.click(findTestObject('Object Repository/Page_Modify Leave - TESSA (Dev)/input__items.0.leaveDate'))

WebUI.click(findTestObject('Object Repository/Page_Modify Leave - TESSA (Dev)/span_2'))

WebUI.click(findTestObject('Object Repository/Page_Modify Leave - TESSA (Dev)/span_OK'))

WebUI.click(findTestObject('Object Repository/Page_Modify Leave - TESSA (Dev)/input__isWithinHoliday'))

WebUI.click(findTestObject('Object Repository/Page_Modify Leave - TESSA (Dev)/span_Submit'))

WebUI.click(findTestObject('Object Repository/Page_Modify Leave - TESSA (Dev)/span_Continue'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Leave Details - TESSA (Dev)/div_Existing PID L0035 has been updated'), 
    0)

WebUI.verifyElementText(findTestObject('Object Repository/Page_Leave Details - TESSA (Dev)/div_Existing PID L0035 has been updated'), 
    'Existing PID L0036 has been updated')

