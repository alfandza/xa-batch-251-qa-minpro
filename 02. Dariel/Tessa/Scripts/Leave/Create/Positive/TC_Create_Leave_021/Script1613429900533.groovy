import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('TC_Login_Global'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Page_Dashboard - TESSA (Dev)/svg_Setup_jss2708 jss2711 jss4649 jss4664'))

WebUI.click(findTestObject('Object Repository/Page_Dashboard - TESSA (Dev)/div_Setup'))

WebUI.click(findTestObject('Object Repository/Page_Dashboard - TESSA (Dev)/div_Leave'))

WebUI.click(findTestObject('Object Repository/Page_Leave - TESSA (Dev)/button_Leave_jss2705 jss2699 jss2700'))

WebUI.click(findTestObject('Object Repository/Page_New Leave - TESSA (Dev)/div__jss2073'))

WebUI.click(findTestObject('Object Repository/Page_New Leave - TESSA (Dev)/div_Equine Global'))

WebUI.click(findTestObject('Object Repository/Page_New Leave - TESSA (Dev)/div__jss8157'))

WebUI.click(findTestObject('Object Repository/Page_New Leave - TESSA (Dev)/div_Tahunan_2'))

WebUI.click(findTestObject('Object Repository/Page_New Leave - TESSA (Dev)/span_Add'))

WebUI.click(findTestObject('Object Repository/Page_New Leave - TESSA (Dev)/input__items.0.leaveDate'))

WebUI.click(findTestObject('Object Repository/Page_New Leave - TESSA (Dev)/span_1'))

WebUI.click(findTestObject('Object Repository/Page_New Leave - TESSA (Dev)/span_OK'))

WebUI.setText(findTestObject('Object Repository/Page_New Leave - TESSA (Dev)/input__items.0.leaveDescription'), 'Sakit keras dan harus dirawat intensif 3 bulan di RS')

WebUI.click(findTestObject('Object Repository/Page_New Leave - TESSA (Dev)/div__jss11041'))

WebUI.click(findTestObject('Object Repository/Page_New Leave - TESSA (Dev)/div_2021_1'))

WebUI.click(findTestObject('Object Repository/Page_New Leave - TESSA (Dev)/div__jss13005 jss14625 jss14629 jss13011 js_85552f'))

WebUI.setText(findTestObject('Object Repository/Page_New Leave - TESSA (Dev)/textarea__name'), 'Sakit Keras')

WebUI.click(findTestObject('Object Repository/Page_New Leave - TESSA (Dev)/input__isWithinHoliday'))

WebUI.click(findTestObject('Object Repository/Page_New Leave - TESSA (Dev)/span_Reset'))

