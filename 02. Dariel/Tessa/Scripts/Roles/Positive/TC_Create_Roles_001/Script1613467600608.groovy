import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('TC_Login_Global'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Page_Dashboard - TESSA (Dev)/svg_Setup_jss2708 jss2711 jss4649 jss4664'))

WebUI.click(findTestObject('Object Repository/Page_Dashboard - TESSA (Dev)/div_Setup'))

WebUI.click(findTestObject('Object Repository/Page_Dashboard - TESSA (Dev)/div_Roles'))

WebUI.click(findTestObject('Object Repository/Page_Roles - TESSA (Dev)/button_Roles_jss2705 jss2699 jss2700'))

WebUI.click(findTestObject('Object Repository/Page_New Role - TESSA (Dev)/div__jss7453'))

WebUI.click(findTestObject('Object Repository/Page_New Role - TESSA (Dev)/div_Equine Global'))

WebUI.click(findTestObject('Object Repository/Page_New Role - TESSA (Dev)/svg_Grade_css-19bqh2r'))

WebUI.click(findTestObject('Object Repository/Page_New Role - TESSA (Dev)/div_5'))

WebUI.setText(findTestObject('Object Repository/Page_New Role - TESSA (Dev)/input__name'), 'Manager')

WebUI.setText(findTestObject('Object Repository/Page_New Role - TESSA (Dev)/input_Description_description'), 'Do all setup')

WebUI.click(findTestObject('Object Repository/Page_New Role - TESSA (Dev)/input_Description_isActive'))

WebUI.click(findTestObject('Object Repository/Page_New Role - TESSA (Dev)/input_Role Menu_menus.0.isAccess'))

WebUI.scrollToPosition(0, 1)

WebUI.click(findTestObject('Page_New Role - TESSA (Dev)/span_Submit'))

WebUI.click(findTestObject('Object Repository/Page_New Role - TESSA (Dev)/span_Continue'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Role Detail - TESSA (Dev)/div_A new Role ID R0082 has been created'), 
    0)

WebUI.verifyElementText(findTestObject('Object Repository/Page_Role Detail - TESSA (Dev)/div_A new Role ID R0082 has been created'), 
    'A new Role ID R0084 has been created')

