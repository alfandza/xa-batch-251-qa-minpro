import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('HR Competency Cluster/TC_CLUS_002'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/HR Competency Cluster/TC_CLUS_003/Page_Add New Cluster - TESSA (Dev)/input__name'), 
    'Tiopan Wahyu')

WebUI.setText(findTestObject('Object Repository/HR Competency Cluster/TC_CLUS_003/Page_Add New Cluster - TESSA (Dev)/textarea_Menambahkan Kompetensi'), 
    'Menambahkan Kompetensi')

WebUI.click(findTestObject('Object Repository/HR Competency Cluster/TC_CLUS_003/Page_Add New Cluster - TESSA (Dev)/span_Add'))

WebUI.setText(findTestObject('Object Repository/HR Competency Cluster/TC_CLUS_003/Page_Add New Cluster - TESSA (Dev)/input__categories.0.name'), 
    'Manual Tester')

WebUI.setText(findTestObject('Object Repository/HR Competency Cluster/TC_CLUS_003/Page_Add New Cluster - TESSA (Dev)/textarea_Melakukan Testing Aplikasi Secara Manual'), 
    'Melakukan Testing Aplikasi Secara Manual')

WebUI.click(findTestObject('Object Repository/HR Competency Cluster/TC_CLUS_003/Page_Add New Cluster - TESSA (Dev)/span_Submit'))

WebUI.click(findTestObject('Object Repository/HR Competency Cluster/TC_CLUS_003/Page_Add New Cluster - TESSA (Dev)/span_Continue'))

WebUI.verifyElementPresent(findTestObject('Object Repository/HR Competency Cluster/TC_CLUS_003/Page_Cluster Detail - TESSA (Dev)/span_Cluster Information'), 
    0)

