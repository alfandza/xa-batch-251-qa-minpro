import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('HR Competency Mapping/TC_MAPP_001'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/HR Competency Mapping/TC_MAPP_014/Page_HR Competency Mapping - TESSA (Dev)/p_BS 1 - Business Support Analyst'))

WebUI.click(findTestObject('Object Repository/HR Competency Mapping/TC_MAPP_014/Page_HR Competency Mapping - TESSA (Dev)/span_Modify'))

WebUI.click(findTestObject('Object Repository/HR Competency Mapping/TC_MAPP_014/Page_Modify Mapping - TESSA (Dev)/div_Equine Global'))

WebUI.click(findTestObject('Object Repository/HR Competency Mapping/TC_MAPP_014/Page_Modify Mapping - TESSA (Dev)/div_Niagaprima Paramitra'))

WebUI.click(findTestObject('Object Repository/HR Competency Mapping/TC_MAPP_014/Page_Modify Mapping - TESSA (Dev)/div__jss17535'))

WebUI.click(findTestObject('Object Repository/HR Competency Mapping/TC_MAPP_014/Page_Modify Mapping - TESSA (Dev)/div_Administrator'))

WebUI.click(findTestObject('Object Repository/HR Competency Mapping/TC_MAPP_014/Page_Modify Mapping - TESSA (Dev)/span_Submit'))

WebUI.click(findTestObject('Object Repository/HR Competency Mapping/TC_MAPP_014/Page_Modify Mapping - TESSA (Dev)/span_Continue'))

