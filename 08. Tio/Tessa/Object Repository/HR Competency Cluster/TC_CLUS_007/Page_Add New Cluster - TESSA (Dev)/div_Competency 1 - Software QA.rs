<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Competency 1 - Software QA</name>
   <tag></tag>
   <elementGuidId>0d111d3f-da23-4020-bc89-ce13294e7603</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/form/div/div[2]/div/div/ul/li/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.jss2705.jss4557.jss4560.jss4565.jss4566.jss4567.jss4568</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss2705 jss4557 jss4560 jss4565 jss4566 jss4567 jss4568</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Competency 1 - Software QA</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1732&quot;]/main[@class=&quot;jss2248 jss2250&quot;]/form[1]/div[@class=&quot;jss61371&quot;]/div[@class=&quot;jss61372&quot;]/div[@class=&quot;jss61375&quot;]/div[@class=&quot;jss3511 jss3514 jss61995&quot;]/ul[@class=&quot;jss4553 jss4554&quot;]/li[@class=&quot;jss4558&quot;]/div[@class=&quot;jss2705 jss4557 jss4560 jss4565 jss4566 jss4567 jss4568&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div/div[2]/div/div/ul/li/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Competencies'])[1]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//ul/li/div</value>
   </webElementXpaths>
</WebElementEntity>
