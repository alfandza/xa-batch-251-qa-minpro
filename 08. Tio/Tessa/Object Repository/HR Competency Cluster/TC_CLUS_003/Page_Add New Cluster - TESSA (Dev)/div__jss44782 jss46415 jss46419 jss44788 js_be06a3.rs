<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div__jss44782 jss46415 jss46419 jss44788 js_be06a3</name>
   <tag></tag>
   <elementGuidId>6add2502-7f98-495f-a389-8e3dbdf63477</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[2]/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.jss44782.jss46415.jss46419.jss44788.jss46420.jss44791.jss46422.jss44783.jss46416.jss44790.jss46421</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss44782 jss46415 jss46419 jss44788 jss46420 jss44791 jss46422 jss44783 jss46416 jss44790 jss46421</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1732&quot;]/main[@class=&quot;jss2248 jss2250&quot;]/form[1]/div[@class=&quot;jss45761&quot;]/div[@class=&quot;jss45762&quot;]/div[@class=&quot;jss45765&quot;]/div[@class=&quot;jss3511 jss3514 jss46385&quot;]/div[@class=&quot;jss46392&quot;]/div[@class=&quot;jss46393 jss46394 jss46396&quot;]/div[@class=&quot;jss44782 jss46415 jss46419 jss44788 jss46420 jss44791 jss46422 jss44783 jss46416 jss44790 jss46421&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[2]/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Competencies'])[1]/preceding::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add'])[1]/preceding::div[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/div[2]/div</value>
   </webElementXpaths>
</WebElementEntity>
