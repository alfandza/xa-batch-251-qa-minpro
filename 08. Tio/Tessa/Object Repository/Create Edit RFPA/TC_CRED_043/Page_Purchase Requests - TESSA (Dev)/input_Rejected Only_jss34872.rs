<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Rejected Only_jss34872</name>
   <tag></tag>
   <elementGuidId>76f45250-9492-432f-bdf6-acd21ad62ccb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//input[@value=''])[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss34872</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;mui-fixed jss3558 jss3544 jss30213&quot;]/div[@class=&quot;jss3547 jss3545&quot;]/div[@class=&quot;jss3511 jss3537 jss3512 jss3548 jss3549 jss3552 jss3557&quot;]/div[@class=&quot;jss34859 jss30255&quot;]/ul[@class=&quot;jss4553 jss4554&quot;]/li[@class=&quot;jss4558&quot;]/div[@class=&quot;jss5057&quot;]/span[@class=&quot;jss34860&quot;]/span[@class=&quot;jss2705 jss2699 jss34869 jss34863 jss34866&quot;]/span[@class=&quot;jss2704&quot;]/input[@class=&quot;jss34872&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@value=''])[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[5]/div[2]/span/span/span/input</value>
   </webElementXpaths>
</WebElementEntity>
