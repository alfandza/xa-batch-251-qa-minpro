<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_AUD (AU)</name>
   <tag></tag>
   <elementGuidId>37843a40-dd21-4644-b5c3-d2e554b3dede</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.jss26936.jss26923.jss26927.jss26945.jss26930.jss26938.jss26925.jss26937.jss26924 > div.jss26946.jss26931.jss26953 > div.jss26955</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[6]/div/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss26955</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>AUD (AU$)</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1&quot;]/main[@class=&quot;jss2898 jss2900&quot;]/form[1]/div[@class=&quot;jss26259&quot;]/div[@class=&quot;jss26260&quot;]/div[@class=&quot;jss26263&quot;]/div[@class=&quot;jss3141 jss3144 jss26893&quot;]/div[@class=&quot;jss26900&quot;]/div[@class=&quot;css-10nd86i&quot;]/div[@class=&quot;jss26901 jss26902 jss26904&quot;]/div[@class=&quot;jss26936 jss26923 jss26927 jss26945 jss26930 jss26938 jss26925 jss26937 jss26924&quot;]/div[@class=&quot;jss26946 jss26931 jss26953&quot;]/div[@class=&quot;jss26955&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[6]/div/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='|'])[3]/preceding::div[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='AUD (AU$)'])[2]/preceding::div[8]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[6]/div/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
