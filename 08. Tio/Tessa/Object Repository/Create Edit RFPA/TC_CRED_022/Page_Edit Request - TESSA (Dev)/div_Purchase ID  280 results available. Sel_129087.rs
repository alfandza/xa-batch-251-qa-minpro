<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Purchase ID  280 results available. Sel_129087</name>
   <tag></tag>
   <elementGuidId>142c4140-05b1-4dbe-9986-1ed09342cb65</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.jss13503</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss13503</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Purchase ID    280 results available. Select is focused ,type to refine list, press Down to open the menu, Customer *CV. Erselia Citra Persada|Project Name *XMU.PS1806.0075 - Erselia Project|Advance CashRequest Date *Currency *AUD (AU$)|Rate *Request ValueRequest Value (IDR)Notes *</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1&quot;]/main[@class=&quot;jss2898 jss2900&quot;]/form[1]/div[@class=&quot;jss12862&quot;]/div[@class=&quot;jss12863&quot;]/div[@class=&quot;jss12866&quot;]/div[@class=&quot;jss3141 jss3144 jss13496&quot;]/div[@class=&quot;jss13503&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Request Detail'])[1]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Collapse Sidebar'])[1]/following::div[7]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div/div/div/div/div[2]</value>
   </webElementXpaths>
</WebElementEntity>
