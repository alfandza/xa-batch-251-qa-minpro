<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Tio_categories.41.isAccess</name>
   <tag></tag>
   <elementGuidId>6d2cff28-8d2d-4876-afb3-b7fcd8c7e0f7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='categories.41.isAccess']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;categories.41.isAccess&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss26523</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>categories.41.isAccess</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-indeterminate</name>
      <type>Main</type>
      <value>false</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>d6467e50-555b-4753-acf0-1045b29fcca9</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1&quot;]/main[@class=&quot;jss3357 jss3359&quot;]/form[1]/div[@class=&quot;jss25551&quot;]/div[@class=&quot;jss25552&quot;]/div[@class=&quot;jss25555&quot;]/div[@class=&quot;jss3600 jss3603 jss26185&quot;]/ul[@class=&quot;jss5156 jss5157&quot;]/li[@class=&quot;jss5161&quot;]/div[@class=&quot;jss3569 jss5160 jss5163 jss5169 jss5170&quot;]/span[@class=&quot;jss3569 jss3572 jss26520 jss26514 jss26519 jss26521 jss26515&quot;]/span[@class=&quot;jss3577&quot;]/input[@class=&quot;jss26523&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='categories.41.isAccess']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div/div[2]/div/div/ul/li[10]/div/span/span/input</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[10]/div/span/span/input</value>
   </webElementXpaths>
</WebElementEntity>
