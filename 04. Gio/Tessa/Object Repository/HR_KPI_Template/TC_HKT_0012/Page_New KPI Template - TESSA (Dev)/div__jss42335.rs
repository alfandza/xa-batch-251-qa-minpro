<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div__jss42335</name>
   <tag></tag>
   <elementGuidId>b6e3d0e6-436c-4dd6-b6dc-93439cf6a37e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.jss42377.jss42364.jss42368.jss42386.jss42371.jss42379.jss42366.jss42378.jss42365 > div.jss42387.jss42372.jss42333 > div.jss42335</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='!@#$%'])[1]/preceding::div[7]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss42335</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;mui-fixed jss13063 jss13049&quot;]/div[@class=&quot;jss13052 jss13050&quot;]/div[@class=&quot;jss13812 jss13838 jss13813 jss13053 jss13054 jss13058 jss13061&quot;]/div[@class=&quot;jss42988&quot;]/div[@class=&quot;css-10nd86i&quot;]/div[@class=&quot;jss42342 jss42343 jss42345&quot;]/div[@class=&quot;jss42377 jss42364 jss42368 jss42386 jss42371 jss42379 jss42366 jss42378 jss42365&quot;]/div[@class=&quot;jss42387 jss42372 jss42333&quot;]/div[@class=&quot;jss42335&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='!@#$%'])[1]/preceding::div[7]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Account Management - NPP'])[1]/preceding::div[8]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/div/div/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
