<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg__css-19bqh2r</name>
   <tag></tag>
   <elementGuidId>fa056180-2060-4a66-92c3-7bde5f774737</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.css-1q3fsek > svg.css-19bqh2r</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>20</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>20</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 20 20</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focusable</name>
      <type>Main</type>
      <value>false</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>css-19bqh2r</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1732&quot;]/main[@class=&quot;jss2248 jss2250&quot;]/form[1]/div[@class=&quot;jss30987 jss31010&quot;]/div[@class=&quot;jss30988 jss31027 jss31052&quot;]/div[@class=&quot;jss3511 jss3514 jss31084&quot;]/div[@class=&quot;jss31085&quot;]/div[@class=&quot;jss30987 jss31010&quot;]/div[@class=&quot;jss30988 jss31027 jss31049 jss31061&quot;]/div[@class=&quot;css-10nd86i&quot;]/div[@class=&quot;jss31095 jss31096 jss31098&quot;]/div[@class=&quot;jss31130 jss31117 jss31121 jss31139 jss31124 jss31132 jss31119 jss31131 jss31118&quot;]/div[@class=&quot;jss31140 jss31125 jss31086&quot;]/div[@class=&quot;jss31087&quot;]/div[@class=&quot;css-1q3fsek&quot;]/svg[@class=&quot;css-19bqh2r&quot;]</value>
   </webElementProperties>
</WebElementEntity>
