<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div__jss61238 jss62855 jss62859 jss61247 js_6f9808</name>
   <tag></tag>
   <elementGuidId>fb04f7be-67c9-4832-b3a9-f6e5195a4ef2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.jss61238.jss62855.jss62859.jss61247.jss62862.jss61239.jss62856.jss61246.jss62861</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[5]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss61238 jss62855 jss62859 jss61247 jss62862 jss61239 jss62856 jss61246 jss62861</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss11889&quot;]/main[@class=&quot;jss12405 jss12407&quot;]/form[1]/div[@class=&quot;jss62201&quot;]/div[@class=&quot;jss62202&quot;]/div[@class=&quot;jss62205&quot;]/div[@class=&quot;jss13812 jss13815 jss62825&quot;]/div[@class=&quot;jss62832&quot;]/div[@class=&quot;jss62833 jss62834 jss62836&quot;]/div[@class=&quot;jss61238 jss62855 jss62859 jss61247 jss62862 jss61239 jss62856 jss61246 jss62861&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[5]/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Employee Level Submission'])[1]/preceding::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reset'])[1]/preceding::div[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[5]/div</value>
   </webElementXpaths>
</WebElementEntity>
