<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Automation Test Submitter</name>
   <tag></tag>
   <elementGuidId>ef79f152-fc45-49c9-8b6c-44e0c965296c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div/div/div[2]/div[2]/div/div/div/ul/div[2]/div/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss19148 jss19156 jss19177 jss19174 jss19271</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Automation Test Submitter</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss18523&quot;]/div[@class=&quot;jss18542&quot;]/div[@class=&quot;jss18755 jss18778 jss18772&quot;]/div[@class=&quot;jss18756 jss18795 jss18805 jss18817 jss18829&quot;]/div[@class=&quot;jss19210 jss19213 jss19284 jss19285&quot;]/div[@class=&quot;jss19293 jss19294&quot;]/div[@class=&quot;jss19295&quot;]/div[@class=&quot;jss19296&quot;]/div[@class=&quot;jss19297 jss18545&quot;]/ul[@class=&quot;jss19237&quot;]/div[2]/div[@class=&quot;jss19281 jss19241 jss19244 jss19249 jss19250&quot;]/div[@class=&quot;jss19268&quot;]/span[@class=&quot;jss19148 jss19156 jss19177 jss19174 jss19271&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/div/div[2]/div[2]/div/div/div/ul/div[2]/div/div/span</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Curriculum Specialist'])[1]/following::span[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Automation Tester'])[1]/following::span[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//ul/div[2]/div/div/span</value>
   </webElementXpaths>
</WebElementEntity>
