import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('HR_KPI_Template/TC_HKT_002'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/HR_KPI_Template/TC_HKT_003/Page_New KPI Template - TESSA (Dev)/div__jss31140 jss31125 jss31086'))

WebUI.click(findTestObject('Object Repository/HR_KPI_Template/TC_HKT_003/Page_New KPI Template - TESSA (Dev)/div_Xsis Mitra Utama_1'))

WebUI.click(findTestObject('Object Repository/HR_KPI_Template/TC_HKT_003/Page_New KPI Template - TESSA (Dev)/div__jss31088'))

WebUI.click(findTestObject('Object Repository/HR_KPI_Template/TC_HKT_003/Page_New KPI Template - TESSA (Dev)/div_Automation Test Submitter'))

WebUI.setText(findTestObject('Object Repository/HR_KPI_Template/TC_HKT_003/Page_New KPI Template - TESSA (Dev)/input__name'), 
    'Tester')

WebUI.setText(findTestObject('Object Repository/HR_KPI_Template/TC_HKT_003/Page_New KPI Template - TESSA (Dev)/textarea_t'), 
    't')

WebUI.setText(findTestObject('Object Repository/HR_KPI_Template/TC_HKT_003/Page_New KPI Template - TESSA (Dev)/textarea_te'), 
    'te')

WebUI.setText(findTestObject('Object Repository/HR_KPI_Template/TC_HKT_003/Page_New KPI Template - TESSA (Dev)/textarea_tes'), 
    'tes')

WebUI.setText(findTestObject('Object Repository/HR_KPI_Template/TC_HKT_003/Page_New KPI Template - TESSA (Dev)/textarea_test'), 
    'test')

WebUI.click(findTestObject('Object Repository/HR_KPI_Template/TC_HKT_003/Page_New KPI Template - TESSA (Dev)/button_Submit'))

WebUI.click(findTestObject('Object Repository/HR_KPI_Template/TC_HKT_003/Page_New KPI Template - TESSA (Dev)/button_Continue'))

