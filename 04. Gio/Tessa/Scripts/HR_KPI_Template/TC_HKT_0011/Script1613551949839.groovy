import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('HR_KPI_Template/TC_HKT_002'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('HR_KPI_Template/TC_HKT_0011/Page_New KPI Template - TESSA (Dev)/button_Add'))

WebUI.click(findTestObject('HR_KPI_Template/TC_HKT_0011/Page_New KPI Template - TESSA (Dev)/div__jss42335'))

WebUI.click(findTestObject('HR_KPI_Template/TC_HKT_0011/Page_New KPI Template - TESSA (Dev)/div_Account Management - NPP'))

WebUI.click(findTestObject('HR_KPI_Template/TC_HKT_0011/Page_New KPI Template - TESSA (Dev)/div__jss42335'))

WebUI.click(findTestObject('HR_KPI_Template/TC_HKT_0011/Page_New KPI Template - TESSA (Dev)/div_Penambahan New Logo'))

WebUI.setText(findTestObject('HR_KPI_Template/TC_HKT_0011/Page_New KPI Template - TESSA (Dev)/textarea_1'), 
    '1')

WebUI.setText(findTestObject('HR_KPI_Template/TC_HKT_0011/Page_New KPI Template - TESSA (Dev)/textarea_10'), 
    '10')

WebUI.setText(findTestObject('HR_KPI_Template/TC_HKT_0011/Page_New KPI Template - TESSA (Dev)/textarea_100'), 
    '100')

WebUI.setText(findTestObject('HR_KPI_Template/TC_HKT_0011/Page_New KPI Template - TESSA (Dev)/input__items.0.weight'), 
    '100')

WebUI.setText(findTestObject('HR_KPI_Template/TC_HKT_0011/Page_New KPI Template - TESSA (Dev)/input_Batas Minimum_items.0.threshold'), 
    '0,100')

WebUI.setText(findTestObject('HR_KPI_Template/TC_HKT_0011/Page_New KPI Template - TESSA (Dev)/input__items.0.amount'), 
    '0,100')

WebUI.click(findTestObject('HR_KPI_Template/TC_HKT_0011/Page_New KPI Template - TESSA (Dev)/button_Close'))

WebUI.click(findTestObject('HR_KPI_Template/TC_HKT_0011/Page_New KPI Template - TESSA (Dev)/div__jss42335_1'))

WebUI.click(findTestObject('HR_KPI_Template/TC_HKT_0011/Page_New KPI Template - TESSA (Dev)/div_Xsis Mitra Utama'))

WebUI.click(findTestObject('HR_KPI_Template/TC_HKT_0011/Page_New KPI Template - TESSA (Dev)/button_Submit'))

WebUI.click(findTestObject('HR_KPI_Template/TC_HKT_0011/Page_New KPI Template - TESSA (Dev)/button_Continue'))

