import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Employee_Level/TC_EPL_002'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Employee_Level/TC_EPL_0014/Page_New Employee Level - TESSA (Dev)/textarea_1'), 
    '1')

WebUI.setText(findTestObject('Object Repository/Employee_Level/TC_EPL_0014/Page_New Employee Level - TESSA (Dev)/textarea_12'), 
    '12')

WebUI.setText(findTestObject('Object Repository/Employee_Level/TC_EPL_0014/Page_New Employee Level - TESSA (Dev)/textarea_123'), 
    '123')

WebUI.setText(findTestObject('Object Repository/Employee_Level/TC_EPL_0014/Page_New Employee Level - TESSA (Dev)/textarea_1234'), 
    '1234')

WebUI.setText(findTestObject('Object Repository/Employee_Level/TC_EPL_0014/Page_New Employee Level - TESSA (Dev)/textarea_12345'), 
    '12345')

WebUI.setText(findTestObject('Object Repository/Employee_Level/TC_EPL_0014/Page_New Employee Level - TESSA (Dev)/textarea_123456'), 
    '123456')

WebUI.click(findTestObject('Object Repository/Employee_Level/TC_EPL_0014/Page_New Employee Level - TESSA (Dev)/button_Submit'))

WebUI.click(findTestObject('Object Repository/Employee_Level/TC_EPL_0014/Page_New Employee Level - TESSA (Dev)/button_Continue'))

