import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Employee_Level/TC_EPL_002'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Employee_Level/TC_EPL_0013/Page_New Employee Level - TESSA (Dev)/input__seq'), 
    '0,123')

WebUI.setText(findTestObject('Object Repository/Employee_Level/TC_EPL_0013/Page_New Employee Level - TESSA (Dev)/input__subSequence'), 
    '0,123')

WebUI.setText(findTestObject('Object Repository/Employee_Level/TC_EPL_0013/Page_New Employee Level - TESSA (Dev)/input__value'), 
    '123')

WebUI.setText(findTestObject('Object Repository/Employee_Level/TC_EPL_0013/Page_New Employee Level - TESSA (Dev)/textarea__description'), 
    '')

WebUI.setText(findTestObject('Object Repository/Employee_Level/TC_EPL_0013/Page_New Employee Level - TESSA (Dev)/textarea_T (1)'), 
    'T')

WebUI.setText(findTestObject('Object Repository/Employee_Level/TC_EPL_0013/Page_New Employee Level - TESSA (Dev)/textarea_Te'), 
    'Te')

WebUI.setText(findTestObject('Object Repository/Employee_Level/TC_EPL_0013/Page_New Employee Level - TESSA (Dev)/textarea_Tes'), 
    'Tes')

WebUI.setText(findTestObject('Object Repository/Employee_Level/TC_EPL_0013/Page_New Employee Level - TESSA (Dev)/textarea_Test'), 
    'Test')

WebUI.setText(findTestObject('Object Repository/Employee_Level/TC_EPL_0013/Page_New Employee Level - TESSA (Dev)/textarea_Teste'), 
    'Teste')

WebUI.setText(findTestObject('Object Repository/Employee_Level/TC_EPL_0013/Page_New Employee Level - TESSA (Dev)/textarea_Tester'), 
    'Tester')

WebUI.click(findTestObject('Object Repository/Employee_Level/TC_EPL_0013/Page_New Employee Level - TESSA (Dev)/button_Submit'))

WebUI.click(findTestObject('Object Repository/Employee_Level/TC_EPL_0013/Page_New Employee Level - TESSA (Dev)/button_Continue'))

