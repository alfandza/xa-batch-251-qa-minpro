import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Travel_Travel_Request/TC_TTR_002'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_003/Page_New Travel - TESSA (Dev)/div__jss34527 jss34512 jss34542'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_003/Page_New Travel - TESSA (Dev)/div_In Indonesia'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_003/Page_New Travel - TESSA (Dev)/input__start'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_003/Page_New Travel - TESSA (Dev)/button_OK'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_003/Page_New Travel - TESSA (Dev)/input__end'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_003/Page_New Travel - TESSA (Dev)/button_20'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_003/Page_New Travel - TESSA (Dev)/button_OK'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_003/Page_New Travel - TESSA (Dev)/div__jss34527 jss34512 jss34542'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_003/Page_New Travel - TESSA (Dev)/div_Aryasa Indah'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_003/Page_New Travel - TESSA (Dev)/div__jss34544'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_003/Page_New Travel - TESSA (Dev)/div_GNLPS00002 - General Purpose'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_003/Page_New Travel - TESSA (Dev)/div__jss34544'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_003/Page_New Travel - TESSA (Dev)/div_General Courtessy'))

WebUI.setText(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_003/Page_New Travel - TESSA (Dev)/input_Objectives_objective'), 
    'Tester')

WebUI.setText(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_003/Page_New Travel - TESSA (Dev)/input_Specific Target_target'), 
    'Submiter')

WebUI.setText(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_003/Page_New Travel - TESSA (Dev)/input_Comments_comment'), 
    'Tidak ada')

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_003/Page_New Travel - TESSA (Dev)/button_Submit'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_003/Page_New Travel - TESSA (Dev)/button_Continue'))

