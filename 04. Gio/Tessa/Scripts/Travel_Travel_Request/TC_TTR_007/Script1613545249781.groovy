import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Travel_Travel_Request/TC_TTR_002'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_007/Page_New Travel - TESSA (Dev)/span_Add'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_007/Page_New Travel - TESSA (Dev)/div__jss33779'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_007/Page_New Travel - TESSA (Dev)/div_AHMAD DEDAT'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_007/Page_New Travel - TESSA (Dev)/div__jss33779_1'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_007/Page_New Travel - TESSA (Dev)/div_Land'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_007/Page_New Travel - TESSA (Dev)/input__items.0.isRoundTrip'))

WebUI.setText(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_007/Page_New Travel - TESSA (Dev)/input__items.0.from'), 
    'jAKARTA')

WebUI.setText(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_007/Page_New Travel - TESSA (Dev)/input__items.0.destination'), 
    'SURABAYA')

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_007/Page_New Travel - TESSA (Dev)/input__items.0.departureDate'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_007/Page_New Travel - TESSA (Dev)/button_OK'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_007/Page_New Travel - TESSA (Dev)/input__items.0.returnDate'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_007/Page_New Travel - TESSA (Dev)/span_21'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_007/Page_New Travel - TESSA (Dev)/span_OK'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_007/Page_New Travel - TESSA (Dev)/input_Transport Cost_items.0.isTransportByCompany'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_007/Page_New Travel - TESSA (Dev)/input_Hotel Cost_items.0.isHotelByCompany'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_007/Page_New Travel - TESSA (Dev)/div__jss33779_1'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_007/Page_New Travel - TESSA (Dev)/div_In Indonesia'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_007/Page_New Travel - TESSA (Dev)/div__jss33779_1'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_007/Page_New Travel - TESSA (Dev)/div_Aryasa Indah'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_007/Page_New Travel - TESSA (Dev)/div__jss33779_1'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_007/Page_New Travel - TESSA (Dev)/div_XMU.P1804.0007 - SAHID INSERT'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_007/Page_New Travel - TESSA (Dev)/div__jss33779_1'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_007/Page_New Travel - TESSA (Dev)/div_Grogol'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_007/Page_New Travel - TESSA (Dev)/button_Submit'))

WebUI.click(findTestObject('Object Repository/Travel_Travel_Request/TC_TTR_007/Page_New Travel - TESSA (Dev)/button_Continue'))

