import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Login/TC_Login_002'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/OR_TimeLimit/Page_Dashboard - TESSA (Dev)/div_Setup'))

WebUI.click(findTestObject('Object Repository/OR_TimeLimit/Page_Dashboard - TESSA (Dev)/span_Time Limit'))

WebUI.click(findTestObject('Object Repository/OR_TimeLimit/Page_System Limit - TESSA (Dev)/button_Collapse Sidebar_option-filter (1)'))

WebUI.click(findTestObject('Object Repository/OR_TimeLimit/Page_System Limit - TESSA (Dev)/div_CategoryNone'))

WebUI.click(findTestObject('Object Repository/OR_TimeLimit/Page_System Limit - TESSA (Dev)/div_Expense'))

WebUI.click(findTestObject('Object Repository/OR_TimeLimit/Page_System Limit - TESSA (Dev)/button_Apply'))

