import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Login/TC_Login_002'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/OR_TimeLimit/Page_Dashboard - TESSA (Dev)/div_Setup'))

WebUI.click(findTestObject('Object Repository/OR_TimeLimit/Page_Dashboard - TESSA (Dev)/span_Time Limit'))

WebUI.click(findTestObject('Object Repository/OR_TimeLimit/Page_System Limit - TESSA (Dev)/div_antar niaga dayaExpense912DELLA MUTIARA_60371a'))

WebUI.click(findTestObject('Object Repository/OR_TimeLimit/Page_System Limit - TESSA (Dev)/div_antar niaga daya360 Assessment13DELLA M_f4412e'))

WebUI.click(findTestObject('Object Repository/OR_TimeLimit/Page_System Limit - TESSA (Dev)/div_antar niaga dayaKPI9DELLA MUTIARA4 hours ago'))

WebUI.click(findTestObject('Object Repository/OR_TimeLimit/Page_System Limit - TESSA (Dev)/div_Xsis Mitra UtamaTime Report Entry14ADIT_f22b6e'))

WebUI.click(findTestObject('Object Repository/OR_TimeLimit/Page_System Limit - TESSA (Dev)/div_Xsis Mitra UtamaTravel Settlement4ADITY_aa7267'))

WebUI.click(findTestObject('Object Repository/OR_TimeLimit/Page_System Limit - TESSA (Dev)/div_Optima Data InternasionalRFPA Settlemen_bd2e2a'))

WebUI.click(findTestObject('Object Repository/OR_TimeLimit/Page_System Limit - TESSA (Dev)/div_Niagaprima ParamitraKPI30Admina year ago'))

WebUI.click(findTestObject('Object Repository/OR_TimeLimit/Page_System Limit - TESSA (Dev)/div_Optima Data Internasional360 Assessment_5d0361'))

WebUI.click(findTestObject('Object Repository/OR_TimeLimit/Page_System Limit - TESSA (Dev)/div_Niagaprima Paramitra360 Assessment7Admi_d29f6e'))

WebUI.click(findTestObject('Object Repository/OR_TimeLimit/Page_System Limit - TESSA (Dev)/div_Xsis Mitra UtamaKPI30Admina year ago'))

