import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Project Notifications Setting/TC_PNS_001'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/TC_PNS_018/Page_Project Notification Settings - TESSA (Dev)/button_Collapse Sidebar_option-filter'))

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/TC_PNS_018/Page_Project Notification Settings - TESSA (Dev)/div_CompanyNone'))

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/TC_PNS_018/Page_Project Notification Settings - TESSA (Dev)/div_Xsis Mitra UtamaXSS'))

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/TC_PNS_018/Page_Project Notification Settings - TESSA (Dev)/div_RangeNone'))

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/TC_PNS_018/Page_Project Notification Settings - TESSA (Dev)/div_In A Range'))

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/TC_PNS_018/Page_Project Notification Settings - TESSA (Dev)/button_Reset'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ProjectNotificationSetting/TC_PNS_018/Page_Project Notification Settings - TESSA (Dev)/p_None'), 
    0)

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/TC_PNS_018/Page_Project Notification Settings - TESSA (Dev)/button_Apply'))

