import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Project Notifications Setting/TC_PNS_002'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/input__name'), 
    'Antar project due 30 days')

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/div__jss15897'))

WebUI.setText(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/input__react-select-2-input'), 
    'antar niaga daya')

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/div_antar niaga daya'))

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/div__jss15897'))

WebUI.setText(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/input__react-select-3-input'), 
    'Project Due Date Template')

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/div_Project Due Date Template'))

WebUI.setText(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/input__mailSubject'), 
    'Antar project due')

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/div__jss15897'))

WebUI.setText(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/input__react-select-4-input'), 
    'In')

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/div_In A Range'))

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/div_Setting IDThis field value will be auto_a01cb2'))

WebUI.setText(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/input_Min_min'), 
    '1')

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/div_Setting IDThis field value will be auto_a01cb2'))

WebUI.setText(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/input_Max_max'), 
    '30')

WebUI.scrollToElement(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/div_Project Type'), 
    0)

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/input_Project Type_types.0.checked'))

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/input_Extra Miles_types.1.checked'))

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/input_Maintenance_types.2.checked'))

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/input_Non Project_types.3.checked'))

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/input_Pre Sales_types.4.checked'))

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/input_Status_statuses.0.checked'))

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/input_Accepted_statuses.1.checked'))

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/input_Adjustment Needed_statuses.2.checked'))

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/input_Approved_statuses.3.checked'))

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/input_Canceled_statuses.4.checked'))

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/input_Closed_statuses.5.checked'))

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/input_In Progress_statuses.6.checked'))

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/input_New_statuses.7.checked'))

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/input_Notified_statuses.8.checked'))

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/input_Not Submitted_statuses.9.checked'))

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/input_Opened_statuses.10.checked'))

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/input_Rejected_statuses.11.checked'))

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/input_Reopened_statuses.12.checked'))

WebUI.scrollToPosition(0, 0)

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/button_Mail To_jss8090 jss8084'))

WebUI.setText(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/input__mailTo.0'), 
    'della.mutiara@xsis.net')

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/button_Mail To_jss8090 jss8084'))

WebUI.setText(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/input__mailCc.0'), 
    'della.mutiara@xsis.net')

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/button_Submit'))

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/Page_New Project Notification Setting - TES_f7eded/button_Continue'))

