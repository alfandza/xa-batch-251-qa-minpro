import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Project Notifications Setting/TC_PNS_002'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/OR_ProjectNotificationSetting/TC_PNS_010/Page_New Project Notification Setting - TES_f7eded/input__name'), 
    'Antar project due 28 days')

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/TC_PNS_010/Page_New Project Notification Setting - TES_f7eded/div__jss7898'))

WebUI.setText(findTestObject('Object Repository/OR_ProjectNotificationSetting/TC_PNS_010/Page_New Project Notification Setting - TES_f7eded/input__react-select-2-input'), 
    'antar niaga')

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/TC_PNS_010/Page_New Project Notification Setting - TES_f7eded/div_antar niaga daya'))

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/TC_PNS_010/Page_New Project Notification Setting - TES_f7eded/div__jss7898'))

WebUI.setText(findTestObject('Object Repository/OR_ProjectNotificationSetting/TC_PNS_010/Page_New Project Notification Setting - TES_f7eded/input__react-select-3-input'), 
    'project')

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/TC_PNS_010/Page_New Project Notification Setting - TES_f7eded/div_Project Due Date Template'))

WebUI.setText(findTestObject('Object Repository/OR_ProjectNotificationSetting/TC_PNS_010/Page_New Project Notification Setting - TES_f7eded/input__mailSubject'), 
    'antar project due')

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/TC_PNS_010/Page_New Project Notification Setting - TES_f7eded/div__jss7898'))

WebUI.setText(findTestObject('Object Repository/OR_ProjectNotificationSetting/TC_PNS_010/Page_New Project Notification Setting - TES_f7eded/input__react-select-4-input'), 
    'In')

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/TC_PNS_010/Page_New Project Notification Setting - TES_f7eded/div_In A Range'))

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/TC_PNS_010/Page_New Project Notification Setting - TES_f7eded/div_Setting IDThis field value will be auto_ac4d29'))

WebUI.setText(findTestObject('Object Repository/OR_ProjectNotificationSetting/TC_PNS_010/Page_New Project Notification Setting - TES_f7eded/input_Min_min'), 
    '1')

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/TC_PNS_010/Page_New Project Notification Setting - TES_f7eded/main_ProjectSetting IDThis field value will_206ef1'))

WebUI.setText(findTestObject('Object Repository/OR_ProjectNotificationSetting/TC_PNS_010/Page_New Project Notification Setting - TES_f7eded/input_Max_max'), 
    '28')

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/TC_PNS_010/Page_New Project Notification Setting - TES_f7eded/button_Submit'))

WebUI.click(findTestObject('Object Repository/OR_ProjectNotificationSetting/TC_PNS_010/Page_New Project Notification Setting - TES_f7eded/button_Continue'))

