<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Name_jss9840 jss10680 jss9841 jss10681</name>
   <tag></tag>
   <elementGuidId>9f891037-7283-40ca-aae6-2f35ebba3484</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@value='Xsis projects due in 30 days']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input.jss9840.jss10680.jss9841.jss10681</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>false</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss9840 jss10680 jss9841 jss10681</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Xsis projects due in 30 days</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1&quot;]/main[@class=&quot;jss517 jss519&quot;]/div[@class=&quot;jss996 jss999 jss10543 jss10544&quot;]/div[@class=&quot;jss3242 jss3243&quot;]/div[@class=&quot;jss3244&quot;]/div[@class=&quot;jss3245&quot;]/div[@class=&quot;jss10649&quot;]/div[@class=&quot;jss10552&quot;]/div[@class=&quot;jss10553 jss10592 jss10600 jss10611&quot;]/div[@class=&quot;jss10650 jss10652 jss10653&quot;]/div[@class=&quot;jss9830 jss10672 jss9839 jss10679 jss9831 jss10673 jss9837&quot;]/input[@class=&quot;jss9840 jss10680 jss9841 jss10681&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@value='Xsis projects due in 30 days']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/div[2]/div[2]/div/div/div/div/div/div/div/input</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div/input</value>
   </webElementXpaths>
</WebElementEntity>
