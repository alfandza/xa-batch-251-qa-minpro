<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Adjustment Needed_statuses.2.checked</name>
   <tag></tag>
   <elementGuidId>9c8c2ded-d8ee-4244-81c6-ad1b8b83b814</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='statuses.2.checked']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;statuses.2.checked&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss7948</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>statuses.2.checked</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-indeterminate</name>
      <type>Main</type>
      <value>false</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>SST03</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1732&quot;]/main[@class=&quot;jss2248 jss2250&quot;]/form[1]/div[@class=&quot;jss7221&quot;]/div[@class=&quot;jss7222&quot;]/div[@class=&quot;jss7225&quot;]/div[@class=&quot;jss3511 jss3514 jss7845&quot;]/div[@class=&quot;jss7852&quot;]/label[@class=&quot;jss7933&quot;]/span[@class=&quot;jss2705 jss2699 jss7945 jss7939 jss7944&quot;]/span[@class=&quot;jss2704&quot;]/input[@class=&quot;jss7948&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='statuses.2.checked']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div/div[2]/div[2]/div/div[2]/label[3]/span/span/input</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/label[3]/span/span/input</value>
   </webElementXpaths>
</WebElementEntity>
