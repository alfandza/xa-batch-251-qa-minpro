<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Create System Limit1DMDELLA MUTIARAXMU _cd7e7d</name>
   <tag></tag>
   <elementGuidId>de771320-5cbd-47a1-85bf-85556d7f3c5e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.jss1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss1</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Create System Limit1DMDELLA MUTIARAXMU - Automation Test SubmitterDashboardCustomer eXperienceEmployee ExpenseCreate / Edit ExpenseExpense ApprovalFinanceFinance ApprovalHuman Resource360 Assessment360 Assessment Input360 Assessment ResultCompetencies StandardEmployee ProfileEmployee Request FormHR Form ProgressHR KPI InputManager KPI InputSubordinates ProfileTraining Request FormTraining Request SettlementLeave RequestCreate / Edit LeaveLeave ApprovalLeave CancellationMileageMileage ApprovalSubmit MileageMy AccountMy ProfileProject AssignmentAssignment AcceptanceCreate / Edit AssignmentProject RegistrationAll ProjectsCreate / Edit ProjectProject ApprovalReportsBillable UtilizationProject ProfitabilityProject ProgressProject Resource MappingResource EffectivenessWinning RatioRequest for Purchase ApprovalCreate / Edit RFPAPurchase Request SettlementPurchase Settlement ApprovalRFPA ApprovalSetupAchievement ChartApproval HierarchyCompanyCurrencyCustomerDiem ValueEmployeeEmployee LeaveEmployee LevelFile UploadHolidayHR Competency ClusterHR Competency LevelHR Competency MappingHR Corner PageHR KPI CategoriesHR KPI Employee AssignHR KPI Open DateHR KPI TemplateHR Notification PeriodHR Notification SettingHR Notification TemplateImage GalleryLeaveMileage ExceptionOrganizationPositionProject Notification SettingProject Notification TemplateRolesSystem SetupTime LimitWork FlowTime ReportTime Approval HistoryTime Report ApprovalTime Report EntryTime Report HistoryTravelTravel RequestTravel Request ApprovalTravel SettlementTravel Settlement ApprovalWeb JobCollapse SidebarSystem LimitSystem Limit IDThis field value will be automatically filled after submittedCompany *Category *Expense|Limit Value *System Limit SubmissionResetSubmit© 2020 | Equine Technologies GroupVersion master.3596</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div</value>
   </webElementXpaths>
</WebElementEntity>
