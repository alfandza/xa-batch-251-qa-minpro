import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Position/Position'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Position/1Filter/button_option-filter'))

WebUI.click(findTestObject('Position/1Filter/option_Company'))

WebUI.click(findTestObject('Position/1Filter/select_list company', [('company') : 'Programmer']))

WebUI.click(findTestObject('Position/1Filter/button_Apply'))

WebUI.delay(5)

WebUI.click(findTestObject('Position/1Filter/button_option-filter'))

WebUI.click(findTestObject('Position/1Filter/option_Company'))

WebUI.click(findTestObject('Position/1Filter/select_list company', [('company') : 'Equine Global']))

WebUI.click(findTestObject('Position/1Filter/button_Apply'))

WebUI.delay(5)

WebUI.click(findTestObject('Position/1Filter/button_option-filter'))

WebUI.click(findTestObject('Position/1Filter/button_Reset Filter'))

WebUI.click(findTestObject('Position/1Filter/button_Apply'))

