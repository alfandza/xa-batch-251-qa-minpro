import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Position/Position'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Position/0Add/button_tambah'))

WebUI.click(findTestObject('Position/0Add/field_Company'))

WebUI.setText(findTestObject('Position/0Add/input_Company'), company)

WebUI.sendKeys(findTestObject('Position/0Add/input_Company'), Keys.chord(Keys.ENTER))

//WebUI.click(findTestObject('Position/0Add/dropdown_Company'))
//
//WebUI.click(findTestObject('Position/0Add/select_Company'))
WebUI.setText(findTestObject('Position/0Add/input__Position'), position)

WebUI.setText(findTestObject('Position/0Add/input_Description'), description)

WebUI.click(findTestObject('Position/0Add/input_Inactive Date'))

WebUI.click(findTestObject('Position/0Add/select_28'))

WebUI.click(findTestObject('Position/0Add/button_OK'))

WebUI.click(findTestObject('Position/0Add/check box_Multiple'))

WebUI.click(findTestObject('Position/0Add/button_Submit'))

WebUI.click(findTestObject('Position/0Add/button_Continue'))

if (WebUI.verifyElementPresent(findTestObject('Position/0Add/div_Submission failed'), 1)) {
    WebUI.click(findTestObject('Position/0Add/button_Reset Add'))
} else {
}

