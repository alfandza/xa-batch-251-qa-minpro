import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Diem Value/diem value'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Diem Value/1Add/button_tambah'))

WebUI.click(findTestObject('Diem Value/1Add/input_Company'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Diem Value/1Add/option_company', [('text') : 'antar niaga daya']))

WebUI.click(findTestObject('Diem Value/1Add/input_Currency'))

WebUI.click(findTestObject('Diem Value/1Add/option_currency', [('text') : 'Euro']))

WebUI.click(findTestObject('Diem Value/1Add/input_Project Type'))

WebUI.click(findTestObject('Diem Value/1Add/option_Project Type', [('text') : 'Extra Miles']))

WebUI.click(findTestObject('Diem Value/1Add/input_Travel Type'))

WebUI.click(findTestObject('Diem Value/1Add/option_Travel Type', [('text') : 'In Indonesia']))

WebUI.setText(findTestObject('Diem Value/1Add/input_value'), '69')

WebUI.scrollToPosition(0, 0)

WebUI.click(findTestObject('Diem Value/1Add/button_Reset'))

WebUI.click(findTestObject('Diem Value/1Add/button_Submit'))

WebUI.click(findTestObject('Diem Value/1Add/button_Continue'))

