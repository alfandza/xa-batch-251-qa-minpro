import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import javax.print.attribute.standard.Destination as Destination
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Travel Settlement/0Add/dropdown_transport type'))

'Input Value\r\n'
WebUI.click(findTestObject('Travel Settlement/0Add/option_Transport Type', [('number') : '1']))

WebUI.sendKeys(findTestObject('Travel Settlement/0Add/input_from'), Keys.chord(Keys.CONTROL, 'A'))

WebUI.sendKeys(findTestObject('Travel Settlement/0Add/input_from'), Keys.chord(Keys.DELETE))

WebUI.setText(findTestObject('Travel Settlement/0Add/input_from'), from)

WebUI.sendKeys(findTestObject('Travel Settlement/0Add/input_destination'), Keys.chord(Keys.CONTROL, 'A'))

WebUI.sendKeys(findTestObject('Travel Settlement/0Add/input_destination'), Keys.chord(Keys.DELETE))

WebUI.setText(findTestObject('Travel Settlement/0Add/input_destination'), destination)

WebUI.click(findTestObject('Travel Settlement/0Add/input_departureDate'))

WebUI.click(findTestObject('Object Repository/Travel Settlement/0Add/button_Today'))

WebUI.click(findTestObject('Object Repository/Travel Settlement/0Add/button_OK'))

WebUI.click(findTestObject('Travel Settlement/0Add/input_returnDate'))

WebUI.click(findTestObject('Object Repository/Travel Settlement/0Add/button_Today'))

WebUI.click(findTestObject('Object Repository/Travel Settlement/0Add/span_OK'))

WebUI.sendKeys(findTestObject('Travel Settlement/0Add/input_Transport Cost'), Keys.chord(Keys.CONTROL, 'A'))

WebUI.sendKeys(findTestObject('Travel Settlement/0Add/input_Transport Cost'), Keys.chord(Keys.BACK_SPACE))

WebUI.setText(findTestObject('Travel Settlement/0Add/input_Transport Cost'), transport_cost)

WebUI.sendKeys(findTestObject('Travel Settlement/0Add/input_Hotel Name'), Keys.chord(Keys.CONTROL, 'A'))

WebUI.sendKeys(findTestObject('Travel Settlement/0Add/input_Hotel Name'), Keys.chord(Keys.BACK_SPACE))

WebUI.setText(findTestObject('Travel Settlement/0Add/input_Hotel Name'), hotel_name)

WebUI.sendKeys(findTestObject('Travel Settlement/0Add/input_Hotel Cost'), Keys.chord(Keys.CONTROL, 'A'))

WebUI.sendKeys(findTestObject('Travel Settlement/0Add/input_Hotel Cost'), Keys.chord(Keys.BACK_SPACE))

WebUI.setText(findTestObject('Travel Settlement/0Add/input_Hotel Cost'), hotel_cost)

WebUI.scrollToPosition(0, 0)

WebUI.click(findTestObject('Object Repository/Travel Settlement/0Add/button_Submit'))

WebUI.click(findTestObject('Object Repository/Travel Settlement/0Add/button_Continue'))

WebUI.verifyElementText(findTestObject('Object Repository/Travel Settlement/0Add/span_Summary Travel'), 'Summary Travel')

