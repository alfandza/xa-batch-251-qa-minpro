<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>option_AddSettlement</name>
   <tag></tag>
   <elementGuidId>0fc273c2-c23c-48c3-8c38-1d0b27781e88</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//li[@role = 'menuitem' and @value = 'add Settlement']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>menuitem</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>add Settlement</value>
   </webElementProperties>
</WebElementEntity>
