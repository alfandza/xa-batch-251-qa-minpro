<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropdown_role</name>
   <tag></tag>
   <elementGuidId>f146b860-2c20-4569-b6e6-d672a5342f07</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h6[(text() = 'Xsis Mitra Utama' or . = 'Xsis Mitra Utama')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h6</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Xsis Mitra Utama</value>
   </webElementProperties>
</WebElementEntity>
