<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropdown_Xsis Mitra Utama</name>
   <tag></tag>
   <elementGuidId>82af1111-3519-4b92-b949-323a9d3b895a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;root&quot;)/div[@class=&quot;jss1148&quot;]/div[@class=&quot;jss1167&quot;]/div[@class=&quot;jss1380 jss1403 jss1397&quot;]/div[@class=&quot;jss1381 jss1420 jss1430 jss1442 jss1454&quot;]/div[@class=&quot;jss1835 jss1838 jss1916&quot;]/div[@class=&quot;jss1906 jss1919&quot;]/div[@class=&quot;jss1906 jss1900 jss1924&quot;]/span[@class=&quot;jss1905&quot;]/svg[@class=&quot;jss1884&quot;]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.jss1906.jss1900.jss1924 > span.jss1905 > svg.jss1884</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Xsis Mitra Utama'])[1]/following::*[name()='svg'][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss1884</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focusable</name>
      <type>Main</type>
      <value>false</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 24 24</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>presentation</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1148&quot;]/div[@class=&quot;jss1167&quot;]/div[@class=&quot;jss1380 jss1403 jss1397&quot;]/div[@class=&quot;jss1381 jss1420 jss1430 jss1442 jss1454&quot;]/div[@class=&quot;jss1835 jss1838 jss1916&quot;]/div[@class=&quot;jss1906 jss1919&quot;]/div[@class=&quot;jss1906 jss1900 jss1924&quot;]/span[@class=&quot;jss1905&quot;]/svg[@class=&quot;jss1884&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Xsis Mitra Utama'])[1]/following::*[name()='svg'][1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hi DELLA MUTIARA'])[1]/following::*[name()='svg'][2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Curriculum Specialist'])[1]/preceding::*[name()='svg'][1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Automation Test Submitter'])[1]/preceding::*[name()='svg'][1]</value>
   </webElementXpaths>
</WebElementEntity>
