<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_travel type</name>
   <tag></tag>
   <elementGuidId>3b980b93-81a5-4ef2-8cb6-e6ca5cce5443</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[@class = 'jss1177 jss1195 jss2988' and (text() = 'Travel Type' or . = 'Travel Type')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss1177 jss1195 jss2988</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Travel Type</value>
   </webElementProperties>
</WebElementEntity>
