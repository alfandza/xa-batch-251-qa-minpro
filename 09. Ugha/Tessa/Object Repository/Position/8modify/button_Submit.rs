<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_submit</name>
   <tag></tag>
   <elementGuidId>4a24b4ae-80a6-4543-ae09-e6256e180da8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;root&quot;]/div/main/form/div/div[2]/div/div/div[2]/button[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//span[@class = 'jss6908' and (text() = 'Submit' or . = 'Submit')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
