<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_position</name>
   <tag></tag>
   <elementGuidId>027dc661-33ce-4c5d-8632-f219d768d5c7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@placeholder = 'Type any position name' and @type = 'text']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Type any position name</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
   </webElementProperties>
</WebElementEntity>
