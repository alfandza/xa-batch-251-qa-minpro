<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Travel Settlement</name>
   <tag></tag>
   <elementGuidId>0c5d91fd-dc81-4937-b26c-d90e0ed1e975</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@role = 'button' and (text() = 'Travel Settlement' or . = 'Travel Settlement')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Travel Settlement</value>
   </webElementProperties>
</WebElementEntity>
