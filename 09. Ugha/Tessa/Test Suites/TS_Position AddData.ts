<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Add Multiple Data to Position</description>
   <name>TS_Position AddData</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>0bee77b3-ba74-4ee1-a3bb-22bb55e098f0</testSuiteGuid>
   <testCaseLink>
      <guid>7d6b6f17-fe18-48dc-a495-0900a5a7376c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Position/Position</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f28314b4-9547-439a-a76a-f085cb1ea8da</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TS_Item/Position Add - TS</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>0cab4d33-3a0a-42f9-9a6a-c53b1e33f4a0</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataPosition</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>0cab4d33-3a0a-42f9-9a6a-c53b1e33f4a0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Company</value>
         <variableId>42277938-ad31-43ab-91be-e479135c8089</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>0cab4d33-3a0a-42f9-9a6a-c53b1e33f4a0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Position</value>
         <variableId>3ae3dd1f-61ef-4889-a8c0-1bcfa9d0b899</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>0cab4d33-3a0a-42f9-9a6a-c53b1e33f4a0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Description</value>
         <variableId>34bd4161-f248-43c9-ad00-4815c6c55457</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
