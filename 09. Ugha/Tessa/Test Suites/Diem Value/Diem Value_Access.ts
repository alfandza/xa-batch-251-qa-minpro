<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Diem Value_Access</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>8be191f2-947c-4a7d-8239-92fa07cba340</testSuiteGuid>
   <testCaseLink>
      <guid>09759834-6155-4612-959c-f62c5f08841f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/0Role/Automation Test Approver</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>60dea030-44e9-4188-9073-b28db9693b0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Diem Value/diem value</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d088c2ed-7eb8-49c8-bb0f-4bad54bed22d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/0Role/Automation Test Submitter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>99407134-cab4-40ec-bd1e-e05f40664e68</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Diem Value/diem value</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a6e8e2a5-81ca-430b-8f1b-9800c05b392d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/0Role/Named Account BDM</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0847a984-11f2-4510-80bc-94087db9dc18</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Diem Value/diem value</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e62fe88f-113a-4c10-af8b-e86e712e9771</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/0Role/Curriculum Specialist</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4894d7c5-fb48-402d-851a-0f8844790e00</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Diem Value/diem value</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
