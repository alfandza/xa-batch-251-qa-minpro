<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Position_Search</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>474ee652-e8be-43c4-b65a-86dc9ef74e3f</testSuiteGuid>
   <testCaseLink>
      <guid>6eb2b46a-0e61-418a-8f40-7da7e27c21e2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/0Role/Automation Test Submitter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>44511d13-b949-47d1-9ed8-ccc358a4b06b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Position/Position Search</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>932f5cb9-4b19-42d2-800a-810c6d2453e0</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Search Keyword</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>932f5cb9-4b19-42d2-800a-810c6d2453e0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>keyword</value>
         <variableId>cc182398-0ba6-4779-9ea6-14c4a51010a1</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>05c2b425-0227-44a5-99fa-12019a38fe51</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/0Role/Curriculum Specialist</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6ca358cf-5ec1-4a7d-9df2-8a74931a1f40</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Position/Position Search</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>0c95d7ac-a695-4148-b908-4f0f91321ac9</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Search Keyword</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>cc182398-0ba6-4779-9ea6-14c4a51010a1</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
