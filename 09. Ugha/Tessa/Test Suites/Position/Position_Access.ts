<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Position_Access</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>5f1bbcf2-ebd0-47bb-9dc3-e109f947b0d1</testSuiteGuid>
   <testCaseLink>
      <guid>9054f6f0-6feb-4807-a41c-aa32e727f3bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/0Role/Automation Test Approver</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>29e2133b-0d92-4ba6-88df-b5a5421aa8cc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Position/Position</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>135ea9c0-dc46-48f3-ba72-ca44403cc9a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/0Role/Automation Test Submitter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d1b5905e-ecd9-4a4c-b10e-6ffbafaf0eb5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Position/Position</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cc09097d-2551-4838-a512-c5217bbd54ed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/0Role/Curriculum Specialist</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>194f4b2b-16da-427f-911b-a28e7adc1c35</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Position/Position</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0887c183-1b70-42c6-887c-92a5e72ff7f5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/0Role/Named Account BDM</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bdbc664d-e887-45c3-8ace-2bdd2576b382</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Position/Position</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
