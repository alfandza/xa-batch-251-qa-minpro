<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Position_Filter</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>2e339fca-8aae-4801-b5d4-9a976aa4384f</testSuiteGuid>
   <testCaseLink>
      <guid>ea2d86a3-ae4c-4aaf-aab6-d3da6f0e531a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/0Role/Curriculum Specialist</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>df0dde5c-7889-43d9-aaad-046c8c3f5942</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Position/Position Filter - Is Active</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0d10ce02-74a6-4c1d-81e4-7e51e43a5aec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/0Role/Curriculum Specialist</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>08b703a7-f41a-4835-8387-d83d1d780df0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Position/Position Filter - Is not Active</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4f17a3a0-3b81-446c-aec5-e9c35fd7bf8e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/0Role/Automation Test Submitter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3921127d-fd4d-445a-9600-0042f9f57649</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Position/Position Filter - Is Active</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2096e5a3-c911-45c7-a15a-f92c227783a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/0Role/Automation Test Submitter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>95451253-1ce1-4771-b6f0-37fa20777700</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Position/Position Filter - Is not Active</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
