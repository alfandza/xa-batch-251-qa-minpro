<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Access</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>02611977-cdd9-4358-9634-ce49e15e4e27</testSuiteGuid>
   <testCaseLink>
      <guid>c5538ed3-8992-4272-8d7a-45fea51ec9c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/0Role/Curriculum Specialist</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>960670fb-7161-4544-b1c2-aeed6966c521</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Settlement/Travel Settlement_Access Travel Settlement</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bda7db9b-ff98-4d9d-b5fb-66009083b23c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/0Role/Automation Test Submitter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f9979922-63a6-4160-9d17-2925bbc67f1c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Settlement/Travel Settlement_Access Travel Settlement</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6d59fef3-8aa0-4eec-a76a-6d771e8fc3ef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/0Role/Automation Test Approver</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>399ba501-95a4-4087-b569-9b512317025a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Settlement/Travel Settlement_Access Travel Settlement</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>70f25c70-522b-4edd-859a-1f2548813b44</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/0Role/Named Account BDM</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>40a2bcb6-f138-4bd3-8751-ac05da85a630</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Settlement/Travel Settlement_Access Travel Settlement</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
