<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Search</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>dce303ad-4054-4a9a-96d0-c8545896ad8f</testSuiteGuid>
   <testCaseLink>
      <guid>2e34eca6-0711-4a4c-ac7b-e65e4447093b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/0Role/Automation Test Submitter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1e8e62d8-65d2-4097-b6f7-65b382b643c8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Settlement/Travel Settlement_Access Travel Settlement</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>41d5b578-8f43-4ae3-8ca8-9ffe81c93f45</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Settlement/6Search/Travel Settlement_Search</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d989c2e1-6a56-4c6b-b1c2-d85306506de8</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>64fb16a8-0489-402b-8539-ee6424212c57</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/0Role/Curriculum Specialist</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b00d0cdd-ae93-4439-bdd3-ecded68235f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Settlement/Travel Settlement_Access Travel Settlement</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9bccab9b-835a-4d4b-83e5-e4ad976c1f1d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Settlement/6Search/Travel Settlement_Search</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d989c2e1-6a56-4c6b-b1c2-d85306506de8</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
