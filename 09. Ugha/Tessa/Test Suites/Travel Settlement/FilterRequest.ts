<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>FilterRequest</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>19e4f509-fb1f-4671-b1af-9c6b4fff755e</testSuiteGuid>
   <testCaseLink>
      <guid>0f8dfe34-a6c9-462d-9bcd-4c042a22776f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/0Role/Automation Test Approver</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cef4e6aa-e4ab-453f-b774-a8b9ae28f445</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Settlement/Travel Settlement_Access Travel Request</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a5acaefd-2cc5-4eec-9240-b8dbcea10ac5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/0Role/Automation Test Submitter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fd7a282c-5117-402e-93b6-c20a0c41418a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Settlement/Travel Settlement_Access Travel Request</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e6f7aadb-1948-40d8-9f6f-82245b7b1975</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/0Role/Curriculum Specialist</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b013b305-4f0c-4ce9-adfa-e55c252e7040</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Settlement/Travel Settlement_Access Travel Request</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ace8a167-0561-4444-a12f-024b28f30f35</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/0Role/Named Account BDM</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1fa3e53e-65a8-4daa-bc56-af50c2167925</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Settlement/Travel Settlement_Access Travel Request</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
