<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Filter</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>e1ce80d9-629e-4abe-8ede-a4a4d32ad1e8</testSuiteGuid>
   <testCaseLink>
      <guid>10e43972-2873-4394-bd3b-2e11802e3271</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/0Role/Automation Test Submitter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>223862ed-f60d-4032-a000-823e54bd023c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Settlement/Travel Settlement_Access Travel Settlement</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>82206204-ea85-4f92-92c5-c03634b61d38</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Settlement/1Filter/Travel Settlement_Filter Adjustment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>81bbadd2-0d07-41fc-bbb9-116cc84d12a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Settlement/1Filter/Travel Settlement_Filter Approval Completion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>af512222-588b-43cb-ae9a-5b1f36441eb7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Settlement/1Filter/Travel Settlement_Filter Customer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>15c3b157-c868-4cef-86ed-d7395277238b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Settlement/1Filter/Travel Settlement_Filter Status</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
