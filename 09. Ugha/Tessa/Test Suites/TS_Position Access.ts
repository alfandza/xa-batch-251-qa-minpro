<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Test Suite untuk mengakses menu Position</description>
   <name>TS_Position Access</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>0de3ab59-994a-437a-8262-91eec94f9e0a</testSuiteGuid>
   <testCaseLink>
      <guid>17920f01-1ed1-4783-93f4-dd2af2a10925</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Automation Test Approver</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c684e3a7-a300-4fb9-8ba3-a500534c009a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TS_Item/Position - TS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8321392b-a520-4bbf-a56c-c6bdbe1eb753</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Automation Test Submitter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>36377a69-c2ed-4ee0-9767-e8a614b17684</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TS_Item/Position - TS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8cb2a34b-0cc5-4229-b406-fb70bb923ea5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Curriculum Specialist</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>83140efa-ee2e-4cce-ae84-03fa2302f130</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TS_Item/Position - TS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7d363fe1-3842-434b-82f7-176278e50ccf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Named Account BDM</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>efd53337-8493-4a0b-b168-3de7d521ed8a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TS_Item/Position - TS</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
