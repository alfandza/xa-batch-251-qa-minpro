import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.webui.contribution.WebUiDriverCleaner
import com.kms.katalon.core.mobile.contribution.MobileDriverCleaner
import com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner
import com.kms.katalon.core.windows.keyword.contribution.WindowsDriverCleaner
import com.kms.katalon.core.testng.keyword.internal.TestNGDriverCleaner


DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.windows.keyword.contribution.WindowsDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.testng.keyword.internal.TestNGDriverCleaner())


RunConfiguration.setExecutionSettingFile('C:\\Users\\XAUSER~1\\AppData\\Local\\Temp\\Katalon\\20210211_154322\\execution.properties')

TestCaseMain.beforeStart()

        TestCaseMain.runWSVerificationScript(new TestCaseBinding('',[:]), 'import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI\nimport com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile\nimport com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW\nimport com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS\nimport com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows\nimport static com.kms.katalon.core.testobject.ObjectRepository.findTestObject\nimport static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject\nimport static com.kms.katalon.core.testdata.TestDataFactory.findTestData\nimport static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase\nimport static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint\nimport com.kms.katalon.core.model.FailureHandling as FailureHandling\nimport com.kms.katalon.core.testcase.TestCase as TestCase\nimport com.kms.katalon.core.testdata.TestData as TestData\nimport com.kms.katalon.core.testobject.TestObject as TestObject\nimport com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint\nimport internal.GlobalVariable as GlobalVariable\nimport org.openqa.selenium.Keys as Keys\n\nWebUI.openBrowser(\'\')\n\nWebUI.navigateToUrl(\'https://xdev.equine.co.id/apps/tessa/\')\n\nWebUI.click(findTestObject(\'Object Repository/OR_OpenDates/Page_Welcome to New TESSA/button_Let me in\'))\n\nWebUI.switchToWindowTitle(\'Login - Equine Identity\')\n\nWebUI.setText(findTestObject(\'Object Repository/OR_OpenDates/Page_Login - Equine Identity/input_Login_Username\'), \'della.mutiara@xsis.net\')\n\nWebUI.setEncryptedText(findTestObject(\'Object Repository/OR_OpenDates/Page_Login - Equine Identity/input_Login_Password\'), \n    \'zP4F53vxAWI=\')\n\nWebUI.click(findTestObject(\'Object Repository/OR_OpenDates/Page_Login - Equine Identity/button_Login\'))\n\nWebUI.switchToWindowTitle(\'Welcome to New TESSA\')\n\nWebUI.click(findTestObject(\'Object Repository/OR_OpenDates/Page_Welcome to New TESSA/div_Xsis Mitra Utama\'))\n\nWebUI.click(findTestObject(\'Object Repository/OR_OpenDates/Page_Welcome to New TESSA/span_Automation Test Submitter\'))\n\nWebUI.click(findTestObject(\'Object Repository/OR_OpenDates/Page_Dashboard - TESSA (Dev)/div_Setup\'))\n\nWebUI.click(findTestObject(\'Object Repository/OR_OpenDates/Page_Dashboard - TESSA (Dev)/div_HR KPI Open Date\'))\n\nWebUI.click(findTestObject(\'Object Repository/OR_OpenDates/Page_HR KPI Open Dates - TESSA (Dev)/button_Collapse Sidebar_option-field\'))\n\nWebUI.click(findTestObject(\'Object Repository/OR_OpenDates/Page_HR KPI Open Dates - TESSA (Dev)/li_Year\'))\n\nWebUI.click(findTestObject(\'Object Repository/OR_OpenDates/Page_HR KPI Open Dates - TESSA (Dev)/button_Collapse Sidebar_option-order\'))\n\nWebUI.click(findTestObject(\'Object Repository/OR_OpenDates/Page_HR KPI Open Dates - TESSA (Dev)/li_Ascending\'))\n\nWebUI.verifyElementPresent(findTestObject(\'Object Repository/OR_OpenDates/Page_HR KPI Open Dates - TESSA (Dev)/p_2018\'), \n    0)\n\nWebUI.openBrowser(\'\')\n\n', FailureHandling.STOP_ON_FAILURE, true)

