import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('KPI_OpenDates/TC_OpenDates_028'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/OR_OpenDates/Record_tombol_opendates/div__jss52357'))

WebUI.click(findTestObject('Object Repository/OR_OpenDates/Record_tombol_opendates/div_2022'))

WebUI.click(findTestObject('Object Repository/OR_OpenDates/Record_tombol_opendates/div__jss52357'))

WebUI.click(findTestObject('Object Repository/OR_OpenDates/Record_tombol_opendates/div_Full Year'))

WebUI.click(findTestObject('Object Repository/OR_OpenDates/Record_tombol_opendates/input__date'))

WebUI.click(findTestObject('Object Repository/OR_OpenDates/Record_tombol_opendates/h6_2021'))

WebUI.scrollToElement(findTestObject('Page_New KPI Open Date - TESSA (Dev)/div_1999'), 2)

WebUI.click(findTestObject('Object Repository/OR_OpenDates/Record_tombol_opendates/div_1999'))

WebUI.click(findTestObject('Object Repository/OR_OpenDates/Record_tombol_opendates/button_13'))

WebUI.click(findTestObject('Object Repository/OR_OpenDates/Record_tombol_opendates/button_OK'))

WebUI.mouseOver(findTestObject('OR_OpenDates/Record_tombol_opendates/button_Submit'))

WebUI.click(findTestObject('OR_OpenDates/Record_tombol_opendates/button_Submit'))

WebUI.mouseOver(findTestObject('Object Repository/OR_OpenDates/Record_tombol_opendates/button_Continue'))

WebUI.click(findTestObject('Object Repository/OR_OpenDates/Record_tombol_opendates/button_Continue'))

