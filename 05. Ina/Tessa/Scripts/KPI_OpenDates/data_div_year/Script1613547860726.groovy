import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Object Repository/OR_OpenDates/Record_tombol_opendates/div_2018Full YearDecember 19, 2019KIKI AYU _59d023'))

WebUI.click(findTestObject('Object Repository/OR_OpenDates/Record_tombol_opendates/div_2018Mid YearDecember 18, 2019KIKI AYU W_723eac'))

WebUI.click(findTestObject('Object Repository/OR_OpenDates/Record_tombol_opendates/div_2019Full YearNovember 14, 2019DELLA MUT_423d89'))

WebUI.click(findTestObject('Object Repository/OR_OpenDates/Record_tombol_opendates/div_2019Mid YearApril 2, 2020ADITYA MEYSIN1_b247ad'))

WebUI.click(findTestObject('Object Repository/OR_OpenDates/Record_tombol_opendates/div_2020Mid YearApril 30, 2021DELLA MUTIARA_f56227'))

WebUI.click(findTestObject('Object Repository/OR_OpenDates/Record_tombol_opendates/div_2020Full YearJanuary 11, 2021ADITYA MEY_201942'))

