import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Employee_Experience/TC_Experience_004'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/OR_Experience/input__company'))

WebUI.setText(findTestObject('Object Repository/OR_Experience/input__company'), 'Xsis Mitra Utama')

WebUI.click(findTestObject('Object Repository/OR_Experience/input__position'))

WebUI.setText(findTestObject('Object Repository/OR_Experience/input__position'), 'Automation Test Submitter')

WebUI.mouseOver(findTestObject('OR_Experience/button_Submit'))

WebUI.click(findTestObject('OR_Experience/button_Submit'))

WebUI.mouseOver(findTestObject('OR_Experience/button_Continue'))

WebUI.click(findTestObject('OR_Experience/button_Continue'))

