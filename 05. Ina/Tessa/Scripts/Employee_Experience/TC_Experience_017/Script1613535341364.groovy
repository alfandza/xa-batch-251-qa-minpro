import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Employee_Experience/TC_Experience_004'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/OR_Experience/input__company'))

WebUI.setText(findTestObject('Object Repository/OR_Experience/input__company'), '!@#$%')

WebUI.click(findTestObject('Object Repository/OR_Experience/input__position'))

WebUI.setText(findTestObject('Object Repository/OR_Experience/input__position'), 'Automation Test Submitter')

WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.PAGE_DOWN))

WebUI.click(findTestObject('Object Repository/OR_Experience/div__jss34244_1'))

WebUI.click(findTestObject('Object Repository/OR_Experience/div_Trainer'))

WebUI.click(findTestObject('Object Repository/OR_Experience/start_year'))

WebUI.click(findTestObject('Object Repository/OR_Experience/div_2019'))

WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.PAGE_DOWN))

WebUI.click(findTestObject('Object Repository/OR_Experience/end_year'))

WebUI.click(findTestObject('Object Repository/OR_Experience/div_2021'))

WebUI.sendKeys(findTestObject(null), Keys.chord(Keys.PAGE_UP))

WebUI.mouseOver(findTestObject('Object Repository/OR_Experience/button_Submit'))

WebUI.click(findTestObject('Object Repository/OR_Experience/button_Submit'))

WebUI.mouseOver(findTestObject('Object Repository/OR_Experience/button_Continue'))

WebUI.click(findTestObject('Object Repository/OR_Experience/button_Continue'))

