import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('360AR/TC_360AR_002'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.mouseOver(findTestObject('OR_360_Assessment/OR_360 Assessment Result - TESSA (Dev)/button_Collapse Sidebar_option-filter'))

WebUI.click(findTestObject('OR_360_Assessment/OR_360 Assessment Result - TESSA (Dev)/button_Collapse Sidebar_option-filter'))

WebUI.delay(1)

WebUI.mouseOver(findTestObject('OR_360_Assessment/OR_360 Assessment Result - TESSA (Dev)/filter_CompanyNone'))

WebUI.click(findTestObject('OR_360_Assessment/OR_360 Assessment Result - TESSA (Dev)/filter_CompanyNone'))

WebUI.mouseOver(findTestObject('OR_360_Assessment/OR_button_Inside_filter/div_Xsis Mitra Utama (1)'))

WebUI.click(findTestObject('OR_360_Assessment/OR_button_Inside_filter/div_Xsis Mitra Utama (1)'))

WebUI.mouseOver(findTestObject('OR_360_Assessment/OR_360 Assessment Result - TESSA (Dev)/filter_PositionNone'))

WebUI.click(findTestObject('OR_360_Assessment/OR_360 Assessment Result - TESSA (Dev)/filter_PositionNone'))

WebUI.mouseOver(findTestObject('OR_360_Assessment/OR_button_Inside_filter/div_Automation Test Submitter'))

WebUI.click(findTestObject('OR_360_Assessment/OR_button_Inside_filter/div_Automation Test Submitter'))

WebUI.delay(1)

WebUI.mouseOver(findTestObject('OR_360_Assessment/OR_button_Inside_filter/button_Apply'))

WebUI.click(findTestObject('OR_360_Assessment/OR_button_Inside_filter/button_Apply'))

