import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://xdev.equine.co.id/apps/tessa/')

WebUI.mouseOver(findTestObject('Object Repository/OR_KPI_Categories/Page_Welcome to New TESSA/button_Let me in'))

WebUI.click(findTestObject('Object Repository/OR_KPI_Categories/Page_Welcome to New TESSA/button_Let me in'))

WebUI.delay(3)

WebUI.switchToWindowTitle('Login - Equine Identity')

WebUI.setText(findTestObject('Object Repository/OR_KPI_Categories/Page_Login - Equine Identity/input_Login_Username'), 'della.mutiara@xsis.net')

WebUI.setEncryptedText(findTestObject('Object Repository/OR_KPI_Categories/Page_Login - Equine Identity/input_Login_Password'), 
    'zP4F53vxAWI=')

WebUI.mouseOver(findTestObject('Object Repository/OR_KPI_Categories/Page_Login - Equine Identity/button_Login'))

WebUI.click(findTestObject('Object Repository/OR_KPI_Categories/Page_Login - Equine Identity/button_Login'))

WebUI.switchToWindowTitle('Welcome to New TESSA')

WebUI.mouseOver(findTestObject('Object Repository/OR_KPI_Categories/Page_Welcome to New TESSA/div_Xsis Mitra Utama'))

WebUI.click(findTestObject('Object Repository/OR_KPI_Categories/Page_Welcome to New TESSA/div_Xsis Mitra Utama'))

WebUI.mouseOver(findTestObject('Object Repository/OR_KPI_Categories/Page_Welcome to New TESSA/div_Automation Test Submitter'))

WebUI.click(findTestObject('Object Repository/OR_KPI_Categories/Page_Welcome to New TESSA/div_Automation Test Submitter'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Object Repository/OR_KPI_Categories/Page_Dashboard - TESSA (Dev)/div_Setup'))

WebUI.click(findTestObject('Object Repository/OR_KPI_Categories/Page_Dashboard - TESSA (Dev)/div_Setup'))

WebUI.mouseOver(findTestObject('Object Repository/OR_KPI_Categories/Page_Dashboard - TESSA (Dev)/div_HR KPI Categories'))

WebUI.click(findTestObject('Object Repository/OR_KPI_Categories/Page_Dashboard - TESSA (Dev)/div_HR KPI Categories'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Object Repository/OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/div_ECS-2020-KPI-01-QUALITY3 MeasurementsKP_3c9cc9'))

WebUI.click(findTestObject('Object Repository/OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/div_ECS-2020-KPI-01-QUALITY3 MeasurementsKP_3c9cc9'))

WebUI.mouseOver(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/tombol_Details'))

WebUI.click(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/tombol_Details'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('OR_KPI_Categories/Page_KPI Category Detail - TESSA (Dev)/tombol_back'))

WebUI.click(findTestObject('OR_KPI_Categories/Page_KPI Category Detail - TESSA (Dev)/tombol_back'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/tombol_next_page'))

WebUI.click(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/tombol_next_page'))

WebUI.mouseOver(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/tombol_next_page'))

WebUI.click(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/tombol_next_page'))

WebUI.mouseOver(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/tombol_next_page'))

WebUI.click(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/tombol_next_page'))

WebUI.mouseOver(findTestObject('Object Repository/OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/div_Tes Corona2 MeasurementsKPIADITYA MEYSI_192ca0'))

WebUI.click(findTestObject('Object Repository/OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/div_Tes Corona2 MeasurementsKPIADITYA MEYSI_192ca0'))

WebUI.mouseOver(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/tombol_Details'))

WebUI.click(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/tombol_Details'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('OR_KPI_Categories/Page_KPI Category Detail - TESSA (Dev)/tombol_back'))

WebUI.click(findTestObject('OR_KPI_Categories/Page_KPI Category Detail - TESSA (Dev)/tombol_back'))

WebUI.mouseOver(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/tombol_prev_page'))

WebUI.click(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/tombol_prev_page'))

WebUI.mouseOver(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/tombol_prev_page'))

WebUI.click(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/tombol_prev_page'))

WebUI.mouseOver(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/tombol_prev_page'))

WebUI.click(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/tombol_prev_page'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/tombol_OrderBy'))

WebUI.click(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/tombol_OrderBy'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/OrderBy_ID'))

WebUI.click(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/OrderBy_ID'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Object Repository/OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/button_Collapse Sidebar_option-field'))

WebUI.click(findTestObject('Object Repository/OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/button_Collapse Sidebar_option-field'))

WebUI.mouseOver(findTestObject('Object Repository/OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/li_Name'))

WebUI.click(findTestObject('Object Repository/OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/li_Name'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Object Repository/OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/button_Collapse Sidebar_option-field'))

WebUI.click(findTestObject('Object Repository/OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/button_Collapse Sidebar_option-field'))

WebUI.mouseOver(findTestObject('Object Repository/OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/li_Last Changes'))

WebUI.click(findTestObject('Object Repository/OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/li_Last Changes'))

WebUI.delay(3)

