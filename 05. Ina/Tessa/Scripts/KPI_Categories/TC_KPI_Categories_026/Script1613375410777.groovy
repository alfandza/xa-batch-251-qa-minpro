import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('KPI_Categories/TC_KPI_Categories_002'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.mouseOver(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/tombol_OrderBy'))

WebUI.click(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/tombol_OrderBy'))

WebUI.delay(2)

WebUI.mouseOver(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/OrderBy_Name'))

WebUI.click(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/OrderBy_Name'))

WebUI.delay(2)

WebUI.mouseOver(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/tombol_SortBy'))

WebUI.click(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/tombol_SortBy'))

WebUI.delay(2)

WebUI.mouseOver(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/SortBy_Descending'))

WebUI.click(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/SortBy_Descending'))

WebUI.delay(2)

WebUI.mouseOver(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/tombol_refresh_data'))

WebUI.click(findTestObject('OR_KPI_Categories/Page_HR KPI Categories - TESSA (Dev)/tombol_refresh_data'))

