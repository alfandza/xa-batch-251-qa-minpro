import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('KPI_Categories/TC_KPI_Categories_045'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/OR_KPI_Categories/input_Name_name'))

WebUI.sendKeys(findTestObject('Object Repository/OR_KPI_Categories/input_Name_name'), Keys.chord(Keys.CONTROL, 'a'))

WebUI.sendKeys(findTestObject('Object Repository/OR_KPI_Categories/input_Name_name'), Keys.chord(Keys.BACK_SPACE))

WebUI.setText(findTestObject('Object Repository/OR_KPI_Categories/input_Name_name'), 'ZmbtwRvpl5HbATx1EkcHKDN436zB5rR1ey3ZTCod0pX1WWPgy6qToOm7xqTKiJjW66T7oAgHSyeCr4uv0JoLOo4q7QQAqMk18gh3vWPyVkmBsvArZjERgvB7JkL3uSj3BuVKOEyubUf2WxIL5xYU8JZhETWWjnYgYdFnwfAwuxjRDW9n2cYIwsNQm4lrGv1s8adaIOI7csx1hQki60OwrQR5rcqYHro5CJylbwTxktIHYx2uoHcdpKYKkzglQxBMii7f8ZfLFvkuEq8lj6GxO1b5afL2IUEYYd4kI0yNwto6G8Zs6L6VGzFvN3GkfPlV0Hff91ysy2WM87rZnX7XnNP8Wb7M8gklUeh3xyaaD6TpXoHQ92Rq2mVJslfU0nwn88knlHlGeleepDxc')

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/OR_KPI_Categories/tombol_Submit'))

WebUI.click(findTestObject('OR_KPI_Categories/tombol_Continue2'))

