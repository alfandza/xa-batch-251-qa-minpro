<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Automation Test Approver</name>
   <tag></tag>
   <elementGuidId>e2fe84c8-24a4-499c-ac93-0b12c22a20af</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div/div/div[2]/div[2]/div/div/div/ul/div/div/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.jss1570.jss1578.jss1599.jss1596.jss1693</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss1570 jss1578 jss1599 jss1596 jss1693</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Automation Test Approver</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss945&quot;]/div[@class=&quot;jss964&quot;]/div[@class=&quot;jss1177 jss1200 jss1194&quot;]/div[@class=&quot;jss1178 jss1217 jss1227 jss1239 jss1251&quot;]/div[@class=&quot;jss1632 jss1635 jss1713 jss1714&quot;]/div[@class=&quot;jss1722 jss1723&quot;]/div[@class=&quot;jss1724&quot;]/div[@class=&quot;jss1725&quot;]/div[@class=&quot;jss1726 jss967&quot;]/ul[@class=&quot;jss1659&quot;]/div[1]/div[@class=&quot;jss1703 jss1663 jss1666 jss1671 jss1672&quot;]/div[@class=&quot;jss1690&quot;]/span[@class=&quot;jss1570 jss1578 jss1599 jss1596 jss1693&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/div/div[2]/div[2]/div/div/div/ul/div/div/div/span</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Xsis Mitra Utama'])[1]/following::span[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hi NEVA RAMADHAN'])[1]/following::span[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Named Account BDM'])[1]/preceding::span[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Automation Test Approver']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//ul/div/div/div/span</value>
   </webElementXpaths>
</WebElementEntity>
