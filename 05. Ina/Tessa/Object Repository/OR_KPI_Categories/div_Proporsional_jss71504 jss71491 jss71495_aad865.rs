<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Proporsional_jss71504 jss71491 jss71495_aad865</name>
   <tag></tag>
   <elementGuidId>97d72395-7a6c-4fbd-934e-dd0c896890ea</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/form/div[2]/div/form[2]/table/tbody/tr/td/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.jss71504.jss71491.jss71495.jss71510.jss71496.jss71513.jss71498.jss71505.jss71492.jss71512.jss71497</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss71504 jss71491 jss71495 jss71510 jss71496 jss71513 jss71498 jss71505 jss71492 jss71512 jss71497</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss36076&quot;]/main[@class=&quot;jss36592 jss36594&quot;]/form[1]/div[@class=&quot;jss70827&quot;]/div[@class=&quot;jss37393 jss37396 jss71461&quot;]/form[2]/table[@class=&quot;jss71790 jss71683&quot;]/tbody[@class=&quot;jss71815&quot;]/tr[@class=&quot;jss71792&quot;]/td[@class=&quot;jss71797 jss71799 jss71802 jss71693&quot;]/div[@class=&quot;jss71469 jss71472&quot;]/div[@class=&quot;jss71504 jss71491 jss71495 jss71510 jss71496 jss71513 jss71498 jss71505 jss71492 jss71512 jss71497&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div[2]/div/form[2]/table/tbody/tr/td/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Proporsional'])[2]/following::div[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Modify'])[1]/following::div[10]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Notifications'])[1]/preceding::div[15]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Travel'])[2]/preceding::div[17]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form[2]/table/tbody/tr/td/div/div</value>
   </webElementXpaths>
</WebElementEntity>
