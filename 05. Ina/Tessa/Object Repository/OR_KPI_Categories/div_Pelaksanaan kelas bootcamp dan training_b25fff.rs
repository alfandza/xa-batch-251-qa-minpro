<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Pelaksanaan kelas bootcamp dan training_b25fff</name>
   <tag></tag>
   <elementGuidId>c0c91e8e-efaf-4e30-a64d-45e8036f0857</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.jss9032.jss9019.jss9023.jss9038.jss9024.jss9041.jss9026.jss9034.jss9021.jss9033.jss9020 > div.jss9042.jss9027.jss9049 > div.jss9051</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/form/div[2]/div/form/table/tbody/tr/td[2]/div/div/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss9051</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1&quot;]/main[@class=&quot;jss2796 jss2798&quot;]/form[1]/div[@class=&quot;jss8355&quot;]/div[@class=&quot;jss3039 jss3042 jss8989&quot;]/form[1]/table[@class=&quot;jss9318 jss9211&quot;]/tbody[@class=&quot;jss9343&quot;]/tr[@class=&quot;jss9320&quot;]/td[@class=&quot;jss9325 jss9327 jss9330 jss9218&quot;]/div[@class=&quot;css-10nd86i&quot;]/div[@class=&quot;jss8997 jss8998 jss9000&quot;]/div[@class=&quot;jss9032 jss9019 jss9023 jss9038 jss9024 jss9041 jss9026 jss9034 jss9021 jss9033 jss9020&quot;]/div[@class=&quot;jss9042 jss9027 jss9049&quot;]/div[@class=&quot;jss9051&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div[2]/div/form/table/tbody/tr/td[2]/div/div/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pelaksanaan kelas bootcamp dan training'])[2]/following::div[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Modify'])[1]/following::div[8]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Batas Minimum'])[1]/preceding::div[7]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nilai Mutlak'])[1]/preceding::div[8]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[2]/div/div/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
