<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Modify KPI Category1DMDELLA MUTIARAXMU _64676f</name>
   <tag></tag>
   <elementGuidId>85b5206e-b4ec-4890-a55c-da662e5aeed6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.jss1</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss1</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Modify KPI Category1DMDELLA MUTIARAXMU - Automation Test SubmitterDashboardCustomer eXperienceEmployee ExpenseCreate / Edit ExpenseExpense ApprovalFinanceFinance ApprovalHuman Resource360 Assessment360 Assessment Input360 Assessment ResultCompetencies StandardEmployee ProfileEmployee Request FormHR Form ProgressHR KPI InputManager KPI InputSubordinates ProfileTraining Request FormTraining Request SettlementLeave RequestCreate / Edit LeaveLeave ApprovalLeave CancellationMileageMileage ApprovalSubmit MileageMy AccountMy ProfileProject AssignmentAssignment AcceptanceCreate / Edit AssignmentProject RegistrationAll ProjectsCreate / Edit ProjectProject ApprovalReportsBillable UtilizationProject ProfitabilityProject ProgressProject Resource MappingResource EffectivenessWinning RatioRequest for Purchase ApprovalCreate / Edit RFPAPurchase Request SettlementPurchase Settlement ApprovalRFPA ApprovalSetupAchievement ChartApproval HierarchyCompanyCurrencyCustomerDiem ValueEmployeeEmployee LeaveEmployee LevelFile UploadHolidayHR Competency ClusterHR Competency LevelHR Competency MappingHR Corner PageHR KPI CategoriesHR KPI Employee AssignHR KPI Open DateHR KPI TemplateHR Notification PeriodHR Notification SettingHR Notification TemplateImage GalleryLeaveMileage ExceptionOrganizationPositionProject Notification SettingProject Notification TemplateRolesSystem SetupTime LimitWork FlowTime ReportTime Approval HistoryTime Report ApprovalTime Report EntryTime Report HistoryTravelTravel RequestTravel Request ApprovalTravel SettlementTravel Settlement ApprovalWeb JobCollapse SidebarKPI CategoryCategory IDNameCategory Group *KPI|SubmissionResetSubmitKPI MeasurementPengukuranTipe PengukuranBobot (dalam persen (%))ModifyMelakukan sesi coaching/mentoring dengan Atasan Langsung
Melakukan sesi coaching/mentoring dengan Atasan Langsung
ProporsionalProporsionalCustomer Satisfaction Survey
Customer Satisfaction Survey
ProporsionalProporsional&quot;Memastikan bahwa seluruh dokumentasi project lengkap tersimpan di server perusahaan dan tepat waktu sesuai jadwal project yang sudah ditentukan.
Khusus untuk project yang closed di bulan Desember tahun berjalan,
maksimum pengumpulan dokumentasi pada tanggal 5 Januari tahun berikutnya.&quot;
&quot;Memastikan bahwa seluruh dokumentasi project lengkap tersimpan di server perusahaan dan tepat waktu sesuai jadwal project yang sudah ditentukan.
Khusus untuk project yang closed di bulan Desember tahun berjalan,
maksimum pengumpulan dokumentasi pada tanggal 5 Januari tahun berikutnya.&quot;
ProporsionalProporsionalAdd© 2020 | Equine Technologies GroupVersion master.3596</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div</value>
   </webElementXpaths>
</WebElementEntity>
