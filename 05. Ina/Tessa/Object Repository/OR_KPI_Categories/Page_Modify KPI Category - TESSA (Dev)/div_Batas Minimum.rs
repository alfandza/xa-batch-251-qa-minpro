<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Batas Minimum</name>
   <tag></tag>
   <elementGuidId>d514f97c-c85e-4201-89a0-76c4ba5aef05</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.css-1q3fsek > svg.css-19bqh2r</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Pelaksanaan kelas bootcamp dan training'])[2]/following::*[name()='svg'][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>20</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>20</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 20 20</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focusable</name>
      <type>Main</type>
      <value>false</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>css-19bqh2r</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1&quot;]/main[@class=&quot;jss2796 jss2798&quot;]/form[1]/div[@class=&quot;jss8355&quot;]/div[@class=&quot;jss3039 jss3042 jss8989&quot;]/form[1]/table[@class=&quot;jss9318 jss9211&quot;]/tbody[@class=&quot;jss9343&quot;]/tr[@class=&quot;jss9320&quot;]/td[@class=&quot;jss9325 jss9327 jss9330 jss9218&quot;]/div[@class=&quot;css-10nd86i&quot;]/div[@class=&quot;jss8997 jss8998 jss9000&quot;]/div[@class=&quot;jss9032 jss9019 jss9023 jss9041 jss9026 jss9034 jss9021 jss9033 jss9020&quot;]/div[@class=&quot;jss9042 jss9027 jss9049&quot;]/div[@class=&quot;jss9050&quot;]/div[@class=&quot;css-1q3fsek&quot;]/svg[@class=&quot;css-19bqh2r&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pelaksanaan kelas bootcamp dan training'])[2]/following::*[name()='svg'][1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Modify'])[1]/following::*[name()='svg'][1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='|'])[2]/preceding::*[name()='svg'][1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Batas Minimum'])[2]/preceding::*[name()='svg'][2]</value>
   </webElementXpaths>
</WebElementEntity>
