<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textarea_6cb901</name>
   <tag></tag>
   <elementGuidId>4a41ab8d-56ac-495c-8836-bcec6a0a97f5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//textarea[@name='description'])[3]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'description' and @placeholder = 'Pengukuran' and (text() = '&quot;Memastikan bahwa seluruh dokumentasi project lengkap tersimpan di server perusahaan dan tepat waktu sesuai jadwal project yang sudah ditentukan.
Khusus untuk project yang closed di bulan Desember tahun berjalan,
maksimum pengumpulan dokumentasi pada tanggal 5 Januari tahun berikutnya.&quot;
' or . = '&quot;Memastikan bahwa seluruh dokumentasi project lengkap tersimpan di server perusahaan dan tepat waktu sesuai jadwal project yang sudah ditentukan.
Khusus untuk project yang closed di bulan Desember tahun berjalan,
maksimum pengumpulan dokumentasi pada tanggal 5 Januari tahun berikutnya.&quot;
')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.jss9304.jss9291.jss9295.jss9313.jss9298.jss9306.jss9293.jss9305.jss9292.jss9312.jss9297 > div.jss9616 > textarea[name=&quot;description&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>textarea</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>rows</name>
      <type>Main</type>
      <value>1</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss9617 jss9314 jss9299 jss9316 jss9301</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>false</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>description</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Pengukuran</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>&quot;Memastikan bahwa seluruh dokumentasi project lengkap tersimpan di server perusahaan dan tepat waktu sesuai jadwal project yang sudah ditentukan.
Khusus untuk project yang closed di bulan Desember tahun berjalan,
maksimum pengumpulan dokumentasi pada tanggal 5 Januari tahun berikutnya.&quot;
</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1&quot;]/main[@class=&quot;jss3138 jss3140&quot;]/form[1]/div[@class=&quot;jss8627&quot;]/div[@class=&quot;jss3381 jss3384 jss9261&quot;]/form[3]/table[@class=&quot;jss9590 jss9483&quot;]/tbody[@class=&quot;jss9615&quot;]/tr[@class=&quot;jss9592&quot;]/td[@class=&quot;jss9597 jss9599 jss9602 jss9493&quot;]/div[@class=&quot;jss9269 jss9272&quot;]/div[@class=&quot;jss9304 jss9291 jss9295 jss9313 jss9298 jss9306 jss9293 jss9305 jss9292 jss9312 jss9297&quot;]/div[@class=&quot;jss9616&quot;]/textarea[@class=&quot;jss9617 jss9314 jss9299 jss9316 jss9301&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//textarea[@name='description'])[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div[2]/div/form[3]/table/tbody/tr/td/div/div/div/textarea[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Proporsional'])[4]/following::textarea[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Customer Satisfaction Survey'])[2]/following::textarea[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='|'])[2]/preceding::textarea[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Memberikan bukti objektif untuk kemajuan mencapai hasil yang diinginkan'])[2]/preceding::textarea[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form[3]/table/tbody/tr/td/div/div/div/textarea[3]</value>
   </webElementXpaths>
</WebElementEntity>
