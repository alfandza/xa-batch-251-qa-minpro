<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_tipe_pengukuran</name>
   <tag></tag>
   <elementGuidId>a2cddd80-8c67-4fe5-bb14-604fd64ea0a6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/form/div[2]/div/form/table/tbody/tr/td[2]/div/div/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.jss69294.jss69281.jss69285.jss69303.jss69288.jss69296.jss69283.jss69295.jss69282 > div.jss69304.jss69289.jss69311 > div.jss69313</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss69313</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss36076&quot;]/main[@class=&quot;jss36592 jss36594&quot;]/form[1]/div[@class=&quot;jss68617&quot;]/div[@class=&quot;jss37393 jss37396 jss69251&quot;]/form[1]/table[@class=&quot;jss69580 jss69473&quot;]/tbody[@class=&quot;jss69605&quot;]/tr[@class=&quot;jss69582&quot;]/td[@class=&quot;jss69587 jss69589 jss69592 jss69480&quot;]/div[@class=&quot;css-10nd86i&quot;]/div[@class=&quot;jss69259 jss69260 jss69262&quot;]/div[@class=&quot;jss69294 jss69281 jss69285 jss69303 jss69288 jss69296 jss69283 jss69295 jss69282&quot;]/div[@class=&quot;jss69304 jss69289 jss69311&quot;]/div[@class=&quot;jss69313&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div[2]/div/form/table/tbody/tr/td[2]/div/div/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Modify'])[1]/following::div[8]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bobot (dalam persen (%))'])[1]/following::div[8]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Batas Minimum'])[1]/preceding::div[7]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nilai Mutlak'])[1]/preceding::div[8]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[2]/div/div/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
