<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_ZmbtwRvpl5HbATx1EkcHKDN436zB5rR1ey3ZTCo_0a62af</name>
   <tag></tag>
   <elementGuidId>a1f52d28-7613-48b1-8ecb-6248cee79cda</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/div[3]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss2795 jss5048</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>ZmbtwRvpl5HbATx1EkcHKDN436zB5rR1ey3ZTCod0pX1WWPgy6qToOm7xqTKiJjW66T7oAgHSyeCr4uv0JoLOo4q7QQAqMk18gh3vWPyVkmBsvArZjERgvB7JkL3uSj3BuVKOEyubUf2WxIL5xYU8JZhETWWjnYgYdFnwfAwuxjRDW9n2cYIwsNQm4lrGv1s8adaIOI7csx1hQki60OwrQR5rcqYHro5CJylbwTxktIHYx2uoHcdpKYKkzglQxBMii7f8ZfLFvkuEq8lj6GxO1b5afL2IUEYYd4kI0yNwto6G8Zs6L6VGzFvN3GkfPlV0Hff91ysy2WM87rZnX7XnNP8Wb7M8gklUeh3xyaaD6TpXoHQ92Rq2mVJslfU0nwn88knlHlGeleepDxc2 MeasurementsPersonal DevelopmentDELLA MUTIARAa day ago</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1&quot;]/main[@class=&quot;jss2583 jss2585&quot;]/div[@class=&quot;jss3077 jss3080 jss5045&quot;]/div[@class=&quot;jss2795 jss5048&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/div[3]/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Details'])[1]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Modify'])[1]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Name'])[2]/preceding::div[14]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Measurement Count'])[2]/preceding::div[18]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//main/div[3]/div</value>
   </webElementXpaths>
</WebElementEntity>
