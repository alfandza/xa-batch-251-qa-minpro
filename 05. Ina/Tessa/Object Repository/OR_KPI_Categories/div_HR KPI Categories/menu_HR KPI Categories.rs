<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>menu_HR KPI Categories</name>
   <tag></tag>
   <elementGuidId>737f65ca-21af-41c1-a28b-19b794c08b7b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.jss48246.jss50825.jss50828.jss50833.jss50834.jss50836 > div.jss50819.jss50387.jss50533</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div[2]/div/nav/div[13]/div/div/div[16]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss50819 jss50387 jss50533</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>HR KPI Categories</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss47273&quot;]/div[@class=&quot;jss49607 jss49608 jss49658 jss49832&quot;]/div[@class=&quot;jss49075 jss49077 jss49609 jss49832 jss49610 jss49614&quot;]/nav[@class=&quot;jss50334&quot;]/div[@class=&quot;jss50839 jss50840&quot;]/div[@class=&quot;jss50841&quot;]/div[@class=&quot;jss50842&quot;]/div[@class=&quot;jss48246 jss50825 jss50828 jss50833 jss50834 jss50836&quot;]/div[@class=&quot;jss50819 jss50387 jss50533&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/div/nav/div[13]/div/div/div[16]/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='HR Corner Page'])[1]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='HR Competency Mapping'])[1]/following::div[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='HR KPI Employee Assign'])[1]/preceding::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[16]/div</value>
   </webElementXpaths>
</WebElementEntity>
