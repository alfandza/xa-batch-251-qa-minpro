<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>path_1</name>
   <tag></tag>
   <elementGuidId>8e7608de-ddab-43c6-8dd6-3d0f2e20792a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>path</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>d</name>
      <type>Main</type>
      <value>M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34a.9959.9959 0 0 0-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1&quot;]/main[@class=&quot;jss3138 jss3140&quot;]/form[1]/div[@class=&quot;jss8627&quot;]/div[@class=&quot;jss3381 jss3384 jss9261&quot;]/form[2]/table[@class=&quot;jss9590 jss9483&quot;]/tbody[@class=&quot;jss9615&quot;]/tr[@class=&quot;jss9592&quot;]/td[@class=&quot;jss9597 jss9599 jss9602 jss9489&quot;]/button[@class=&quot;jss3350 jss3353 jss3356&quot;]/span[@class=&quot;jss3358&quot;]/svg[@class=&quot;jss3341&quot;]/path[1]</value>
   </webElementProperties>
</WebElementEntity>
