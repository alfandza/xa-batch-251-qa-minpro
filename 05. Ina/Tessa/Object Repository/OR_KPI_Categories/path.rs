<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>path</name>
   <tag></tag>
   <elementGuidId>5eca4256-08c3-42ee-a8f6-d21581ca7bb8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.jss3008.jss3011.jss3013 > span.jss3016 > svg.jss2999 > path</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>path</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>d</name>
      <type>Main</type>
      <value>M12 2C6.47 2 2 6.47 2 12s4.47 10 10 10 10-4.47 10-10S17.53 2 12 2zm5 13.59L15.59 17 12 13.41 8.41 17 7 15.59 10.59 12 7 8.41 8.41 7 12 10.59 15.59 7 17 8.41 13.41 12 17 15.59z</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1&quot;]/main[@class=&quot;jss2796 jss2798&quot;]/form[1]/div[@class=&quot;jss2549&quot;]/div[@class=&quot;jss3039 jss3042 jss4545&quot;]/form[4]/table[@class=&quot;jss4874 jss4767&quot;]/tbody[@class=&quot;jss4899&quot;]/tr[@class=&quot;jss4876&quot;]/td[@class=&quot;jss4881 jss4883 jss4886 jss4773&quot;]/button[@class=&quot;jss3008 jss3011 jss3013&quot;]/span[@class=&quot;jss3016&quot;]/svg[@class=&quot;jss2999&quot;]/path[1]</value>
   </webElementProperties>
</WebElementEntity>
