<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Note</name>
   <tag></tag>
   <elementGuidId>9a204991-0516-468d-8121-4accd13570e6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/form/div[2]/div/div/table/tbody/tr[4]/td/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>td.jss41408.jss41410 > div.jss40848.jss40849.jss40851</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss40848 jss40849 jss40851</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Note *</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1&quot;]/main[@class=&quot;jss4919 jss4921&quot;]/form[1]/div[@class=&quot;jss40206&quot;]/div[@class=&quot;jss40210&quot;]/div[@class=&quot;jss5397 jss5400 jss40840 jss41327&quot;]/table[@class=&quot;jss41401 jss41392&quot;]/tbody[@class=&quot;jss41402&quot;]/tr[@class=&quot;jss41403&quot;]/td[@class=&quot;jss41408 jss41410&quot;]/div[@class=&quot;jss40848 jss40849 jss40851&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div[2]/div/div/table/tbody/tr[4]/td/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='HR'])[1]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/div</value>
   </webElementXpaths>
</WebElementEntity>
