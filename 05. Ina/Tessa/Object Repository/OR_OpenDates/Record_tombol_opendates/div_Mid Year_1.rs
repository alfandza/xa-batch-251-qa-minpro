<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Mid Year_1</name>
   <tag></tag>
   <elementGuidId>938bff3c-2efb-47e9-9d23-a716f11df5dd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[3]/div/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.jss50711.jss52334.jss52338.jss50720.jss52341.jss50713.jss52336.jss50712.jss52335 > div.jss50721.jss52342.jss52355 > div.jss52357</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss52357</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Mid Year</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss36076&quot;]/main[@class=&quot;jss36592 jss36594&quot;]/form[1]/div[@class=&quot;jss51680&quot;]/div[@class=&quot;jss51681&quot;]/div[@class=&quot;jss51684&quot;]/div[@class=&quot;jss37393 jss37396 jss52304&quot;]/div[@class=&quot;jss52311&quot;]/div[@class=&quot;css-10nd86i&quot;]/div[@class=&quot;jss52312 jss52313 jss52315&quot;]/div[@class=&quot;jss50711 jss52334 jss52338 jss50720 jss52341 jss50713 jss52336 jss50712 jss52335&quot;]/div[@class=&quot;jss50721 jss52342 jss52355&quot;]/div[@class=&quot;jss52357&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[3]/div/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='|'])[2]/preceding::div[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mid Year'])[2]/preceding::div[8]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[3]/div/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
