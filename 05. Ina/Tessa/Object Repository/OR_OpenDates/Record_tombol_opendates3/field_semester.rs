<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>field_semester</name>
   <tag></tag>
   <elementGuidId>c60f4abd-faf7-41a8-ad73-ba37994edcca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[3]/div/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.jss50316.jss50303.jss50307.jss50322.jss50308.jss50325.jss50310.jss50318.jss50305.jss50317.jss50304 > div.jss50326.jss50311.jss50333 > div.jss50335</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss50335</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss36076&quot;]/main[@class=&quot;jss36592 jss36594&quot;]/form[1]/div[@class=&quot;jss49639&quot;]/div[@class=&quot;jss49640&quot;]/div[@class=&quot;jss49643&quot;]/div[@class=&quot;jss37393 jss37396 jss50273&quot;]/div[@class=&quot;jss50280&quot;]/div[@class=&quot;css-10nd86i&quot;]/div[@class=&quot;jss50281 jss50282 jss50284&quot;]/div[@class=&quot;jss50316 jss50303 jss50307 jss50322 jss50308 jss50325 jss50310 jss50318 jss50305 jss50317 jss50304&quot;]/div[@class=&quot;jss50326 jss50311 jss50333&quot;]/div[@class=&quot;jss50335&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[3]/div/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[3]/div/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
