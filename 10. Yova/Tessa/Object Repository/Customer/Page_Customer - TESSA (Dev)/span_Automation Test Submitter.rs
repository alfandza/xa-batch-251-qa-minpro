<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Automation Test Submitter</name>
   <tag></tag>
   <elementGuidId>7530cb96-b077-4315-b336-d3c6e93ced14</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div/div/div[2]/div[2]/div/div/div/ul/div[2]/div/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss18942 jss18950 jss18971 jss18968 jss19065</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Automation Test Submitter</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss18317&quot;]/div[@class=&quot;jss18336&quot;]/div[@class=&quot;jss18549 jss18572 jss18566&quot;]/div[@class=&quot;jss18550 jss18589 jss18599 jss18611 jss18623&quot;]/div[@class=&quot;jss19004 jss19007 jss19085 jss19086&quot;]/div[@class=&quot;jss19094 jss19095&quot;]/div[@class=&quot;jss19096&quot;]/div[@class=&quot;jss19097&quot;]/div[@class=&quot;jss19098 jss18339&quot;]/ul[@class=&quot;jss19031&quot;]/div[2]/div[@class=&quot;jss19075 jss19035 jss19038 jss19043 jss19044&quot;]/div[@class=&quot;jss19062&quot;]/span[@class=&quot;jss18942 jss18950 jss18971 jss18968 jss19065&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/div/div[2]/div[2]/div/div/div/ul/div[2]/div/div/span</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Curriculum Specialist'])[1]/following::span[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Automation TesterAutomation Tester'])[1]/following::span[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Automation Test Submitter']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//ul/div[2]/div/div/span</value>
   </webElementXpaths>
</WebElementEntity>
