<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Curriculum Specialist</name>
   <tag></tag>
   <elementGuidId>735f03f0-1f48-44b4-88c3-0267023a3b1d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div/div/div[2]/div[2]/div/div/div/ul/div/div/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.jss10334.jss10342.jss10363.jss10360.jss10457</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss10334 jss10342 jss10363 jss10360 jss10457</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Curriculum Specialist</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss9709&quot;]/div[@class=&quot;jss9728&quot;]/div[@class=&quot;jss9941 jss9964 jss9958&quot;]/div[@class=&quot;jss9942 jss9981 jss9991 jss10003 jss10015&quot;]/div[@class=&quot;jss10396 jss10399 jss10470 jss10471&quot;]/div[@class=&quot;jss10479 jss10480&quot;]/div[@class=&quot;jss10481&quot;]/div[@class=&quot;jss10482&quot;]/div[@class=&quot;jss10483 jss9731&quot;]/ul[@class=&quot;jss10423&quot;]/div[1]/div[@class=&quot;jss10467 jss10427 jss10430 jss10435 jss10436&quot;]/div[@class=&quot;jss10454&quot;]/span[@class=&quot;jss10334 jss10342 jss10363 jss10360 jss10457&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/div/div[2]/div[2]/div/div/div/ul/div/div/div/span</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Automation TesterAutomation Tester'])[1]/following::span[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hi DELLA MUTIARA'])[1]/following::span[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Automation Test Submitter'])[1]/preceding::span[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Curriculum Specialist']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//ul/div/div/div/span</value>
   </webElementXpaths>
</WebElementEntity>
