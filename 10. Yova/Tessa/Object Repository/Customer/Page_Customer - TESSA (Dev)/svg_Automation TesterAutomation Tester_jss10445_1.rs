<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>svg_Automation TesterAutomation Tester_jss10445_1</name>
   <tag></tag>
   <elementGuidId>7ff0543d-92d8-4bd2-b92e-4c4e32567f1f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Automation TesterAutomation Tester'])[1]/following::*[name()='svg'][1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.jss10467.jss10461.jss10478 > span.jss10466 > svg.jss10445</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>svg</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss10445</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focusable</name>
      <type>Main</type>
      <value>false</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>viewBox</name>
      <type>Main</type>
      <value>0 0 24 24</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-hidden</name>
      <type>Main</type>
      <value>true</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>presentation</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss9709&quot;]/div[@class=&quot;jss9728&quot;]/div[@class=&quot;jss9941 jss9964 jss9958&quot;]/div[@class=&quot;jss9942 jss9981 jss9991 jss10003 jss10015&quot;]/div[@class=&quot;jss10396 jss10399 jss10470&quot;]/div[@class=&quot;jss10467 jss10473&quot;]/div[@class=&quot;jss10467 jss10461 jss10478&quot;]/span[@class=&quot;jss10466&quot;]/svg[@class=&quot;jss10445&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Automation TesterAutomation Tester'])[1]/following::*[name()='svg'][1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hi DELLA MUTIARA'])[1]/following::*[name()='svg'][2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Curriculum Specialist'])[1]/preceding::*[name()='svg'][1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Automation Test Submitter'])[1]/preceding::*[name()='svg'][1]</value>
   </webElementXpaths>
</WebElementEntity>
