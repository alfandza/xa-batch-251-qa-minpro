<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div__jss10621</name>
   <tag></tag>
   <elementGuidId>5e02ea9e-c396-45cd-871c-eb78dbbd23dc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[2]/div/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.jss10621</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss10621</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1935&quot;]/main[@class=&quot;jss2451 jss2453&quot;]/form[1]/div[@class=&quot;jss9944&quot;]/div[@class=&quot;jss9945&quot;]/div[@class=&quot;jss9948&quot;]/div[@class=&quot;jss3714 jss3717 jss10568&quot;]/div[@class=&quot;jss10575&quot;]/div[@class=&quot;css-10nd86i&quot;]/div[@class=&quot;jss10576 jss10577 jss10579&quot;]/div[@class=&quot;jss8965 jss10598 jss10602 jss8974 jss10605 jss8967 jss10600 jss8966 jss10599&quot;]/div[@class=&quot;jss8975 jss10606 jss10619&quot;]/div[@class=&quot;jss10621&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[2]/div/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='antar niaga daya'])[1]/preceding::div[7]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Equine Global'])[1]/preceding::div[8]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
