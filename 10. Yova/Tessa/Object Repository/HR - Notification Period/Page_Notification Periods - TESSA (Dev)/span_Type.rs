<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Type</name>
   <tag></tag>
   <elementGuidId>ad92877d-bbe0-4df4-9a91-eb3a753cf692</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>ul.jss2969.jss2970 > li.jss2974 > div.jss1265.jss2973.jss2976.jss2981.jss2982.jss2983 > div.jss2985 > span.jss1177.jss1195.jss2988</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Apply'])[1]/following::span[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss1177 jss1195 jss2988</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Type</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;mui-fixed jss943 jss929 jss12529&quot;]/div[@class=&quot;jss932 jss930&quot;]/div[@class=&quot;jss1228 jss1254 jss1229 jss933 jss934 jss937 jss942&quot;]/div[@class=&quot;jss14073 jss12571&quot;]/ul[@class=&quot;jss2969 jss2970&quot;]/li[@class=&quot;jss2974&quot;]/div[@class=&quot;jss1265 jss2973 jss2976 jss2981 jss2982 jss2983&quot;]/div[@class=&quot;jss2985&quot;]/span[@class=&quot;jss1177 jss1195 jss2988&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Apply'])[1]/following::span[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Filter'])[1]/following::span[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[2]/div/div/ul/li/div/div/span</value>
   </webElementXpaths>
</WebElementEntity>
