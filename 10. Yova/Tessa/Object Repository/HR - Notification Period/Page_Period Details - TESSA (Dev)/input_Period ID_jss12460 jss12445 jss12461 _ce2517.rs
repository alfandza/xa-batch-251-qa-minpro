<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Period ID_jss12460 jss12445 jss12461 _ce2517</name>
   <tag></tag>
   <elementGuidId>9d756a80-b2bb-43b0-977e-d7df7b9f0302</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input.jss12460.jss12445.jss12461.jss12446</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@value='abfa093c-a7b5-42a7-854f-452b28191199']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>false</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss12460 jss12445 jss12461 jss12446</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>abfa093c-a7b5-42a7-854f-452b28191199</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1&quot;]/main[@class=&quot;jss517 jss519&quot;]/div[@class=&quot;jss12005&quot;]/div[@class=&quot;jss12006&quot;]/div[@class=&quot;jss12009&quot;]/div[@class=&quot;jss1228 jss1231 jss12407&quot;]/div[@class=&quot;jss12414&quot;]/div[@class=&quot;jss12415 jss12417 jss12418&quot;]/div[@class=&quot;jss12450 jss12437 jss12459 jss12444 jss12451 jss12438 jss12457&quot;]/input[@class=&quot;jss12460 jss12445 jss12461 jss12446&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@value='abfa093c-a7b5-42a7-854f-452b28191199']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/div/div/div/div/div[2]/div/div/input</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
   </webElementXpaths>
</WebElementEntity>
