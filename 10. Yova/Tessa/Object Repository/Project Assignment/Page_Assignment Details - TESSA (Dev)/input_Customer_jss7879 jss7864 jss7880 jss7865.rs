<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Customer_jss7879 jss7864 jss7880 jss7865</name>
   <tag></tag>
   <elementGuidId>3c55bfc5-f3ea-4621-ba94-c14fde7108e7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@value='Barima Handal R4jas@']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.jss7869.jss7856.jss7878.jss7863.jss7871.jss7858.jss7870.jss7857.jss7876 > input.jss7879.jss7864.jss7880.jss7865</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>false</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss7879 jss7864 jss7880 jss7865</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Barima Handal R4jas@</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1710&quot;]/main[@class=&quot;jss2226 jss2228&quot;]/div[@class=&quot;jss7424&quot;]/div[@class=&quot;jss7425&quot;]/div[@class=&quot;jss7428&quot;]/div[@class=&quot;jss3489 jss3492 jss7826&quot;]/div[@class=&quot;jss7833&quot;]/div[@class=&quot;jss7834 jss7836 jss7837&quot;]/div[@class=&quot;jss7869 jss7856 jss7878 jss7863 jss7871 jss7858 jss7870 jss7857 jss7876&quot;]/input[@class=&quot;jss7879 jss7864 jss7880 jss7865&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@value='Barima Handal R4jas@']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/div/div/div/div/div[2]/div[4]/div/input</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/input</value>
   </webElementXpaths>
</WebElementEntity>
