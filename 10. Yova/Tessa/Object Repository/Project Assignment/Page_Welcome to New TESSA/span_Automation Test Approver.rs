<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Automation Test Approver</name>
   <tag></tag>
   <elementGuidId>c63139ea-c819-42e1-a75f-7d1f884c4f7c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.jss1563.jss1571.jss1592.jss1589.jss1686</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div/div/div[2]/div[2]/div/div/div/ul/div/div/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss1563 jss1571 jss1592 jss1589 jss1686</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Automation Test Approver</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss938&quot;]/div[@class=&quot;jss957&quot;]/div[@class=&quot;jss1170 jss1193 jss1187&quot;]/div[@class=&quot;jss1171 jss1210 jss1220 jss1232 jss1244&quot;]/div[@class=&quot;jss1625 jss1628 jss1706 jss1707&quot;]/div[@class=&quot;jss1715 jss1716&quot;]/div[@class=&quot;jss1717&quot;]/div[@class=&quot;jss1718&quot;]/div[@class=&quot;jss1719 jss960&quot;]/ul[@class=&quot;jss1652&quot;]/div[1]/div[@class=&quot;jss1696 jss1656 jss1659 jss1664 jss1665&quot;]/div[@class=&quot;jss1683&quot;]/span[@class=&quot;jss1563 jss1571 jss1592 jss1589 jss1686&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/div/div[2]/div[2]/div/div/div/ul/div/div/div/span</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Xsis Mitra Utama'])[1]/following::span[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hi NEVA RAMADHAN'])[1]/following::span[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Named Account BDM'])[1]/preceding::span[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Automation Test Approver']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//ul/div/div/div/span</value>
   </webElementXpaths>
</WebElementEntity>
