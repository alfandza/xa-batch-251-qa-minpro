<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Customer</name>
   <tag></tag>
   <elementGuidId>de93f6f1-acd0-4ca8-a334-7e5ed05ea4a4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Apply'])[1]/following::span[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>ul.jss31141.jss31142 > li.jss31146 > div.jss29262.jss31145.jss31148.jss31153.jss31154.jss31155 > div.jss31158 > span.jss29174.jss29192.jss31161</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss29174 jss29192 jss31161</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Customer</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;mui-fixed jss29172 jss29158 jss59207&quot;]/div[@class=&quot;jss29161 jss29159&quot;]/div[@class=&quot;jss29225 jss29251 jss29226 jss29162 jss29163 jss29166 jss29171&quot;]/ul[@class=&quot;jss31141 jss31142&quot;]/li[@class=&quot;jss31146&quot;]/div[@class=&quot;jss29262 jss31145 jss31148 jss31153 jss31154 jss31155&quot;]/div[@class=&quot;jss31158&quot;]/span[@class=&quot;jss29174 jss29192 jss31161&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Apply'])[1]/following::span[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reset'])[1]/following::span[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Project Type'])[4]/preceding::span[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/ul/li/div/div/span</value>
   </webElementXpaths>
</WebElementEntity>
