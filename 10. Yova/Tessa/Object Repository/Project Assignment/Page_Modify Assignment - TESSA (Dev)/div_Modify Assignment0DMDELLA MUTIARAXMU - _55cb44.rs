<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Modify Assignment0DMDELLA MUTIARAXMU - _55cb44</name>
   <tag></tag>
   <elementGuidId>671c17ef-7a7c-4f2a-804e-03f2eec6961f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.jss1710</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss1710</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Modify Assignment0DMDELLA MUTIARAXMU - Curriculum SpecialistDashboardEmployee ExpenseCreate / Edit ExpenseExpense ApprovalFinanceFinance ApprovalLeave RequestCreate / Edit LeaveLeave ApprovalLeave CancellationMileageMileage ApprovalSubmit MileageMy AccountProject AssignmentAssignment AcceptanceCreate / Edit AssignmentProject RegistrationCreate / Edit ProjectProject ApprovalReportsBillable UtilizationProject ProfitabilityProject ProgressResource EffectivenessRequest for Purchase ApprovalCreate / Edit RFPAPurchase Request SettlementPurchase Settlement ApprovalRFPA ApprovalSetupApproval HierarchyCompanyCurrencyCustomerDiem ValueEmployeeEmployee LeaveHolidayHR Corner PageLeaveMileage ExceptionOrganizationPositionRolesSystem SetupTime LimitWork FlowTime ReportTime Approval HistoryTime Report ApprovalTime Report EntryTime Report HistoryTravelTravel RequestTravel Request ApprovalTravel SettlementTravel Settlement ApprovalCollapse SidebarProjectAssignment IDProject IDOwnerCustomerProject TypePO/Contract No.NameDescriptionStartEndMaximum HoursAssigned HoursConsumed HoursUnassigned HoursAssignment Items1 Item(s)DELLA MUTIARA60 Mandays | 480 Allocated Hours | 0 Consumed HoursName *DELLA MUTIARA|Role *Job Description *Job Description is a required fieldMandays *Allocated HoursConsumed HoursAssignment SubmissionResetSubmit© 2020 | Equine Technologies GroupVersion master.3596Some fields has invalid value, please correct it.</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1710&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div</value>
   </webElementXpaths>
</WebElementEntity>
