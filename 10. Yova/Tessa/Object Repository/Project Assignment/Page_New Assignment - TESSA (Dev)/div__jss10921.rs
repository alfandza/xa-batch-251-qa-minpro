<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div__jss10921</name>
   <tag></tag>
   <elementGuidId>b1d5b7a9-7671-4c24-9c60-2df2b7039820</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/form/div/div[2]/div/div[2]/div[2]/div/div/div/div/div/div/div/div/div</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.jss9321.jss10950.jss10954.jss9327.jss10955.jss9330.jss10957.jss9323.jss10952.jss9322.jss10951 > div.jss9331.jss10958.jss10919 > div.jss10921</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss10921</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1710&quot;]/main[@class=&quot;jss2226 jss2228&quot;]/form[1]/div[@class=&quot;jss10287&quot;]/div[@class=&quot;jss10288&quot;]/div[@class=&quot;jss10291&quot;]/div[@class=&quot;jss3489 jss3492 jss11002 jss11003&quot;]/div[@class=&quot;jss5036 jss5037&quot;]/div[@class=&quot;jss5038&quot;]/div[@class=&quot;jss5039&quot;]/div[@class=&quot;jss11011&quot;]/div[1]/div[@class=&quot;css-10nd86i&quot;]/div[@class=&quot;jss10928 jss10929 jss10931&quot;]/div[@class=&quot;jss9321 jss10950 jss10954 jss9327 jss10955 jss9330 jss10957 jss9323 jss10952 jss9322 jss10951&quot;]/div[@class=&quot;jss9331 jss10958 jss10919&quot;]/div[@class=&quot;jss10921&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div/div[2]/div/div[2]/div[2]/div/div/div/div/div/div/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ADITYA MEYSIN'])[1]/preceding::div[7]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='AGUS KURNIAWAN'])[1]/preceding::div[8]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div/div/div/div/div/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
