import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('HR - Notification Period/TC_HRNotification_064'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('HR - Notification Period/Page_Modify Period - TESSA (Dev)/input__name'), Keys.chord(Keys.CONTROL, 
        'a', Keys.DELETE, Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/HR - Notification Period/Page_Modify Period - TESSA (Dev)/input__name'), 
    'Contract Expiring in 13 days')

WebUI.setText(findTestObject('HR - Notification Period/Page_Modify Period - TESSA (Dev)/input_To_to'), Keys.chord(Keys.CONTROL, 
        'a', Keys.DELETE, Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/HR - Notification Period/Page_Modify Period - TESSA (Dev)/input_To_to'), 
    '13')

WebUI.mouseOver(findTestObject('Object Repository/HR - Notification Period/Page_Modify Period - TESSA (Dev)/span_Submit'))

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Modify Period - TESSA (Dev)/span_Submit'))

WebUI.mouseOver(findTestObject('Object Repository/HR - Notification Period/Page_Modify Period - TESSA (Dev)/span_Continue'))

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Modify Period - TESSA (Dev)/span_Continue'))

