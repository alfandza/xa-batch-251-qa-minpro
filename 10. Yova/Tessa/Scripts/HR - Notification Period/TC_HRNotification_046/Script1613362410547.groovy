import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('HR - Notification Period/TC_HRNotification_045'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Period Details - TESSA (Dev)/svg_Period Details_jss1268'))

WebUI.mouseOver(findTestObject('Object Repository/HR - Notification Period/Page_Period Details - TESSA (Dev)/li_Modify'))

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Period Details - TESSA (Dev)/li_Modify'))

WebUI.mouseOver(findTestObject('Object Repository/HR - Notification Period/Page_Period Details - TESSA (Dev)/span_Agree'))

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Period Details - TESSA (Dev)/span_Agree'))

WebUI.setText(findTestObject('HR - Notification Period/Page_Modify Period - TESSA (Dev)/input__name'), Keys.chord(Keys.CONTROL, 
        'a', Keys.DELETE, Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/HR - Notification Period/Page_Modify Period - TESSA (Dev)/input__name'), 
    'Birthday Upcoming in 57 days')

WebUI.setText(findTestObject('HR - Notification Period/Page_Modify Period - TESSA (Dev)/input_To_to'), Keys.chord(Keys.CONTROL, 
        'a', Keys.DELETE, Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/HR - Notification Period/Page_Modify Period - TESSA (Dev)/input_To_to'), 
    '57')

WebUI.mouseOver(findTestObject('Object Repository/HR - Notification Period/Page_Modify Period - TESSA (Dev)/span_Submit'))

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Modify Period - TESSA (Dev)/span_Submit'))

WebUI.mouseOver(findTestObject('Object Repository/HR - Notification Period/Page_Modify Period - TESSA (Dev)/span_Continue'))

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Modify Period - TESSA (Dev)/span_Continue'))

WebUI.verifyElementPresent(findTestObject('Object Repository/HR - Notification Period/Page_Period Details - TESSA (Dev)/div_Existing period has been updated'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/HR - Notification Period/Page_Period Details - TESSA (Dev)/h6_Period Details'), 
    0)

