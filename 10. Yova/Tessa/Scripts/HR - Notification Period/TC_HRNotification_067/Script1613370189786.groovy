import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('HR - Notification Period/TC_HRNotification_002'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/input_Notification Periods_jss42421 jss42209'), 
    'Contract')

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/svg_Notification Periods_jss29840 jss29847'))

WebUI.mouseOver(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/li_Contract in Name'))

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/li_Contract in Name'))

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/div_Contract Expiring in 30 DaysContractEnd_1a692c'))

WebUI.mouseOver(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/span_Modify'))

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/span_Modify'))

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Modify Period - TESSA (Dev)/main_Period InformationPeriod IDTypeContrac_46e823'))

WebUI.setText(findTestObject('HR - Notification Period/Page_Modify Period - TESSA (Dev)/input__name'), Keys.chord(Keys.CONTROL, 
        'a', Keys.DELETE, Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/HR - Notification Period/Page_Modify Period - TESSA (Dev)/input__name'), 
    '')

WebUI.mouseOver(findTestObject('Object Repository/HR - Notification Period/Page_Modify Period - TESSA (Dev)/span_Submit'))

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Modify Period - TESSA (Dev)/span_Submit'))

WebUI.mouseOver(findTestObject('Object Repository/HR - Notification Period/Page_Modify Period - TESSA (Dev)/span_Continue'))

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Modify Period - TESSA (Dev)/span_Continue'))

WebUI.verifyElementPresent(findTestObject('Object Repository/HR - Notification Period/Page_Modify Period - TESSA (Dev)/div_Some fields has invalid value, please c_a06eb9'), 
    0)

WebUI.mouseOver(findTestObject('Object Repository/HR - Notification Period/Page_Modify Period - TESSA (Dev)/button_Modify Period_jss1265 jss1259 jss1260'))

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Modify Period - TESSA (Dev)/button_Modify Period_jss1265 jss1259 jss1260'))

WebUI.mouseOver(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/svg_Notification Periods_jss2686 jss2693'))

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/svg_Notification Periods_jss2686 jss2693'))

WebUI.verifyElementPresent(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/p_5 Record(s)'), 
    0)

