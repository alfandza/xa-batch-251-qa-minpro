import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('HR - Notification Period/TC_HRNotification_002'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/input_Notification Periods_jss1519 jss1307'), 
    'In A Range')

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/svg_Notification Periods_jss1268 jss1275'))

WebUI.mouseOver(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/li_In A Range in Any'))

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/li_In A Range in Any'))

WebUI.verifyElementPresent(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/ul_Internal Server ErrorAn error occur.Try _867e26'), 
    0)

