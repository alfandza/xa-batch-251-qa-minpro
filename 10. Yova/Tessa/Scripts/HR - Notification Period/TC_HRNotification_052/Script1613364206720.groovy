import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('HR - Notification Period/TC_HRNotification_044'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.doubleClick(findTestObject('Object Repository/HR - Notification Period/Page_New Period - TESSA (Dev)/input_From_from'))

WebUI.setText(findTestObject('Object Repository/HR - Notification Period/Page_New Period - TESSA (Dev)/input_From_from'), 
    '13')

WebUI.doubleClick(findTestObject('Object Repository/HR - Notification Period/Page_New Period - TESSA (Dev)/input_To_to'))

WebUI.setText(findTestObject('Object Repository/HR - Notification Period/Page_New Period - TESSA (Dev)/input_To_to'), '56')

WebUI.mouseOver(findTestObject('Object Repository/HR - Notification Period/Page_New Period - TESSA (Dev)/span_Submit'))

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_New Period - TESSA (Dev)/span_Submit'))

WebUI.mouseOver(findTestObject('Object Repository/HR - Notification Period/Page_New Period - TESSA (Dev)/span_Continue'))

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_New Period - TESSA (Dev)/span_Continue'))

WebUI.verifyElementPresent(findTestObject('Object Repository/HR - Notification Period/Page_New Period - TESSA (Dev)/div_Some fields has invalid value, please c_a06eb9'), 
    0)

