import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('HR - Notification Period/TC_HRNotification_065'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Period Details - TESSA (Dev)/svg_Period Details_jss29840'))

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/div_Contract Expiring in 14 DaysContractEnd_4403f2'))

WebUI.mouseOver(findTestObject('HR - Notification Period/Page_Notification Periods - TESSA (Dev)/span_Modify'))

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/span_Modify'))

WebUI.setText(findTestObject('HR - Notification Period/Page_Modify Period - TESSA (Dev)/input__name'), Keys.chord(Keys.CONTROL, 
        'a', Keys.DELETE, Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/HR - Notification Period/Page_Modify Period - TESSA (Dev)/input__name'), 
    'Contract Expiring in 13 days')

WebUI.setText(findTestObject('HR - Notification Period/Page_Modify Period - TESSA (Dev)/input_To_to'), Keys.chord(Keys.CONTROL, 
        'a', Keys.DELETE, Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/HR - Notification Period/Page_Modify Period - TESSA (Dev)/input_To_to'), 
    '07')

WebUI.mouseOver(findTestObject('Object Repository/HR - Notification Period/Page_Modify Period - TESSA (Dev)/span_Submit'))

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Modify Period - TESSA (Dev)/span_Submit'))

WebUI.mouseOver(findTestObject('Object Repository/HR - Notification Period/Page_Modify Period - TESSA (Dev)/span_Discard'))

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Modify Period - TESSA (Dev)/span_Discard'))

WebUI.mouseOver(findTestObject('Object Repository/HR - Notification Period/Page_Modify Period - TESSA (Dev)/svg_Modify Period_jss29840'))

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Modify Period - TESSA (Dev)/svg_Modify Period_jss29840'))

WebUI.verifyElementPresent(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/p_8 - 14'), 
    0)

