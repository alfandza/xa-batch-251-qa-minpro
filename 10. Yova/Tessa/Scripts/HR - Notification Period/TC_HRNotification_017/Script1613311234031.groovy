import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('HR - Notification Period/TC_HRNotification_016'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/svg_Collapse Sidebar_jss1268'))

WebUI.mouseOver(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/span_Type'))

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/span_Type'))

WebUI.mouseOver(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/span_None'))

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/span_None'))

WebUI.mouseOver(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/span_Apply'))

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/span_Apply'))

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/svg_Collapse Sidebar_jss1268_1'))

WebUI.mouseOver(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/li_Last Changes'))

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/li_Last Changes'))

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/svg_Collapse Sidebar_jss1268_1_2'))

WebUI.mouseOver(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/li_Descending'))

WebUI.click(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/li_Descending'))

WebUI.verifyElementPresent(findTestObject('Object Repository/HR - Notification Period/Page_Notification Periods - TESSA (Dev)/p_5 Record(s)'), 
    0)

