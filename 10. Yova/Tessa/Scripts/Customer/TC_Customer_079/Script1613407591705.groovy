import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Customer/LOGIN_002'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/svg_Customer_jss29202'))

WebUI.click(findTestObject('Object Repository/Customer/Page_New Customer - TESSA (Dev)/div__jss9030 jss10658 jss10671'))

WebUI.click(findTestObject('Object Repository/Customer/Page_New Customer - TESSA (Dev)/div_Xsis Mitra Utama'))

WebUI.click(findTestObject('Object Repository/Customer/Page_New Customer - TESSA (Dev)/div__jss9030 jss10658 jss10671'))

WebUI.click(findTestObject('Object Repository/Customer/Page_New Customer - TESSA (Dev)/div_Telecommunication'))

WebUI.setText(findTestObject('Object Repository/Customer/Page_New Customer - TESSA (Dev)/input__name'), 'Yovanda Priscilla')

WebUI.setText(findTestObject('Object Repository/Customer/Page_New Customer - TESSA (Dev)/textarea_Jl Paingan 7 No 40'), 
    'Jl Paingan 7 No 40')

WebUI.setText(findTestObject('Object Repository/Customer/Page_New Customer - TESSA (Dev)/input_Mobile 1_mobile'), '082134945713')

WebUI.setText(findTestObject('Object Repository/Customer/Page_New Customer - TESSA (Dev)/input_E-mail 1_email'), 'scillapriscillayovanda@gmail.com')

WebUI.click(findTestObject('Object Repository/Customer/Page_New Customer - TESSA (Dev)/span_Active Status'))

WebUI.scrollToPosition(0, 0)

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_New Customer - TESSA (Dev)/span_Submit'))

WebUI.click(findTestObject('Object Repository/Customer/Page_New Customer - TESSA (Dev)/span_Submit'))

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_New Customer - TESSA (Dev)/span_Continue'))

WebUI.click(findTestObject('Object Repository/Customer/Page_New Customer - TESSA (Dev)/span_Continue'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Customer/Page_Customer Detail - TESSA (Dev)/h6_Customer Detail'), 
    0)

