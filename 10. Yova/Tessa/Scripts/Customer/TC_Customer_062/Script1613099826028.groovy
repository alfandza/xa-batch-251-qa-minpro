import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Customer/TC_Customer_061'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Customer Detail - TESSA (Dev)/svg_Customer Detail_jss1036'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer Detail - TESSA (Dev)/svg_Customer Detail_jss1036'))

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Customer Detail - TESSA (Dev)/li_Modify'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer Detail - TESSA (Dev)/li_Modify'))

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Customer Detail - TESSA (Dev)/span_Agree'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer Detail - TESSA (Dev)/span_Agree'))

WebUI.setText(findTestObject('Object Repository/Customer/Page_Modify Customer - TESSA (Dev)/textarea_Jl Prayan I No 88 B'), 
    'Jl Prayan I No 88 B ')

WebUI.setText(findTestObject('Object Repository/Customer/Page_Modify Customer - TESSA (Dev)/input_Phone 1_phone'), '087865423123')

WebUI.click(findTestObject('Object Repository/Customer/Page_Modify Customer - TESSA (Dev)/input_concat(Contact Person 2, , s Title)_isActive'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Modify Customer - TESSA (Dev)/input_Active Status_isAuthorize'))

WebUI.scrollToPosition(0, 0)

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Modify Customer - TESSA (Dev)/button_Submit'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Modify Customer - TESSA (Dev)/button_Submit'))

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Modify Customer - TESSA (Dev)/button_Continue'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Modify Customer - TESSA (Dev)/button_Continue'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Customer/Page_Modify Customer - TESSA (Dev)/div_Update submission failed'), 
    0)

