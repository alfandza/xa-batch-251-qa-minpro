import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Customer/TC_Customer_036'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.mouseOver(findTestObject('Customer/Page_Customer - TESSA (Dev)/button_Collapse Sidebar_option-filter'))

WebUI.click(findTestObject('Customer/Page_Customer - TESSA (Dev)/button_Collapse Sidebar_option-filter'))

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/span_Company'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/span_Company'))

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/input_Company_jss17463'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/input_Company_jss17463'))

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/span_Industry'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/span_Industry'))

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/input_Company_jss17463'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/input_Company_jss17463'))

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/span_Apply'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/span_Apply'))

WebUI.mouseOver(findTestObject('Customer/Page_Customer - TESSA (Dev)/button_Collapse Sidebar_option-order'))

WebUI.click(findTestObject('Customer/Page_Customer - TESSA (Dev)/button_Collapse Sidebar_option-order'))

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/li_Last Changes'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/li_Last Changes'))

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/svg_Collapse Sidebar_jss1036_1_2'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/svg_Collapse Sidebar_jss1036_1_2'))

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/li_Descending'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/li_Descending'))

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/svg_Collapse Sidebar_jss1036_1_2_3'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/svg_Collapse Sidebar_jss1036_1_2_3'))

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/li_10'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/li_10'))

