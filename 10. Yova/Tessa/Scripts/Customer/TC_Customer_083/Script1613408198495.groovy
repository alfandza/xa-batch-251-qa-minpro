import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Customer/TC_Customer_081'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer Detail - TESSA (Dev)/svg_Customer Detail_jss29202'))

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Customer Detail - TESSA (Dev)/li_Modify'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer Detail - TESSA (Dev)/li_Modify'))

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Customer Detail - TESSA (Dev)/span_Agree'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer Detail - TESSA (Dev)/span_Agree'))

WebUI.setText(findTestObject('Object Repository/Customer/Page_Modify Customer - TESSA (Dev)/textarea_Jl Paingan 7 No 96 C'), 
    Keys.chord(Keys.CONTROL, 'a', Keys.DELETE))

WebUI.setText(findTestObject('Object Repository/Customer/Page_Modify Customer - TESSA (Dev)/textarea_Jl Paingan 7 No 96 C'), 
    'Jl Paingan 7 No 96 C')

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Modify Customer - TESSA (Dev)/span_Submit'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Modify Customer - TESSA (Dev)/span_Submit'))

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Modify Customer - TESSA (Dev)/span_Discard'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Modify Customer - TESSA (Dev)/span_Discard'))

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Modify Customer - TESSA (Dev)/svg_Modify Customer_jss29202'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Modify Customer - TESSA (Dev)/svg_Modify Customer_jss29202'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/div_Yovanda Priscillaantar niaga dayascilla_c993af'))

