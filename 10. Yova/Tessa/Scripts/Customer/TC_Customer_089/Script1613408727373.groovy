import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Customer/TC_Customer_088'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/button_HR Corner_jss45883 jss45877 jss45878'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/button_HR Corner_jss45883 jss45877 jss45878'))

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Dashboard - TESSA (Dev)/span_Setup'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Dashboard - TESSA (Dev)/span_Setup'))

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Dashboard - TESSA (Dev)/span_Customer'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Dashboard - TESSA (Dev)/span_Customer'))

