import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Customer/TC_Customer_076'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.mouseOver(findTestObject('Customer/Page_Customer - TESSA (Dev) (1)/button_Customer_jss1729 jss1723 jss15446'))

WebUI.click(findTestObject('Customer/Page_Customer - TESSA (Dev) (1)/button_Customer_jss1729 jss1723 jss15446'))

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Dashboard - TESSA (Dev)/button_XMU - Automation Test Submitter_user_9f1d36'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Dashboard - TESSA (Dev)/button_XMU - Automation Test Submitter_user_9f1d36'))

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/li_Switch Access'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/li_Switch Access'))

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/span_Continue'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/span_Continue'))

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/svg_Automation TesterAutomation Tester_jss19053'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/svg_Automation TesterAutomation Tester_jss19053'))

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/span_Automation Test Submitter'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/span_Automation Test Submitter'))

