import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Customer/TC_Customer_083'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/span_Details'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/span_Details'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer Detail - TESSA (Dev)/svg_Customer Detail_jss29202'))

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Customer Detail - TESSA (Dev)/li_Delete'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer Detail - TESSA (Dev)/li_Delete'))

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Customer Detail - TESSA (Dev)/span_Agree'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer Detail - TESSA (Dev)/span_Agree'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/div_customer with ID CS00001174 has been deleted'), 
    0)

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/svg_Customer_jss29202 jss29209'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/svg_Customer_jss29202 jss29209'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/h6_Customer'), 0)

