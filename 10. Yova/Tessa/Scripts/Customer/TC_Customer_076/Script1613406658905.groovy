import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Customer/TC_Customer_039'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/div_Terang AbadiAutomation TesterAutomation_8e9188'))

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/span_Modify'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/span_Modify'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Modify Customer - TESSA (Dev)/div_Customer IDCompanyAutomation TesterAuto_f4808d'))

WebUI.setText(findTestObject('Customer/Page_Modify Customer - TESSA (Dev)/input_Phone 2_phoneAdditional'), Keys.chord(Keys.CONTROL, 
        'a', Keys.DELETE))

WebUI.setText(findTestObject('Object Repository/Customer/Page_Modify Customer - TESSA (Dev)/input_Phone 2_phoneAdditional'), 
    '0815243715263')

WebUI.scrollToPosition(0, 0)

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Modify Customer - TESSA (Dev)/span_Submit'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Modify Customer - TESSA (Dev)/span_Submit'))

WebUI.mouseOver(findTestObject('Object Repository/Customer/Page_Modify Customer - TESSA (Dev)/span_Continue'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Modify Customer - TESSA (Dev)/span_Continue'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Customer/Page_Customer Detail - TESSA (Dev)/div_Customer has been updated'), 
    0)

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer Detail - TESSA (Dev)/svg_Customer Detail_jss1036'))

WebUI.click(findTestObject('Object Repository/Customer/Page_Customer - TESSA (Dev)/svg_Customer_jss1036 jss1043'))

