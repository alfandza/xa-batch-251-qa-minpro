import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Project Assignment/TC_Assignment_014'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.mouseOver(findTestObject('Object Repository/Project Assignment/Page_Modify Assignment - TESSA (Dev)/button_Assignment Items_jss2683 jss2677 jss2679'))

WebUI.click(findTestObject('Object Repository/Project Assignment/Page_Modify Assignment - TESSA (Dev)/button_Assignment Items_jss2683 jss2677 jss2679'))

WebUI.executeJavaScript('window.scrollTo(0, document.body.scrollHeight)', a)

WebUI.click(findTestObject('Object Repository/Project Assignment/Page_Modify Assignment - TESSA (Dev)/div__jss7751 jss7736 jss7768'), 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Project Assignment/Page_Modify Assignment - TESSA (Dev)/div_CHAIRANI ADELINA'))

WebUI.setText(findTestObject('Object Repository/Project Assignment/Page_Modify Assignment - TESSA (Dev)/input__items.1.role'), 
    'UI/UX')

WebUI.setText(findTestObject('Object Repository/Project Assignment/Page_Modify Assignment - TESSA (Dev)/input__items.1.jobDescription'), 
    'Membuat design tampilan aplikasi penjualan mobil')

WebUI.doubleClick(findTestObject('Object Repository/Project Assignment/Page_Modify Assignment - TESSA (Dev)/input__items.1.mandays'))

WebUI.setText(findTestObject('Object Repository/Project Assignment/Page_Modify Assignment - TESSA (Dev)/input__items.1.mandays'), 
    '30')

WebUI.scrollToPosition(0, 0)

WebUI.mouseOver(findTestObject('Object Repository/Project Assignment/Page_Modify Assignment - TESSA (Dev)/span_Submit'))

WebUI.click(findTestObject('Object Repository/Project Assignment/Page_Modify Assignment - TESSA (Dev)/span_Submit'))

WebUI.mouseOver(findTestObject('Object Repository/Project Assignment/Page_Modify Assignment - TESSA (Dev)/span_Continue'))

WebUI.click(findTestObject('Object Repository/Project Assignment/Page_Modify Assignment - TESSA (Dev)/span_Continue'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Project Assignment/Page_Modify Assignment - TESSA (Dev)/div_Unable to process your submission'), 
    0)

