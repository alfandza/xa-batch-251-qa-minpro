import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Project Assignment/TC_Assignment_078'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Project Assignment/Page_Project Assignments - TESSA (Dev)/span_Purchase'))

WebUI.click(findTestObject('Object Repository/Project Assignment/Page_Project Assignments - TESSA (Dev)/span_11 Approval'))

WebUI.click(findTestObject('Object Repository/Project Assignment/Page_Project Assignments - TESSA (Dev)/span_Beli Laptop - cocobi yeuh - Aryasa Indah'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Project Assignment/Page_Request Approval Form - TESSA (Dev)/h6_Request Approval Form'), 
    0)

WebUI.click(findTestObject('Object Repository/Project Assignment/Page_Request Approval Form - TESSA (Dev)/div_Project Assignment'))

WebUI.click(findTestObject('Object Repository/Project Assignment/Page_Request Approval Form - TESSA (Dev)/span_Create  Edit Assignment'))

