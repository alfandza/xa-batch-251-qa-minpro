import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Project Assignment/LOGIN_002'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Project Assignment/Page_Project Assignments - TESSA (Dev)/input_Project Assignments_jss12590 jss12378'), 
    'ASG2100001')

WebUI.mouseOver(findTestObject('Object Repository/Project Assignment/Page_Project Assignments - TESSA (Dev)/button_Project Assignments_search.fields'))

WebUI.click(findTestObject('Object Repository/Project Assignment/Page_Project Assignments - TESSA (Dev)/button_Project Assignments_search.fields'))

WebUI.mouseOver(findTestObject('Object Repository/Project Assignment/Page_Project Assignments - TESSA (Dev)/li_ASG2100001 in Assignment ID'))

WebUI.click(findTestObject('Object Repository/Project Assignment/Page_Project Assignments - TESSA (Dev)/li_ASG2100001 in Assignment ID'))

WebUI.mouseOver(findTestObject('Object Repository/Project Assignment/Page_Project Assignments - TESSA (Dev)/button_Project Assignments_jss29262 jss2925_e439d7'))

WebUI.click(findTestObject('Object Repository/Project Assignment/Page_Project Assignments - TESSA (Dev)/button_Project Assignments_jss29262 jss2925_e439d7'))

