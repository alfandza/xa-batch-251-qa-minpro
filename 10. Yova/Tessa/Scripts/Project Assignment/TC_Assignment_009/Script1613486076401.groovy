import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Project Assignment/TC_Assignment_007'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.mouseOver(findTestObject('Object Repository/Web Job/Page_Assignment Details - TESSA (Dev)/button_Assignment Details_project-assignmen_ced3f0'))

WebUI.click(findTestObject('Object Repository/Web Job/Page_Assignment Details - TESSA (Dev)/button_Assignment Details_project-assignmen_ced3f0'))

WebUI.mouseOver(findTestObject('Object Repository/Web Job/Page_Assignment Details - TESSA (Dev)/li_Modify'))

WebUI.click(findTestObject('Object Repository/Web Job/Page_Assignment Details - TESSA (Dev)/li_Modify'))

WebUI.mouseOver(findTestObject('Object Repository/Web Job/Page_Assignment Details - TESSA (Dev)/span_Continue'))

WebUI.click(findTestObject('Object Repository/Web Job/Page_Assignment Details - TESSA (Dev)/span_Continue'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Web Job/Page_Modify Assignment - TESSA (Dev)/h6_Modify Assignment'), 
    0)

