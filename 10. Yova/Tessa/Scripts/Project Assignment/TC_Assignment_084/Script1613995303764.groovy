import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Project Assignment/TC_Assignment_076'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Project Assignment/Page_Dashboard - TESSA (Dev)/button_XMU - Automation Test Approver_user-_15156a'))

WebUI.click(findTestObject('Object Repository/Project Assignment/Page_Dashboard - TESSA (Dev)/li_Logout'))

WebUI.click(findTestObject('Object Repository/Project Assignment/Page_Dashboard - TESSA (Dev)/span_Yes'))

WebUI.click(findTestObject('Object Repository/Project Assignment/Page_Welcome to New TESSA/button_Let me in'))

WebUI.switchToWindowTitle('Login - Equine Identity')

WebUI.setText(findTestObject('Object Repository/Project Assignment/Page_Login - Equine Identity/input_Login_Username'), 
    'della.mutiara@xsis.net')

WebUI.setEncryptedText(findTestObject('Object Repository/Project Assignment/Page_Login - Equine Identity/input_Login_Password'), 
    'zP4F53vxAWI=')

WebUI.click(findTestObject('Object Repository/Project Assignment/Page_Login - Equine Identity/button_Login'))

WebUI.switchToWindowTitle('Welcome to New TESSA')

WebUI.click(findTestObject('Object Repository/Project Assignment/Page_Welcome to New TESSA/div_Xsis Mitra Utama'))

WebUI.click(findTestObject('Object Repository/Project Assignment/Page_Welcome to New TESSA/span_Automation Test Submitter'))

WebUI.click(findTestObject('Object Repository/Project Assignment/Page_Dashboard - TESSA (Dev)/span_Project Assignment'))

WebUI.click(findTestObject('Object Repository/Project Assignment/Page_Dashboard - TESSA (Dev)/span_Create  Edit Assignment'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Project Assignment/Page_Project Assignments - TESSA (Dev)/h6_Project Assignments'), 
    0)

