import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Project Assignment/TC_Assignment_003'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Project Assignment/Page_New Assignment - TESSA (Dev)/div_Select'))

WebUI.click(findTestObject('Object Repository/Project Assignment/Page_New Assignment - TESSA (Dev)/div_XMU.PS2102.0005 - testing'))

WebUI.click(findTestObject('Object Repository/Project Assignment/Page_New Assignment - TESSA (Dev)/button_Assignment Items_jss2705 jss2699 jss2701'))

WebUI.click(findTestObject('Object Repository/Project Assignment/Page_New Assignment - TESSA (Dev)/div__jss34746 jss36373 jss36334'))

WebUI.setText(findTestObject('Object Repository/Project Assignment/Page_New Assignment - TESSA (Dev)/input__react-select-5-input'), 
    'DELL')

WebUI.click(findTestObject('Object Repository/Project Assignment/Page_New Assignment - TESSA (Dev)/div_DELLA MUTIARA'))

WebUI.setText(findTestObject('Object Repository/Project Assignment/Page_New Assignment - TESSA (Dev)/input__items.0.role'), 
    'Automation Tester')

WebUI.setText(findTestObject('Object Repository/Project Assignment/Page_New Assignment - TESSA (Dev)/input__items.0.jobDescription'), 
    'Testing web client')

WebUI.doubleClick(findTestObject('Object Repository/Project Assignment/Page_New Assignment - TESSA (Dev)/input__items.0.mandays'))

WebUI.setText(findTestObject('Object Repository/Project Assignment/Page_New Assignment - TESSA (Dev)/input__items.0.mandays'), 
    '7')

WebUI.click(findTestObject('Object Repository/Project Assignment/Page_New Assignment - TESSA (Dev)/span_Submit'))

WebUI.click(findTestObject('Object Repository/Project Assignment/Page_New Assignment - TESSA (Dev)/span_Continue'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Project Assignment/Page_Assignment Details - TESSA (Dev)/div_A new assignment has been created'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Project Assignment/Page_Assignment Details - TESSA (Dev)/h6_Assignment Details'), 
    0)

