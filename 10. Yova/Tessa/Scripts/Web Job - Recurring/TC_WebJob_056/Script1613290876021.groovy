import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Web Job - Recurring/TC_WebJob_055'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Web Job/Page_Modify Recurring - TESSA (Dev)/input__description'), Keys.chord(Keys.CONTROL, 
        'a', Keys.DELETE, Keys.ENTER))

WebUI.setText(findTestObject('Web Job/Page_Modify Recurring - TESSA (Dev)/input__description'), 'Send project due date at 07 AM')

WebUI.mouseOver(findTestObject('Web Job/Page_Modify Recurring - TESSA (Dev)/span_Submit'))

WebUI.click(findTestObject('Object Repository/Web Job/Page_Modify Recurring - TESSA (Dev)/span_Submit'))

WebUI.mouseOver(findTestObject('Web Job/Page_Modify Recurring - TESSA (Dev)/span_Continue'))

WebUI.click(findTestObject('Object Repository/Web Job/Page_Modify Recurring - TESSA (Dev)/span_Continue'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Web Job/Page_Web Job Recurring - TESSA (Dev)/div_Recurring with name Project Notificatio_5300c7'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Web Job/Page_Web Job Recurring - TESSA (Dev)/span_Detail Information'), 
    0)

