<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>path</name>
   <tag></tag>
   <elementGuidId>f14ba633-966c-4067-b57e-1cf93201d3f6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>svg.jss6109.jss6112.jss8032.jss8047.jss8033 > path</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>path</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>d</name>
      <type>Main</type>
      <value>M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss5133&quot;]/div[@class=&quot;jss7227 jss7228 jss7278 jss7452&quot;]/div[@class=&quot;jss6912 jss6914 jss7229 jss7452 jss7230 jss7234&quot;]/nav[@class=&quot;jss7954&quot;]/li[@class=&quot;jss8446&quot;]/div[@class=&quot;jss8458&quot;]/svg[@class=&quot;jss6109 jss6112 jss8032 jss8047 jss8033&quot;]/path[1]</value>
   </webElementProperties>
</WebElementEntity>
