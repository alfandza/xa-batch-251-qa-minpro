<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>html_Expenses - TESSA (Dev).jss13778   top _844c90</name>
   <tag></tag>
   <elementGuidId>f4ace089-c1e5-4db5-9e9f-0cf15a723486</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//html</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>html</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>html</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>lang</name>
      <type>Main</type>
      <value>en</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Expenses - TESSA (Dev)
.jss13778 {
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  display: block;
  z-index: 0;
  position: absolute;
  overflow: hidden;
  border-radius: inherit;
  pointer-events: none;
}
.jss13779 {
  top: 0;
  left: 0;
  width: 50px;
  height: 50px;
  opacity: 0;
  position: absolute;
}
.jss13780 {
  opacity: 0.3;
  transform: scale(1);
  animation: mui-ripple-enter 550ms cubic-bezier(0.4, 0, 0.2, 1);
}
.jss13781 {
  animation-duration: 200ms;
}
.jss13782 {
  width: 100%;
  height: 100%;
  opacity: 1;
  display: block;
  border-radius: 50%;
  background-color: currentColor;
}
.jss13783 {
  opacity: 0;
  animation: mui-ripple-exit 550ms cubic-bezier(0.4, 0, 0.2, 1);
}
.jss13784 {
  top: 0;
  left: 0;
  position: absolute;
  animation: mui-ripple-pulsate 2500ms cubic-bezier(0.4, 0, 0.2, 1) 200ms infinite;
}
@-webkit-keyframes mui-ripple-enter {
  0% {
    opacity: 0.1;
    transform: scale(0);
  }
  100% {
    opacity: 0.3;
    transform: scale(1);
  }
}
@-webkit-keyframes mui-ripple-exit {
  0% {
    opacity: 1;
  }
  100% {
    opacity: 0;
  }
}
@-webkit-keyframes mui-ripple-pulsate {
  0% {
    transform: scale(1);
  }
  50% {
    transform: scale(0.92);
  }
  100% {
    transform: scale(1);
  }
}

.jss12173 {
  color: inherit;
  border: 0;
  margin: 0;
  cursor: pointer;
  display: inline-flex;
  outline: none;
  padding: 0;
  position: relative;
  align-items: center;
  user-select: none;
  border-radius: 0;
  vertical-align: middle;
  justify-content: center;
  -moz-appearance: none;
  text-decoration: none;
  background-color: transparent;
  -webkit-appearance: none;
  -webkit-tap-highlight-color: transparent;
}
.jss12173::-moz-focus-inner {
  border-style: none;
}
.jss12173.jss12174 {
  cursor: default;
  pointer-events: none;
}

.jss19991 {
  color: rgba(0, 0, 0, 0.87);
  padding: 8px 16px;
  font-size: 0.8125rem;
  min-width: 64px;
  box-sizing: border-box;
  min-height: 36px;
  transition: background-color 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,box-shadow 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,border 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: 500;
  line-height: 1.5;
  border-radius: 4px;
  letter-spacing: 0.02857em;
  text-transform: uppercase;
}
.jss19991:hover {
  text-decoration: none;
  background-color: rgba(0, 0, 0, 0.08);
}
.jss19991.jss20011 {
  color: rgba(0, 0, 0, 0.26);
}
@media (hover: none) {
  .jss19991:hover {
    background-color: transparent;
  }
}
.jss19991:hover.jss20011 {
  background-color: transparent;
}
.jss19992 {
  width: 100%;
  display: inherit;
  align-items: inherit;
  justify-content: inherit;
}
.jss19994 {
  color: #03a9f4;
}
.jss19994:hover {
  background-color: rgba(3, 169, 244, 0.08);
}
@media (hover: none) {
  .jss19994:hover {
    background-color: transparent;
  }
}
.jss19995 {
  color: #ff9800;
}
.jss19995:hover {
  background-color: rgba(255, 152, 0, 0.08);
}
@media (hover: none) {
  .jss19995:hover {
    background-color: transparent;
  }
}
.jss19999 {
  border: 1px solid rgba(0, 0, 0, 0.23);
}
.jss20000 {
  color: #03a9f4;
  border: 1px solid rgba(3, 169, 244, 0.5);
}
.jss20000:hover {
  border: 1px solid #03a9f4;
  background-color: rgba(3, 169, 244, 0.08);
}
.jss20000.jss20011 {
  border: 1px solid rgba(0, 0, 0, 0.26);
}
@media (hover: none) {
  .jss20000:hover {
    background-color: transparent;
  }
}
.jss20001 {
  color: #ff9800;
  border: 1px solid rgba(255, 152, 0, 0.5);
}
.jss20001:hover {
  border: 1px solid #ff9800;
  background-color: rgba(255, 152, 0, 0.08);
}
.jss20001.jss20011 {
  border: 1px solid rgba(0, 0, 0, 0.26);
}
@media (hover: none) {
  .jss20001:hover {
    background-color: transparent;
  }
}
.jss20002 {
  color: rgba(0, 0, 0, 0.87);
  box-shadow: 0px 1px 5px 0px rgba(0, 0, 0, 0.2),0px 2px 2px 0px rgba(0, 0, 0, 0.14),0px 3px 1px -2px rgba(0, 0, 0, 0.12);
  background-color: #e0e0e0;
}
.jss20002.jss20010 {
  box-shadow: 0px 3px 5px -1px rgba(0, 0, 0, 0.2),0px 6px 10px 0px rgba(0, 0, 0, 0.14),0px 1px 18px 0px rgba(0, 0, 0, 0.12);
}
.jss20002:active {
  box-shadow: 0px 5px 5px -3px rgba(0, 0, 0, 0.2),0px 8px 10px 1px rgba(0, 0, 0, 0.14),0px 3px 14px 2px rgba(0, 0, 0, 0.12);
}
.jss20002.jss20011 {
  color: rgba(0, 0, 0, 0.26);
  box-shadow: none;
  background-color: rgba(0, 0, 0, 0.12);
}
.jss20002:hover {
  background-color: #d5d5d5;
}
@media (hover: none) {
  .jss20002:hover {
    background-color: #e0e0e0;
  }
}
.jss20002:hover.jss20011 {
  background-color: rgba(0, 0, 0, 0.12);
}
.jss20003 {
  color: rgba(0, 0, 0, 0.87);
  background-color: #03a9f4;
}
.jss20003:hover {
  background-color: #0288d1;
}
@media (hover: none) {
  .jss20003:hover {
    background-color: #03a9f4;
  }
}
.jss20004 {
  color: rgba(0, 0, 0, 0.87);
  background-color: #ff9800;
}
.jss20004:hover {
  background-color: #ff6d00;
}
@media (hover: none) {
  .jss20004:hover {
    background-color: #ff9800;
  }
}
.jss20008 {
  width: 56px;
  height: 56px;
  padding: 0;
  min-width: 0;
  box-shadow: 0px 3px 5px -1px rgba(0, 0, 0, 0.2),0px 6px 10px 0px rgba(0, 0, 0, 0.14),0px 1px 18px 0px rgba(0, 0, 0, 0.12);
  border-radius: 50%;
}
.jss20008:active {
  box-shadow: 0px 7px 8px -4px rgba(0, 0, 0, 0.2),0px 12px 17px 2px rgba(0, 0, 0, 0.14),0px 5px 22px 4px rgba(0, 0, 0, 0.12);
}
.jss20009 {
  width: auto;
  height: 48px;
  padding: 0 16px;
  min-width: 48px;
  border-radius: 24px;
}
.jss20012 {
  color: inherit;
}
.jss20013 {
  width: 40px;
  height: 40px;
}
.jss20014 {
  padding: 7px 8px;
  min-width: 64px;
  font-size: 0.7544642857142857rem;
  min-height: 32px;
}
.jss20015 {
  padding: 8px 24px;
  min-width: 112px;
  font-size: 0.8705357142857143rem;
  min-height: 40px;
}
.jss20016 {
  width: 100%;
}

.jss13297 {
  height: 1px;
  margin: 0;
  border: none;
  flex-shrink: 0;
  background-color: rgba(0, 0, 0, 0.12);
}
.jss13298 {
  left: 0;
  width: 100%;
  bottom: 0;
  position: absolute;
}
.jss13299 {
  margin-left: 72px;
}
.jss13300 {
  background-color: rgba(0, 0, 0, 0.08);
}
.jss13301 {
  margin-left: 16px;
  margin-right: 16px;
}

.jss14261 {
  margin: 0;
  padding: 0;
  position: relative;
  list-style: none;
}
.jss14262 {
  padding-top: 8px;
  padding-bottom: 8px;
}
.jss14263 {
  padding-top: 4px;
  padding-bottom: 4px;
}
.jss14264 {
  padding-top: 0;
}

.jss14752 {
  width: 100%;
  display: flex;
  position: relative;
  box-sizing: border-box;
  text-align: left;
  align-items: center;
  padding-top: 11px;
  padding-bottom: 11px;
  justify-content: flex-start;
  text-decoration: none;
}
.jss14752.jss14763, .jss14752.jss14763:hover {
  background-color: rgba(0, 0, 0, 0.14);
}
.jss14753 {
  position: relative;
}
.jss14754 {
  background-color: rgba(0, 0, 0, 0.08);
}
.jss14756 {
  padding-top: 8px;
  padding-bottom: 8px;
}
.jss14757 {
  align-items: flex-start;
}
.jss14758 {
  opacity: 0.5;
}
.jss14759 {
  border-bottom: 1px solid rgba(0, 0, 0, 0.12);
  background-clip: padding-box;
}
.jss14760 {
  padding-left: 16px;
  padding-right: 16px;
}
@media (min-width:600px) {
  .jss14760 {
    padding-left: 24px;
    padding-right: 24px;
  }
}
.jss14761 {
  transition: background-color 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
}
.jss14761:hover {
  text-decoration: none;
  background-color: rgba(0, 0, 0, 0.08);
}
@media (hover: none) {
  .jss14761:hover {
    background-color: transparent;
  }
}
.jss14762 {
  padding-right: 32px;
}

.jss14764 {
  color: rgba(0, 0, 0, 0.54);
  display: inline-flex;
  flex-shrink: 0;
  margin-right: 16px;
}

.jss14765 {
  top: 50%;
  right: 4px;
  position: absolute;
  transform: translateY(-50%);
}

.jss12132 {
  margin: 0;
  display: block;
}
.jss12133 {
  color: rgba(0, 0, 0, 0.54);
  font-size: 6.5rem;
  font-weight: 300;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  line-height: 1.14286em;
  margin-left: -.04em;
  letter-spacing: -.04em;
}
.jss12134 {
  color: rgba(0, 0, 0, 0.54);
  font-size: 3.25rem;
  font-weight: 400;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  line-height: 1.30357em;
  margin-left: -.02em;
  letter-spacing: -.02em;
}
.jss12135 {
  color: rgba(0, 0, 0, 0.54);
  font-size: 2.611607142857143rem;
  font-weight: 400;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  line-height: 1.13333em;
  margin-left: -.02em;
}
.jss12136 {
  color: rgba(0, 0, 0, 0.54);
  font-size: 1.9732142857142858rem;
  font-weight: 400;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  line-height: 1.20588em;
}
.jss12137 {
  color: rgba(0, 0, 0, 0.87);
  font-size: 1.3928571428571428rem;
  font-weight: 400;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  line-height: 1.35417em;
}
.jss12138 {
  color: rgba(0, 0, 0, 0.87);
  font-size: 1.21875rem;
  font-weight: 500;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  line-height: 1.16667em;
}
.jss12139 {
  color: rgba(0, 0, 0, 0.87);
  font-size: 0.9285714285714286rem;
  font-weight: 400;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  line-height: 1.5em;
}
.jss12140 {
  color: rgba(0, 0, 0, 0.87);
  font-size: 0.8125rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: 400;
  line-height: 1.5;
  letter-spacing: 0.01071em;
}
.jss12141 {
  color: rgba(0, 0, 0, 0.87);
  font-size: 0.9285714285714286rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: 400;
  line-height: 1.5;
  letter-spacing: 0.00938em;
}
.jss12142 {
  color: rgba(0, 0, 0, 0.87);
  font-size: 0.6964285714285714rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: 400;
  line-height: 1.66;
  letter-spacing: 0.03333em;
}
.jss12143 {
  color: rgba(0, 0, 0, 0.87);
  font-size: 0.8125rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: 500;
  line-height: 1.5;
  letter-spacing: 0.02857em;
  text-transform: uppercase;
}
.jss12144 {
  color: rgba(0, 0, 0, 0.87);
  font-size: 5.571428571428571rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: 300;
  line-height: 1;
  letter-spacing: -0.01562em;
}
.jss12145 {
  color: rgba(0, 0, 0, 0.87);
  font-size: 3.482142857142857rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: 300;
  line-height: 1;
  letter-spacing: -0.00833em;
}
.jss12146 {
  color: rgba(0, 0, 0, 0.87);
  font-size: 2.7857142857142856rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: 400;
  line-height: 1.04;
  letter-spacing: 0em;
}
.jss12147 {
  color: rgba(0, 0, 0, 0.87);
  font-size: 1.9732142857142858rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: 400;
  line-height: 1.17;
  letter-spacing: 0.00735em;
}
.jss12148 {
  color: rgba(0, 0, 0, 0.87);
  font-size: 1.3928571428571428rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: 400;
  line-height: 1.33;
  letter-spacing: 0em;
}
.jss12149 {
  color: rgba(0, 0, 0, 0.87);
  font-size: 1.1607142857142858rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: 500;
  line-height: 1.6;
  letter-spacing: 0.0075em;
}
.jss12150 {
  color: rgba(0, 0, 0, 0.87);
  font-size: 0.9285714285714286rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: 400;
  line-height: 1.75;
  letter-spacing: 0.00938em;
}
.jss12151 {
  color: rgba(0, 0, 0, 0.87);
  font-size: 0.8125rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: 500;
  line-height: 1.57;
  letter-spacing: 0.00714em;
}
.jss12152 {
  color: rgba(0, 0, 0, 0.87);
  font-size: 0.6964285714285714rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: 400;
  line-height: 2.66;
  letter-spacing: 0.08333em;
  text-transform: uppercase;
}
.jss12153 {
  width: 1px;
  height: 1px;
  position: absolute;
  overflow: hidden;
}
.jss12154 {
  text-align: left;
}
.jss12155 {
  text-align: center;
}
.jss12156 {
  text-align: right;
}
.jss12157 {
  text-align: justify;
}
.jss12158 {
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
.jss12159 {
  margin-bottom: 0.35em;
}
.jss12160 {
  margin-bottom: 16px;
}
.jss12161 {
  color: inherit;
}
.jss12162 {
  color: #03a9f4;
}
.jss12163 {
  color: #ff9800;
}
.jss12164 {
  color: rgba(0, 0, 0, 0.87);
}
.jss12165 {
  color: rgba(0, 0, 0, 0.54);
}
.jss12166 {
  color: #f44336;
}

.jss14746 {
  flex: 1 1 auto;
  padding: 0 16px;
  min-width: 0;
}
.jss14746:first-child {
  padding-left: 0;
}
.jss14747:first-child {
  padding-left: 56px;
}
.jss14748 {
  font-size: 0.7544642857142857rem;
}
.jss14749.jss14751 {
  font-size: inherit;
}
.jss14750.jss14751 {
  font-size: inherit;
}

.jss13002 {
  background-color: #fff;
}
.jss13003 {
  border-radius: 4px;
}
.jss13004 {
  box-shadow: none;
}
.jss13005 {
  box-shadow: 0px 1px 3px 0px rgba(0, 0, 0, 0.2),0px 1px 1px 0px rgba(0, 0, 0, 0.14),0px 2px 1px -1px rgba(0, 0, 0, 0.12);
}
.jss13006 {
  box-shadow: 0px 1px 5px 0px rgba(0, 0, 0, 0.2),0px 2px 2px 0px rgba(0, 0, 0, 0.14),0px 3px 1px -2px rgba(0, 0, 0, 0.12);
}
.jss13007 {
  box-shadow: 0px 1px 8px 0px rgba(0, 0, 0, 0.2),0px 3px 4px 0px rgba(0, 0, 0, 0.14),0px 3px 3px -2px rgba(0, 0, 0, 0.12);
}
.jss13008 {
  box-shadow: 0px 2px 4px -1px rgba(0, 0, 0, 0.2),0px 4px 5px 0px rgba(0, 0, 0, 0.14),0px 1px 10px 0px rgba(0, 0, 0, 0.12);
}
.jss13009 {
  box-shadow: 0px 3px 5px -1px rgba(0, 0, 0, 0.2),0px 5px 8px 0px rgba(0, 0, 0, 0.14),0px 1px 14px 0px rgba(0, 0, 0, 0.12);
}
.jss13010 {
  box-shadow: 0px 3px 5px -1px rgba(0, 0, 0, 0.2),0px 6px 10px 0px rgba(0, 0, 0, 0.14),0px 1px 18px 0px rgba(0, 0, 0, 0.12);
}
.jss13011 {
  box-shadow: 0px 4px 5px -2px rgba(0, 0, 0, 0.2),0px 7px 10px 1px rgba(0, 0, 0, 0.14),0px 2px 16px 1px rgba(0, 0, 0, 0.12);
}
.jss13012 {
  box-shadow: 0px 5px 5px -3px rgba(0, 0, 0, 0.2),0px 8px 10px 1px rgba(0, 0, 0, 0.14),0px 3px 14px 2px rgba(0, 0, 0, 0.12);
}
.jss13013 {
  box-shadow: 0px 5px 6px -3px rgba(0, 0, 0, 0.2),0px 9px 12px 1px rgba(0, 0, 0, 0.14),0px 3px 16px 2px rgba(0, 0, 0, 0.12);
}
.jss13014 {
  box-shadow: 0px 6px 6px -3px rgba(0, 0, 0, 0.2),0px 10px 14px 1px rgba(0, 0, 0, 0.14),0px 4px 18px 3px rgba(0, 0, 0, 0.12);
}
.jss13015 {
  box-shadow: 0px 6px 7px -4px rgba(0, 0, 0, 0.2),0px 11px 15px 1px rgba(0, 0, 0, 0.14),0px 4px 20px 3px rgba(0, 0, 0, 0.12);
}
.jss13016 {
  box-shadow: 0px 7px 8px -4px rgba(0, 0, 0, 0.2),0px 12px 17px 2px rgba(0, 0, 0, 0.14),0px 5px 22px 4px rgba(0, 0, 0, 0.12);
}
.jss13017 {
  box-shadow: 0px 7px 8px -4px rgba(0, 0, 0, 0.2),0px 13px 19px 2px rgba(0, 0, 0, 0.14),0px 5px 24px 4px rgba(0, 0, 0, 0.12);
}
.jss13018 {
  box-shadow: 0px 7px 9px -4px rgba(0, 0, 0, 0.2),0px 14px 21px 2px rgba(0, 0, 0, 0.14),0px 5px 26px 4px rgba(0, 0, 0, 0.12);
}
.jss13019 {
  box-shadow: 0px 8px 9px -5px rgba(0, 0, 0, 0.2),0px 15px 22px 2px rgba(0, 0, 0, 0.14),0px 6px 28px 5px rgba(0, 0, 0, 0.12);
}
.jss13020 {
  box-shadow: 0px 8px 10px -5px rgba(0, 0, 0, 0.2),0px 16px 24px 2px rgba(0, 0, 0, 0.14),0px 6px 30px 5px rgba(0, 0, 0, 0.12);
}
.jss13021 {
  box-shadow: 0px 8px 11px -5px rgba(0, 0, 0, 0.2),0px 17px 26px 2px rgba(0, 0, 0, 0.14),0px 6px 32px 5px rgba(0, 0, 0, 0.12);
}
.jss13022 {
  box-shadow: 0px 9px 11px -5px rgba(0, 0, 0, 0.2),0px 18px 28px 2px rgba(0, 0, 0, 0.14),0px 7px 34px 6px rgba(0, 0, 0, 0.12);
}
.jss13023 {
  box-shadow: 0px 9px 12px -6px rgba(0, 0, 0, 0.2),0px 19px 29px 2px rgba(0, 0, 0, 0.14),0px 7px 36px 6px rgba(0, 0, 0, 0.12);
}
.jss13024 {
  box-shadow: 0px 10px 13px -6px rgba(0, 0, 0, 0.2),0px 20px 31px 3px rgba(0, 0, 0, 0.14),0px 8px 38px 7px rgba(0, 0, 0, 0.12);
}
.jss13025 {
  box-shadow: 0px 10px 13px -6px rgba(0, 0, 0, 0.2),0px 21px 33px 3px rgba(0, 0, 0, 0.14),0px 8px 40px 7px rgba(0, 0, 0, 0.12);
}
.jss13026 {
  box-shadow: 0px 10px 14px -6px rgba(0, 0, 0, 0.2),0px 22px 35px 3px rgba(0, 0, 0, 0.14),0px 8px 42px 7px rgba(0, 0, 0, 0.12);
}
.jss13027 {
  box-shadow: 0px 11px 14px -7px rgba(0, 0, 0, 0.2),0px 23px 36px 3px rgba(0, 0, 0, 0.14),0px 9px 44px 8px rgba(0, 0, 0, 0.12);
}
.jss13028 {
  box-shadow: 0px 11px 15px -7px rgba(0, 0, 0, 0.2),0px 24px 38px 3px rgba(0, 0, 0, 0.14),0px 9px 46px 8px rgba(0, 0, 0, 0.12);
}

.jss12176 {
  fill: currentColor;
  width: 1em;
  height: 1em;
  display: inline-block;
  font-size: 24px;
  transition: fill 200ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  user-select: none;
  flex-shrink: 0;
}
.jss12177 {
  color: #03a9f4;
}
.jss12178 {
  color: #ff9800;
}
.jss12179 {
  color: rgba(0, 0, 0, 0.54);
}
.jss12180 {
  color: #f44336;
}
.jss12181 {
  color: rgba(0, 0, 0, 0.26);
}
.jss12182 {
  font-size: inherit;
}
.jss12183 {
  font-size: 20px;
}
.jss12184 {
  font-size: 35px;
}

@media (min-width:960px) {
  ::-webkit-scrollbar {
    width: 5px;
    height: 5px;
  }
  ::-webkit-scrollbar-track {
    background: rgba(250, 250, 250, 0);
  }
  ::-webkit-scrollbar-thumb {
    background: #eeeeee;
  }
  ::-webkit-scrollbar-thumb:hover {
    background: #e0e0e0;
  }
}
.jss19351 {
  width: 100%;
  z-index: 1;
  overflow: hidden;
  margin-bottom: 0;
}
.jss19352 {
  background-color: #424242;
}
.jss19353 {
  height: 70px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/etg.9362c8fb.svg&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss19354 {
  height: 150px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/tessa.55f0b81f.svg&quot;);
  background-repeat: no-repeat;
}
.jss19355 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2hJREFUeNrsWtGR2jAQ1TEU4FQQUsGRCuBm+IerAKgguAKgAnMVHFcBvn/PnFwBvg6cCuIOEslacRvFyJJswL74zQgDNrKedvftSoaQDh06dHDAnfGVfjRjr0PWRg3kkbD2RIJJakfMjzbsdd0CQz0yciF/0zf8wRyOKbSmgXuSx1rAmhWxgbSdnJFG4cOj5DhJz7KLrC3i0a8wSwM8QzcRjGCS1U+MkLcbE9uztjQn5kcyEFMsn5q4uxW09+8hQh5rR/buCNY4OtxsBzMpEaLZ/cIm6g6uyZTrtih+d6C8/PxDLuEiT1mhp7jWEH32HIi95sr5kRKe4PiTtRWbuN/5UfS9hXOUEd6gSXgBa/gwpoMyLiMh6ym5gJxmSsyuCxaK71OUAyka0AC+i8H9YzjnofEsNfd5NyWWnCwVTGgF35/CoPbK9ykMXt5nDEQpWIaC1aZwnlvqnrXvLq7YV2Z2CDesijUMCLv0FNxQYgQWkWIl3fOAfrcq+N46xuJTh8I1XOCBi4nyRqjqEMUZVSyWIQvNlZAI4fr4TIyNTIlh93O12gHJ8AxUluMHkI2V+xwhJmVsviniEAOBtbsr8izuRykM7L7G4lRacl2SizxFiRd1llQUOjSx2BYN9lpVvyzjMkgtxsRiIDbIa0Fd5SFyz6apRXCvwGJV46yBxISFUhPVaeOyJQE/HpcsW8aO2wV8sZqgfhZI6m2w1IVKEbE4l2oRZ55mzTN2dNeZUknMHftZ6GK8iJgaZ6FmZ8il9KIFE+mC0M4VuZv4Ec7u4RlVDMs6N4JQ14urojqrrVXGc8TiU+XA4+yTqCJRgnt8RhU9CGAb4mLljAVJbArZlk9pHgYOmznUoIpeEffd4R16/+zo8p7Sj4EriplIkDxfEtklfqfbfkugOh+cVTM/spd7dXUeTB4h2duRwkneklhc6vvVthDq78dAFYnLPkMb5J6AqbO2Eivb4k40cr8i4rGNS/G6R/08O66WH3QuXPa0JS6RW9dVsO6z7baDk8WoJle51om7f5Yxbill706Mm/qjIC6KweoCU1c/lq7YWnU0IUY/K7H3GgTj6ih/oiKq+F+oqk5IyZOOK+MriI98WPnNjJggF5C/Hyg0Faf/edj8M4cn6mlZ/rgRuBe9XqLm7NChw3+GPwIMAA139Xg+K8vEAAAAAElFTkSuQmCC&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss19356 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABANJREFUeNrsWt1x4jAQNpl7P18FcSqIqSBmhvdABYEKiCvwUQFcBfZVEHhnBl8F8VUQSvB1cJJ3jddC/pOlQBg0YwYUIfbT/n27imXdxtcag947+DuPvXqa5TpYq3F0PmD+bsZeQ0OHvmbg/HMB+2CvDntS9iSaADn48PGDgUtVNvmmQQg+fjEBfmqBBaa9x08ue2KVbe6uNXhcOrDDdQJbjQ/XqjHrBuxLAYMIeVZg9xrhuJegsTx5ztjphuyxe2qI58LVJURFSnk4vfpgwr0qmRywmECgVMrAdJBgB095QmaTDPRqHBv7rnFgZUcPCc3igzP0pfTkwewW7LGJWfO168soW9oJXHDJ6gPwVQnv5wCrNrEDgnSFuXlfszPB7utYfw7EFioBOmLy9wuroE8Lz6ACRBPZXfatmvUD83cu+o0r5DiukT+SIpSve0RTtU2Y5kCTlkLl05dreX7unkeIiTkfS+VKGhJ7QDQYsb3mnw+srCludiMmSNKwPq5lE2DSewLOV81rA0VQtC/RFlSIax9q89UpuJGKz6lyRepT9aBg5BzQFnKbrGpOsj0Lgq3U3rtTNEGH+FTSImLSwPDSoiWQZHvn+Q9+07jGgmP0axcoRCAeMpMmcGtStgRmgfm7iUV7ie3GRKjdrEZzpFG20Nqki6hdKdWTQFxFfuhIErFDhHzBuQVbn0i0JAaJDfGxJ/xsBJh35Hh5ZANA7y0430YAu5dYBKxbjacINGVzG9SwZ9LHXFIM0tEEKs9fG8EkZUPc669KL0Q13P8j5gP8riwwfz/FsM3TwYisfTjOFz5Eq+epZM5guIewLf8x4HUjMm8ffUP0G25eMOcJ0S7CQ0grGkaiDNo0lta2yIrEGhFwe6kwwDEDsu8844XNFXSqH1ibjhFoYy6YmFNRiFLmErUIWJ16+V2jYl4RPzas+14KHNBvnGWmCma47RDl7rtqSyV4xCenWJ+UEzTbd+yBcNN8s8qXeU0Uy1MJIl2BbY/+U8UEytzQxnzlCKD3UlOr32trEhjN/ItGnyiESjElrCW5yq3hjouK39YMDKJWRMis7LSfJeY7zAIE/BfAVOIvXoW2ZqSaPpjUWM75ilrp9CLCJVrys+RMhVqNN5ikNw3MJZSQYYPAQMglMTWxEMzD/bCyrIe0MM3WwNpIkudcUvMduorZp+fxRqJfr8ZLTYOIp4ehyjZ9OsFz1Bj4AgSAqXL/HUw6FFLFSFU49fsxAED5IQ8Aqvdjs+y7IqgelxS6OsE8+b4KDCXKck9Vhwki6jOCoeF+jX7V6+ZF9/1YUJFwExJJ7YraKkZAsQ5xTNyPeUiTJla7mxSu2d+6r5LM3I+VQboSgCkhxLdxG2z8F2AAPtd0UKyJkPIAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss19357 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAzRJREFUeNrsWs2Nm0AUnl1RAFIKCK4gdLBwy9GuYL0VePeWm+1bbt5UYLsC7FtueCswqWDdQCTSQd5bfRM9IcADDD+OeNIIbI8f8837f4NSI4000kg16K7uHz99++nTZUkj/f3965PNRRHvFV2+0FgT76QOj/sGz2dQUxpzWkhgEVQgeC/r8mkCzO1Ao9w+gEnyb8bGSCU8qEPRri3Ebycab5bW9JnVG/cXGvuCeSmNA9ngxRgYgeIFv3ekbk2IwU0IXGqqiv4NgNI2mGsGjsGfi9ThkYZXoIoPDRbrCb4siaTOxhsBI1GvctT1QSzgLW9OA3cf42NCfMOcOfx70JZXlDt5atGGapHT4KFrGn+QeVgDxrxIIi9QtV3nwOCJVm2IiXi/NuXhVNB9T8SXV+1iYROB6pY8mxKbi9yNPeKEQHEAj4YYB6oAk+qxg8ocCFzYg8Qer0nNaWpTcBynLlFlQk2rSfDg6L7DXQ5s1m2DAIZqm7OFGPf/jcS2Bfe3Cwz9C1+kRz6+u11gCOo69v1AGvZRpOK32wMGRxGJLH2FWJggD4zadCaOZelMUYsFol5iIKGOhQjoMdSTnckFc44cD4tK/V4kRouL0ErYZPokLKFQlu64D0UmozeEnco78doMSWJuJvX6hUZLWpLFvBCINUBxc/TZNMHt0saehNcLykDlADyIXDMFr2EAg13MRD+iSgd3I8LBxGRDOvWKSIY1uGcTd44sRNd4M1ugrLt7LmNEOT83LD8UvKHVCqGNOHbE1aQFp/PG/WDjmKAq6qSBGcUuqHfQF7CiLOQfcHHmlapqHWeev1AGhyBtANMPdQnQVuUcbND3KXLH1HBjPjIa2hCOfRwOzn3YmCsAzjOpVSLmLMUmXPOg7Fi4UN1C2qe+VZE95BHeMiuBhbAX91og1zkmXc992ZjOAXdFCS2AHhDHpqqk4yuObmfIMWMTG7srYRaLGBP22ZgRUtLrkGd3YV4MdNRACa49AhgtqRj3yTW3bwLM66KUL/GwZwTwNUqb2JaNearBawkWKPt8oy5XkbtPVIOzqQ6p6MSz9K0BX5T6QyQGtK/75s5II400Ui79FWAANjYkpEJzM+AAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss19358 {
  color: #FFF;
  padding: 48px;
}
.jss19359 {
  color: rgba(0, 0, 0, 0.87);
  width: 100%;
  display: flex;
  z-index: 2;
  opacity: 0.5;
  position: fixed;
  align-items: center;
  padding-left: 16px;
  padding-right: 16px;
  background-color: #424242;
}
.jss19360 {
  height: 100vh;
  display: flex;
  position: relative;
  max-height: 1600px;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: linear-gradient(rgba(0,130,170,0), #424242), url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/1.20e79364.jpg&quot;);
}
.jss19361 {
  margin: -200px auto 0 auto;
  padding: 64px 0 48px;
  max-width: 1200px;
}
.jss19362 {
  width: 50%;
  color: #FFF;
}
@media (max-width:1279.95px) {
  .jss19362 {
    width: 100%;
    padding: 24px;
  }
}
.jss19363 {
  margin: -150px auto 0 auto;
  padding: 16px;
  position: relative;
  max-width: 1200px;
  background: #fafafa;
  justify-content: center;
}
.jss19364 {
  margin: 0 auto;
  padding: 24px;
  display: flex;
}
.jss19365 {
  margin-bottom: 24px;
}
.jss19366 {
  text-decoration: none;
}
.jss19367 {
  margin: 24px auto;
  max-width: 1200px;
}
@media (max-width:1279.95px) {
  .jss19367 {
    max-width: 1366px;
  }
}
.jss19368 {
  height: 160px;
  display: flex;
  align-items: center;
}
.jss19369 {
  margin: auto;
  display: grid;
}
.jss19370 {
  height: 100vh;
  display: flex;
  position: relative;
  overflow: hidden;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/2.adfa400c.jpg&quot;);
  background-position: center;
}
.jss19371 {
  color: #FFF;
  background-color: #03a9f4;
}
.jss19372 {
  color: #03a9f4;
  background-color: #fff;
}
.jss19373 {
  color: #03a9f4;
  padding: 0;
}
.jss19374 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss19374 {
    width: calc(100%);
  }
}
.jss19375 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss19375 {
    width: calc(100% - 300px);
  }
}
.jss19376 {
  display: none;
}
@media (min-width:600px) {
  .jss19376 {
    display: block;
  }
}
.jss19377 {
  color: #fff;
  position: relative;
  background: #03a9f4;
}
.jss19378 {
  width: 100%;
  position: relative;
  border-radius: 14px;
  background-color: rgba(255, 255, 255, 0.5);
}
.jss19378:hover {
  background-color: rgba(255, 255, 255, 0.7);
}
@media (min-width:600px) {
  .jss19378 {
    width: auto;
  }
}
.jss19379 {
  width: 72px;
  color: #212121;
  height: 100%;
  display: flex;
  position: absolute;
  align-items: center;
  pointer-events: none;
  justify-content: center;
}
.jss19380 {
  color: #fff;
  width: 100%;
}
.jss19381 {
  color: #212121;
  width: 100%;
  transition: width 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  padding-top: 8px;
  padding-left: 64px;
  padding-right: 8px;
  padding-bottom: 8px;
}
@media (min-width:600px) {
  .jss19381 {
    width: 80px;
  }
  .jss19381:focus {
    width: 200px;
  }
}
.jss19382 {
  color: #212121;
}
.jss19383 {
  background-color: #4fc3f7;
}
.jss19384 {
  margin-left: 12px;
  margin-right: 36px;
}
.jss19385 {
  display: none;
}
@media (min-width:960px) {
  .jss19386 {
    display: none;
  }
}
.jss19387 {
  width: 300px;
}
.jss19388 {
  display: flex;
  padding: 0 8px;
  min-height: 56px;
  align-items: center;
}
@media (min-width:0px) and (orientation: landscape) {
  .jss19388 {
    min-height: 48px;
  }
}
@media (min-width:600px) {
  .jss19388 {
    min-height: 64px;
  }
}
.jss19389 {
  margin-top: 65px;
  justify-content: flex-start;
}
.jss19390 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss19392 {
  width: 100%;
  height: 100%;
  opacity: 0.2;
  content: &quot;&quot;;
  display: block;
  position: absolute;
  background: linear-gradient(rgba(0,130,170,0), #03a9f4));
  background-size: cover;
  background-position: center center;
}
.jss19393 {
  width: 60px;
  overflow-x: hidden;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss19394 {
  color: inherit;
}
.jss19395 {
  margin-left: 56px;
}
.jss19396 {
  bottom: 16px;
}
.jss19397 {
  color: rgba(0, 0, 0, 0.87);
  background-size: cover;
  background-color: #fff;
  background-image: url(https://xdev.equine.co.id/apps/tessa/static/media/pattern2.f069796d.svg);
  background-repeat: no-repeat;
  background-position: inherit;
}
.jss19398 {
  color: #fff;
  background-color: #03a9f4;
}
.jss19399 {
  color: #fff;
  background-color: #ff9800;
}
.jss19400 {
  color: #fff;
  background-color: #f44336;
}
.jss19401 {
  color: #fff;
  background: #4caf50;
}
.jss19402 {
  flex: 1;
}
.jss19403 {
  padding: 16px;
  flex-grow: 1;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss19403 {
    margin-top: 68px;
  }
}
@media (max-width:959.95px) {
  .jss19403 {
    padding: 0;
    margin-top: 56px;
  }
}
@media (max-width:959.95px) {
  .jss19404 {
    margin-top: 68px;
  }
}
@media (min-width:960px) {
  .jss19404 {
    left: 50%;
    width: 982px;
    position: absolute;
    transform: translateX(-50%);
  }
}
@media (min-width:960px) {
  .jss19405 {
    margin-left: 300px;
    margin-right: 0;
  }
}
.jss19406 {
  margin-bottom: 56px;
}
.jss19407 {
  width: calc(100% - 0px);
  right: 0;
  bottom: 0;
  display: flex;
  position: fixed;
}
@media (min-width:960px) {
  .jss19407 {
    width: calc(100% - 300px);
  }
}
.jss19409 {
  background: #fff;
}
.jss19410 {
  margin-bottom: 60px;
}
.jss19411 {
  height: 4px;
}
.jss19412 {
  display: flex;
  align-items: center;
  justify-content: space-between;
}
.jss19413 {
  margin: 0 15px 0 0;
  padding: 16px 0;
  flex-grow: 1;
}
@media (min-width:600px) {
  .jss19413 {
    width: 30%;
    flex-grow: 0;
  }
}
.jss19414 {
  color: inherit;
  font-size: 0.6964285714285714rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: inherit;
  line-height: 1.66;
  letter-spacing: 0.03333em;
}
.jss19415 {
  color: #fff;
  background: #03a9f4;
}
.jss19416 {
  color: #fff;
  min-height: 40px;
  background: #ff9800;
  margin-bottom: 16px;
}
.jss19417 {
  height: 500px;
  display: flex;
  overflow: hidden;
  align-items: center;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss19417 {
    height: 300px;
  }
}
.jss19418 {
  height: 100%;
  margin: 0 auto;
}
.jss19419 {
  width: 25px;
  height: 25px;
  margin-right: 16px;
}
.jss19420 {
  transform: rotate(0deg);
  transition: transform 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
}
.jss19421 {
  transform: rotate(180deg);
}
.jss19422 {
  min-width: 350px;
}
.jss19423 {
  display: flex;
  flex-wrap: wrap;
}
.jss19424 {
  padding: 0;
}
@media (min-width:0px) {
  .jss19424 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss19424 {
    width: calc(100% / 2);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1280px) {
  .jss19424 {
    width: calc(100% / 3);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1920px) {
  .jss19424 {
    width: calc(100% / 4);
    padding: 0 16px 16px 0;
  }
}
.jss19425 {
  padding: 0;
}
@media (min-width:0px) {
  .jss19425 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss19425 {
    width: calc(100% / 2);
  }
}
@media (min-width:1280px) {
  .jss19425 {
    width: calc(100% - (100% / 3));
  }
}
@media (min-width:1920px) {
  .jss19425 {
    width: calc(100% - (100% / 4));
  }
}
.jss19426 {
  padding: 0;
}
@media (min-width:0px) {
  .jss19426 {
    width: 100%;
  }
}
.jss19427 {
  width: 100%;
  margin-bottom: 16px;
  vertical-align: middle;
}
.jss19428 {
  right: 16px;
  display: flex;
  position: absolute;
}
.jss19429 {
  right: 16px;
  bottom: 16px;
  position: fixed;
}
.jss19430 {
  float: right;
}
.jss19431 {
  margin: 0;
}
.jss19432 {
  margin: 8px;
}
.jss19433 {
  margin-left: 8px;
}
.jss19434 {
  margin-top: 8px;
}
.jss19435 {
  margin-right: 8px;
}
.jss19436 {
  margin-bottom: 8px;
}
.jss19437 {
  margin: 16px;
}
.jss19438 {
  margin-left: 16px;
}
.jss19439 {
  margin-top: 16px;
}
.jss19440 {
  margin-right: 16px;
}
.jss19441 {
  margin-bottom: 16px;
}
.jss19442 {
  margin: 24px;
}
.jss19443 {
  margin-left: 24px;
}
.jss19444 {
  margin-top: 24px;
}
.jss19445 {
  margin-right: 24px;
}
.jss19446 {
  margin-bottom: 24px;
}
.jss19447 {
  padding: 0;
}
.jss19448 {
  padding: 8px;
}
.jss19449 {
  padding-left: 8px;
}
.jss19450 {
  padding-top: 8px;
}
.jss19451 {
  padding-right: 8px;
}
.jss19452 {
  padding-bottom: 8px;
}
.jss19453 {
  padding: 16px;
}
.jss19454 {
  padding-left: 16px;
}
.jss19455 {
  padding-top: 16px;
}
.jss19456 {
  padding-right: 16px;
}
.jss19457 {
  padding-bottom: 16px;
}
.jss19458 {
  padding: 24px;
}
.jss19459 {
  padding-left: 24px;
}
.jss19460 {
  padding-top: 24px;
}
.jss19461 {
  padding-right: 24px;
}
.jss19462 {
  padding-bottom: 24px;
}
.jss19463 {
  background-color: #03a9f4;
}
.jss19464 {
  background-color: #ff9800;
}
.jss19465 {
  color: white;
  background-color: #f44336;
}
.jss19466 {
  color: #03a9f4;
}
.jss19467 {
  color: #ff9800;
}
.jss19468 {
  color: #f44336;
}
.jss19469 {
  text-decoration: line-through;
}
.jss19470 {
  padding: 3px;
}
.jss19471 {
  height: calc(100vh - 96px - 72px);
  overflow-x: auto;
}
.jss19472 {
  width: calc(100vw - 32px - 300px);
}
.jss19473 {
  height: calc(100vh - 56px - 68px);
  overflow-x: auto;
}
.jss19474 {
  width: calc(100vw);
}
.jss19475 {
  overflow-x: auto;
}
.jss19476 {
  table-layout: initial;
}
.jss19477 {
  top: 0;
  position: sticky;
  background-color: #fff;
}
.jss19478 {
  width: 3vw;
}
.jss19479 {
  width: 5vw;
}
.jss19480 {
  width: 10vw;
}
.jss19481 {
  width: 15vw;
}
.jss19482 {
  min-width: 15vw;
  max-width: 15vw;
}
@media (min-width:960px) {
  .jss19482 {
    min-width: calc((100vw - 300px - 48px) * 0.15);
    max-width: calc((100vw - 300px - 48px) * 0.15);
  }
}
.jss19483 {
  min-width: 25vw;
  max-width: 25vw;
}
@media (min-width:960px) {
  .jss19483 {
    min-width: calc((100vw - 300px - 48px) * 0.25);
    max-width: calc((100vw - 300px - 48px) * 0.25);
  }
}
.jss19484 {
  width: 25vw;
}
.jss19485 {
  width: 30vw;
}
.jss19486 {
  min-width: 45vw;
  max-width: 45vw;
}
@media (min-width:960px) {
  .jss19486 {
    min-width: calc((100vw - 300px - 48px) * 0.45);
    max-width: calc((100vw - 300px - 48px) * 0.45);
  }
}
.jss19487 {
  width: 100%;
  overflow-x: auto;
}
.jss19488 {
  width: 100%;
  margin-top: 24px;
  overflow-x: auto;
}
.jss19489 {
  min-width: 700px;
}
.jss19490 {
  width: 100%;
}
.jss19491 {
  min-width: 100px;
}
.jss19492 {
  color: rgba(0, 0, 0, 0.54);
  flex-shrink: 0;
}
.jss19493 {
  color: #2196f3;
}
.jss19494 {
  padding-top: 16px;
  padding-bottom: 16px;
}
.jss19496 {
  max-height: 100px;
}
.jss19497 {
  margin-top: -144px;
  margin-left: -56px;
  margin-right: -136px;
  margin-bottom: -64px;
}
.jss19498 {
  margin-top: -80px;
}
.jss19499 {
  width: 500px;
  height: 450px;
}
.jss19500 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss19501 {
  display: flex;
  box-sizing: border-box;
  align-items: center;
}
.jss19503:hover {
  background-color: #eeeeee;
}
.jss19504 {
  flex: 1;
}
.jss19505 {
  cursor: initial;
}
.jss19506 {
  background: #f44336;
}
.jss19507 {
  color: #fff;
}
.jss19508 {
  width: 100%;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
.jss19509 {
  max-height: 75vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss19510 {
  padding: 10px;
  text-align: center;
}
.jss19511 {
  padding: 0;
  vertical-align: top;
}
.jss19512 {
  margin-top: 8px;
  padding-left: 32px;
}
.jss19513 {
  top: 0;
  padding: 5px;
  position: sticky;
  overflow: hidden;
  max-width: 100px;
  max-height: 10px;
  text-align: center;
  background-color: #fff;
}
.jss19514 {
  max-height: 76vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss19515 {
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss19516 {
  padding: 0;
  margin-top: 8px;
}
.jss19517 {
  left: 0;
  width: 500px;
  z-index: 1;
  padding: 8px 16px 0 16px;
  min-width: 300px;
  background: #fff;
  vertical-align: top;
}
.jss19518 {
  padding: 8px 16px 0 16px;
  min-width: 300px;
  vertical-align: top;
}
.jss19519 {
  min-width: 300px;
}
.jss19520 {
  transform: rotate(180deg);
  writing-mode: vertical-rl;
}
.jss19521 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss19522 {
  cursor: pointer;
  transition: all .3s;
}
.jss19522:hover {
  background-color: #ebebeb;
}
.jss19523 {
  background-color: #ebebeb;
}
.jss19524 {
  transition: all .3s;
}
.jss19524:hover {
  transform: scale(1.03);
}
.jss19525 {
  padding: 0;
  transition: all .3s;
}
.jss19525:hover {
  transform: scale(1.03);
}
.jss19526 {
  display: grid;
  align-items: center;
  grid-column-gap: 1.5rem;
  grid-template-columns: 1fr max-content 1fr;
}
.jss19526::after, .jss19526::before {
  height: 1px;
  content: '';
  display: block;
  background-color: currentColor;
}
.jss19527 a {
  text-decoration: none;
}
.jss19528 {
  left: 50%;
  position: absolute;
  transform: ;
}
.jss19529 {
  display: -webkit-box;
  overflow: hidden;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;
}
.jss19530 {
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
.jss19531 {
  display: inline;
  text-transform: capitalize;
}
.jss19532 {
  text-decoration: none;
}
.jss19532 p:hover {
  color: #03a9f4;
}
.jss19533 {
  margin-left: 4px;
}
.jss19534 {
  padding: 0 16px;
  display: inline-flex;
  position: relative;
  vertical-align: middle;
}
.jss19535 {
  top: 0;
  color: #fff;
  right: 0;
  height: 20px;
  display: flex;
  padding: 0 4px;
  z-index: 1;
  position: absolute;
  flex-wrap: wrap;
  min-width: 20px;
  transform: translate(20px, -50%);
  box-sizing: border-box;
  transition: transform 225ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  align-items: center;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  align-content: center;
  border-radius: 10px;
  flex-direction: row;
  justify-content: center;
  background-color: #ff9800;
  transform-origin: 100% 0%;
}
.jss19536 {
  margin: 0;
  padding: 16px;
  border-bottom: 1px solid rgba(0, 0, 0, 0.12);
}
.jss19537 {
  top: 8px;
  right: 8px;
  color: #9e9e9e;
  position: absolute;
}
.jss19538 {
  margin: 0;
  padding: 16px;
}
.jss19539 {
  margin: 0;
  padding: 8px;
  border-top: 1px solid rgba(0, 0, 0, 0.12);
}
.jss19540 {
  font-size: 13px;
}
.jss19541 {
  padding: 0;
  overflow: hidden;
}
.jss19541 span {
  display: inline-flex;
}
.jss19541 span:hover {
  position: relative;
  animation: moveTextHorz 3s linear infinite;
}
@-webkit-keyframes moveTextHorz {
  0% {
    transform: translate(0, 0);
  }
  100% {
    transform: translate(-70%, 0);
  }
}
.jss19542 span {
  color: #f44336;
}
.jss19543 {
  width: 50px;
}
@media (min-width:600px) {
  .jss19543 {
    right: 90px;
  }
}
@media (min-width:960px) {
  .jss19543 {
    right: 15px;
  }
}
.jss19544 {
  cursor: pointer;
}
.jss19544:hover {
  background: #0288d1;
}
.jss19545 {
  margin-left: 10px;
  vertical-align: middle;
}
.jss19546 {
  width: 100%;
}
.jss19547 {
  width: 100%;
  margin: 0;
}
@media (min-width:960px) {
  .jss19548 {
    margin-left: auto;
    margin-right: 16px;
  }
}
@media (min-width:1280px) {
  .jss19548 {
    margin-left: auto;
    margin-right: 32px;
  }
}
@media (min-width:960px) {
  .jss19549 {
    transform: translateY(75%);
    margin-left: 16px;
  }
}
@media (min-width:1280px) {
  .jss19549 {
    transform: translateY(75%);
    margin-left: 32px;
  }
}
.jss19550 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
@media (max-width:1279.95px) {
  .jss19551 {
    display: none;
  }
}
@media (max-width:1919.95px) {
  .jss19552 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss19553 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss19554 {
    text-align: right;
  }
}
.jss19555 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss19555 {
    text-align: right;
  }
}
.jss19556 {
  padding: 0;
}
@media (min-width:1280px) {
  .jss19557 {
    margin-top: 0 !important;
  }
}
@media (min-width:1280px) {
  .jss19558 {
    margin-top: 64px;
  }
}
.jss19559 div textarea {
  overflow: auto;
}
.jss19560 {
  width: 70%;
}
@media (min-width:1280px) {
  .jss19560 {
    float: right;
  }
}
@media (min-width:1280px) {
  .jss19561 {
    float: right;
  }
}
@media (max-width:1279.95px) {
  .jss19561 {
    margin-top: 6px;
  }
}
@media (min-width:1280px) {
  .jss19562 {
    padding: 12px 0;
  }
}
@media (max-width:1279.95px) {
  .jss19562 {
    padding: 6px 0 3px;
  }
}
.jss19563 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss19563 {
    margin-top: 0 !important;
  }
}
.jss19563 div textarea {
  overflow: auto;
}
.jss19564 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss19565 {
  width: 57px;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
  overflow-x: hidden;
}
@media (min-width:600px) {
  .jss19565 {
    width: 73px;
  }
}
@media (min-width:960px) {
  .jss19566 {
    width: calc(100% - 73px);
    margin-left: 73px;
    margin-right: 0;
  }
}
@media (min-width:960px) {
  .jss19567 {
    margin-left: 73px;
    margin-right: 0;
  }
}
.jss19568 {
  bottom: 0px;
  z-index: 1;
  position: sticky;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss19568 {
    display: none;
  }
}
.jss19568:hover {
  background-color: rgb(235, 235, 235);
}
@media (max-width:959.95px) {
  .jss19569 {
    display: none;
  }
}
.jss19570 {
  top: 0;
  left: auto;
  width: 100%;
  right: 0;
  height: 100%;
  z-index: 1301;
  overflow: hidden;
  position: absolute;
  overflow-y: auto;
  transition: transform 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
@media (min-width:960px) {
  .jss19570 {
    width: 24vw;
  }
}
.jss19571 {
  cursor: pointer;
}
.jss19571:hover {
  background-color: #eeeeee;
}
.jss19572 {
  background-color: #e0e0e0 !important;
}
.jss19573 {
  transition: width 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
.jss19574 {
  border-collapse: separate;
}
.jss19575:hover {
  background-color: rgb(235, 235, 235);
}
.jss19576 {
  position: relative;
  min-height: 80vh;
}
.jss19576 .back-icon {
  top: 5px;
  left: 5px;
  z-index: 1;
  position: absolute;
}
.jss19576 > .ninebox-inner {
  width: 100%;
  position: relative;
  min-height: 80vh;
}
.jss19576 > .ninebox-inner .category-paper {
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #424242;
}
.jss19576 > .ninebox-inner .percentage-wrapper {
  width: 100%;
  height: 100%;
  display: flex;
  flex-wrap: wrap;
  box-sizing: border-box;
}
.jss19576 > .ninebox-inner .horizontal-category {
  height: 50px;
  margin: 0 60px;
  display: flex;
  align-items: center;
  justify-content: center;
}
.jss19576 > .ninebox-inner .vertical-category {
  width: 50px;
  height: calc(80vh - 120px);
  display: inline-flex;
  align-items: center;
  justify-content: center;
}
.jss19576 > .ninebox-inner .ninebox-content {
  width: calc(100% - 100px);
  height: calc(80vh - 120px);
  display: inline-block;
  position: relative;
}
.jss19576 > .ninebox-inner .ninebox-content .wrapper {
  width: 100%;
  height: 100%;
  margin: 0;
}
.jss19576 > .ninebox-inner .ninebox-content .wrapper .box {
  height: calc(100% / 3);
  padding: 10px;
}
.jss19576 > .ninebox-inner .ninebox-content .wrapper .box .has-item {
  padding: 26px 10px !important;
}
.jss19576 > .ninebox-inner .ninebox-content .wrapper .box .disabled {
  background: #d1d1d1;
}
.jss19576 > .ninebox-inner .ninebox-content .wrapper .box .card {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 30px;
  position: relative;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  -webkit-transition: all .4s ease-out;
}
.jss19576 > .ninebox-inner .ninebox-content .wrapper .box .card h6 {
  color: rgba(0, 0, 0, 0.87);
}
.jss19576 > .ninebox-inner .ninebox-content .wrapper .box .card .total-person {
  top: 5px;
  right: 5px;
  width: 15px;
  height: 15px;
  position: absolute;
}
.jss19576 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list {
  width: 100%;
  height: 100%;
  display: flex;
  overflow-y: auto;
  align-items: center;
}
.jss19576 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list > ol {
  height: 100%;
  margin: 0;
  padding: 0;
  text-align: left;
  list-style: none;
}
.jss19576 > .ninebox-inner .ninebox-content .wrapper .box .active:hover {
  cursor: pointer;
  transform: scale(1.1);
}
.jss19576 > .ninebox-inner .vertical-category .text-rotate {
  transform: rotate(270deg);
}
.jss19576 > .ninebox-inner .vertical-category.left {
  padding: 10px 0;
  margin-right: 9px;
}
.jss19576 > .ninebox-inner .vertical-category.right {
  margin-left: 9px;
}
.jss19576 > .ninebox-inner .vertical-category .text-rotate.nowrap {
  white-space: nowrap;
}
.jss19576 > .ninebox-inner .horizontal-category.top {
  padding: 0 9px;
  margin-bottom: 9px;
}
.jss19576 > .ninebox-inner .horizontal-category.bottom {
  margin-top: 9px;
}
.jss19576 > .ninebox-inner .percentage-wrapper > .percentage-item {
  padding: 0 10px;
  flex-grow: 0;
  max-width: calc(100% / 3);
  flex-basis: calc(100% / 3);
}
.jss19576 > .ninebox-inner .percentage-wrapper > .percentage-item.horizontal {
  height: calc(100% / 3);
  padding: 10px 0;
  max-width: 100%;
}
.jss19576 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 10px;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  background-color: #424242;
  -webkit-transition: all .4s ease-out;
}
.jss19576 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper > p {
  color: #fff;
}
.jss19576 > .ninebox-inner .category-paper > p {
  color: #fff;
}
.jss19577 {
  max-height: 45vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss19577 .table {
  border-collapse: separate;
}
.jss19577 .table .sticky-head {
  top: 0;
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss19578 {
  height: 44vh;
  overflow: auto;
}
.jss19579 {
  width: 100%;
  height: 50vh;
  overflow: auto;
}
.jss19580 {
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss19581 {
  width: 100%;
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss19582 {
  max-width: 82%;
}

html {
  box-sizing: border-box;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}
*, *::before, *::after {
  box-sizing: inherit;
}
body {
  margin: 0;
  background-color: #fafafa;
}
@media print {
  body {
    background-color: #fff;
  }
}

.jss12167 {
  flex: 0 0 auto;
  color: rgba(0, 0, 0, 0.54);
  padding: 12px;
  overflow: visible;
  font-size: 1.3928571428571428rem;
  text-align: center;
  transition: background-color 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  border-radius: 50%;
}
.jss12167:hover {
  background-color: rgba(0, 0, 0, 0.08);
}
.jss12167.jss12171 {
  color: rgba(0, 0, 0, 0.26);
}
@media (hover: none) {
  .jss12167:hover {
    background-color: transparent;
  }
}
.jss12167:hover.jss12171 {
  background-color: transparent;
}
.jss12168 {
  color: inherit;
}
.jss12169 {
  color: #03a9f4;
}
.jss12169:hover {
  background-color: rgba(3, 169, 244, 0.08);
}
@media (hover: none) {
  .jss12169:hover {
    background-color: transparent;
  }
}
.jss12170 {
  color: #ff9800;
}
.jss12170:hover {
  background-color: rgba(255, 152, 0, 0.08);
}
@media (hover: none) {
  .jss12170:hover {
    background-color: transparent;
  }
}
.jss12172 {
  width: 100%;
  display: flex;
  align-items: inherit;
  justify-content: inherit;
}

.jss19819 {
  height: 0.01em;
  display: flex;
  max-height: 2em;
  align-items: center;
}
.jss19820.jss19821 {
  margin-top: 16px;
}
.jss19821 {
  margin-right: 8px;
}
.jss19822 {
  margin-left: 8px;
}

.jss19978 {
  width: 100%;
  position: relative;
}
.jss19979 {
  font: inherit;
  width: 100%;
  height: 100%;
  resize: none;
  cursor: inherit;
  border: none;
  padding: 0;
  outline: none;
  box-sizing: border-box;
  background: transparent;
  line-height: inherit;
}
.jss19980 {
  height: auto;
  overflow: hidden;
  position: absolute;
  visibility: hidden;
  white-space: pre-wrap;
}

.jss18866 {
  color: rgba(0, 0, 0, 0.87);
  cursor: text;
  display: inline-flex;
  font-size: 0.9285714285714286rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  line-height: 1.1875em;
  align-items: center;
}
.jss18866.jss18869 {
  color: rgba(0, 0, 0, 0.38);
  cursor: default;
}
.jss18874 {
  padding: 6px 0 7px;
}
.jss18875 {
  width: 100%;
}
.jss18876 {
  font: inherit;
  color: currentColor;
  width: 100%;
  border: 0;
  margin: 0;
  padding: 6px 0 7px;
  display: block;
  min-width: 0;
  box-sizing: content-box;
  background: none;
  -webkit-tap-highlight-color: transparent;
}
.jss18876::-webkit-input-placeholder {
  color: currentColor;
  opacity: 0.42;
  transition: opacity 200ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
}
.jss18876::-moz-placeholder {
  color: currentColor;
  opacity: 0.42;
  transition: opacity 200ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
}
.jss18876:-ms-input-placeholder {
  color: currentColor;
  opacity: 0.42;
  transition: opacity 200ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
}
.jss18876::-ms-input-placeholder {
  color: currentColor;
  opacity: 0.42;
  transition: opacity 200ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
}
.jss18876:focus {
  outline: 0;
}
.jss18876:invalid {
  box-shadow: none;
}
.jss18876::-webkit-search-decoration {
  -webkit-appearance: none;
}
.jss18876.jss18869 {
  opacity: 1;
}
label[data-shrink=false] + .jss18867 .jss18876::-webkit-input-placeholder {
  opacity: 0;
}
label[data-shrink=false] + .jss18867 .jss18876::-moz-placeholder {
  opacity: 0;
}
label[data-shrink=false] + .jss18867 .jss18876:-ms-input-placeholder {
  opacity: 0;
}
label[data-shrink=false] + .jss18867 .jss18876::-ms-input-placeholder {
  opacity: 0;
}
label[data-shrink=false] + .jss18867 .jss18876:focus::-webkit-input-placeholder {
  opacity: 0.42;
}
label[data-shrink=false] + .jss18867 .jss18876:focus::-moz-placeholder {
  opacity: 0.42;
}
label[data-shrink=false] + .jss18867 .jss18876:focus:-ms-input-placeholder {
  opacity: 0.42;
}
label[data-shrink=false] + .jss18867 .jss18876:focus::-ms-input-placeholder {
  opacity: 0.42;
}
.jss18877 {
  padding-top: 3px;
}
.jss18878 {
  resize: none;
  padding: 0;
}
.jss18879 {
  height: 1.1875em;
}
.jss18880 {
  -moz-appearance: textfield;
  -webkit-appearance: textfield;
}

.jss19965 {
  position: relative;
}
label + .jss19966 {
  margin-top: 16px;
}
.jss19969:after {
  left: 0;
  right: 0;
  bottom: 0;
  content: &quot;&quot;;
  position: absolute;
  transform: scaleX(0);
  transition: transform 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms;
  border-bottom: 2px solid #0288d1;
  pointer-events: none;
}
.jss19969.jss19967:after {
  transform: scaleX(1);
}
.jss19969.jss19970:after {
  transform: scaleX(1);
  border-bottom-color: #f44336;
}
.jss19969:before {
  left: 0;
  right: 0;
  bottom: 0;
  content: &quot;\00a0&quot;;
  position: absolute;
  transition: border-bottom-color 200ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  border-bottom: 1px solid rgba(0, 0, 0, 0.42);
  pointer-events: none;
}
.jss19969:hover:not(.jss19968):not(.jss19967):not(.jss19970):before {
  border-bottom: 2px solid rgba(0, 0, 0, 0.87);
}
.jss19969.jss19968:before {
  border-bottom: 1px dotted rgba(0, 0, 0, 0.42);
}
@media (hover: none) {
  .jss19969:hover:not(.jss19968):not(.jss19967):not(.jss19970):before {
    border-bottom: 1px solid rgba(0, 0, 0, 0.42);
  }
}

.jss19958 {
  color: rgba(0, 0, 0, 0.54);
  padding: 0;
  font-size: 0.9285714285714286rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  line-height: 1;
}
.jss19958.jss19959 {
  color: #0288d1;
}
.jss19958.jss19960 {
  color: rgba(0, 0, 0, 0.38);
}
.jss19958.jss19961 {
  color: #f44336;
}
.jss19964.jss19961 {
  color: #f44336;
}

.jss19947 {
  transform-origin: top left;
}
.jss19952 {
  top: 0;
  left: 0;
  position: absolute;
  transform: translate(0, 24px) scale(1);
}
.jss19953 {
  transform: translate(0, 21px) scale(1);
}
.jss19954 {
  transform: translate(0, 1.5px) scale(0.75);
  transform-origin: top left;
}
.jss19955 {
  transition: color 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms,transform 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms;
}
.jss19956 {
  z-index: 1;
  transform: translate(12px, 22px) scale(1);
  pointer-events: none;
}
.jss19956.jss19953 {
  transform: translate(12px, 19px) scale(1);
}
.jss19956.jss19954 {
  transform: translate(12px, 10px) scale(0.75);
}
.jss19956.jss19954.jss19953 {
  transform: translate(12px, 7px) scale(0.75);
}
.jss19957 {
  z-index: 1;
  transform: translate(14px, 22px) scale(1);
  pointer-events: none;
}
.jss19957.jss19953 {
  transform: translate(14px, 17.5px) scale(1);
}
.jss19957.jss19954 {
  transform: translate(14px, -6px) scale(0.75);
}

.jss19943 {
  margin: 0;
  border: 0;
  display: inline-flex;
  padding: 0;
  position: relative;
  min-width: 0;
  flex-direction: column;
  vertical-align: top;
}
.jss19944 {
  margin-top: 16px;
  margin-bottom: 8px;
}
.jss19945 {
  margin-top: 8px;
  margin-bottom: 4px;
}
.jss19946 {
  width: 100%;
}

.jss19981 {
  color: rgba(0, 0, 0, 0.54);
  margin: 0;
  font-size: 0.6964285714285714rem;
  text-align: left;
  margin-top: 8px;
  min-height: 1em;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  line-height: 1em;
}
.jss19981.jss19983 {
  color: rgba(0, 0, 0, 0.38);
}
.jss19981.jss19982 {
  color: #f44336;
}
.jss19984 {
  margin-top: 4px;
}
.jss19985 {
  margin: 8px 12px 0;
}

.jss14027 {
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: -1;
  position: fixed;
  touch-action: none;
  background-color: rgba(0, 0, 0, 0.5);
  -webkit-tap-highlight-color: transparent;
}
.jss14028 {
  background-color: transparent;
}

.jss12992 {
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 1300;
  position: fixed;
}
.jss12993 {
  visibility: hidden;
}

.jss14777 {
  outline: none;
  position: absolute;
  min-width: 16px;
  max-width: calc(100% - 32px);
  overflow-y: auto;
  overflow-x: hidden;
  min-height: 16px;
  max-height: calc(100% - 32px);
}

.jss14776 {
  max-height: calc(100% - 96px);
  -webkit-overflow-scrolling: touch;
}

.jss12979 {
  display: flex;
  align-items: center;
  justify-content: center;
}
.jss12980 {
  overflow-y: auto;
  overflow-x: hidden;
}
.jss12981 {
  height: 100%;
  outline: none;
}
.jss12982 {
  margin: 48px;
  display: flex;
  position: relative;
  overflow-y: auto;
  flex-direction: column;
}
.jss12983 {
  flex: 0 1 auto;
  max-height: calc(100% - 96px);
}
.jss12984 {
  margin: 48px auto;
}
.jss12985 {
  max-width: 360px;
}
@media (max-width:455.95px) {
  .jss12985.jss12984 {
    margin: 48px;
  }
}
.jss12986 {
  max-width: 600px;
}
@media (max-width:695.95px) {
  .jss12986.jss12984 {
    margin: 48px;
  }
}
.jss12987 {
  max-width: 960px;
}
@media (max-width:1055.95px) {
  .jss12987.jss12984 {
    margin: 48px;
  }
}
.jss12988 {
  max-width: 1280px;
}
@media (max-width:1375.95px) {
  .jss12988.jss12984 {
    margin: 48px;
  }
}
.jss12989 {
  max-width: 1920px;
}
@media (max-width:2015.95px) {
  .jss12989.jss12984 {
    margin: 48px;
  }
}
.jss12990 {
  width: 100%;
}
.jss12991 {
  width: 100%;
  margin: 0;
  height: 100%;
  max-width: 100%;
  max-height: none;
  border-radius: 0;
}
.jss12991.jss12984 {
  margin: 0;
}

.jss12128 {
  display: flex;
  position: relative;
  align-items: center;
}
.jss12129 {
  padding-left: 16px;
  padding-right: 16px;
}
@media (min-width:600px) {
  .jss12129 {
    padding-left: 24px;
    padding-right: 24px;
  }
}
.jss12130 {
  min-height: 56px;
}
@media (min-width:0px) and (orientation: landscape) {
  .jss12130 {
    min-height: 48px;
  }
}
@media (min-width:600px) {
  .jss12130 {
    min-height: 64px;
  }
}
.jss12131 {
  min-height: 48px;
}

.jss14743 {
  width: 40px;
  height: 40px;
  display: flex;
  position: relative;
  overflow: hidden;
  font-size: 1.1607142857142858rem;
  align-items: center;
  flex-shrink: 0;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  user-select: none;
  border-radius: 50%;
  justify-content: center;
}
.jss14744 {
  color: #fafafa;
  background-color: #bdbdbd;
}
.jss14745 {
  width: 100%;
  height: 100%;
  text-align: center;
  object-fit: cover;
}

.jss14766 {
  height: 0;
  overflow: hidden;
  transition: height 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
}
.jss14767 {
  height: auto;
  overflow: visible;
}
.jss14768 {
  display: flex;
}
.jss14769 {
  width: 100%;
}

.jss19836 {
  position: relative;
  transition: margin 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
}
.jss19836:before {
  top: -1px;
  left: 0;
  right: 0;
  height: 1px;
  content: &quot;&quot;;
  opacity: 1;
  position: absolute;
  transition: opacity 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,background-color 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  background-color: rgba(0, 0, 0, 0.12);
}
.jss19836:first-child {
  border-top-left-radius: 2px;
  border-top-right-radius: 2px;
}
.jss19836:last-child {
  border-bottom-left-radius: 2px;
  border-bottom-right-radius: 2px;
}
.jss19836.jss19837 + .jss19836:before {
  display: none;
}
@supports (-ms-ime-align: auto) {
  .jss19836:last-child {
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
  }
}
.jss19836:first-child:before {
  display: none;
}
.jss19837 {
  margin: 16px 0;
}
.jss19837:first-child {
  margin-top: 0;
}
.jss19837:last-child {
  margin-bottom: 0;
}
.jss19837:before {
  opacity: 0;
}
.jss19838 {
  background-color: rgba(0, 0, 0, 0.12);
}

.jss19942 {
  display: flex;
  padding: 8px 24px 24px;
}

.jss19839 {
  display: flex;
  padding: 0 24px 0 24px;
  min-height: 48px;
  transition: min-height 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,background-color 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
}
.jss19839:hover:not(.jss19842) {
  cursor: pointer;
}
.jss19839.jss19840 {
  min-height: 64px;
}
.jss19839.jss19841 {
  background-color: #e0e0e0;
}
.jss19839.jss19842 {
  opacity: 0.38;
}
.jss19840 {
  margin: 0;
}
.jss19843 {
  margin: 12px 0;
  display: flex;
  flex-grow: 1;
  transition: margin 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
}
.jss19843 > :last-child {
  padding-right: 32px;
}
.jss19843.jss19840 {
  margin: 20px 0;
}
.jss19844 {
  top: 50%;
  right: 8px;
  position: absolute;
  transform: translateY(-50%) rotate(0deg);
  transition: transform 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
}
.jss19844:hover {
  background-color: transparent;
}
.jss19844.jss19840 {
  transform: translateY(-50%) rotate(180deg);
}

.jss19845 {
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  box-sizing: border-box;
}
.jss19846 {
  margin: 0;
  box-sizing: border-box;
}
.jss19847 {
  min-width: 0;
}
.jss19848 {
  flex-direction: column;
}
.jss19849 {
  flex-direction: column-reverse;
}
.jss19850 {
  flex-direction: row-reverse;
}
.jss19851 {
  flex-wrap: nowrap;
}
.jss19852 {
  flex-wrap: wrap-reverse;
}
.jss19853 {
  align-items: center;
}
.jss19854 {
  align-items: flex-start;
}
.jss19855 {
  align-items: flex-end;
}
.jss19856 {
  align-items: baseline;
}
.jss19857 {
  align-content: center;
}
.jss19858 {
  align-content: flex-start;
}
.jss19859 {
  align-content: flex-end;
}
.jss19860 {
  align-content: space-between;
}
.jss19861 {
  align-content: space-around;
}
.jss19862 {
  justify-content: center;
}
.jss19863 {
  justify-content: flex-end;
}
.jss19864 {
  justify-content: space-between;
}
.jss19865 {
  justify-content: space-around;
}
.jss19866 {
  justify-content: space-evenly;
}
.jss19867 {
  width: calc(100% + 8px);
  margin: -4px;
}
.jss19867 > .jss19846 {
  padding: 4px;
}
.jss19868 {
  width: calc(100% + 16px);
  margin: -8px;
}
.jss19868 > .jss19846 {
  padding: 8px;
}
.jss19869 {
  width: calc(100% + 24px);
  margin: -12px;
}
.jss19869 > .jss19846 {
  padding: 12px;
}
.jss19870 {
  width: calc(100% + 32px);
  margin: -16px;
}
.jss19870 > .jss19846 {
  padding: 16px;
}
.jss19871 {
  width: calc(100% + 40px);
  margin: -20px;
}
.jss19871 > .jss19846 {
  padding: 20px;
}
.jss19872 {
  flex-grow: 0;
  max-width: none;
  flex-basis: auto;
}
.jss19873 {
  flex-grow: 1;
  max-width: 100%;
  flex-basis: 0;
}
.jss19874 {
  flex-grow: 0;
  max-width: 8.333333%;
  flex-basis: 8.333333%;
}
.jss19875 {
  flex-grow: 0;
  max-width: 16.666667%;
  flex-basis: 16.666667%;
}
.jss19876 {
  flex-grow: 0;
  max-width: 25%;
  flex-basis: 25%;
}
.jss19877 {
  flex-grow: 0;
  max-width: 33.333333%;
  flex-basis: 33.333333%;
}
.jss19878 {
  flex-grow: 0;
  max-width: 41.666667%;
  flex-basis: 41.666667%;
}
.jss19879 {
  flex-grow: 0;
  max-width: 50%;
  flex-basis: 50%;
}
.jss19880 {
  flex-grow: 0;
  max-width: 58.333333%;
  flex-basis: 58.333333%;
}
.jss19881 {
  flex-grow: 0;
  max-width: 66.666667%;
  flex-basis: 66.666667%;
}
.jss19882 {
  flex-grow: 0;
  max-width: 75%;
  flex-basis: 75%;
}
.jss19883 {
  flex-grow: 0;
  max-width: 83.333333%;
  flex-basis: 83.333333%;
}
.jss19884 {
  flex-grow: 0;
  max-width: 91.666667%;
  flex-basis: 91.666667%;
}
.jss19885 {
  flex-grow: 0;
  max-width: 100%;
  flex-basis: 100%;
}
@media (min-width:600px) {
  .jss19886 {
    flex-grow: 0;
    max-width: none;
    flex-basis: auto;
  }
  .jss19887 {
    flex-grow: 1;
    max-width: 100%;
    flex-basis: 0;
  }
  .jss19888 {
    flex-grow: 0;
    max-width: 8.333333%;
    flex-basis: 8.333333%;
  }
  .jss19889 {
    flex-grow: 0;
    max-width: 16.666667%;
    flex-basis: 16.666667%;
  }
  .jss19890 {
    flex-grow: 0;
    max-width: 25%;
    flex-basis: 25%;
  }
  .jss19891 {
    flex-grow: 0;
    max-width: 33.333333%;
    flex-basis: 33.333333%;
  }
  .jss19892 {
    flex-grow: 0;
    max-width: 41.666667%;
    flex-basis: 41.666667%;
  }
  .jss19893 {
    flex-grow: 0;
    max-width: 50%;
    flex-basis: 50%;
  }
  .jss19894 {
    flex-grow: 0;
    max-width: 58.333333%;
    flex-basis: 58.333333%;
  }
  .jss19895 {
    flex-grow: 0;
    max-width: 66.666667%;
    flex-basis: 66.666667%;
  }
  .jss19896 {
    flex-grow: 0;
    max-width: 75%;
    flex-basis: 75%;
  }
  .jss19897 {
    flex-grow: 0;
    max-width: 83.333333%;
    flex-basis: 83.333333%;
  }
  .jss19898 {
    flex-grow: 0;
    max-width: 91.666667%;
    flex-basis: 91.666667%;
  }
  .jss19899 {
    flex-grow: 0;
    max-width: 100%;
    flex-basis: 100%;
  }
}
@media (min-width:960px) {
  .jss19900 {
    flex-grow: 0;
    max-width: none;
    flex-basis: auto;
  }
  .jss19901 {
    flex-grow: 1;
    max-width: 100%;
    flex-basis: 0;
  }
  .jss19902 {
    flex-grow: 0;
    max-width: 8.333333%;
    flex-basis: 8.333333%;
  }
  .jss19903 {
    flex-grow: 0;
    max-width: 16.666667%;
    flex-basis: 16.666667%;
  }
  .jss19904 {
    flex-grow: 0;
    max-width: 25%;
    flex-basis: 25%;
  }
  .jss19905 {
    flex-grow: 0;
    max-width: 33.333333%;
    flex-basis: 33.333333%;
  }
  .jss19906 {
    flex-grow: 0;
    max-width: 41.666667%;
    flex-basis: 41.666667%;
  }
  .jss19907 {
    flex-grow: 0;
    max-width: 50%;
    flex-basis: 50%;
  }
  .jss19908 {
    flex-grow: 0;
    max-width: 58.333333%;
    flex-basis: 58.333333%;
  }
  .jss19909 {
    flex-grow: 0;
    max-width: 66.666667%;
    flex-basis: 66.666667%;
  }
  .jss19910 {
    flex-grow: 0;
    max-width: 75%;
    flex-basis: 75%;
  }
  .jss19911 {
    flex-grow: 0;
    max-width: 83.333333%;
    flex-basis: 83.333333%;
  }
  .jss19912 {
    flex-grow: 0;
    max-width: 91.666667%;
    flex-basis: 91.666667%;
  }
  .jss19913 {
    flex-grow: 0;
    max-width: 100%;
    flex-basis: 100%;
  }
}
@media (min-width:1280px) {
  .jss19914 {
    flex-grow: 0;
    max-width: none;
    flex-basis: auto;
  }
  .jss19915 {
    flex-grow: 1;
    max-width: 100%;
    flex-basis: 0;
  }
  .jss19916 {
    flex-grow: 0;
    max-width: 8.333333%;
    flex-basis: 8.333333%;
  }
  .jss19917 {
    flex-grow: 0;
    max-width: 16.666667%;
    flex-basis: 16.666667%;
  }
  .jss19918 {
    flex-grow: 0;
    max-width: 25%;
    flex-basis: 25%;
  }
  .jss19919 {
    flex-grow: 0;
    max-width: 33.333333%;
    flex-basis: 33.333333%;
  }
  .jss19920 {
    flex-grow: 0;
    max-width: 41.666667%;
    flex-basis: 41.666667%;
  }
  .jss19921 {
    flex-grow: 0;
    max-width: 50%;
    flex-basis: 50%;
  }
  .jss19922 {
    flex-grow: 0;
    max-width: 58.333333%;
    flex-basis: 58.333333%;
  }
  .jss19923 {
    flex-grow: 0;
    max-width: 66.666667%;
    flex-basis: 66.666667%;
  }
  .jss19924 {
    flex-grow: 0;
    max-width: 75%;
    flex-basis: 75%;
  }
  .jss19925 {
    flex-grow: 0;
    max-width: 83.333333%;
    flex-basis: 83.333333%;
  }
  .jss19926 {
    flex-grow: 0;
    max-width: 91.666667%;
    flex-basis: 91.666667%;
  }
  .jss19927 {
    flex-grow: 0;
    max-width: 100%;
    flex-basis: 100%;
  }
}
@media (min-width:1920px) {
  .jss19928 {
    flex-grow: 0;
    max-width: none;
    flex-basis: auto;
  }
  .jss19929 {
    flex-grow: 1;
    max-width: 100%;
    flex-basis: 0;
  }
  .jss19930 {
    flex-grow: 0;
    max-width: 8.333333%;
    flex-basis: 8.333333%;
  }
  .jss19931 {
    flex-grow: 0;
    max-width: 16.666667%;
    flex-basis: 16.666667%;
  }
  .jss19932 {
    flex-grow: 0;
    max-width: 25%;
    flex-basis: 25%;
  }
  .jss19933 {
    flex-grow: 0;
    max-width: 33.333333%;
    flex-basis: 33.333333%;
  }
  .jss19934 {
    flex-grow: 0;
    max-width: 41.666667%;
    flex-basis: 41.666667%;
  }
  .jss19935 {
    flex-grow: 0;
    max-width: 50%;
    flex-basis: 50%;
  }
  .jss19936 {
    flex-grow: 0;
    max-width: 58.333333%;
    flex-basis: 58.333333%;
  }
  .jss19937 {
    flex-grow: 0;
    max-width: 66.666667%;
    flex-basis: 66.666667%;
  }
  .jss19938 {
    flex-grow: 0;
    max-width: 75%;
    flex-basis: 75%;
  }
  .jss19939 {
    flex-grow: 0;
    max-width: 83.333333%;
    flex-basis: 83.333333%;
  }
  .jss19940 {
    flex-grow: 0;
    max-width: 91.666667%;
    flex-basis: 91.666667%;
  }
  .jss19941 {
    flex-grow: 0;
    max-width: 100%;
    flex-basis: 100%;
  }
}

.jss14740 {
  width: 36px;
  height: 36px;
  font-size: 1.0446428571428572rem;
  margin-right: 4px;
}
.jss14741 {
  margin-top: 4px;
}
.jss14742 {
  width: 20px;
  height: 20px;
  font-size: 1.1607142857142858rem;
}

.jss13275 {
  width: 100%;
  display: flex;
  z-index: 1100;
  box-sizing: border-box;
  flex-shrink: 0;
  flex-direction: column;
}
.jss13276 {
  top: 0;
  left: auto;
  right: 0;
  position: fixed;
}
.jss13277 {
  top: 0;
  left: auto;
  right: 0;
  position: absolute;
}
.jss13278 {
  top: 0;
  left: auto;
  right: 0;
  position: sticky;
}
.jss13279 {
  position: static;
}
.jss13280 {
  position: relative;
}
.jss13281 {
  color: rgba(0, 0, 0, 0.87);
  background-color: #f5f5f5;
}
.jss13282 {
  color: rgba(0, 0, 0, 0.87);
  background-color: #03a9f4;
}
.jss13283 {
  color: rgba(0, 0, 0, 0.87);
  background-color: #ff9800;
}

.jss13284 {
  z-index: 1500;
  opacity: 0.9;
}
.jss13285 {
  color: #fff;
  padding: 4px 8px;
  font-size: 0.5803571428571429rem;
  max-width: 300px;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  line-height: 1.4em;
  border-radius: 4px;
  background-color: #616161;
}
.jss13286 {
  padding: 8px 16px;
  font-size: 0.8125rem;
  line-height: 1.14286em;
}
.jss13287 {
  margin: 0 24px ;
  transform-origin: right center;
}
@media (min-width:600px) {
  .jss13287 {
    margin: 0 14px;
  }
}
.jss13288 {
  margin: 0 24px;
  transform-origin: left center;
}
@media (min-width:600px) {
  .jss13288 {
    margin: 0 14px;
  }
}
.jss13289 {
  margin: 24px 0;
  transform-origin: center bottom;
}
@media (min-width:600px) {
  .jss13289 {
    margin: 14px 0;
  }
}
.jss13290 {
  margin: 24px 0;
  transform-origin: center top;
}
@media (min-width:600px) {
  .jss13290 {
    margin: 14px 0;
  }
}

.jss19989 {
  display: flex;
  padding: 8px;
  align-items: center;
  justify-content: flex-end;
}
.jss19990 {
  margin-left: 8px;
}

@media (min-width:960px) {
  ::-webkit-scrollbar {
    width: 5px;
    height: 5px;
  }
  ::-webkit-scrollbar-track {
    background: rgba(250, 250, 250, 0);
  }
  ::-webkit-scrollbar-thumb {
    background: #eeeeee;
  }
  ::-webkit-scrollbar-thumb:hover {
    background: #e0e0e0;
  }
}
.jss19119 {
  width: 100%;
  z-index: 1;
  overflow: hidden;
  margin-bottom: 0;
}
.jss19120 {
  background-color: #424242;
}
.jss19121 {
  height: 70px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/etg.9362c8fb.svg&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss19122 {
  height: 150px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/tessa.55f0b81f.svg&quot;);
  background-repeat: no-repeat;
}
.jss19123 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2hJREFUeNrsWtGR2jAQ1TEU4FQQUsGRCuBm+IerAKgguAKgAnMVHFcBvn/PnFwBvg6cCuIOEslacRvFyJJswL74zQgDNrKedvftSoaQDh06dHDAnfGVfjRjr0PWRg3kkbD2RIJJakfMjzbsdd0CQz0yciF/0zf8wRyOKbSmgXuSx1rAmhWxgbSdnJFG4cOj5DhJz7KLrC3i0a8wSwM8QzcRjGCS1U+MkLcbE9uztjQn5kcyEFMsn5q4uxW09+8hQh5rR/buCNY4OtxsBzMpEaLZ/cIm6g6uyZTrtih+d6C8/PxDLuEiT1mhp7jWEH32HIi95sr5kRKe4PiTtRWbuN/5UfS9hXOUEd6gSXgBa/gwpoMyLiMh6ym5gJxmSsyuCxaK71OUAyka0AC+i8H9YzjnofEsNfd5NyWWnCwVTGgF35/CoPbK9ykMXt5nDEQpWIaC1aZwnlvqnrXvLq7YV2Z2CDesijUMCLv0FNxQYgQWkWIl3fOAfrcq+N46xuJTh8I1XOCBi4nyRqjqEMUZVSyWIQvNlZAI4fr4TIyNTIlh93O12gHJ8AxUluMHkI2V+xwhJmVsviniEAOBtbsr8izuRykM7L7G4lRacl2SizxFiRd1llQUOjSx2BYN9lpVvyzjMkgtxsRiIDbIa0Fd5SFyz6apRXCvwGJV46yBxISFUhPVaeOyJQE/HpcsW8aO2wV8sZqgfhZI6m2w1IVKEbE4l2oRZ55mzTN2dNeZUknMHftZ6GK8iJgaZ6FmZ8il9KIFE+mC0M4VuZv4Ec7u4RlVDMs6N4JQ14urojqrrVXGc8TiU+XA4+yTqCJRgnt8RhU9CGAb4mLljAVJbArZlk9pHgYOmznUoIpeEffd4R16/+zo8p7Sj4EriplIkDxfEtklfqfbfkugOh+cVTM/spd7dXUeTB4h2duRwkneklhc6vvVthDq78dAFYnLPkMb5J6AqbO2Eivb4k40cr8i4rGNS/G6R/08O66WH3QuXPa0JS6RW9dVsO6z7baDk8WoJle51om7f5Yxbill706Mm/qjIC6KweoCU1c/lq7YWnU0IUY/K7H3GgTj6ih/oiKq+F+oqk5IyZOOK+MriI98WPnNjJggF5C/Hyg0Faf/edj8M4cn6mlZ/rgRuBe9XqLm7NChw3+GPwIMAA139Xg+K8vEAAAAAElFTkSuQmCC&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss19124 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABANJREFUeNrsWt1x4jAQNpl7P18FcSqIqSBmhvdABYEKiCvwUQFcBfZVEHhnBl8F8VUQSvB1cJJ3jddC/pOlQBg0YwYUIfbT/n27imXdxtcag947+DuPvXqa5TpYq3F0PmD+bsZeQ0OHvmbg/HMB+2CvDntS9iSaADn48PGDgUtVNvmmQQg+fjEBfmqBBaa9x08ue2KVbe6uNXhcOrDDdQJbjQ/XqjHrBuxLAYMIeVZg9xrhuJegsTx5ztjphuyxe2qI58LVJURFSnk4vfpgwr0qmRywmECgVMrAdJBgB095QmaTDPRqHBv7rnFgZUcPCc3igzP0pfTkwewW7LGJWfO168soW9oJXHDJ6gPwVQnv5wCrNrEDgnSFuXlfszPB7utYfw7EFioBOmLy9wuroE8Lz6ACRBPZXfatmvUD83cu+o0r5DiukT+SIpSve0RTtU2Y5kCTlkLl05dreX7unkeIiTkfS+VKGhJ7QDQYsb3mnw+srCludiMmSNKwPq5lE2DSewLOV81rA0VQtC/RFlSIax9q89UpuJGKz6lyRepT9aBg5BzQFnKbrGpOsj0Lgq3U3rtTNEGH+FTSImLSwPDSoiWQZHvn+Q9+07jGgmP0axcoRCAeMpMmcGtStgRmgfm7iUV7ie3GRKjdrEZzpFG20Nqki6hdKdWTQFxFfuhIErFDhHzBuQVbn0i0JAaJDfGxJ/xsBJh35Hh5ZANA7y0430YAu5dYBKxbjacINGVzG9SwZ9LHXFIM0tEEKs9fG8EkZUPc669KL0Q13P8j5gP8riwwfz/FsM3TwYisfTjOFz5Eq+epZM5guIewLf8x4HUjMm8ffUP0G25eMOcJ0S7CQ0grGkaiDNo0lta2yIrEGhFwe6kwwDEDsu8844XNFXSqH1ibjhFoYy6YmFNRiFLmErUIWJ16+V2jYl4RPzas+14KHNBvnGWmCma47RDl7rtqSyV4xCenWJ+UEzTbd+yBcNN8s8qXeU0Uy1MJIl2BbY/+U8UEytzQxnzlCKD3UlOr32trEhjN/ItGnyiESjElrCW5yq3hjouK39YMDKJWRMis7LSfJeY7zAIE/BfAVOIvXoW2ZqSaPpjUWM75ilrp9CLCJVrys+RMhVqNN5ikNw3MJZSQYYPAQMglMTWxEMzD/bCyrIe0MM3WwNpIkudcUvMduorZp+fxRqJfr8ZLTYOIp4ehyjZ9OsFz1Bj4AgSAqXL/HUw6FFLFSFU49fsxAED5IQ8Aqvdjs+y7IqgelxS6OsE8+b4KDCXKck9Vhwki6jOCoeF+jX7V6+ZF9/1YUJFwExJJ7YraKkZAsQ5xTNyPeUiTJla7mxSu2d+6r5LM3I+VQboSgCkhxLdxG2z8F2AAPtd0UKyJkPIAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss19125 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAzRJREFUeNrsWs2Nm0AUnl1RAFIKCK4gdLBwy9GuYL0VePeWm+1bbt5UYLsC7FtueCswqWDdQCTSQd5bfRM9IcADDD+OeNIIbI8f8837f4NSI4000kg16K7uHz99++nTZUkj/f3965PNRRHvFV2+0FgT76QOj/sGz2dQUxpzWkhgEVQgeC/r8mkCzO1Ao9w+gEnyb8bGSCU8qEPRri3Ebycab5bW9JnVG/cXGvuCeSmNA9ngxRgYgeIFv3ekbk2IwU0IXGqqiv4NgNI2mGsGjsGfi9ThkYZXoIoPDRbrCb4siaTOxhsBI1GvctT1QSzgLW9OA3cf42NCfMOcOfx70JZXlDt5atGGapHT4KFrGn+QeVgDxrxIIi9QtV3nwOCJVm2IiXi/NuXhVNB9T8SXV+1iYROB6pY8mxKbi9yNPeKEQHEAj4YYB6oAk+qxg8ocCFzYg8Qer0nNaWpTcBynLlFlQk2rSfDg6L7DXQ5s1m2DAIZqm7OFGPf/jcS2Bfe3Cwz9C1+kRz6+u11gCOo69v1AGvZRpOK32wMGRxGJLH2FWJggD4zadCaOZelMUYsFol5iIKGOhQjoMdSTnckFc44cD4tK/V4kRouL0ErYZPokLKFQlu64D0UmozeEnco78doMSWJuJvX6hUZLWpLFvBCINUBxc/TZNMHt0saehNcLykDlADyIXDMFr2EAg13MRD+iSgd3I8LBxGRDOvWKSIY1uGcTd44sRNd4M1ugrLt7LmNEOT83LD8UvKHVCqGNOHbE1aQFp/PG/WDjmKAq6qSBGcUuqHfQF7CiLOQfcHHmlapqHWeev1AGhyBtANMPdQnQVuUcbND3KXLH1HBjPjIa2hCOfRwOzn3YmCsAzjOpVSLmLMUmXPOg7Fi4UN1C2qe+VZE95BHeMiuBhbAX91og1zkmXc992ZjOAXdFCS2AHhDHpqqk4yuObmfIMWMTG7srYRaLGBP22ZgRUtLrkGd3YV4MdNRACa49AhgtqRj3yTW3bwLM66KUL/GwZwTwNUqb2JaNearBawkWKPt8oy5XkbtPVIOzqQ6p6MSz9K0BX5T6QyQGtK/75s5II400Ui79FWAANjYkpEJzM+AAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss19126 {
  color: #FFF;
  padding: 48px;
}
.jss19127 {
  color: rgba(0, 0, 0, 0.87);
  width: 100%;
  display: flex;
  z-index: 2;
  opacity: 0.5;
  position: fixed;
  align-items: center;
  padding-left: 16px;
  padding-right: 16px;
  background-color: #424242;
}
.jss19128 {
  height: 100vh;
  display: flex;
  position: relative;
  max-height: 1600px;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: linear-gradient(rgba(0,130,170,0), #424242), url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/1.20e79364.jpg&quot;);
}
.jss19129 {
  margin: -200px auto 0 auto;
  padding: 64px 0 48px;
  max-width: 1200px;
}
.jss19130 {
  width: 50%;
  color: #FFF;
}
@media (max-width:1279.95px) {
  .jss19130 {
    width: 100%;
    padding: 24px;
  }
}
.jss19131 {
  margin: -150px auto 0 auto;
  padding: 16px;
  position: relative;
  max-width: 1200px;
  background: #fafafa;
  justify-content: center;
}
.jss19132 {
  margin: 0 auto;
  padding: 24px;
  display: flex;
}
.jss19133 {
  margin-bottom: 24px;
}
.jss19134 {
  text-decoration: none;
}
.jss19135 {
  margin: 24px auto;
  max-width: 1200px;
}
@media (max-width:1279.95px) {
  .jss19135 {
    max-width: 1366px;
  }
}
.jss19136 {
  height: 160px;
  display: flex;
  align-items: center;
}
.jss19137 {
  margin: auto;
  display: grid;
}
.jss19138 {
  height: 100vh;
  display: flex;
  position: relative;
  overflow: hidden;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/2.adfa400c.jpg&quot;);
  background-position: center;
}
.jss19139 {
  color: #FFF;
  background-color: #03a9f4;
}
.jss19140 {
  color: #03a9f4;
  background-color: #fff;
}
.jss19141 {
  color: #03a9f4;
  padding: 0;
}
.jss19142 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss19142 {
    width: calc(100%);
  }
}
.jss19143 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss19143 {
    width: calc(100% - 300px);
  }
}
.jss19144 {
  display: none;
}
@media (min-width:600px) {
  .jss19144 {
    display: block;
  }
}
.jss19145 {
  color: #fff;
  position: relative;
  background: #03a9f4;
}
.jss19146 {
  width: 100%;
  position: relative;
  border-radius: 14px;
  background-color: rgba(255, 255, 255, 0.5);
}
.jss19146:hover {
  background-color: rgba(255, 255, 255, 0.7);
}
@media (min-width:600px) {
  .jss19146 {
    width: auto;
  }
}
.jss19147 {
  width: 72px;
  color: #212121;
  height: 100%;
  display: flex;
  position: absolute;
  align-items: center;
  pointer-events: none;
  justify-content: center;
}
.jss19148 {
  color: #fff;
  width: 100%;
}
.jss19149 {
  color: #212121;
  width: 100%;
  transition: width 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  padding-top: 8px;
  padding-left: 64px;
  padding-right: 8px;
  padding-bottom: 8px;
}
@media (min-width:600px) {
  .jss19149 {
    width: 80px;
  }
  .jss19149:focus {
    width: 200px;
  }
}
.jss19150 {
  color: #212121;
}
.jss19151 {
  background-color: #4fc3f7;
}
.jss19152 {
  margin-left: 12px;
  margin-right: 36px;
}
.jss19153 {
  display: none;
}
@media (min-width:960px) {
  .jss19154 {
    display: none;
  }
}
.jss19155 {
  width: 300px;
}
.jss19156 {
  display: flex;
  padding: 0 8px;
  min-height: 56px;
  align-items: center;
}
@media (min-width:0px) and (orientation: landscape) {
  .jss19156 {
    min-height: 48px;
  }
}
@media (min-width:600px) {
  .jss19156 {
    min-height: 64px;
  }
}
.jss19157 {
  margin-top: 65px;
  justify-content: flex-start;
}
.jss19158 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss19160 {
  width: 100%;
  height: 100%;
  opacity: 0.2;
  content: &quot;&quot;;
  display: block;
  position: absolute;
  background: linear-gradient(rgba(0,130,170,0), #03a9f4));
  background-size: cover;
  background-position: center center;
}
.jss19161 {
  width: 60px;
  overflow-x: hidden;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss19162 {
  color: inherit;
}
.jss19163 {
  margin-left: 56px;
}
.jss19164 {
  bottom: 16px;
}
.jss19165 {
  color: rgba(0, 0, 0, 0.87);
  background-size: cover;
  background-color: #fff;
  background-image: url(https://xdev.equine.co.id/apps/tessa/static/media/pattern2.f069796d.svg);
  background-repeat: no-repeat;
  background-position: inherit;
}
.jss19166 {
  color: #fff;
  background-color: #03a9f4;
}
.jss19167 {
  color: #fff;
  background-color: #ff9800;
}
.jss19168 {
  color: #fff;
  background-color: #f44336;
}
.jss19169 {
  color: #fff;
  background: #4caf50;
}
.jss19170 {
  flex: 1;
}
.jss19171 {
  padding: 16px;
  flex-grow: 1;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss19171 {
    margin-top: 68px;
  }
}
@media (max-width:959.95px) {
  .jss19171 {
    padding: 0;
    margin-top: 56px;
  }
}
@media (max-width:959.95px) {
  .jss19172 {
    margin-top: 68px;
  }
}
@media (min-width:960px) {
  .jss19172 {
    left: 50%;
    width: 982px;
    position: absolute;
    transform: translateX(-50%);
  }
}
@media (min-width:960px) {
  .jss19173 {
    margin-left: 300px;
    margin-right: 0;
  }
}
.jss19174 {
  margin-bottom: 56px;
}
.jss19175 {
  width: calc(100% - 0px);
  right: 0;
  bottom: 0;
  display: flex;
  position: fixed;
}
@media (min-width:960px) {
  .jss19175 {
    width: calc(100% - 300px);
  }
}
.jss19177 {
  background: #fff;
}
.jss19178 {
  margin-bottom: 60px;
}
.jss19179 {
  height: 4px;
}
.jss19180 {
  display: flex;
  align-items: center;
  justify-content: space-between;
}
.jss19181 {
  margin: 0 15px 0 0;
  padding: 16px 0;
  flex-grow: 1;
}
@media (min-width:600px) {
  .jss19181 {
    width: 30%;
    flex-grow: 0;
  }
}
.jss19182 {
  color: inherit;
  font-size: 0.6964285714285714rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: inherit;
  line-height: 1.66;
  letter-spacing: 0.03333em;
}
.jss19183 {
  color: #fff;
  background: #03a9f4;
}
.jss19184 {
  color: #fff;
  min-height: 40px;
  background: #ff9800;
  margin-bottom: 16px;
}
.jss19185 {
  height: 500px;
  display: flex;
  overflow: hidden;
  align-items: center;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss19185 {
    height: 300px;
  }
}
.jss19186 {
  height: 100%;
  margin: 0 auto;
}
.jss19187 {
  width: 25px;
  height: 25px;
  margin-right: 16px;
}
.jss19188 {
  transform: rotate(0deg);
  transition: transform 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
}
.jss19189 {
  transform: rotate(180deg);
}
.jss19190 {
  min-width: 350px;
}
.jss19191 {
  display: flex;
  flex-wrap: wrap;
}
.jss19192 {
  padding: 0;
}
@media (min-width:0px) {
  .jss19192 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss19192 {
    width: calc(100% / 2);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1280px) {
  .jss19192 {
    width: calc(100% / 3);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1920px) {
  .jss19192 {
    width: calc(100% / 4);
    padding: 0 16px 16px 0;
  }
}
.jss19193 {
  padding: 0;
}
@media (min-width:0px) {
  .jss19193 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss19193 {
    width: calc(100% / 2);
  }
}
@media (min-width:1280px) {
  .jss19193 {
    width: calc(100% - (100% / 3));
  }
}
@media (min-width:1920px) {
  .jss19193 {
    width: calc(100% - (100% / 4));
  }
}
.jss19194 {
  padding: 0;
}
@media (min-width:0px) {
  .jss19194 {
    width: 100%;
  }
}
.jss19195 {
  width: 100%;
  margin-bottom: 16px;
  vertical-align: middle;
}
.jss19196 {
  right: 16px;
  display: flex;
  position: absolute;
}
.jss19197 {
  right: 16px;
  bottom: 16px;
  position: fixed;
}
.jss19198 {
  float: right;
}
.jss19199 {
  margin: 0;
}
.jss19200 {
  margin: 8px;
}
.jss19201 {
  margin-left: 8px;
}
.jss19202 {
  margin-top: 8px;
}
.jss19203 {
  margin-right: 8px;
}
.jss19204 {
  margin-bottom: 8px;
}
.jss19205 {
  margin: 16px;
}
.jss19206 {
  margin-left: 16px;
}
.jss19207 {
  margin-top: 16px;
}
.jss19208 {
  margin-right: 16px;
}
.jss19209 {
  margin-bottom: 16px;
}
.jss19210 {
  margin: 24px;
}
.jss19211 {
  margin-left: 24px;
}
.jss19212 {
  margin-top: 24px;
}
.jss19213 {
  margin-right: 24px;
}
.jss19214 {
  margin-bottom: 24px;
}
.jss19215 {
  padding: 0;
}
.jss19216 {
  padding: 8px;
}
.jss19217 {
  padding-left: 8px;
}
.jss19218 {
  padding-top: 8px;
}
.jss19219 {
  padding-right: 8px;
}
.jss19220 {
  padding-bottom: 8px;
}
.jss19221 {
  padding: 16px;
}
.jss19222 {
  padding-left: 16px;
}
.jss19223 {
  padding-top: 16px;
}
.jss19224 {
  padding-right: 16px;
}
.jss19225 {
  padding-bottom: 16px;
}
.jss19226 {
  padding: 24px;
}
.jss19227 {
  padding-left: 24px;
}
.jss19228 {
  padding-top: 24px;
}
.jss19229 {
  padding-right: 24px;
}
.jss19230 {
  padding-bottom: 24px;
}
.jss19231 {
  background-color: #03a9f4;
}
.jss19232 {
  background-color: #ff9800;
}
.jss19233 {
  color: white;
  background-color: #f44336;
}
.jss19234 {
  color: #03a9f4;
}
.jss19235 {
  color: #ff9800;
}
.jss19236 {
  color: #f44336;
}
.jss19237 {
  text-decoration: line-through;
}
.jss19238 {
  padding: 3px;
}
.jss19239 {
  height: calc(100vh - 96px - 72px);
  overflow-x: auto;
}
.jss19240 {
  width: calc(100vw - 32px - 300px);
}
.jss19241 {
  height: calc(100vh - 56px - 68px);
  overflow-x: auto;
}
.jss19242 {
  width: calc(100vw);
}
.jss19243 {
  overflow-x: auto;
}
.jss19244 {
  table-layout: initial;
}
.jss19245 {
  top: 0;
  position: sticky;
  background-color: #fff;
}
.jss19246 {
  width: 3vw;
}
.jss19247 {
  width: 5vw;
}
.jss19248 {
  width: 10vw;
}
.jss19249 {
  width: 15vw;
}
.jss19250 {
  min-width: 15vw;
  max-width: 15vw;
}
@media (min-width:960px) {
  .jss19250 {
    min-width: calc((100vw - 300px - 48px) * 0.15);
    max-width: calc((100vw - 300px - 48px) * 0.15);
  }
}
.jss19251 {
  min-width: 25vw;
  max-width: 25vw;
}
@media (min-width:960px) {
  .jss19251 {
    min-width: calc((100vw - 300px - 48px) * 0.25);
    max-width: calc((100vw - 300px - 48px) * 0.25);
  }
}
.jss19252 {
  width: 25vw;
}
.jss19253 {
  width: 30vw;
}
.jss19254 {
  min-width: 45vw;
  max-width: 45vw;
}
@media (min-width:960px) {
  .jss19254 {
    min-width: calc((100vw - 300px - 48px) * 0.45);
    max-width: calc((100vw - 300px - 48px) * 0.45);
  }
}
.jss19255 {
  width: 100%;
  overflow-x: auto;
}
.jss19256 {
  width: 100%;
  margin-top: 24px;
  overflow-x: auto;
}
.jss19257 {
  min-width: 700px;
}
.jss19258 {
  width: 100%;
}
.jss19259 {
  min-width: 100px;
}
.jss19260 {
  color: rgba(0, 0, 0, 0.54);
  flex-shrink: 0;
}
.jss19261 {
  color: #2196f3;
}
.jss19262 {
  padding-top: 16px;
  padding-bottom: 16px;
}
.jss19264 {
  max-height: 100px;
}
.jss19265 {
  margin-top: -144px;
  margin-left: -56px;
  margin-right: -136px;
  margin-bottom: -64px;
}
.jss19266 {
  margin-top: -80px;
}
.jss19267 {
  width: 500px;
  height: 450px;
}
.jss19268 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss19269 {
  display: flex;
  box-sizing: border-box;
  align-items: center;
}
.jss19271:hover {
  background-color: #eeeeee;
}
.jss19272 {
  flex: 1;
}
.jss19273 {
  cursor: initial;
}
.jss19274 {
  background: #f44336;
}
.jss19275 {
  color: #fff;
}
.jss19276 {
  width: 100%;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
.jss19277 {
  max-height: 75vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss19278 {
  padding: 10px;
  text-align: center;
}
.jss19279 {
  padding: 0;
  vertical-align: top;
}
.jss19280 {
  margin-top: 8px;
  padding-left: 32px;
}
.jss19281 {
  top: 0;
  padding: 5px;
  position: sticky;
  overflow: hidden;
  max-width: 100px;
  max-height: 10px;
  text-align: center;
  background-color: #fff;
}
.jss19282 {
  max-height: 76vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss19283 {
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss19284 {
  padding: 0;
  margin-top: 8px;
}
.jss19285 {
  left: 0;
  width: 500px;
  z-index: 1;
  padding: 8px 16px 0 16px;
  min-width: 300px;
  background: #fff;
  vertical-align: top;
}
.jss19286 {
  padding: 8px 16px 0 16px;
  min-width: 300px;
  vertical-align: top;
}
.jss19287 {
  min-width: 300px;
}
.jss19288 {
  transform: rotate(180deg);
  writing-mode: vertical-rl;
}
.jss19289 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss19290 {
  cursor: pointer;
  transition: all .3s;
}
.jss19290:hover {
  background-color: #ebebeb;
}
.jss19291 {
  background-color: #ebebeb;
}
.jss19292 {
  transition: all .3s;
}
.jss19292:hover {
  transform: scale(1.03);
}
.jss19293 {
  padding: 0;
  transition: all .3s;
}
.jss19293:hover {
  transform: scale(1.03);
}
.jss19294 {
  display: grid;
  align-items: center;
  grid-column-gap: 1.5rem;
  grid-template-columns: 1fr max-content 1fr;
}
.jss19294::after, .jss19294::before {
  height: 1px;
  content: '';
  display: block;
  background-color: currentColor;
}
.jss19295 a {
  text-decoration: none;
}
.jss19296 {
  left: 50%;
  position: absolute;
  transform: ;
}
.jss19297 {
  display: -webkit-box;
  overflow: hidden;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;
}
.jss19298 {
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
.jss19299 {
  display: inline;
  text-transform: capitalize;
}
.jss19300 {
  text-decoration: none;
}
.jss19300 p:hover {
  color: #03a9f4;
}
.jss19301 {
  margin-left: 4px;
}
.jss19302 {
  padding: 0 16px;
  display: inline-flex;
  position: relative;
  vertical-align: middle;
}
.jss19303 {
  top: 0;
  color: #fff;
  right: 0;
  height: 20px;
  display: flex;
  padding: 0 4px;
  z-index: 1;
  position: absolute;
  flex-wrap: wrap;
  min-width: 20px;
  transform: translate(20px, -50%);
  box-sizing: border-box;
  transition: transform 225ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  align-items: center;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  align-content: center;
  border-radius: 10px;
  flex-direction: row;
  justify-content: center;
  background-color: #ff9800;
  transform-origin: 100% 0%;
}
.jss19304 {
  margin: 0;
  padding: 16px;
  border-bottom: 1px solid rgba(0, 0, 0, 0.12);
}
.jss19305 {
  top: 8px;
  right: 8px;
  color: #9e9e9e;
  position: absolute;
}
.jss19306 {
  margin: 0;
  padding: 16px;
}
.jss19307 {
  margin: 0;
  padding: 8px;
  border-top: 1px solid rgba(0, 0, 0, 0.12);
}
.jss19308 {
  font-size: 13px;
}
.jss19309 {
  padding: 0;
  overflow: hidden;
}
.jss19309 span {
  display: inline-flex;
}
.jss19309 span:hover {
  position: relative;
  animation: moveTextHorz 3s linear infinite;
}
@-webkit-keyframes moveTextHorz {
  0% {
    transform: translate(0, 0);
  }
  100% {
    transform: translate(-70%, 0);
  }
}
.jss19310 span {
  color: #f44336;
}
.jss19311 {
  width: 50px;
}
@media (min-width:600px) {
  .jss19311 {
    right: 90px;
  }
}
@media (min-width:960px) {
  .jss19311 {
    right: 15px;
  }
}
.jss19312 {
  cursor: pointer;
}
.jss19312:hover {
  background: #0288d1;
}
.jss19313 {
  margin-left: 10px;
  vertical-align: middle;
}
.jss19314 {
  width: 100%;
}
.jss19315 {
  width: 100%;
  margin: 0;
}
@media (min-width:960px) {
  .jss19316 {
    margin-left: auto;
    margin-right: 16px;
  }
}
@media (min-width:1280px) {
  .jss19316 {
    margin-left: auto;
    margin-right: 32px;
  }
}
@media (min-width:960px) {
  .jss19317 {
    transform: translateY(75%);
    margin-left: 16px;
  }
}
@media (min-width:1280px) {
  .jss19317 {
    transform: translateY(75%);
    margin-left: 32px;
  }
}
.jss19318 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
@media (max-width:1279.95px) {
  .jss19319 {
    display: none;
  }
}
@media (max-width:1919.95px) {
  .jss19320 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss19321 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss19322 {
    text-align: right;
  }
}
.jss19323 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss19323 {
    text-align: right;
  }
}
.jss19324 {
  padding: 0;
}
@media (min-width:1280px) {
  .jss19325 {
    margin-top: 0 !important;
  }
}
@media (min-width:1280px) {
  .jss19326 {
    margin-top: 64px;
  }
}
.jss19327 div textarea {
  overflow: auto;
}
.jss19328 {
  width: 70%;
}
@media (min-width:1280px) {
  .jss19328 {
    float: right;
  }
}
@media (min-width:1280px) {
  .jss19329 {
    float: right;
  }
}
@media (max-width:1279.95px) {
  .jss19329 {
    margin-top: 6px;
  }
}
@media (min-width:1280px) {
  .jss19330 {
    padding: 12px 0;
  }
}
@media (max-width:1279.95px) {
  .jss19330 {
    padding: 6px 0 3px;
  }
}
.jss19331 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss19331 {
    margin-top: 0 !important;
  }
}
.jss19331 div textarea {
  overflow: auto;
}
.jss19332 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss19333 {
  width: 57px;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
  overflow-x: hidden;
}
@media (min-width:600px) {
  .jss19333 {
    width: 73px;
  }
}
@media (min-width:960px) {
  .jss19334 {
    width: calc(100% - 73px);
    margin-left: 73px;
    margin-right: 0;
  }
}
@media (min-width:960px) {
  .jss19335 {
    margin-left: 73px;
    margin-right: 0;
  }
}
.jss19336 {
  bottom: 0px;
  z-index: 1;
  position: sticky;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss19336 {
    display: none;
  }
}
.jss19336:hover {
  background-color: rgb(235, 235, 235);
}
@media (max-width:959.95px) {
  .jss19337 {
    display: none;
  }
}
.jss19338 {
  top: 0;
  left: auto;
  width: 100%;
  right: 0;
  height: 100%;
  z-index: 1301;
  overflow: hidden;
  position: absolute;
  overflow-y: auto;
  transition: transform 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
@media (min-width:960px) {
  .jss19338 {
    width: 24vw;
  }
}
.jss19339 {
  cursor: pointer;
}
.jss19339:hover {
  background-color: #eeeeee;
}
.jss19340 {
  background-color: #e0e0e0 !important;
}
.jss19341 {
  transition: width 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
.jss19342 {
  border-collapse: separate;
}
.jss19343:hover {
  background-color: rgb(235, 235, 235);
}
.jss19344 {
  position: relative;
  min-height: 80vh;
}
.jss19344 .back-icon {
  top: 5px;
  left: 5px;
  z-index: 1;
  position: absolute;
}
.jss19344 > .ninebox-inner {
  width: 100%;
  position: relative;
  min-height: 80vh;
}
.jss19344 > .ninebox-inner .category-paper {
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #424242;
}
.jss19344 > .ninebox-inner .percentage-wrapper {
  width: 100%;
  height: 100%;
  display: flex;
  flex-wrap: wrap;
  box-sizing: border-box;
}
.jss19344 > .ninebox-inner .horizontal-category {
  height: 50px;
  margin: 0 60px;
  display: flex;
  align-items: center;
  justify-content: center;
}
.jss19344 > .ninebox-inner .vertical-category {
  width: 50px;
  height: calc(80vh - 120px);
  display: inline-flex;
  align-items: center;
  justify-content: center;
}
.jss19344 > .ninebox-inner .ninebox-content {
  width: calc(100% - 100px);
  height: calc(80vh - 120px);
  display: inline-block;
  position: relative;
}
.jss19344 > .ninebox-inner .ninebox-content .wrapper {
  width: 100%;
  height: 100%;
  margin: 0;
}
.jss19344 > .ninebox-inner .ninebox-content .wrapper .box {
  height: calc(100% / 3);
  padding: 10px;
}
.jss19344 > .ninebox-inner .ninebox-content .wrapper .box .has-item {
  padding: 26px 10px !important;
}
.jss19344 > .ninebox-inner .ninebox-content .wrapper .box .disabled {
  background: #d1d1d1;
}
.jss19344 > .ninebox-inner .ninebox-content .wrapper .box .card {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 30px;
  position: relative;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  -webkit-transition: all .4s ease-out;
}
.jss19344 > .ninebox-inner .ninebox-content .wrapper .box .card h6 {
  color: rgba(0, 0, 0, 0.87);
}
.jss19344 > .ninebox-inner .ninebox-content .wrapper .box .card .total-person {
  top: 5px;
  right: 5px;
  width: 15px;
  height: 15px;
  position: absolute;
}
.jss19344 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list {
  width: 100%;
  height: 100%;
  display: flex;
  overflow-y: auto;
  align-items: center;
}
.jss19344 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list > ol {
  height: 100%;
  margin: 0;
  padding: 0;
  text-align: left;
  list-style: none;
}
.jss19344 > .ninebox-inner .ninebox-content .wrapper .box .active:hover {
  cursor: pointer;
  transform: scale(1.1);
}
.jss19344 > .ninebox-inner .vertical-category .text-rotate {
  transform: rotate(270deg);
}
.jss19344 > .ninebox-inner .vertical-category.left {
  padding: 10px 0;
  margin-right: 9px;
}
.jss19344 > .ninebox-inner .vertical-category.right {
  margin-left: 9px;
}
.jss19344 > .ninebox-inner .vertical-category .text-rotate.nowrap {
  white-space: nowrap;
}
.jss19344 > .ninebox-inner .horizontal-category.top {
  padding: 0 9px;
  margin-bottom: 9px;
}
.jss19344 > .ninebox-inner .horizontal-category.bottom {
  margin-top: 9px;
}
.jss19344 > .ninebox-inner .percentage-wrapper > .percentage-item {
  padding: 0 10px;
  flex-grow: 0;
  max-width: calc(100% / 3);
  flex-basis: calc(100% / 3);
}
.jss19344 > .ninebox-inner .percentage-wrapper > .percentage-item.horizontal {
  height: calc(100% / 3);
  padding: 10px 0;
  max-width: 100%;
}
.jss19344 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 10px;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  background-color: #424242;
  -webkit-transition: all .4s ease-out;
}
.jss19344 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper > p {
  color: #fff;
}
.jss19344 > .ninebox-inner .category-paper > p {
  color: #fff;
}
.jss19345 {
  max-height: 45vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss19345 .table {
  border-collapse: separate;
}
.jss19345 .table .sticky-head {
  top: 0;
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss19346 {
  height: 44vh;
  overflow: auto;
}
.jss19347 {
  width: 100%;
  height: 50vh;
  overflow: auto;
}
.jss19348 {
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss19349 {
  width: 100%;
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss19350 {
  max-width: 82%;
}

@media (min-width:960px) {
  ::-webkit-scrollbar {
    width: 5px;
    height: 5px;
  }
  ::-webkit-scrollbar-track {
    background: rgba(250, 250, 250, 0);
  }
  ::-webkit-scrollbar-thumb {
    background: #eeeeee;
  }
  ::-webkit-scrollbar-thumb:hover {
    background: #e0e0e0;
  }
}
.jss18887 {
  width: 100%;
  z-index: 1;
  overflow: hidden;
  margin-bottom: 0;
}
.jss18888 {
  background-color: #424242;
}
.jss18889 {
  height: 70px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/etg.9362c8fb.svg&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss18890 {
  height: 150px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/tessa.55f0b81f.svg&quot;);
  background-repeat: no-repeat;
}
.jss18891 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2hJREFUeNrsWtGR2jAQ1TEU4FQQUsGRCuBm+IerAKgguAKgAnMVHFcBvn/PnFwBvg6cCuIOEslacRvFyJJswL74zQgDNrKedvftSoaQDh06dHDAnfGVfjRjr0PWRg3kkbD2RIJJakfMjzbsdd0CQz0yciF/0zf8wRyOKbSmgXuSx1rAmhWxgbSdnJFG4cOj5DhJz7KLrC3i0a8wSwM8QzcRjGCS1U+MkLcbE9uztjQn5kcyEFMsn5q4uxW09+8hQh5rR/buCNY4OtxsBzMpEaLZ/cIm6g6uyZTrtih+d6C8/PxDLuEiT1mhp7jWEH32HIi95sr5kRKe4PiTtRWbuN/5UfS9hXOUEd6gSXgBa/gwpoMyLiMh6ym5gJxmSsyuCxaK71OUAyka0AC+i8H9YzjnofEsNfd5NyWWnCwVTGgF35/CoPbK9ykMXt5nDEQpWIaC1aZwnlvqnrXvLq7YV2Z2CDesijUMCLv0FNxQYgQWkWIl3fOAfrcq+N46xuJTh8I1XOCBi4nyRqjqEMUZVSyWIQvNlZAI4fr4TIyNTIlh93O12gHJ8AxUluMHkI2V+xwhJmVsviniEAOBtbsr8izuRykM7L7G4lRacl2SizxFiRd1llQUOjSx2BYN9lpVvyzjMkgtxsRiIDbIa0Fd5SFyz6apRXCvwGJV46yBxISFUhPVaeOyJQE/HpcsW8aO2wV8sZqgfhZI6m2w1IVKEbE4l2oRZ55mzTN2dNeZUknMHftZ6GK8iJgaZ6FmZ8il9KIFE+mC0M4VuZv4Ec7u4RlVDMs6N4JQ14urojqrrVXGc8TiU+XA4+yTqCJRgnt8RhU9CGAb4mLljAVJbArZlk9pHgYOmznUoIpeEffd4R16/+zo8p7Sj4EriplIkDxfEtklfqfbfkugOh+cVTM/spd7dXUeTB4h2duRwkneklhc6vvVthDq78dAFYnLPkMb5J6AqbO2Eivb4k40cr8i4rGNS/G6R/08O66WH3QuXPa0JS6RW9dVsO6z7baDk8WoJle51om7f5Yxbill706Mm/qjIC6KweoCU1c/lq7YWnU0IUY/K7H3GgTj6ih/oiKq+F+oqk5IyZOOK+MriI98WPnNjJggF5C/Hyg0Faf/edj8M4cn6mlZ/rgRuBe9XqLm7NChw3+GPwIMAA139Xg+K8vEAAAAAElFTkSuQmCC&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss18892 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABANJREFUeNrsWt1x4jAQNpl7P18FcSqIqSBmhvdABYEKiCvwUQFcBfZVEHhnBl8F8VUQSvB1cJJ3jddC/pOlQBg0YwYUIfbT/n27imXdxtcag947+DuPvXqa5TpYq3F0PmD+bsZeQ0OHvmbg/HMB+2CvDntS9iSaADn48PGDgUtVNvmmQQg+fjEBfmqBBaa9x08ue2KVbe6uNXhcOrDDdQJbjQ/XqjHrBuxLAYMIeVZg9xrhuJegsTx5ztjphuyxe2qI58LVJURFSnk4vfpgwr0qmRywmECgVMrAdJBgB095QmaTDPRqHBv7rnFgZUcPCc3igzP0pfTkwewW7LGJWfO168soW9oJXHDJ6gPwVQnv5wCrNrEDgnSFuXlfszPB7utYfw7EFioBOmLy9wuroE8Lz6ACRBPZXfatmvUD83cu+o0r5DiukT+SIpSve0RTtU2Y5kCTlkLl05dreX7unkeIiTkfS+VKGhJ7QDQYsb3mnw+srCludiMmSNKwPq5lE2DSewLOV81rA0VQtC/RFlSIax9q89UpuJGKz6lyRepT9aBg5BzQFnKbrGpOsj0Lgq3U3rtTNEGH+FTSImLSwPDSoiWQZHvn+Q9+07jGgmP0axcoRCAeMpMmcGtStgRmgfm7iUV7ie3GRKjdrEZzpFG20Nqki6hdKdWTQFxFfuhIErFDhHzBuQVbn0i0JAaJDfGxJ/xsBJh35Hh5ZANA7y0430YAu5dYBKxbjacINGVzG9SwZ9LHXFIM0tEEKs9fG8EkZUPc669KL0Q13P8j5gP8riwwfz/FsM3TwYisfTjOFz5Eq+epZM5guIewLf8x4HUjMm8ffUP0G25eMOcJ0S7CQ0grGkaiDNo0lta2yIrEGhFwe6kwwDEDsu8844XNFXSqH1ibjhFoYy6YmFNRiFLmErUIWJ16+V2jYl4RPzas+14KHNBvnGWmCma47RDl7rtqSyV4xCenWJ+UEzTbd+yBcNN8s8qXeU0Uy1MJIl2BbY/+U8UEytzQxnzlCKD3UlOr32trEhjN/ItGnyiESjElrCW5yq3hjouK39YMDKJWRMis7LSfJeY7zAIE/BfAVOIvXoW2ZqSaPpjUWM75ilrp9CLCJVrys+RMhVqNN5ikNw3MJZSQYYPAQMglMTWxEMzD/bCyrIe0MM3WwNpIkudcUvMduorZp+fxRqJfr8ZLTYOIp4ehyjZ9OsFz1Bj4AgSAqXL/HUw6FFLFSFU49fsxAED5IQ8Aqvdjs+y7IqgelxS6OsE8+b4KDCXKck9Vhwki6jOCoeF+jX7V6+ZF9/1YUJFwExJJ7YraKkZAsQ5xTNyPeUiTJla7mxSu2d+6r5LM3I+VQboSgCkhxLdxG2z8F2AAPtd0UKyJkPIAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss18893 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAzRJREFUeNrsWs2Nm0AUnl1RAFIKCK4gdLBwy9GuYL0VePeWm+1bbt5UYLsC7FtueCswqWDdQCTSQd5bfRM9IcADDD+OeNIIbI8f8837f4NSI4000kg16K7uHz99++nTZUkj/f3965PNRRHvFV2+0FgT76QOj/sGz2dQUxpzWkhgEVQgeC/r8mkCzO1Ao9w+gEnyb8bGSCU8qEPRri3Ebycab5bW9JnVG/cXGvuCeSmNA9ngxRgYgeIFv3ekbk2IwU0IXGqqiv4NgNI2mGsGjsGfi9ThkYZXoIoPDRbrCb4siaTOxhsBI1GvctT1QSzgLW9OA3cf42NCfMOcOfx70JZXlDt5atGGapHT4KFrGn+QeVgDxrxIIi9QtV3nwOCJVm2IiXi/NuXhVNB9T8SXV+1iYROB6pY8mxKbi9yNPeKEQHEAj4YYB6oAk+qxg8ocCFzYg8Qer0nNaWpTcBynLlFlQk2rSfDg6L7DXQ5s1m2DAIZqm7OFGPf/jcS2Bfe3Cwz9C1+kRz6+u11gCOo69v1AGvZRpOK32wMGRxGJLH2FWJggD4zadCaOZelMUYsFol5iIKGOhQjoMdSTnckFc44cD4tK/V4kRouL0ErYZPokLKFQlu64D0UmozeEnco78doMSWJuJvX6hUZLWpLFvBCINUBxc/TZNMHt0saehNcLykDlADyIXDMFr2EAg13MRD+iSgd3I8LBxGRDOvWKSIY1uGcTd44sRNd4M1ugrLt7LmNEOT83LD8UvKHVCqGNOHbE1aQFp/PG/WDjmKAq6qSBGcUuqHfQF7CiLOQfcHHmlapqHWeev1AGhyBtANMPdQnQVuUcbND3KXLH1HBjPjIa2hCOfRwOzn3YmCsAzjOpVSLmLMUmXPOg7Fi4UN1C2qe+VZE95BHeMiuBhbAX91og1zkmXc992ZjOAXdFCS2AHhDHpqqk4yuObmfIMWMTG7srYRaLGBP22ZgRUtLrkGd3YV4MdNRACa49AhgtqRj3yTW3bwLM66KUL/GwZwTwNUqb2JaNearBawkWKPt8oy5XkbtPVIOzqQ6p6MSz9K0BX5T6QyQGtK/75s5II400Ui79FWAANjYkpEJzM+AAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss18894 {
  color: #FFF;
  padding: 48px;
}
.jss18895 {
  color: rgba(0, 0, 0, 0.87);
  width: 100%;
  display: flex;
  z-index: 2;
  opacity: 0.5;
  position: fixed;
  align-items: center;
  padding-left: 16px;
  padding-right: 16px;
  background-color: #424242;
}
.jss18896 {
  height: 100vh;
  display: flex;
  position: relative;
  max-height: 1600px;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: linear-gradient(rgba(0,130,170,0), #424242), url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/1.20e79364.jpg&quot;);
}
.jss18897 {
  margin: -200px auto 0 auto;
  padding: 64px 0 48px;
  max-width: 1200px;
}
.jss18898 {
  width: 50%;
  color: #FFF;
}
@media (max-width:1279.95px) {
  .jss18898 {
    width: 100%;
    padding: 24px;
  }
}
.jss18899 {
  margin: -150px auto 0 auto;
  padding: 16px;
  position: relative;
  max-width: 1200px;
  background: #fafafa;
  justify-content: center;
}
.jss18900 {
  margin: 0 auto;
  padding: 24px;
  display: flex;
}
.jss18901 {
  margin-bottom: 24px;
}
.jss18902 {
  text-decoration: none;
}
.jss18903 {
  margin: 24px auto;
  max-width: 1200px;
}
@media (max-width:1279.95px) {
  .jss18903 {
    max-width: 1366px;
  }
}
.jss18904 {
  height: 160px;
  display: flex;
  align-items: center;
}
.jss18905 {
  margin: auto;
  display: grid;
}
.jss18906 {
  height: 100vh;
  display: flex;
  position: relative;
  overflow: hidden;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/2.adfa400c.jpg&quot;);
  background-position: center;
}
.jss18907 {
  color: #FFF;
  background-color: #03a9f4;
}
.jss18908 {
  color: #03a9f4;
  background-color: #fff;
}
.jss18909 {
  color: #03a9f4;
  padding: 0;
}
.jss18910 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss18910 {
    width: calc(100%);
  }
}
.jss18911 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss18911 {
    width: calc(100% - 300px);
  }
}
.jss18912 {
  display: none;
}
@media (min-width:600px) {
  .jss18912 {
    display: block;
  }
}
.jss18913 {
  color: #fff;
  position: relative;
  background: #03a9f4;
}
.jss18914 {
  width: 100%;
  position: relative;
  border-radius: 14px;
  background-color: rgba(255, 255, 255, 0.5);
}
.jss18914:hover {
  background-color: rgba(255, 255, 255, 0.7);
}
@media (min-width:600px) {
  .jss18914 {
    width: auto;
  }
}
.jss18915 {
  width: 72px;
  color: #212121;
  height: 100%;
  display: flex;
  position: absolute;
  align-items: center;
  pointer-events: none;
  justify-content: center;
}
.jss18916 {
  color: #fff;
  width: 100%;
}
.jss18917 {
  color: #212121;
  width: 100%;
  transition: width 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  padding-top: 8px;
  padding-left: 64px;
  padding-right: 8px;
  padding-bottom: 8px;
}
@media (min-width:600px) {
  .jss18917 {
    width: 80px;
  }
  .jss18917:focus {
    width: 200px;
  }
}
.jss18918 {
  color: #212121;
}
.jss18919 {
  background-color: #4fc3f7;
}
.jss18920 {
  margin-left: 12px;
  margin-right: 36px;
}
.jss18921 {
  display: none;
}
@media (min-width:960px) {
  .jss18922 {
    display: none;
  }
}
.jss18923 {
  width: 300px;
}
.jss18924 {
  display: flex;
  padding: 0 8px;
  min-height: 56px;
  align-items: center;
}
@media (min-width:0px) and (orientation: landscape) {
  .jss18924 {
    min-height: 48px;
  }
}
@media (min-width:600px) {
  .jss18924 {
    min-height: 64px;
  }
}
.jss18925 {
  margin-top: 65px;
  justify-content: flex-start;
}
.jss18926 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss18928 {
  width: 100%;
  height: 100%;
  opacity: 0.2;
  content: &quot;&quot;;
  display: block;
  position: absolute;
  background: linear-gradient(rgba(0,130,170,0), #03a9f4));
  background-size: cover;
  background-position: center center;
}
.jss18929 {
  width: 60px;
  overflow-x: hidden;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss18930 {
  color: inherit;
}
.jss18931 {
  margin-left: 56px;
}
.jss18932 {
  bottom: 16px;
}
.jss18933 {
  color: rgba(0, 0, 0, 0.87);
  background-size: cover;
  background-color: #fff;
  background-image: url(https://xdev.equine.co.id/apps/tessa/static/media/pattern2.f069796d.svg);
  background-repeat: no-repeat;
  background-position: inherit;
}
.jss18934 {
  color: #fff;
  background-color: #03a9f4;
}
.jss18935 {
  color: #fff;
  background-color: #ff9800;
}
.jss18936 {
  color: #fff;
  background-color: #f44336;
}
.jss18937 {
  color: #fff;
  background: #4caf50;
}
.jss18938 {
  flex: 1;
}
.jss18939 {
  padding: 16px;
  flex-grow: 1;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss18939 {
    margin-top: 68px;
  }
}
@media (max-width:959.95px) {
  .jss18939 {
    padding: 0;
    margin-top: 56px;
  }
}
@media (max-width:959.95px) {
  .jss18940 {
    margin-top: 68px;
  }
}
@media (min-width:960px) {
  .jss18940 {
    left: 50%;
    width: 982px;
    position: absolute;
    transform: translateX(-50%);
  }
}
@media (min-width:960px) {
  .jss18941 {
    margin-left: 300px;
    margin-right: 0;
  }
}
.jss18942 {
  margin-bottom: 56px;
}
.jss18943 {
  width: calc(100% - 0px);
  right: 0;
  bottom: 0;
  display: flex;
  position: fixed;
}
@media (min-width:960px) {
  .jss18943 {
    width: calc(100% - 300px);
  }
}
.jss18945 {
  background: #fff;
}
.jss18946 {
  margin-bottom: 60px;
}
.jss18947 {
  height: 4px;
}
.jss18948 {
  display: flex;
  align-items: center;
  justify-content: space-between;
}
.jss18949 {
  margin: 0 15px 0 0;
  padding: 16px 0;
  flex-grow: 1;
}
@media (min-width:600px) {
  .jss18949 {
    width: 30%;
    flex-grow: 0;
  }
}
.jss18950 {
  color: inherit;
  font-size: 0.6964285714285714rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: inherit;
  line-height: 1.66;
  letter-spacing: 0.03333em;
}
.jss18951 {
  color: #fff;
  background: #03a9f4;
}
.jss18952 {
  color: #fff;
  min-height: 40px;
  background: #ff9800;
  margin-bottom: 16px;
}
.jss18953 {
  height: 500px;
  display: flex;
  overflow: hidden;
  align-items: center;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss18953 {
    height: 300px;
  }
}
.jss18954 {
  height: 100%;
  margin: 0 auto;
}
.jss18955 {
  width: 25px;
  height: 25px;
  margin-right: 16px;
}
.jss18956 {
  transform: rotate(0deg);
  transition: transform 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
}
.jss18957 {
  transform: rotate(180deg);
}
.jss18958 {
  min-width: 350px;
}
.jss18959 {
  display: flex;
  flex-wrap: wrap;
}
.jss18960 {
  padding: 0;
}
@media (min-width:0px) {
  .jss18960 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss18960 {
    width: calc(100% / 2);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1280px) {
  .jss18960 {
    width: calc(100% / 3);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1920px) {
  .jss18960 {
    width: calc(100% / 4);
    padding: 0 16px 16px 0;
  }
}
.jss18961 {
  padding: 0;
}
@media (min-width:0px) {
  .jss18961 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss18961 {
    width: calc(100% / 2);
  }
}
@media (min-width:1280px) {
  .jss18961 {
    width: calc(100% - (100% / 3));
  }
}
@media (min-width:1920px) {
  .jss18961 {
    width: calc(100% - (100% / 4));
  }
}
.jss18962 {
  padding: 0;
}
@media (min-width:0px) {
  .jss18962 {
    width: 100%;
  }
}
.jss18963 {
  width: 100%;
  margin-bottom: 16px;
  vertical-align: middle;
}
.jss18964 {
  right: 16px;
  display: flex;
  position: absolute;
}
.jss18965 {
  right: 16px;
  bottom: 16px;
  position: fixed;
}
.jss18966 {
  float: right;
}
.jss18967 {
  margin: 0;
}
.jss18968 {
  margin: 8px;
}
.jss18969 {
  margin-left: 8px;
}
.jss18970 {
  margin-top: 8px;
}
.jss18971 {
  margin-right: 8px;
}
.jss18972 {
  margin-bottom: 8px;
}
.jss18973 {
  margin: 16px;
}
.jss18974 {
  margin-left: 16px;
}
.jss18975 {
  margin-top: 16px;
}
.jss18976 {
  margin-right: 16px;
}
.jss18977 {
  margin-bottom: 16px;
}
.jss18978 {
  margin: 24px;
}
.jss18979 {
  margin-left: 24px;
}
.jss18980 {
  margin-top: 24px;
}
.jss18981 {
  margin-right: 24px;
}
.jss18982 {
  margin-bottom: 24px;
}
.jss18983 {
  padding: 0;
}
.jss18984 {
  padding: 8px;
}
.jss18985 {
  padding-left: 8px;
}
.jss18986 {
  padding-top: 8px;
}
.jss18987 {
  padding-right: 8px;
}
.jss18988 {
  padding-bottom: 8px;
}
.jss18989 {
  padding: 16px;
}
.jss18990 {
  padding-left: 16px;
}
.jss18991 {
  padding-top: 16px;
}
.jss18992 {
  padding-right: 16px;
}
.jss18993 {
  padding-bottom: 16px;
}
.jss18994 {
  padding: 24px;
}
.jss18995 {
  padding-left: 24px;
}
.jss18996 {
  padding-top: 24px;
}
.jss18997 {
  padding-right: 24px;
}
.jss18998 {
  padding-bottom: 24px;
}
.jss18999 {
  background-color: #03a9f4;
}
.jss19000 {
  background-color: #ff9800;
}
.jss19001 {
  color: white;
  background-color: #f44336;
}
.jss19002 {
  color: #03a9f4;
}
.jss19003 {
  color: #ff9800;
}
.jss19004 {
  color: #f44336;
}
.jss19005 {
  text-decoration: line-through;
}
.jss19006 {
  padding: 3px;
}
.jss19007 {
  height: calc(100vh - 96px - 72px);
  overflow-x: auto;
}
.jss19008 {
  width: calc(100vw - 32px - 300px);
}
.jss19009 {
  height: calc(100vh - 56px - 68px);
  overflow-x: auto;
}
.jss19010 {
  width: calc(100vw);
}
.jss19011 {
  overflow-x: auto;
}
.jss19012 {
  table-layout: initial;
}
.jss19013 {
  top: 0;
  position: sticky;
  background-color: #fff;
}
.jss19014 {
  width: 3vw;
}
.jss19015 {
  width: 5vw;
}
.jss19016 {
  width: 10vw;
}
.jss19017 {
  width: 15vw;
}
.jss19018 {
  min-width: 15vw;
  max-width: 15vw;
}
@media (min-width:960px) {
  .jss19018 {
    min-width: calc((100vw - 300px - 48px) * 0.15);
    max-width: calc((100vw - 300px - 48px) * 0.15);
  }
}
.jss19019 {
  min-width: 25vw;
  max-width: 25vw;
}
@media (min-width:960px) {
  .jss19019 {
    min-width: calc((100vw - 300px - 48px) * 0.25);
    max-width: calc((100vw - 300px - 48px) * 0.25);
  }
}
.jss19020 {
  width: 25vw;
}
.jss19021 {
  width: 30vw;
}
.jss19022 {
  min-width: 45vw;
  max-width: 45vw;
}
@media (min-width:960px) {
  .jss19022 {
    min-width: calc((100vw - 300px - 48px) * 0.45);
    max-width: calc((100vw - 300px - 48px) * 0.45);
  }
}
.jss19023 {
  width: 100%;
  overflow-x: auto;
}
.jss19024 {
  width: 100%;
  margin-top: 24px;
  overflow-x: auto;
}
.jss19025 {
  min-width: 700px;
}
.jss19026 {
  width: 100%;
}
.jss19027 {
  min-width: 100px;
}
.jss19028 {
  color: rgba(0, 0, 0, 0.54);
  flex-shrink: 0;
}
.jss19029 {
  color: #2196f3;
}
.jss19030 {
  padding-top: 16px;
  padding-bottom: 16px;
}
.jss19032 {
  max-height: 100px;
}
.jss19033 {
  margin-top: -144px;
  margin-left: -56px;
  margin-right: -136px;
  margin-bottom: -64px;
}
.jss19034 {
  margin-top: -80px;
}
.jss19035 {
  width: 500px;
  height: 450px;
}
.jss19036 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss19037 {
  display: flex;
  box-sizing: border-box;
  align-items: center;
}
.jss19039:hover {
  background-color: #eeeeee;
}
.jss19040 {
  flex: 1;
}
.jss19041 {
  cursor: initial;
}
.jss19042 {
  background: #f44336;
}
.jss19043 {
  color: #fff;
}
.jss19044 {
  width: 100%;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
.jss19045 {
  max-height: 75vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss19046 {
  padding: 10px;
  text-align: center;
}
.jss19047 {
  padding: 0;
  vertical-align: top;
}
.jss19048 {
  margin-top: 8px;
  padding-left: 32px;
}
.jss19049 {
  top: 0;
  padding: 5px;
  position: sticky;
  overflow: hidden;
  max-width: 100px;
  max-height: 10px;
  text-align: center;
  background-color: #fff;
}
.jss19050 {
  max-height: 76vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss19051 {
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss19052 {
  padding: 0;
  margin-top: 8px;
}
.jss19053 {
  left: 0;
  width: 500px;
  z-index: 1;
  padding: 8px 16px 0 16px;
  min-width: 300px;
  background: #fff;
  vertical-align: top;
}
.jss19054 {
  padding: 8px 16px 0 16px;
  min-width: 300px;
  vertical-align: top;
}
.jss19055 {
  min-width: 300px;
}
.jss19056 {
  transform: rotate(180deg);
  writing-mode: vertical-rl;
}
.jss19057 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss19058 {
  cursor: pointer;
  transition: all .3s;
}
.jss19058:hover {
  background-color: #ebebeb;
}
.jss19059 {
  background-color: #ebebeb;
}
.jss19060 {
  transition: all .3s;
}
.jss19060:hover {
  transform: scale(1.03);
}
.jss19061 {
  padding: 0;
  transition: all .3s;
}
.jss19061:hover {
  transform: scale(1.03);
}
.jss19062 {
  display: grid;
  align-items: center;
  grid-column-gap: 1.5rem;
  grid-template-columns: 1fr max-content 1fr;
}
.jss19062::after, .jss19062::before {
  height: 1px;
  content: '';
  display: block;
  background-color: currentColor;
}
.jss19063 a {
  text-decoration: none;
}
.jss19064 {
  left: 50%;
  position: absolute;
  transform: ;
}
.jss19065 {
  display: -webkit-box;
  overflow: hidden;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;
}
.jss19066 {
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
.jss19067 {
  display: inline;
  text-transform: capitalize;
}
.jss19068 {
  text-decoration: none;
}
.jss19068 p:hover {
  color: #03a9f4;
}
.jss19069 {
  margin-left: 4px;
}
.jss19070 {
  padding: 0 16px;
  display: inline-flex;
  position: relative;
  vertical-align: middle;
}
.jss19071 {
  top: 0;
  color: #fff;
  right: 0;
  height: 20px;
  display: flex;
  padding: 0 4px;
  z-index: 1;
  position: absolute;
  flex-wrap: wrap;
  min-width: 20px;
  transform: translate(20px, -50%);
  box-sizing: border-box;
  transition: transform 225ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  align-items: center;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  align-content: center;
  border-radius: 10px;
  flex-direction: row;
  justify-content: center;
  background-color: #ff9800;
  transform-origin: 100% 0%;
}
.jss19072 {
  margin: 0;
  padding: 16px;
  border-bottom: 1px solid rgba(0, 0, 0, 0.12);
}
.jss19073 {
  top: 8px;
  right: 8px;
  color: #9e9e9e;
  position: absolute;
}
.jss19074 {
  margin: 0;
  padding: 16px;
}
.jss19075 {
  margin: 0;
  padding: 8px;
  border-top: 1px solid rgba(0, 0, 0, 0.12);
}
.jss19076 {
  font-size: 13px;
}
.jss19077 {
  padding: 0;
  overflow: hidden;
}
.jss19077 span {
  display: inline-flex;
}
.jss19077 span:hover {
  position: relative;
  animation: moveTextHorz 3s linear infinite;
}
@-webkit-keyframes moveTextHorz {
  0% {
    transform: translate(0, 0);
  }
  100% {
    transform: translate(-70%, 0);
  }
}
.jss19078 span {
  color: #f44336;
}
.jss19079 {
  width: 50px;
}
@media (min-width:600px) {
  .jss19079 {
    right: 90px;
  }
}
@media (min-width:960px) {
  .jss19079 {
    right: 15px;
  }
}
.jss19080 {
  cursor: pointer;
}
.jss19080:hover {
  background: #0288d1;
}
.jss19081 {
  margin-left: 10px;
  vertical-align: middle;
}
.jss19082 {
  width: 100%;
}
.jss19083 {
  width: 100%;
  margin: 0;
}
@media (min-width:960px) {
  .jss19084 {
    margin-left: auto;
    margin-right: 16px;
  }
}
@media (min-width:1280px) {
  .jss19084 {
    margin-left: auto;
    margin-right: 32px;
  }
}
@media (min-width:960px) {
  .jss19085 {
    transform: translateY(75%);
    margin-left: 16px;
  }
}
@media (min-width:1280px) {
  .jss19085 {
    transform: translateY(75%);
    margin-left: 32px;
  }
}
.jss19086 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
@media (max-width:1279.95px) {
  .jss19087 {
    display: none;
  }
}
@media (max-width:1919.95px) {
  .jss19088 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss19089 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss19090 {
    text-align: right;
  }
}
.jss19091 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss19091 {
    text-align: right;
  }
}
.jss19092 {
  padding: 0;
}
@media (min-width:1280px) {
  .jss19093 {
    margin-top: 0 !important;
  }
}
@media (min-width:1280px) {
  .jss19094 {
    margin-top: 64px;
  }
}
.jss19095 div textarea {
  overflow: auto;
}
.jss19096 {
  width: 70%;
}
@media (min-width:1280px) {
  .jss19096 {
    float: right;
  }
}
@media (min-width:1280px) {
  .jss19097 {
    float: right;
  }
}
@media (max-width:1279.95px) {
  .jss19097 {
    margin-top: 6px;
  }
}
@media (min-width:1280px) {
  .jss19098 {
    padding: 12px 0;
  }
}
@media (max-width:1279.95px) {
  .jss19098 {
    padding: 6px 0 3px;
  }
}
.jss19099 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss19099 {
    margin-top: 0 !important;
  }
}
.jss19099 div textarea {
  overflow: auto;
}
.jss19100 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss19101 {
  width: 57px;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
  overflow-x: hidden;
}
@media (min-width:600px) {
  .jss19101 {
    width: 73px;
  }
}
@media (min-width:960px) {
  .jss19102 {
    width: calc(100% - 73px);
    margin-left: 73px;
    margin-right: 0;
  }
}
@media (min-width:960px) {
  .jss19103 {
    margin-left: 73px;
    margin-right: 0;
  }
}
.jss19104 {
  bottom: 0px;
  z-index: 1;
  position: sticky;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss19104 {
    display: none;
  }
}
.jss19104:hover {
  background-color: rgb(235, 235, 235);
}
@media (max-width:959.95px) {
  .jss19105 {
    display: none;
  }
}
.jss19106 {
  top: 0;
  left: auto;
  width: 100%;
  right: 0;
  height: 100%;
  z-index: 1301;
  overflow: hidden;
  position: absolute;
  overflow-y: auto;
  transition: transform 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
@media (min-width:960px) {
  .jss19106 {
    width: 24vw;
  }
}
.jss19107 {
  cursor: pointer;
}
.jss19107:hover {
  background-color: #eeeeee;
}
.jss19108 {
  background-color: #e0e0e0 !important;
}
.jss19109 {
  transition: width 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
.jss19110 {
  border-collapse: separate;
}
.jss19111:hover {
  background-color: rgb(235, 235, 235);
}
.jss19112 {
  position: relative;
  min-height: 80vh;
}
.jss19112 .back-icon {
  top: 5px;
  left: 5px;
  z-index: 1;
  position: absolute;
}
.jss19112 > .ninebox-inner {
  width: 100%;
  position: relative;
  min-height: 80vh;
}
.jss19112 > .ninebox-inner .category-paper {
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #424242;
}
.jss19112 > .ninebox-inner .percentage-wrapper {
  width: 100%;
  height: 100%;
  display: flex;
  flex-wrap: wrap;
  box-sizing: border-box;
}
.jss19112 > .ninebox-inner .horizontal-category {
  height: 50px;
  margin: 0 60px;
  display: flex;
  align-items: center;
  justify-content: center;
}
.jss19112 > .ninebox-inner .vertical-category {
  width: 50px;
  height: calc(80vh - 120px);
  display: inline-flex;
  align-items: center;
  justify-content: center;
}
.jss19112 > .ninebox-inner .ninebox-content {
  width: calc(100% - 100px);
  height: calc(80vh - 120px);
  display: inline-block;
  position: relative;
}
.jss19112 > .ninebox-inner .ninebox-content .wrapper {
  width: 100%;
  height: 100%;
  margin: 0;
}
.jss19112 > .ninebox-inner .ninebox-content .wrapper .box {
  height: calc(100% / 3);
  padding: 10px;
}
.jss19112 > .ninebox-inner .ninebox-content .wrapper .box .has-item {
  padding: 26px 10px !important;
}
.jss19112 > .ninebox-inner .ninebox-content .wrapper .box .disabled {
  background: #d1d1d1;
}
.jss19112 > .ninebox-inner .ninebox-content .wrapper .box .card {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 30px;
  position: relative;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  -webkit-transition: all .4s ease-out;
}
.jss19112 > .ninebox-inner .ninebox-content .wrapper .box .card h6 {
  color: rgba(0, 0, 0, 0.87);
}
.jss19112 > .ninebox-inner .ninebox-content .wrapper .box .card .total-person {
  top: 5px;
  right: 5px;
  width: 15px;
  height: 15px;
  position: absolute;
}
.jss19112 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list {
  width: 100%;
  height: 100%;
  display: flex;
  overflow-y: auto;
  align-items: center;
}
.jss19112 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list > ol {
  height: 100%;
  margin: 0;
  padding: 0;
  text-align: left;
  list-style: none;
}
.jss19112 > .ninebox-inner .ninebox-content .wrapper .box .active:hover {
  cursor: pointer;
  transform: scale(1.1);
}
.jss19112 > .ninebox-inner .vertical-category .text-rotate {
  transform: rotate(270deg);
}
.jss19112 > .ninebox-inner .vertical-category.left {
  padding: 10px 0;
  margin-right: 9px;
}
.jss19112 > .ninebox-inner .vertical-category.right {
  margin-left: 9px;
}
.jss19112 > .ninebox-inner .vertical-category .text-rotate.nowrap {
  white-space: nowrap;
}
.jss19112 > .ninebox-inner .horizontal-category.top {
  padding: 0 9px;
  margin-bottom: 9px;
}
.jss19112 > .ninebox-inner .horizontal-category.bottom {
  margin-top: 9px;
}
.jss19112 > .ninebox-inner .percentage-wrapper > .percentage-item {
  padding: 0 10px;
  flex-grow: 0;
  max-width: calc(100% / 3);
  flex-basis: calc(100% / 3);
}
.jss19112 > .ninebox-inner .percentage-wrapper > .percentage-item.horizontal {
  height: calc(100% / 3);
  padding: 10px 0;
  max-width: 100%;
}
.jss19112 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 10px;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  background-color: #424242;
  -webkit-transition: all .4s ease-out;
}
.jss19112 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper > p {
  color: #fff;
}
.jss19112 > .ninebox-inner .category-paper > p {
  color: #fff;
}
.jss19113 {
  max-height: 45vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss19113 .table {
  border-collapse: separate;
}
.jss19113 .table .sticky-head {
  top: 0;
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss19114 {
  height: 44vh;
  overflow: auto;
}
.jss19115 {
  width: 100%;
  height: 50vh;
  overflow: auto;
}
.jss19116 {
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss19117 {
  width: 100%;
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss19118 {
  max-width: 82%;
}

@media (min-width:960px) {
  ::-webkit-scrollbar {
    width: 5px;
    height: 5px;
  }
  ::-webkit-scrollbar-track {
    background: rgba(250, 250, 250, 0);
  }
  ::-webkit-scrollbar-thumb {
    background: #eeeeee;
  }
  ::-webkit-scrollbar-thumb:hover {
    background: #e0e0e0;
  }
}
.jss18634 {
  width: 100%;
  z-index: 1;
  overflow: hidden;
  margin-bottom: 0;
}
.jss18635 {
  background-color: #424242;
}
.jss18636 {
  height: 70px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/etg.9362c8fb.svg&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss18637 {
  height: 150px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/tessa.55f0b81f.svg&quot;);
  background-repeat: no-repeat;
}
.jss18638 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2hJREFUeNrsWtGR2jAQ1TEU4FQQUsGRCuBm+IerAKgguAKgAnMVHFcBvn/PnFwBvg6cCuIOEslacRvFyJJswL74zQgDNrKedvftSoaQDh06dHDAnfGVfjRjr0PWRg3kkbD2RIJJakfMjzbsdd0CQz0yciF/0zf8wRyOKbSmgXuSx1rAmhWxgbSdnJFG4cOj5DhJz7KLrC3i0a8wSwM8QzcRjGCS1U+MkLcbE9uztjQn5kcyEFMsn5q4uxW09+8hQh5rR/buCNY4OtxsBzMpEaLZ/cIm6g6uyZTrtih+d6C8/PxDLuEiT1mhp7jWEH32HIi95sr5kRKe4PiTtRWbuN/5UfS9hXOUEd6gSXgBa/gwpoMyLiMh6ym5gJxmSsyuCxaK71OUAyka0AC+i8H9YzjnofEsNfd5NyWWnCwVTGgF35/CoPbK9ykMXt5nDEQpWIaC1aZwnlvqnrXvLq7YV2Z2CDesijUMCLv0FNxQYgQWkWIl3fOAfrcq+N46xuJTh8I1XOCBi4nyRqjqEMUZVSyWIQvNlZAI4fr4TIyNTIlh93O12gHJ8AxUluMHkI2V+xwhJmVsviniEAOBtbsr8izuRykM7L7G4lRacl2SizxFiRd1llQUOjSx2BYN9lpVvyzjMkgtxsRiIDbIa0Fd5SFyz6apRXCvwGJV46yBxISFUhPVaeOyJQE/HpcsW8aO2wV8sZqgfhZI6m2w1IVKEbE4l2oRZ55mzTN2dNeZUknMHftZ6GK8iJgaZ6FmZ8il9KIFE+mC0M4VuZv4Ec7u4RlVDMs6N4JQ14urojqrrVXGc8TiU+XA4+yTqCJRgnt8RhU9CGAb4mLljAVJbArZlk9pHgYOmznUoIpeEffd4R16/+zo8p7Sj4EriplIkDxfEtklfqfbfkugOh+cVTM/spd7dXUeTB4h2duRwkneklhc6vvVthDq78dAFYnLPkMb5J6AqbO2Eivb4k40cr8i4rGNS/G6R/08O66WH3QuXPa0JS6RW9dVsO6z7baDk8WoJle51om7f5Yxbill706Mm/qjIC6KweoCU1c/lq7YWnU0IUY/K7H3GgTj6ih/oiKq+F+oqk5IyZOOK+MriI98WPnNjJggF5C/Hyg0Faf/edj8M4cn6mlZ/rgRuBe9XqLm7NChw3+GPwIMAA139Xg+K8vEAAAAAElFTkSuQmCC&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss18639 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABANJREFUeNrsWt1x4jAQNpl7P18FcSqIqSBmhvdABYEKiCvwUQFcBfZVEHhnBl8F8VUQSvB1cJJ3jddC/pOlQBg0YwYUIfbT/n27imXdxtcag947+DuPvXqa5TpYq3F0PmD+bsZeQ0OHvmbg/HMB+2CvDntS9iSaADn48PGDgUtVNvmmQQg+fjEBfmqBBaa9x08ue2KVbe6uNXhcOrDDdQJbjQ/XqjHrBuxLAYMIeVZg9xrhuJegsTx5ztjphuyxe2qI58LVJURFSnk4vfpgwr0qmRywmECgVMrAdJBgB095QmaTDPRqHBv7rnFgZUcPCc3igzP0pfTkwewW7LGJWfO168soW9oJXHDJ6gPwVQnv5wCrNrEDgnSFuXlfszPB7utYfw7EFioBOmLy9wuroE8Lz6ACRBPZXfatmvUD83cu+o0r5DiukT+SIpSve0RTtU2Y5kCTlkLl05dreX7unkeIiTkfS+VKGhJ7QDQYsb3mnw+srCludiMmSNKwPq5lE2DSewLOV81rA0VQtC/RFlSIax9q89UpuJGKz6lyRepT9aBg5BzQFnKbrGpOsj0Lgq3U3rtTNEGH+FTSImLSwPDSoiWQZHvn+Q9+07jGgmP0axcoRCAeMpMmcGtStgRmgfm7iUV7ie3GRKjdrEZzpFG20Nqki6hdKdWTQFxFfuhIErFDhHzBuQVbn0i0JAaJDfGxJ/xsBJh35Hh5ZANA7y0430YAu5dYBKxbjacINGVzG9SwZ9LHXFIM0tEEKs9fG8EkZUPc669KL0Q13P8j5gP8riwwfz/FsM3TwYisfTjOFz5Eq+epZM5guIewLf8x4HUjMm8ffUP0G25eMOcJ0S7CQ0grGkaiDNo0lta2yIrEGhFwe6kwwDEDsu8844XNFXSqH1ibjhFoYy6YmFNRiFLmErUIWJ16+V2jYl4RPzas+14KHNBvnGWmCma47RDl7rtqSyV4xCenWJ+UEzTbd+yBcNN8s8qXeU0Uy1MJIl2BbY/+U8UEytzQxnzlCKD3UlOr32trEhjN/ItGnyiESjElrCW5yq3hjouK39YMDKJWRMis7LSfJeY7zAIE/BfAVOIvXoW2ZqSaPpjUWM75ilrp9CLCJVrys+RMhVqNN5ikNw3MJZSQYYPAQMglMTWxEMzD/bCyrIe0MM3WwNpIkudcUvMduorZp+fxRqJfr8ZLTYOIp4ehyjZ9OsFz1Bj4AgSAqXL/HUw6FFLFSFU49fsxAED5IQ8Aqvdjs+y7IqgelxS6OsE8+b4KDCXKck9Vhwki6jOCoeF+jX7V6+ZF9/1YUJFwExJJ7YraKkZAsQ5xTNyPeUiTJla7mxSu2d+6r5LM3I+VQboSgCkhxLdxG2z8F2AAPtd0UKyJkPIAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss18640 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAzRJREFUeNrsWs2Nm0AUnl1RAFIKCK4gdLBwy9GuYL0VePeWm+1bbt5UYLsC7FtueCswqWDdQCTSQd5bfRM9IcADDD+OeNIIbI8f8837f4NSI4000kg16K7uHz99++nTZUkj/f3965PNRRHvFV2+0FgT76QOj/sGz2dQUxpzWkhgEVQgeC/r8mkCzO1Ao9w+gEnyb8bGSCU8qEPRri3Ebycab5bW9JnVG/cXGvuCeSmNA9ngxRgYgeIFv3ekbk2IwU0IXGqqiv4NgNI2mGsGjsGfi9ThkYZXoIoPDRbrCb4siaTOxhsBI1GvctT1QSzgLW9OA3cf42NCfMOcOfx70JZXlDt5atGGapHT4KFrGn+QeVgDxrxIIi9QtV3nwOCJVm2IiXi/NuXhVNB9T8SXV+1iYROB6pY8mxKbi9yNPeKEQHEAj4YYB6oAk+qxg8ocCFzYg8Qer0nNaWpTcBynLlFlQk2rSfDg6L7DXQ5s1m2DAIZqm7OFGPf/jcS2Bfe3Cwz9C1+kRz6+u11gCOo69v1AGvZRpOK32wMGRxGJLH2FWJggD4zadCaOZelMUYsFol5iIKGOhQjoMdSTnckFc44cD4tK/V4kRouL0ErYZPokLKFQlu64D0UmozeEnco78doMSWJuJvX6hUZLWpLFvBCINUBxc/TZNMHt0saehNcLykDlADyIXDMFr2EAg13MRD+iSgd3I8LBxGRDOvWKSIY1uGcTd44sRNd4M1ugrLt7LmNEOT83LD8UvKHVCqGNOHbE1aQFp/PG/WDjmKAq6qSBGcUuqHfQF7CiLOQfcHHmlapqHWeev1AGhyBtANMPdQnQVuUcbND3KXLH1HBjPjIa2hCOfRwOzn3YmCsAzjOpVSLmLMUmXPOg7Fi4UN1C2qe+VZE95BHeMiuBhbAX91og1zkmXc992ZjOAXdFCS2AHhDHpqqk4yuObmfIMWMTG7srYRaLGBP22ZgRUtLrkGd3YV4MdNRACa49AhgtqRj3yTW3bwLM66KUL/GwZwTwNUqb2JaNearBawkWKPt8oy5XkbtPVIOzqQ6p6MSz9K0BX5T6QyQGtK/75s5II400Ui79FWAANjYkpEJzM+AAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss18641 {
  color: #FFF;
  padding: 48px;
}
.jss18642 {
  color: rgba(0, 0, 0, 0.87);
  width: 100%;
  display: flex;
  z-index: 2;
  opacity: 0.5;
  position: fixed;
  align-items: center;
  padding-left: 16px;
  padding-right: 16px;
  background-color: #424242;
}
.jss18643 {
  height: 100vh;
  display: flex;
  position: relative;
  max-height: 1600px;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: linear-gradient(rgba(0,130,170,0), #424242), url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/1.20e79364.jpg&quot;);
}
.jss18644 {
  margin: -200px auto 0 auto;
  padding: 64px 0 48px;
  max-width: 1200px;
}
.jss18645 {
  width: 50%;
  color: #FFF;
}
@media (max-width:1279.95px) {
  .jss18645 {
    width: 100%;
    padding: 24px;
  }
}
.jss18646 {
  margin: -150px auto 0 auto;
  padding: 16px;
  position: relative;
  max-width: 1200px;
  background: #fafafa;
  justify-content: center;
}
.jss18647 {
  margin: 0 auto;
  padding: 24px;
  display: flex;
}
.jss18648 {
  margin-bottom: 24px;
}
.jss18649 {
  text-decoration: none;
}
.jss18650 {
  margin: 24px auto;
  max-width: 1200px;
}
@media (max-width:1279.95px) {
  .jss18650 {
    max-width: 1366px;
  }
}
.jss18651 {
  height: 160px;
  display: flex;
  align-items: center;
}
.jss18652 {
  margin: auto;
  display: grid;
}
.jss18653 {
  height: 100vh;
  display: flex;
  position: relative;
  overflow: hidden;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/2.adfa400c.jpg&quot;);
  background-position: center;
}
.jss18654 {
  color: #FFF;
  background-color: #03a9f4;
}
.jss18655 {
  color: #03a9f4;
  background-color: #fff;
}
.jss18656 {
  color: #03a9f4;
  padding: 0;
}
.jss18657 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss18657 {
    width: calc(100%);
  }
}
.jss18658 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss18658 {
    width: calc(100% - 300px);
  }
}
.jss18659 {
  display: none;
}
@media (min-width:600px) {
  .jss18659 {
    display: block;
  }
}
.jss18660 {
  color: #fff;
  position: relative;
  background: #03a9f4;
}
.jss18661 {
  width: 100%;
  position: relative;
  border-radius: 14px;
  background-color: rgba(255, 255, 255, 0.5);
}
.jss18661:hover {
  background-color: rgba(255, 255, 255, 0.7);
}
@media (min-width:600px) {
  .jss18661 {
    width: auto;
  }
}
.jss18662 {
  width: 72px;
  color: #212121;
  height: 100%;
  display: flex;
  position: absolute;
  align-items: center;
  pointer-events: none;
  justify-content: center;
}
.jss18663 {
  color: #fff;
  width: 100%;
}
.jss18664 {
  color: #212121;
  width: 100%;
  transition: width 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  padding-top: 8px;
  padding-left: 64px;
  padding-right: 8px;
  padding-bottom: 8px;
}
@media (min-width:600px) {
  .jss18664 {
    width: 80px;
  }
  .jss18664:focus {
    width: 200px;
  }
}
.jss18665 {
  color: #212121;
}
.jss18666 {
  background-color: #4fc3f7;
}
.jss18667 {
  margin-left: 12px;
  margin-right: 36px;
}
.jss18668 {
  display: none;
}
@media (min-width:960px) {
  .jss18669 {
    display: none;
  }
}
.jss18670 {
  width: 300px;
}
.jss18671 {
  display: flex;
  padding: 0 8px;
  min-height: 56px;
  align-items: center;
}
@media (min-width:0px) and (orientation: landscape) {
  .jss18671 {
    min-height: 48px;
  }
}
@media (min-width:600px) {
  .jss18671 {
    min-height: 64px;
  }
}
.jss18672 {
  margin-top: 65px;
  justify-content: flex-start;
}
.jss18673 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss18675 {
  width: 100%;
  height: 100%;
  opacity: 0.2;
  content: &quot;&quot;;
  display: block;
  position: absolute;
  background: linear-gradient(rgba(0,130,170,0), #03a9f4));
  background-size: cover;
  background-position: center center;
}
.jss18676 {
  width: 60px;
  overflow-x: hidden;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss18677 {
  color: inherit;
}
.jss18678 {
  margin-left: 56px;
}
.jss18679 {
  bottom: 16px;
}
.jss18680 {
  color: rgba(0, 0, 0, 0.87);
  background-size: cover;
  background-color: #fff;
  background-image: url(https://xdev.equine.co.id/apps/tessa/static/media/pattern2.f069796d.svg);
  background-repeat: no-repeat;
  background-position: inherit;
}
.jss18681 {
  color: #fff;
  background-color: #03a9f4;
}
.jss18682 {
  color: #fff;
  background-color: #ff9800;
}
.jss18683 {
  color: #fff;
  background-color: #f44336;
}
.jss18684 {
  color: #fff;
  background: #4caf50;
}
.jss18685 {
  flex: 1;
}
.jss18686 {
  padding: 16px;
  flex-grow: 1;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss18686 {
    margin-top: 68px;
  }
}
@media (max-width:959.95px) {
  .jss18686 {
    padding: 0;
    margin-top: 56px;
  }
}
@media (max-width:959.95px) {
  .jss18687 {
    margin-top: 68px;
  }
}
@media (min-width:960px) {
  .jss18687 {
    left: 50%;
    width: 982px;
    position: absolute;
    transform: translateX(-50%);
  }
}
@media (min-width:960px) {
  .jss18688 {
    margin-left: 300px;
    margin-right: 0;
  }
}
.jss18689 {
  margin-bottom: 56px;
}
.jss18690 {
  width: calc(100% - 0px);
  right: 0;
  bottom: 0;
  display: flex;
  position: fixed;
}
@media (min-width:960px) {
  .jss18690 {
    width: calc(100% - 300px);
  }
}
.jss18692 {
  background: #fff;
}
.jss18693 {
  margin-bottom: 60px;
}
.jss18694 {
  height: 4px;
}
.jss18695 {
  display: flex;
  align-items: center;
  justify-content: space-between;
}
.jss18696 {
  margin: 0 15px 0 0;
  padding: 16px 0;
  flex-grow: 1;
}
@media (min-width:600px) {
  .jss18696 {
    width: 30%;
    flex-grow: 0;
  }
}
.jss18697 {
  color: inherit;
  font-size: 0.6964285714285714rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: inherit;
  line-height: 1.66;
  letter-spacing: 0.03333em;
}
.jss18698 {
  color: #fff;
  background: #03a9f4;
}
.jss18699 {
  color: #fff;
  min-height: 40px;
  background: #ff9800;
  margin-bottom: 16px;
}
.jss18700 {
  height: 500px;
  display: flex;
  overflow: hidden;
  align-items: center;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss18700 {
    height: 300px;
  }
}
.jss18701 {
  height: 100%;
  margin: 0 auto;
}
.jss18702 {
  width: 25px;
  height: 25px;
  margin-right: 16px;
}
.jss18703 {
  transform: rotate(0deg);
  transition: transform 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
}
.jss18704 {
  transform: rotate(180deg);
}
.jss18705 {
  min-width: 350px;
}
.jss18706 {
  display: flex;
  flex-wrap: wrap;
}
.jss18707 {
  padding: 0;
}
@media (min-width:0px) {
  .jss18707 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss18707 {
    width: calc(100% / 2);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1280px) {
  .jss18707 {
    width: calc(100% / 3);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1920px) {
  .jss18707 {
    width: calc(100% / 4);
    padding: 0 16px 16px 0;
  }
}
.jss18708 {
  padding: 0;
}
@media (min-width:0px) {
  .jss18708 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss18708 {
    width: calc(100% / 2);
  }
}
@media (min-width:1280px) {
  .jss18708 {
    width: calc(100% - (100% / 3));
  }
}
@media (min-width:1920px) {
  .jss18708 {
    width: calc(100% - (100% / 4));
  }
}
.jss18709 {
  padding: 0;
}
@media (min-width:0px) {
  .jss18709 {
    width: 100%;
  }
}
.jss18710 {
  width: 100%;
  margin-bottom: 16px;
  vertical-align: middle;
}
.jss18711 {
  right: 16px;
  display: flex;
  position: absolute;
}
.jss18712 {
  right: 16px;
  bottom: 16px;
  position: fixed;
}
.jss18713 {
  float: right;
}
.jss18714 {
  margin: 0;
}
.jss18715 {
  margin: 8px;
}
.jss18716 {
  margin-left: 8px;
}
.jss18717 {
  margin-top: 8px;
}
.jss18718 {
  margin-right: 8px;
}
.jss18719 {
  margin-bottom: 8px;
}
.jss18720 {
  margin: 16px;
}
.jss18721 {
  margin-left: 16px;
}
.jss18722 {
  margin-top: 16px;
}
.jss18723 {
  margin-right: 16px;
}
.jss18724 {
  margin-bottom: 16px;
}
.jss18725 {
  margin: 24px;
}
.jss18726 {
  margin-left: 24px;
}
.jss18727 {
  margin-top: 24px;
}
.jss18728 {
  margin-right: 24px;
}
.jss18729 {
  margin-bottom: 24px;
}
.jss18730 {
  padding: 0;
}
.jss18731 {
  padding: 8px;
}
.jss18732 {
  padding-left: 8px;
}
.jss18733 {
  padding-top: 8px;
}
.jss18734 {
  padding-right: 8px;
}
.jss18735 {
  padding-bottom: 8px;
}
.jss18736 {
  padding: 16px;
}
.jss18737 {
  padding-left: 16px;
}
.jss18738 {
  padding-top: 16px;
}
.jss18739 {
  padding-right: 16px;
}
.jss18740 {
  padding-bottom: 16px;
}
.jss18741 {
  padding: 24px;
}
.jss18742 {
  padding-left: 24px;
}
.jss18743 {
  padding-top: 24px;
}
.jss18744 {
  padding-right: 24px;
}
.jss18745 {
  padding-bottom: 24px;
}
.jss18746 {
  background-color: #03a9f4;
}
.jss18747 {
  background-color: #ff9800;
}
.jss18748 {
  color: white;
  background-color: #f44336;
}
.jss18749 {
  color: #03a9f4;
}
.jss18750 {
  color: #ff9800;
}
.jss18751 {
  color: #f44336;
}
.jss18752 {
  text-decoration: line-through;
}
.jss18753 {
  padding: 3px;
}
.jss18754 {
  height: calc(100vh - 96px - 72px);
  overflow-x: auto;
}
.jss18755 {
  width: calc(100vw - 32px - 300px);
}
.jss18756 {
  height: calc(100vh - 56px - 68px);
  overflow-x: auto;
}
.jss18757 {
  width: calc(100vw);
}
.jss18758 {
  overflow-x: auto;
}
.jss18759 {
  table-layout: initial;
}
.jss18760 {
  top: 0;
  position: sticky;
  background-color: #fff;
}
.jss18761 {
  width: 3vw;
}
.jss18762 {
  width: 5vw;
}
.jss18763 {
  width: 10vw;
}
.jss18764 {
  width: 15vw;
}
.jss18765 {
  min-width: 15vw;
  max-width: 15vw;
}
@media (min-width:960px) {
  .jss18765 {
    min-width: calc((100vw - 300px - 48px) * 0.15);
    max-width: calc((100vw - 300px - 48px) * 0.15);
  }
}
.jss18766 {
  min-width: 25vw;
  max-width: 25vw;
}
@media (min-width:960px) {
  .jss18766 {
    min-width: calc((100vw - 300px - 48px) * 0.25);
    max-width: calc((100vw - 300px - 48px) * 0.25);
  }
}
.jss18767 {
  width: 25vw;
}
.jss18768 {
  width: 30vw;
}
.jss18769 {
  min-width: 45vw;
  max-width: 45vw;
}
@media (min-width:960px) {
  .jss18769 {
    min-width: calc((100vw - 300px - 48px) * 0.45);
    max-width: calc((100vw - 300px - 48px) * 0.45);
  }
}
.jss18770 {
  width: 100%;
  overflow-x: auto;
}
.jss18771 {
  width: 100%;
  margin-top: 24px;
  overflow-x: auto;
}
.jss18772 {
  min-width: 700px;
}
.jss18773 {
  width: 100%;
}
.jss18774 {
  min-width: 100px;
}
.jss18775 {
  color: rgba(0, 0, 0, 0.54);
  flex-shrink: 0;
}
.jss18776 {
  color: #2196f3;
}
.jss18777 {
  padding-top: 16px;
  padding-bottom: 16px;
}
.jss18779 {
  max-height: 100px;
}
.jss18780 {
  margin-top: -144px;
  margin-left: -56px;
  margin-right: -136px;
  margin-bottom: -64px;
}
.jss18781 {
  margin-top: -80px;
}
.jss18782 {
  width: 500px;
  height: 450px;
}
.jss18783 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss18784 {
  display: flex;
  box-sizing: border-box;
  align-items: center;
}
.jss18786:hover {
  background-color: #eeeeee;
}
.jss18787 {
  flex: 1;
}
.jss18788 {
  cursor: initial;
}
.jss18789 {
  background: #f44336;
}
.jss18790 {
  color: #fff;
}
.jss18791 {
  width: 100%;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
.jss18792 {
  max-height: 75vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss18793 {
  padding: 10px;
  text-align: center;
}
.jss18794 {
  padding: 0;
  vertical-align: top;
}
.jss18795 {
  margin-top: 8px;
  padding-left: 32px;
}
.jss18796 {
  top: 0;
  padding: 5px;
  position: sticky;
  overflow: hidden;
  max-width: 100px;
  max-height: 10px;
  text-align: center;
  background-color: #fff;
}
.jss18797 {
  max-height: 76vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss18798 {
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss18799 {
  padding: 0;
  margin-top: 8px;
}
.jss18800 {
  left: 0;
  width: 500px;
  z-index: 1;
  padding: 8px 16px 0 16px;
  min-width: 300px;
  background: #fff;
  vertical-align: top;
}
.jss18801 {
  padding: 8px 16px 0 16px;
  min-width: 300px;
  vertical-align: top;
}
.jss18802 {
  min-width: 300px;
}
.jss18803 {
  transform: rotate(180deg);
  writing-mode: vertical-rl;
}
.jss18804 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss18805 {
  cursor: pointer;
  transition: all .3s;
}
.jss18805:hover {
  background-color: #ebebeb;
}
.jss18806 {
  background-color: #ebebeb;
}
.jss18807 {
  transition: all .3s;
}
.jss18807:hover {
  transform: scale(1.03);
}
.jss18808 {
  padding: 0;
  transition: all .3s;
}
.jss18808:hover {
  transform: scale(1.03);
}
.jss18809 {
  display: grid;
  align-items: center;
  grid-column-gap: 1.5rem;
  grid-template-columns: 1fr max-content 1fr;
}
.jss18809::after, .jss18809::before {
  height: 1px;
  content: '';
  display: block;
  background-color: currentColor;
}
.jss18810 a {
  text-decoration: none;
}
.jss18811 {
  left: 50%;
  position: absolute;
  transform: ;
}
.jss18812 {
  display: -webkit-box;
  overflow: hidden;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;
}
.jss18813 {
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
.jss18814 {
  display: inline;
  text-transform: capitalize;
}
.jss18815 {
  text-decoration: none;
}
.jss18815 p:hover {
  color: #03a9f4;
}
.jss18816 {
  margin-left: 4px;
}
.jss18817 {
  padding: 0 16px;
  display: inline-flex;
  position: relative;
  vertical-align: middle;
}
.jss18818 {
  top: 0;
  color: #fff;
  right: 0;
  height: 20px;
  display: flex;
  padding: 0 4px;
  z-index: 1;
  position: absolute;
  flex-wrap: wrap;
  min-width: 20px;
  transform: translate(20px, -50%);
  box-sizing: border-box;
  transition: transform 225ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  align-items: center;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  align-content: center;
  border-radius: 10px;
  flex-direction: row;
  justify-content: center;
  background-color: #ff9800;
  transform-origin: 100% 0%;
}
.jss18819 {
  margin: 0;
  padding: 16px;
  border-bottom: 1px solid rgba(0, 0, 0, 0.12);
}
.jss18820 {
  top: 8px;
  right: 8px;
  color: #9e9e9e;
  position: absolute;
}
.jss18821 {
  margin: 0;
  padding: 16px;
}
.jss18822 {
  margin: 0;
  padding: 8px;
  border-top: 1px solid rgba(0, 0, 0, 0.12);
}
.jss18823 {
  font-size: 13px;
}
.jss18824 {
  padding: 0;
  overflow: hidden;
}
.jss18824 span {
  display: inline-flex;
}
.jss18824 span:hover {
  position: relative;
  animation: moveTextHorz 3s linear infinite;
}
@-webkit-keyframes moveTextHorz {
  0% {
    transform: translate(0, 0);
  }
  100% {
    transform: translate(-70%, 0);
  }
}
.jss18825 span {
  color: #f44336;
}
.jss18826 {
  width: 50px;
}
@media (min-width:600px) {
  .jss18826 {
    right: 90px;
  }
}
@media (min-width:960px) {
  .jss18826 {
    right: 15px;
  }
}
.jss18827 {
  cursor: pointer;
}
.jss18827:hover {
  background: #0288d1;
}
.jss18828 {
  margin-left: 10px;
  vertical-align: middle;
}
.jss18829 {
  width: 100%;
}
.jss18830 {
  width: 100%;
  margin: 0;
}
@media (min-width:960px) {
  .jss18831 {
    margin-left: auto;
    margin-right: 16px;
  }
}
@media (min-width:1280px) {
  .jss18831 {
    margin-left: auto;
    margin-right: 32px;
  }
}
@media (min-width:960px) {
  .jss18832 {
    transform: translateY(75%);
    margin-left: 16px;
  }
}
@media (min-width:1280px) {
  .jss18832 {
    transform: translateY(75%);
    margin-left: 32px;
  }
}
.jss18833 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
@media (max-width:1279.95px) {
  .jss18834 {
    display: none;
  }
}
@media (max-width:1919.95px) {
  .jss18835 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss18836 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss18837 {
    text-align: right;
  }
}
.jss18838 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss18838 {
    text-align: right;
  }
}
.jss18839 {
  padding: 0;
}
@media (min-width:1280px) {
  .jss18840 {
    margin-top: 0 !important;
  }
}
@media (min-width:1280px) {
  .jss18841 {
    margin-top: 64px;
  }
}
.jss18842 div textarea {
  overflow: auto;
}
.jss18843 {
  width: 70%;
}
@media (min-width:1280px) {
  .jss18843 {
    float: right;
  }
}
@media (min-width:1280px) {
  .jss18844 {
    float: right;
  }
}
@media (max-width:1279.95px) {
  .jss18844 {
    margin-top: 6px;
  }
}
@media (min-width:1280px) {
  .jss18845 {
    padding: 12px 0;
  }
}
@media (max-width:1279.95px) {
  .jss18845 {
    padding: 6px 0 3px;
  }
}
.jss18846 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss18846 {
    margin-top: 0 !important;
  }
}
.jss18846 div textarea {
  overflow: auto;
}
.jss18847 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss18848 {
  width: 57px;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
  overflow-x: hidden;
}
@media (min-width:600px) {
  .jss18848 {
    width: 73px;
  }
}
@media (min-width:960px) {
  .jss18849 {
    width: calc(100% - 73px);
    margin-left: 73px;
    margin-right: 0;
  }
}
@media (min-width:960px) {
  .jss18850 {
    margin-left: 73px;
    margin-right: 0;
  }
}
.jss18851 {
  bottom: 0px;
  z-index: 1;
  position: sticky;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss18851 {
    display: none;
  }
}
.jss18851:hover {
  background-color: rgb(235, 235, 235);
}
@media (max-width:959.95px) {
  .jss18852 {
    display: none;
  }
}
.jss18853 {
  top: 0;
  left: auto;
  width: 100%;
  right: 0;
  height: 100%;
  z-index: 1301;
  overflow: hidden;
  position: absolute;
  overflow-y: auto;
  transition: transform 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
@media (min-width:960px) {
  .jss18853 {
    width: 24vw;
  }
}
.jss18854 {
  cursor: pointer;
}
.jss18854:hover {
  background-color: #eeeeee;
}
.jss18855 {
  background-color: #e0e0e0 !important;
}
.jss18856 {
  transition: width 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
.jss18857 {
  border-collapse: separate;
}
.jss18858:hover {
  background-color: rgb(235, 235, 235);
}
.jss18859 {
  position: relative;
  min-height: 80vh;
}
.jss18859 .back-icon {
  top: 5px;
  left: 5px;
  z-index: 1;
  position: absolute;
}
.jss18859 > .ninebox-inner {
  width: 100%;
  position: relative;
  min-height: 80vh;
}
.jss18859 > .ninebox-inner .category-paper {
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #424242;
}
.jss18859 > .ninebox-inner .percentage-wrapper {
  width: 100%;
  height: 100%;
  display: flex;
  flex-wrap: wrap;
  box-sizing: border-box;
}
.jss18859 > .ninebox-inner .horizontal-category {
  height: 50px;
  margin: 0 60px;
  display: flex;
  align-items: center;
  justify-content: center;
}
.jss18859 > .ninebox-inner .vertical-category {
  width: 50px;
  height: calc(80vh - 120px);
  display: inline-flex;
  align-items: center;
  justify-content: center;
}
.jss18859 > .ninebox-inner .ninebox-content {
  width: calc(100% - 100px);
  height: calc(80vh - 120px);
  display: inline-block;
  position: relative;
}
.jss18859 > .ninebox-inner .ninebox-content .wrapper {
  width: 100%;
  height: 100%;
  margin: 0;
}
.jss18859 > .ninebox-inner .ninebox-content .wrapper .box {
  height: calc(100% / 3);
  padding: 10px;
}
.jss18859 > .ninebox-inner .ninebox-content .wrapper .box .has-item {
  padding: 26px 10px !important;
}
.jss18859 > .ninebox-inner .ninebox-content .wrapper .box .disabled {
  background: #d1d1d1;
}
.jss18859 > .ninebox-inner .ninebox-content .wrapper .box .card {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 30px;
  position: relative;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  -webkit-transition: all .4s ease-out;
}
.jss18859 > .ninebox-inner .ninebox-content .wrapper .box .card h6 {
  color: rgba(0, 0, 0, 0.87);
}
.jss18859 > .ninebox-inner .ninebox-content .wrapper .box .card .total-person {
  top: 5px;
  right: 5px;
  width: 15px;
  height: 15px;
  position: absolute;
}
.jss18859 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list {
  width: 100%;
  height: 100%;
  display: flex;
  overflow-y: auto;
  align-items: center;
}
.jss18859 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list > ol {
  height: 100%;
  margin: 0;
  padding: 0;
  text-align: left;
  list-style: none;
}
.jss18859 > .ninebox-inner .ninebox-content .wrapper .box .active:hover {
  cursor: pointer;
  transform: scale(1.1);
}
.jss18859 > .ninebox-inner .vertical-category .text-rotate {
  transform: rotate(270deg);
}
.jss18859 > .ninebox-inner .vertical-category.left {
  padding: 10px 0;
  margin-right: 9px;
}
.jss18859 > .ninebox-inner .vertical-category.right {
  margin-left: 9px;
}
.jss18859 > .ninebox-inner .vertical-category .text-rotate.nowrap {
  white-space: nowrap;
}
.jss18859 > .ninebox-inner .horizontal-category.top {
  padding: 0 9px;
  margin-bottom: 9px;
}
.jss18859 > .ninebox-inner .horizontal-category.bottom {
  margin-top: 9px;
}
.jss18859 > .ninebox-inner .percentage-wrapper > .percentage-item {
  padding: 0 10px;
  flex-grow: 0;
  max-width: calc(100% / 3);
  flex-basis: calc(100% / 3);
}
.jss18859 > .ninebox-inner .percentage-wrapper > .percentage-item.horizontal {
  height: calc(100% / 3);
  padding: 10px 0;
  max-width: 100%;
}
.jss18859 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 10px;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  background-color: #424242;
  -webkit-transition: all .4s ease-out;
}
.jss18859 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper > p {
  color: #fff;
}
.jss18859 > .ninebox-inner .category-paper > p {
  color: #fff;
}
.jss18860 {
  max-height: 45vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss18860 .table {
  border-collapse: separate;
}
.jss18860 .table .sticky-head {
  top: 0;
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss18861 {
  height: 44vh;
  overflow: auto;
}
.jss18862 {
  width: 100%;
  height: 50vh;
  overflow: auto;
}
.jss18863 {
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss18864 {
  width: 100%;
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss18865 {
  max-width: 82%;
}

.jss13291 {
  display: inline-flex;
  position: relative;
  vertical-align: middle;
}
.jss13292 {
  top: -11px;
  right: -11px;
  width: 22px;
  height: 22px;
  display: flex;
  z-index: 1;
  position: absolute;
  flex-wrap: wrap;
  font-size: 0.6964285714285714rem;
  transform: scale(1);
  transition: transform 225ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  align-items: center;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  align-content: center;
  border-radius: 50%;
  flex-direction: row;
  justify-content: center;
}
.jss13293 {
  color: rgba(0, 0, 0, 0.87);
  background-color: #03a9f4;
}
.jss13294 {
  color: rgba(0, 0, 0, 0.87);
  background-color: #ff9800;
}
.jss13295 {
  color: #fff;
  background-color: #f44336;
}
.jss13296 {
  transform: scale(0);
  transition: transform 195ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
}

@media (min-width:960px) {
  ::-webkit-scrollbar {
    width: 5px;
    height: 5px;
  }
  ::-webkit-scrollbar-track {
    background: rgba(250, 250, 250, 0);
  }
  ::-webkit-scrollbar-thumb {
    background: #eeeeee;
  }
  ::-webkit-scrollbar-thumb:hover {
    background: #e0e0e0;
  }
}
.jss18170 {
  width: 100%;
  z-index: 1;
  overflow: hidden;
  margin-bottom: 0;
}
.jss18171 {
  background-color: #424242;
}
.jss18172 {
  height: 70px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/etg.9362c8fb.svg&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss18173 {
  height: 150px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/tessa.55f0b81f.svg&quot;);
  background-repeat: no-repeat;
}
.jss18174 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2hJREFUeNrsWtGR2jAQ1TEU4FQQUsGRCuBm+IerAKgguAKgAnMVHFcBvn/PnFwBvg6cCuIOEslacRvFyJJswL74zQgDNrKedvftSoaQDh06dHDAnfGVfjRjr0PWRg3kkbD2RIJJakfMjzbsdd0CQz0yciF/0zf8wRyOKbSmgXuSx1rAmhWxgbSdnJFG4cOj5DhJz7KLrC3i0a8wSwM8QzcRjGCS1U+MkLcbE9uztjQn5kcyEFMsn5q4uxW09+8hQh5rR/buCNY4OtxsBzMpEaLZ/cIm6g6uyZTrtih+d6C8/PxDLuEiT1mhp7jWEH32HIi95sr5kRKe4PiTtRWbuN/5UfS9hXOUEd6gSXgBa/gwpoMyLiMh6ym5gJxmSsyuCxaK71OUAyka0AC+i8H9YzjnofEsNfd5NyWWnCwVTGgF35/CoPbK9ykMXt5nDEQpWIaC1aZwnlvqnrXvLq7YV2Z2CDesijUMCLv0FNxQYgQWkWIl3fOAfrcq+N46xuJTh8I1XOCBi4nyRqjqEMUZVSyWIQvNlZAI4fr4TIyNTIlh93O12gHJ8AxUluMHkI2V+xwhJmVsviniEAOBtbsr8izuRykM7L7G4lRacl2SizxFiRd1llQUOjSx2BYN9lpVvyzjMkgtxsRiIDbIa0Fd5SFyz6apRXCvwGJV46yBxISFUhPVaeOyJQE/HpcsW8aO2wV8sZqgfhZI6m2w1IVKEbE4l2oRZ55mzTN2dNeZUknMHftZ6GK8iJgaZ6FmZ8il9KIFE+mC0M4VuZv4Ec7u4RlVDMs6N4JQ14urojqrrVXGc8TiU+XA4+yTqCJRgnt8RhU9CGAb4mLljAVJbArZlk9pHgYOmznUoIpeEffd4R16/+zo8p7Sj4EriplIkDxfEtklfqfbfkugOh+cVTM/spd7dXUeTB4h2duRwkneklhc6vvVthDq78dAFYnLPkMb5J6AqbO2Eivb4k40cr8i4rGNS/G6R/08O66WH3QuXPa0JS6RW9dVsO6z7baDk8WoJle51om7f5Yxbill706Mm/qjIC6KweoCU1c/lq7YWnU0IUY/K7H3GgTj6ih/oiKq+F+oqk5IyZOOK+MriI98WPnNjJggF5C/Hyg0Faf/edj8M4cn6mlZ/rgRuBe9XqLm7NChw3+GPwIMAA139Xg+K8vEAAAAAElFTkSuQmCC&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss18175 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABANJREFUeNrsWt1x4jAQNpl7P18FcSqIqSBmhvdABYEKiCvwUQFcBfZVEHhnBl8F8VUQSvB1cJJ3jddC/pOlQBg0YwYUIfbT/n27imXdxtcag947+DuPvXqa5TpYq3F0PmD+bsZeQ0OHvmbg/HMB+2CvDntS9iSaADn48PGDgUtVNvmmQQg+fjEBfmqBBaa9x08ue2KVbe6uNXhcOrDDdQJbjQ/XqjHrBuxLAYMIeVZg9xrhuJegsTx5ztjphuyxe2qI58LVJURFSnk4vfpgwr0qmRywmECgVMrAdJBgB095QmaTDPRqHBv7rnFgZUcPCc3igzP0pfTkwewW7LGJWfO168soW9oJXHDJ6gPwVQnv5wCrNrEDgnSFuXlfszPB7utYfw7EFioBOmLy9wuroE8Lz6ACRBPZXfatmvUD83cu+o0r5DiukT+SIpSve0RTtU2Y5kCTlkLl05dreX7unkeIiTkfS+VKGhJ7QDQYsb3mnw+srCludiMmSNKwPq5lE2DSewLOV81rA0VQtC/RFlSIax9q89UpuJGKz6lyRepT9aBg5BzQFnKbrGpOsj0Lgq3U3rtTNEGH+FTSImLSwPDSoiWQZHvn+Q9+07jGgmP0axcoRCAeMpMmcGtStgRmgfm7iUV7ie3GRKjdrEZzpFG20Nqki6hdKdWTQFxFfuhIErFDhHzBuQVbn0i0JAaJDfGxJ/xsBJh35Hh5ZANA7y0430YAu5dYBKxbjacINGVzG9SwZ9LHXFIM0tEEKs9fG8EkZUPc669KL0Q13P8j5gP8riwwfz/FsM3TwYisfTjOFz5Eq+epZM5guIewLf8x4HUjMm8ffUP0G25eMOcJ0S7CQ0grGkaiDNo0lta2yIrEGhFwe6kwwDEDsu8844XNFXSqH1ibjhFoYy6YmFNRiFLmErUIWJ16+V2jYl4RPzas+14KHNBvnGWmCma47RDl7rtqSyV4xCenWJ+UEzTbd+yBcNN8s8qXeU0Uy1MJIl2BbY/+U8UEytzQxnzlCKD3UlOr32trEhjN/ItGnyiESjElrCW5yq3hjouK39YMDKJWRMis7LSfJeY7zAIE/BfAVOIvXoW2ZqSaPpjUWM75ilrp9CLCJVrys+RMhVqNN5ikNw3MJZSQYYPAQMglMTWxEMzD/bCyrIe0MM3WwNpIkudcUvMduorZp+fxRqJfr8ZLTYOIp4ehyjZ9OsFz1Bj4AgSAqXL/HUw6FFLFSFU49fsxAED5IQ8Aqvdjs+y7IqgelxS6OsE8+b4KDCXKck9Vhwki6jOCoeF+jX7V6+ZF9/1YUJFwExJJ7YraKkZAsQ5xTNyPeUiTJla7mxSu2d+6r5LM3I+VQboSgCkhxLdxG2z8F2AAPtd0UKyJkPIAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss18176 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAzRJREFUeNrsWs2Nm0AUnl1RAFIKCK4gdLBwy9GuYL0VePeWm+1bbt5UYLsC7FtueCswqWDdQCTSQd5bfRM9IcADDD+OeNIIbI8f8837f4NSI4000kg16K7uHz99++nTZUkj/f3965PNRRHvFV2+0FgT76QOj/sGz2dQUxpzWkhgEVQgeC/r8mkCzO1Ao9w+gEnyb8bGSCU8qEPRri3Ebycab5bW9JnVG/cXGvuCeSmNA9ngxRgYgeIFv3ekbk2IwU0IXGqqiv4NgNI2mGsGjsGfi9ThkYZXoIoPDRbrCb4siaTOxhsBI1GvctT1QSzgLW9OA3cf42NCfMOcOfx70JZXlDt5atGGapHT4KFrGn+QeVgDxrxIIi9QtV3nwOCJVm2IiXi/NuXhVNB9T8SXV+1iYROB6pY8mxKbi9yNPeKEQHEAj4YYB6oAk+qxg8ocCFzYg8Qer0nNaWpTcBynLlFlQk2rSfDg6L7DXQ5s1m2DAIZqm7OFGPf/jcS2Bfe3Cwz9C1+kRz6+u11gCOo69v1AGvZRpOK32wMGRxGJLH2FWJggD4zadCaOZelMUYsFol5iIKGOhQjoMdSTnckFc44cD4tK/V4kRouL0ErYZPokLKFQlu64D0UmozeEnco78doMSWJuJvX6hUZLWpLFvBCINUBxc/TZNMHt0saehNcLykDlADyIXDMFr2EAg13MRD+iSgd3I8LBxGRDOvWKSIY1uGcTd44sRNd4M1ugrLt7LmNEOT83LD8UvKHVCqGNOHbE1aQFp/PG/WDjmKAq6qSBGcUuqHfQF7CiLOQfcHHmlapqHWeev1AGhyBtANMPdQnQVuUcbND3KXLH1HBjPjIa2hCOfRwOzn3YmCsAzjOpVSLmLMUmXPOg7Fi4UN1C2qe+VZE95BHeMiuBhbAX91og1zkmXc992ZjOAXdFCS2AHhDHpqqk4yuObmfIMWMTG7srYRaLGBP22ZgRUtLrkGd3YV4MdNRACa49AhgtqRj3yTW3bwLM66KUL/GwZwTwNUqb2JaNearBawkWKPt8oy5XkbtPVIOzqQ6p6MSz9K0BX5T6QyQGtK/75s5II400Ui79FWAANjYkpEJzM+AAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss18177 {
  color: #FFF;
  padding: 48px;
}
.jss18178 {
  color: rgba(0, 0, 0, 0.87);
  width: 100%;
  display: flex;
  z-index: 2;
  opacity: 0.5;
  position: fixed;
  align-items: center;
  padding-left: 16px;
  padding-right: 16px;
  background-color: #424242;
}
.jss18179 {
  height: 100vh;
  display: flex;
  position: relative;
  max-height: 1600px;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: linear-gradient(rgba(0,130,170,0), #424242), url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/1.20e79364.jpg&quot;);
}
.jss18180 {
  margin: -200px auto 0 auto;
  padding: 64px 0 48px;
  max-width: 1200px;
}
.jss18181 {
  width: 50%;
  color: #FFF;
}
@media (max-width:1279.95px) {
  .jss18181 {
    width: 100%;
    padding: 24px;
  }
}
.jss18182 {
  margin: -150px auto 0 auto;
  padding: 16px;
  position: relative;
  max-width: 1200px;
  background: #fafafa;
  justify-content: center;
}
.jss18183 {
  margin: 0 auto;
  padding: 24px;
  display: flex;
}
.jss18184 {
  margin-bottom: 24px;
}
.jss18185 {
  text-decoration: none;
}
.jss18186 {
  margin: 24px auto;
  max-width: 1200px;
}
@media (max-width:1279.95px) {
  .jss18186 {
    max-width: 1366px;
  }
}
.jss18187 {
  height: 160px;
  display: flex;
  align-items: center;
}
.jss18188 {
  margin: auto;
  display: grid;
}
.jss18189 {
  height: 100vh;
  display: flex;
  position: relative;
  overflow: hidden;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/2.adfa400c.jpg&quot;);
  background-position: center;
}
.jss18190 {
  color: #FFF;
  background-color: #03a9f4;
}
.jss18191 {
  color: #03a9f4;
  background-color: #fff;
}
.jss18192 {
  color: #03a9f4;
  padding: 0;
}
.jss18193 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss18193 {
    width: calc(100%);
  }
}
.jss18194 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss18194 {
    width: calc(100% - 300px);
  }
}
.jss18195 {
  display: none;
}
@media (min-width:600px) {
  .jss18195 {
    display: block;
  }
}
.jss18196 {
  color: #fff;
  position: relative;
  background: #03a9f4;
}
.jss18197 {
  width: 100%;
  position: relative;
  border-radius: 14px;
  background-color: rgba(255, 255, 255, 0.5);
}
.jss18197:hover {
  background-color: rgba(255, 255, 255, 0.7);
}
@media (min-width:600px) {
  .jss18197 {
    width: auto;
  }
}
.jss18198 {
  width: 72px;
  color: #212121;
  height: 100%;
  display: flex;
  position: absolute;
  align-items: center;
  pointer-events: none;
  justify-content: center;
}
.jss18199 {
  color: #fff;
  width: 100%;
}
.jss18200 {
  color: #212121;
  width: 100%;
  transition: width 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  padding-top: 8px;
  padding-left: 64px;
  padding-right: 8px;
  padding-bottom: 8px;
}
@media (min-width:600px) {
  .jss18200 {
    width: 80px;
  }
  .jss18200:focus {
    width: 200px;
  }
}
.jss18201 {
  color: #212121;
}
.jss18202 {
  background-color: #4fc3f7;
}
.jss18203 {
  margin-left: 12px;
  margin-right: 36px;
}
.jss18204 {
  display: none;
}
@media (min-width:960px) {
  .jss18205 {
    display: none;
  }
}
.jss18206 {
  width: 300px;
}
.jss18207 {
  display: flex;
  padding: 0 8px;
  min-height: 56px;
  align-items: center;
}
@media (min-width:0px) and (orientation: landscape) {
  .jss18207 {
    min-height: 48px;
  }
}
@media (min-width:600px) {
  .jss18207 {
    min-height: 64px;
  }
}
.jss18208 {
  margin-top: 65px;
  justify-content: flex-start;
}
.jss18209 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss18211 {
  width: 100%;
  height: 100%;
  opacity: 0.2;
  content: &quot;&quot;;
  display: block;
  position: absolute;
  background: linear-gradient(rgba(0,130,170,0), #03a9f4));
  background-size: cover;
  background-position: center center;
}
.jss18212 {
  width: 60px;
  overflow-x: hidden;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss18213 {
  color: inherit;
}
.jss18214 {
  margin-left: 56px;
}
.jss18215 {
  bottom: 16px;
}
.jss18216 {
  color: rgba(0, 0, 0, 0.87);
  background-size: cover;
  background-color: #fff;
  background-image: url(https://xdev.equine.co.id/apps/tessa/static/media/pattern2.f069796d.svg);
  background-repeat: no-repeat;
  background-position: inherit;
}
.jss18217 {
  color: #fff;
  background-color: #03a9f4;
}
.jss18218 {
  color: #fff;
  background-color: #ff9800;
}
.jss18219 {
  color: #fff;
  background-color: #f44336;
}
.jss18220 {
  color: #fff;
  background: #4caf50;
}
.jss18221 {
  flex: 1;
}
.jss18222 {
  padding: 16px;
  flex-grow: 1;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss18222 {
    margin-top: 68px;
  }
}
@media (max-width:959.95px) {
  .jss18222 {
    padding: 0;
    margin-top: 56px;
  }
}
@media (max-width:959.95px) {
  .jss18223 {
    margin-top: 68px;
  }
}
@media (min-width:960px) {
  .jss18223 {
    left: 50%;
    width: 982px;
    position: absolute;
    transform: translateX(-50%);
  }
}
@media (min-width:960px) {
  .jss18224 {
    margin-left: 300px;
    margin-right: 0;
  }
}
.jss18225 {
  margin-bottom: 56px;
}
.jss18226 {
  width: calc(100% - 0px);
  right: 0;
  bottom: 0;
  display: flex;
  position: fixed;
}
@media (min-width:960px) {
  .jss18226 {
    width: calc(100% - 300px);
  }
}
.jss18228 {
  background: #fff;
}
.jss18229 {
  margin-bottom: 60px;
}
.jss18230 {
  height: 4px;
}
.jss18231 {
  display: flex;
  align-items: center;
  justify-content: space-between;
}
.jss18232 {
  margin: 0 15px 0 0;
  padding: 16px 0;
  flex-grow: 1;
}
@media (min-width:600px) {
  .jss18232 {
    width: 30%;
    flex-grow: 0;
  }
}
.jss18233 {
  color: inherit;
  font-size: 0.6964285714285714rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: inherit;
  line-height: 1.66;
  letter-spacing: 0.03333em;
}
.jss18234 {
  color: #fff;
  background: #03a9f4;
}
.jss18235 {
  color: #fff;
  min-height: 40px;
  background: #ff9800;
  margin-bottom: 16px;
}
.jss18236 {
  height: 500px;
  display: flex;
  overflow: hidden;
  align-items: center;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss18236 {
    height: 300px;
  }
}
.jss18237 {
  height: 100%;
  margin: 0 auto;
}
.jss18238 {
  width: 25px;
  height: 25px;
  margin-right: 16px;
}
.jss18239 {
  transform: rotate(0deg);
  transition: transform 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
}
.jss18240 {
  transform: rotate(180deg);
}
.jss18241 {
  min-width: 350px;
}
.jss18242 {
  display: flex;
  flex-wrap: wrap;
}
.jss18243 {
  padding: 0;
}
@media (min-width:0px) {
  .jss18243 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss18243 {
    width: calc(100% / 2);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1280px) {
  .jss18243 {
    width: calc(100% / 3);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1920px) {
  .jss18243 {
    width: calc(100% / 4);
    padding: 0 16px 16px 0;
  }
}
.jss18244 {
  padding: 0;
}
@media (min-width:0px) {
  .jss18244 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss18244 {
    width: calc(100% / 2);
  }
}
@media (min-width:1280px) {
  .jss18244 {
    width: calc(100% - (100% / 3));
  }
}
@media (min-width:1920px) {
  .jss18244 {
    width: calc(100% - (100% / 4));
  }
}
.jss18245 {
  padding: 0;
}
@media (min-width:0px) {
  .jss18245 {
    width: 100%;
  }
}
.jss18246 {
  width: 100%;
  margin-bottom: 16px;
  vertical-align: middle;
}
.jss18247 {
  right: 16px;
  display: flex;
  position: absolute;
}
.jss18248 {
  right: 16px;
  bottom: 16px;
  position: fixed;
}
.jss18249 {
  float: right;
}
.jss18250 {
  margin: 0;
}
.jss18251 {
  margin: 8px;
}
.jss18252 {
  margin-left: 8px;
}
.jss18253 {
  margin-top: 8px;
}
.jss18254 {
  margin-right: 8px;
}
.jss18255 {
  margin-bottom: 8px;
}
.jss18256 {
  margin: 16px;
}
.jss18257 {
  margin-left: 16px;
}
.jss18258 {
  margin-top: 16px;
}
.jss18259 {
  margin-right: 16px;
}
.jss18260 {
  margin-bottom: 16px;
}
.jss18261 {
  margin: 24px;
}
.jss18262 {
  margin-left: 24px;
}
.jss18263 {
  margin-top: 24px;
}
.jss18264 {
  margin-right: 24px;
}
.jss18265 {
  margin-bottom: 24px;
}
.jss18266 {
  padding: 0;
}
.jss18267 {
  padding: 8px;
}
.jss18268 {
  padding-left: 8px;
}
.jss18269 {
  padding-top: 8px;
}
.jss18270 {
  padding-right: 8px;
}
.jss18271 {
  padding-bottom: 8px;
}
.jss18272 {
  padding: 16px;
}
.jss18273 {
  padding-left: 16px;
}
.jss18274 {
  padding-top: 16px;
}
.jss18275 {
  padding-right: 16px;
}
.jss18276 {
  padding-bottom: 16px;
}
.jss18277 {
  padding: 24px;
}
.jss18278 {
  padding-left: 24px;
}
.jss18279 {
  padding-top: 24px;
}
.jss18280 {
  padding-right: 24px;
}
.jss18281 {
  padding-bottom: 24px;
}
.jss18282 {
  background-color: #03a9f4;
}
.jss18283 {
  background-color: #ff9800;
}
.jss18284 {
  color: white;
  background-color: #f44336;
}
.jss18285 {
  color: #03a9f4;
}
.jss18286 {
  color: #ff9800;
}
.jss18287 {
  color: #f44336;
}
.jss18288 {
  text-decoration: line-through;
}
.jss18289 {
  padding: 3px;
}
.jss18290 {
  height: calc(100vh - 96px - 72px);
  overflow-x: auto;
}
.jss18291 {
  width: calc(100vw - 32px - 300px);
}
.jss18292 {
  height: calc(100vh - 56px - 68px);
  overflow-x: auto;
}
.jss18293 {
  width: calc(100vw);
}
.jss18294 {
  overflow-x: auto;
}
.jss18295 {
  table-layout: initial;
}
.jss18296 {
  top: 0;
  position: sticky;
  background-color: #fff;
}
.jss18297 {
  width: 3vw;
}
.jss18298 {
  width: 5vw;
}
.jss18299 {
  width: 10vw;
}
.jss18300 {
  width: 15vw;
}
.jss18301 {
  min-width: 15vw;
  max-width: 15vw;
}
@media (min-width:960px) {
  .jss18301 {
    min-width: calc((100vw - 300px - 48px) * 0.15);
    max-width: calc((100vw - 300px - 48px) * 0.15);
  }
}
.jss18302 {
  min-width: 25vw;
  max-width: 25vw;
}
@media (min-width:960px) {
  .jss18302 {
    min-width: calc((100vw - 300px - 48px) * 0.25);
    max-width: calc((100vw - 300px - 48px) * 0.25);
  }
}
.jss18303 {
  width: 25vw;
}
.jss18304 {
  width: 30vw;
}
.jss18305 {
  min-width: 45vw;
  max-width: 45vw;
}
@media (min-width:960px) {
  .jss18305 {
    min-width: calc((100vw - 300px - 48px) * 0.45);
    max-width: calc((100vw - 300px - 48px) * 0.45);
  }
}
.jss18306 {
  width: 100%;
  overflow-x: auto;
}
.jss18307 {
  width: 100%;
  margin-top: 24px;
  overflow-x: auto;
}
.jss18308 {
  min-width: 700px;
}
.jss18309 {
  width: 100%;
}
.jss18310 {
  min-width: 100px;
}
.jss18311 {
  color: rgba(0, 0, 0, 0.54);
  flex-shrink: 0;
}
.jss18312 {
  color: #2196f3;
}
.jss18313 {
  padding-top: 16px;
  padding-bottom: 16px;
}
.jss18315 {
  max-height: 100px;
}
.jss18316 {
  margin-top: -144px;
  margin-left: -56px;
  margin-right: -136px;
  margin-bottom: -64px;
}
.jss18317 {
  margin-top: -80px;
}
.jss18318 {
  width: 500px;
  height: 450px;
}
.jss18319 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss18320 {
  display: flex;
  box-sizing: border-box;
  align-items: center;
}
.jss18322:hover {
  background-color: #eeeeee;
}
.jss18323 {
  flex: 1;
}
.jss18324 {
  cursor: initial;
}
.jss18325 {
  background: #f44336;
}
.jss18326 {
  color: #fff;
}
.jss18327 {
  width: 100%;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
.jss18328 {
  max-height: 75vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss18329 {
  padding: 10px;
  text-align: center;
}
.jss18330 {
  padding: 0;
  vertical-align: top;
}
.jss18331 {
  margin-top: 8px;
  padding-left: 32px;
}
.jss18332 {
  top: 0;
  padding: 5px;
  position: sticky;
  overflow: hidden;
  max-width: 100px;
  max-height: 10px;
  text-align: center;
  background-color: #fff;
}
.jss18333 {
  max-height: 76vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss18334 {
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss18335 {
  padding: 0;
  margin-top: 8px;
}
.jss18336 {
  left: 0;
  width: 500px;
  z-index: 1;
  padding: 8px 16px 0 16px;
  min-width: 300px;
  background: #fff;
  vertical-align: top;
}
.jss18337 {
  padding: 8px 16px 0 16px;
  min-width: 300px;
  vertical-align: top;
}
.jss18338 {
  min-width: 300px;
}
.jss18339 {
  transform: rotate(180deg);
  writing-mode: vertical-rl;
}
.jss18340 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss18341 {
  cursor: pointer;
  transition: all .3s;
}
.jss18341:hover {
  background-color: #ebebeb;
}
.jss18342 {
  background-color: #ebebeb;
}
.jss18343 {
  transition: all .3s;
}
.jss18343:hover {
  transform: scale(1.03);
}
.jss18344 {
  padding: 0;
  transition: all .3s;
}
.jss18344:hover {
  transform: scale(1.03);
}
.jss18345 {
  display: grid;
  align-items: center;
  grid-column-gap: 1.5rem;
  grid-template-columns: 1fr max-content 1fr;
}
.jss18345::after, .jss18345::before {
  height: 1px;
  content: '';
  display: block;
  background-color: currentColor;
}
.jss18346 a {
  text-decoration: none;
}
.jss18347 {
  left: 50%;
  position: absolute;
  transform: ;
}
.jss18348 {
  display: -webkit-box;
  overflow: hidden;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;
}
.jss18349 {
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
.jss18350 {
  display: inline;
  text-transform: capitalize;
}
.jss18351 {
  text-decoration: none;
}
.jss18351 p:hover {
  color: #03a9f4;
}
.jss18352 {
  margin-left: 4px;
}
.jss18353 {
  padding: 0 16px;
  display: inline-flex;
  position: relative;
  vertical-align: middle;
}
.jss18354 {
  top: 0;
  color: #fff;
  right: 0;
  height: 20px;
  display: flex;
  padding: 0 4px;
  z-index: 1;
  position: absolute;
  flex-wrap: wrap;
  min-width: 20px;
  transform: translate(20px, -50%);
  box-sizing: border-box;
  transition: transform 225ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  align-items: center;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  align-content: center;
  border-radius: 10px;
  flex-direction: row;
  justify-content: center;
  background-color: #ff9800;
  transform-origin: 100% 0%;
}
.jss18355 {
  margin: 0;
  padding: 16px;
  border-bottom: 1px solid rgba(0, 0, 0, 0.12);
}
.jss18356 {
  top: 8px;
  right: 8px;
  color: #9e9e9e;
  position: absolute;
}
.jss18357 {
  margin: 0;
  padding: 16px;
}
.jss18358 {
  margin: 0;
  padding: 8px;
  border-top: 1px solid rgba(0, 0, 0, 0.12);
}
.jss18359 {
  font-size: 13px;
}
.jss18360 {
  padding: 0;
  overflow: hidden;
}
.jss18360 span {
  display: inline-flex;
}
.jss18360 span:hover {
  position: relative;
  animation: moveTextHorz 3s linear infinite;
}
@-webkit-keyframes moveTextHorz {
  0% {
    transform: translate(0, 0);
  }
  100% {
    transform: translate(-70%, 0);
  }
}
.jss18361 span {
  color: #f44336;
}
.jss18362 {
  width: 50px;
}
@media (min-width:600px) {
  .jss18362 {
    right: 90px;
  }
}
@media (min-width:960px) {
  .jss18362 {
    right: 15px;
  }
}
.jss18363 {
  cursor: pointer;
}
.jss18363:hover {
  background: #0288d1;
}
.jss18364 {
  margin-left: 10px;
  vertical-align: middle;
}
.jss18365 {
  width: 100%;
}
.jss18366 {
  width: 100%;
  margin: 0;
}
@media (min-width:960px) {
  .jss18367 {
    margin-left: auto;
    margin-right: 16px;
  }
}
@media (min-width:1280px) {
  .jss18367 {
    margin-left: auto;
    margin-right: 32px;
  }
}
@media (min-width:960px) {
  .jss18368 {
    transform: translateY(75%);
    margin-left: 16px;
  }
}
@media (min-width:1280px) {
  .jss18368 {
    transform: translateY(75%);
    margin-left: 32px;
  }
}
.jss18369 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
@media (max-width:1279.95px) {
  .jss18370 {
    display: none;
  }
}
@media (max-width:1919.95px) {
  .jss18371 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss18372 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss18373 {
    text-align: right;
  }
}
.jss18374 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss18374 {
    text-align: right;
  }
}
.jss18375 {
  padding: 0;
}
@media (min-width:1280px) {
  .jss18376 {
    margin-top: 0 !important;
  }
}
@media (min-width:1280px) {
  .jss18377 {
    margin-top: 64px;
  }
}
.jss18378 div textarea {
  overflow: auto;
}
.jss18379 {
  width: 70%;
}
@media (min-width:1280px) {
  .jss18379 {
    float: right;
  }
}
@media (min-width:1280px) {
  .jss18380 {
    float: right;
  }
}
@media (max-width:1279.95px) {
  .jss18380 {
    margin-top: 6px;
  }
}
@media (min-width:1280px) {
  .jss18381 {
    padding: 12px 0;
  }
}
@media (max-width:1279.95px) {
  .jss18381 {
    padding: 6px 0 3px;
  }
}
.jss18382 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss18382 {
    margin-top: 0 !important;
  }
}
.jss18382 div textarea {
  overflow: auto;
}
.jss18383 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss18384 {
  width: 57px;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
  overflow-x: hidden;
}
@media (min-width:600px) {
  .jss18384 {
    width: 73px;
  }
}
@media (min-width:960px) {
  .jss18385 {
    width: calc(100% - 73px);
    margin-left: 73px;
    margin-right: 0;
  }
}
@media (min-width:960px) {
  .jss18386 {
    margin-left: 73px;
    margin-right: 0;
  }
}
.jss18387 {
  bottom: 0px;
  z-index: 1;
  position: sticky;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss18387 {
    display: none;
  }
}
.jss18387:hover {
  background-color: rgb(235, 235, 235);
}
@media (max-width:959.95px) {
  .jss18388 {
    display: none;
  }
}
.jss18389 {
  top: 0;
  left: auto;
  width: 100%;
  right: 0;
  height: 100%;
  z-index: 1301;
  overflow: hidden;
  position: absolute;
  overflow-y: auto;
  transition: transform 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
@media (min-width:960px) {
  .jss18389 {
    width: 24vw;
  }
}
.jss18390 {
  cursor: pointer;
}
.jss18390:hover {
  background-color: #eeeeee;
}
.jss18391 {
  background-color: #e0e0e0 !important;
}
.jss18392 {
  transition: width 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
.jss18393 {
  border-collapse: separate;
}
.jss18394:hover {
  background-color: rgb(235, 235, 235);
}
.jss18395 {
  position: relative;
  min-height: 80vh;
}
.jss18395 .back-icon {
  top: 5px;
  left: 5px;
  z-index: 1;
  position: absolute;
}
.jss18395 > .ninebox-inner {
  width: 100%;
  position: relative;
  min-height: 80vh;
}
.jss18395 > .ninebox-inner .category-paper {
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #424242;
}
.jss18395 > .ninebox-inner .percentage-wrapper {
  width: 100%;
  height: 100%;
  display: flex;
  flex-wrap: wrap;
  box-sizing: border-box;
}
.jss18395 > .ninebox-inner .horizontal-category {
  height: 50px;
  margin: 0 60px;
  display: flex;
  align-items: center;
  justify-content: center;
}
.jss18395 > .ninebox-inner .vertical-category {
  width: 50px;
  height: calc(80vh - 120px);
  display: inline-flex;
  align-items: center;
  justify-content: center;
}
.jss18395 > .ninebox-inner .ninebox-content {
  width: calc(100% - 100px);
  height: calc(80vh - 120px);
  display: inline-block;
  position: relative;
}
.jss18395 > .ninebox-inner .ninebox-content .wrapper {
  width: 100%;
  height: 100%;
  margin: 0;
}
.jss18395 > .ninebox-inner .ninebox-content .wrapper .box {
  height: calc(100% / 3);
  padding: 10px;
}
.jss18395 > .ninebox-inner .ninebox-content .wrapper .box .has-item {
  padding: 26px 10px !important;
}
.jss18395 > .ninebox-inner .ninebox-content .wrapper .box .disabled {
  background: #d1d1d1;
}
.jss18395 > .ninebox-inner .ninebox-content .wrapper .box .card {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 30px;
  position: relative;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  -webkit-transition: all .4s ease-out;
}
.jss18395 > .ninebox-inner .ninebox-content .wrapper .box .card h6 {
  color: rgba(0, 0, 0, 0.87);
}
.jss18395 > .ninebox-inner .ninebox-content .wrapper .box .card .total-person {
  top: 5px;
  right: 5px;
  width: 15px;
  height: 15px;
  position: absolute;
}
.jss18395 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list {
  width: 100%;
  height: 100%;
  display: flex;
  overflow-y: auto;
  align-items: center;
}
.jss18395 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list > ol {
  height: 100%;
  margin: 0;
  padding: 0;
  text-align: left;
  list-style: none;
}
.jss18395 > .ninebox-inner .ninebox-content .wrapper .box .active:hover {
  cursor: pointer;
  transform: scale(1.1);
}
.jss18395 > .ninebox-inner .vertical-category .text-rotate {
  transform: rotate(270deg);
}
.jss18395 > .ninebox-inner .vertical-category.left {
  padding: 10px 0;
  margin-right: 9px;
}
.jss18395 > .ninebox-inner .vertical-category.right {
  margin-left: 9px;
}
.jss18395 > .ninebox-inner .vertical-category .text-rotate.nowrap {
  white-space: nowrap;
}
.jss18395 > .ninebox-inner .horizontal-category.top {
  padding: 0 9px;
  margin-bottom: 9px;
}
.jss18395 > .ninebox-inner .horizontal-category.bottom {
  margin-top: 9px;
}
.jss18395 > .ninebox-inner .percentage-wrapper > .percentage-item {
  padding: 0 10px;
  flex-grow: 0;
  max-width: calc(100% / 3);
  flex-basis: calc(100% / 3);
}
.jss18395 > .ninebox-inner .percentage-wrapper > .percentage-item.horizontal {
  height: calc(100% / 3);
  padding: 10px 0;
  max-width: 100%;
}
.jss18395 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 10px;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  background-color: #424242;
  -webkit-transition: all .4s ease-out;
}
.jss18395 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper > p {
  color: #fff;
}
.jss18395 > .ninebox-inner .category-paper > p {
  color: #fff;
}
.jss18396 {
  max-height: 45vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss18396 .table {
  border-collapse: separate;
}
.jss18396 .table .sticky-head {
  top: 0;
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss18397 {
  height: 44vh;
  overflow: auto;
}
.jss18398 {
  width: 100%;
  height: 50vh;
  overflow: auto;
}
.jss18399 {
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss18400 {
  width: 100%;
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss18401 {
  max-width: 82%;
}

@media (min-width:960px) {
  ::-webkit-scrollbar {
    width: 5px;
    height: 5px;
  }
  ::-webkit-scrollbar-track {
    background: rgba(250, 250, 250, 0);
  }
  ::-webkit-scrollbar-thumb {
    background: #eeeeee;
  }
  ::-webkit-scrollbar-thumb:hover {
    background: #e0e0e0;
  }
}
.jss18402 {
  width: 100%;
  z-index: 1;
  overflow: hidden;
  margin-bottom: 0;
}
.jss18403 {
  background-color: #424242;
}
.jss18404 {
  height: 70px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/etg.9362c8fb.svg&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss18405 {
  height: 150px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/tessa.55f0b81f.svg&quot;);
  background-repeat: no-repeat;
}
.jss18406 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2hJREFUeNrsWtGR2jAQ1TEU4FQQUsGRCuBm+IerAKgguAKgAnMVHFcBvn/PnFwBvg6cCuIOEslacRvFyJJswL74zQgDNrKedvftSoaQDh06dHDAnfGVfjRjr0PWRg3kkbD2RIJJakfMjzbsdd0CQz0yciF/0zf8wRyOKbSmgXuSx1rAmhWxgbSdnJFG4cOj5DhJz7KLrC3i0a8wSwM8QzcRjGCS1U+MkLcbE9uztjQn5kcyEFMsn5q4uxW09+8hQh5rR/buCNY4OtxsBzMpEaLZ/cIm6g6uyZTrtih+d6C8/PxDLuEiT1mhp7jWEH32HIi95sr5kRKe4PiTtRWbuN/5UfS9hXOUEd6gSXgBa/gwpoMyLiMh6ym5gJxmSsyuCxaK71OUAyka0AC+i8H9YzjnofEsNfd5NyWWnCwVTGgF35/CoPbK9ykMXt5nDEQpWIaC1aZwnlvqnrXvLq7YV2Z2CDesijUMCLv0FNxQYgQWkWIl3fOAfrcq+N46xuJTh8I1XOCBi4nyRqjqEMUZVSyWIQvNlZAI4fr4TIyNTIlh93O12gHJ8AxUluMHkI2V+xwhJmVsviniEAOBtbsr8izuRykM7L7G4lRacl2SizxFiRd1llQUOjSx2BYN9lpVvyzjMkgtxsRiIDbIa0Fd5SFyz6apRXCvwGJV46yBxISFUhPVaeOyJQE/HpcsW8aO2wV8sZqgfhZI6m2w1IVKEbE4l2oRZ55mzTN2dNeZUknMHftZ6GK8iJgaZ6FmZ8il9KIFE+mC0M4VuZv4Ec7u4RlVDMs6N4JQ14urojqrrVXGc8TiU+XA4+yTqCJRgnt8RhU9CGAb4mLljAVJbArZlk9pHgYOmznUoIpeEffd4R16/+zo8p7Sj4EriplIkDxfEtklfqfbfkugOh+cVTM/spd7dXUeTB4h2duRwkneklhc6vvVthDq78dAFYnLPkMb5J6AqbO2Eivb4k40cr8i4rGNS/G6R/08O66WH3QuXPa0JS6RW9dVsO6z7baDk8WoJle51om7f5Yxbill706Mm/qjIC6KweoCU1c/lq7YWnU0IUY/K7H3GgTj6ih/oiKq+F+oqk5IyZOOK+MriI98WPnNjJggF5C/Hyg0Faf/edj8M4cn6mlZ/rgRuBe9XqLm7NChw3+GPwIMAA139Xg+K8vEAAAAAElFTkSuQmCC&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss18407 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABANJREFUeNrsWt1x4jAQNpl7P18FcSqIqSBmhvdABYEKiCvwUQFcBfZVEHhnBl8F8VUQSvB1cJJ3jddC/pOlQBg0YwYUIfbT/n27imXdxtcag947+DuPvXqa5TpYq3F0PmD+bsZeQ0OHvmbg/HMB+2CvDntS9iSaADn48PGDgUtVNvmmQQg+fjEBfmqBBaa9x08ue2KVbe6uNXhcOrDDdQJbjQ/XqjHrBuxLAYMIeVZg9xrhuJegsTx5ztjphuyxe2qI58LVJURFSnk4vfpgwr0qmRywmECgVMrAdJBgB095QmaTDPRqHBv7rnFgZUcPCc3igzP0pfTkwewW7LGJWfO168soW9oJXHDJ6gPwVQnv5wCrNrEDgnSFuXlfszPB7utYfw7EFioBOmLy9wuroE8Lz6ACRBPZXfatmvUD83cu+o0r5DiukT+SIpSve0RTtU2Y5kCTlkLl05dreX7unkeIiTkfS+VKGhJ7QDQYsb3mnw+srCludiMmSNKwPq5lE2DSewLOV81rA0VQtC/RFlSIax9q89UpuJGKz6lyRepT9aBg5BzQFnKbrGpOsj0Lgq3U3rtTNEGH+FTSImLSwPDSoiWQZHvn+Q9+07jGgmP0axcoRCAeMpMmcGtStgRmgfm7iUV7ie3GRKjdrEZzpFG20Nqki6hdKdWTQFxFfuhIErFDhHzBuQVbn0i0JAaJDfGxJ/xsBJh35Hh5ZANA7y0430YAu5dYBKxbjacINGVzG9SwZ9LHXFIM0tEEKs9fG8EkZUPc669KL0Q13P8j5gP8riwwfz/FsM3TwYisfTjOFz5Eq+epZM5guIewLf8x4HUjMm8ffUP0G25eMOcJ0S7CQ0grGkaiDNo0lta2yIrEGhFwe6kwwDEDsu8844XNFXSqH1ibjhFoYy6YmFNRiFLmErUIWJ16+V2jYl4RPzas+14KHNBvnGWmCma47RDl7rtqSyV4xCenWJ+UEzTbd+yBcNN8s8qXeU0Uy1MJIl2BbY/+U8UEytzQxnzlCKD3UlOr32trEhjN/ItGnyiESjElrCW5yq3hjouK39YMDKJWRMis7LSfJeY7zAIE/BfAVOIvXoW2ZqSaPpjUWM75ilrp9CLCJVrys+RMhVqNN5ikNw3MJZSQYYPAQMglMTWxEMzD/bCyrIe0MM3WwNpIkudcUvMduorZp+fxRqJfr8ZLTYOIp4ehyjZ9OsFz1Bj4AgSAqXL/HUw6FFLFSFU49fsxAED5IQ8Aqvdjs+y7IqgelxS6OsE8+b4KDCXKck9Vhwki6jOCoeF+jX7V6+ZF9/1YUJFwExJJ7YraKkZAsQ5xTNyPeUiTJla7mxSu2d+6r5LM3I+VQboSgCkhxLdxG2z8F2AAPtd0UKyJkPIAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss18408 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAzRJREFUeNrsWs2Nm0AUnl1RAFIKCK4gdLBwy9GuYL0VePeWm+1bbt5UYLsC7FtueCswqWDdQCTSQd5bfRM9IcADDD+OeNIIbI8f8837f4NSI4000kg16K7uHz99++nTZUkj/f3965PNRRHvFV2+0FgT76QOj/sGz2dQUxpzWkhgEVQgeC/r8mkCzO1Ao9w+gEnyb8bGSCU8qEPRri3Ebycab5bW9JnVG/cXGvuCeSmNA9ngxRgYgeIFv3ekbk2IwU0IXGqqiv4NgNI2mGsGjsGfi9ThkYZXoIoPDRbrCb4siaTOxhsBI1GvctT1QSzgLW9OA3cf42NCfMOcOfx70JZXlDt5atGGapHT4KFrGn+QeVgDxrxIIi9QtV3nwOCJVm2IiXi/NuXhVNB9T8SXV+1iYROB6pY8mxKbi9yNPeKEQHEAj4YYB6oAk+qxg8ocCFzYg8Qer0nNaWpTcBynLlFlQk2rSfDg6L7DXQ5s1m2DAIZqm7OFGPf/jcS2Bfe3Cwz9C1+kRz6+u11gCOo69v1AGvZRpOK32wMGRxGJLH2FWJggD4zadCaOZelMUYsFol5iIKGOhQjoMdSTnckFc44cD4tK/V4kRouL0ErYZPokLKFQlu64D0UmozeEnco78doMSWJuJvX6hUZLWpLFvBCINUBxc/TZNMHt0saehNcLykDlADyIXDMFr2EAg13MRD+iSgd3I8LBxGRDOvWKSIY1uGcTd44sRNd4M1ugrLt7LmNEOT83LD8UvKHVCqGNOHbE1aQFp/PG/WDjmKAq6qSBGcUuqHfQF7CiLOQfcHHmlapqHWeev1AGhyBtANMPdQnQVuUcbND3KXLH1HBjPjIa2hCOfRwOzn3YmCsAzjOpVSLmLMUmXPOg7Fi4UN1C2qe+VZE95BHeMiuBhbAX91og1zkmXc992ZjOAXdFCS2AHhDHpqqk4yuObmfIMWMTG7srYRaLGBP22ZgRUtLrkGd3YV4MdNRACa49AhgtqRj3yTW3bwLM66KUL/GwZwTwNUqb2JaNearBawkWKPt8oy5XkbtPVIOzqQ6p6MSz9K0BX5T6QyQGtK/75s5II400Ui79FWAANjYkpEJzM+AAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss18409 {
  color: #FFF;
  padding: 48px;
}
.jss18410 {
  color: rgba(0, 0, 0, 0.87);
  width: 100%;
  display: flex;
  z-index: 2;
  opacity: 0.5;
  position: fixed;
  align-items: center;
  padding-left: 16px;
  padding-right: 16px;
  background-color: #424242;
}
.jss18411 {
  height: 100vh;
  display: flex;
  position: relative;
  max-height: 1600px;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: linear-gradient(rgba(0,130,170,0), #424242), url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/1.20e79364.jpg&quot;);
}
.jss18412 {
  margin: -200px auto 0 auto;
  padding: 64px 0 48px;
  max-width: 1200px;
}
.jss18413 {
  width: 50%;
  color: #FFF;
}
@media (max-width:1279.95px) {
  .jss18413 {
    width: 100%;
    padding: 24px;
  }
}
.jss18414 {
  margin: -150px auto 0 auto;
  padding: 16px;
  position: relative;
  max-width: 1200px;
  background: #fafafa;
  justify-content: center;
}
.jss18415 {
  margin: 0 auto;
  padding: 24px;
  display: flex;
}
.jss18416 {
  margin-bottom: 24px;
}
.jss18417 {
  text-decoration: none;
}
.jss18418 {
  margin: 24px auto;
  max-width: 1200px;
}
@media (max-width:1279.95px) {
  .jss18418 {
    max-width: 1366px;
  }
}
.jss18419 {
  height: 160px;
  display: flex;
  align-items: center;
}
.jss18420 {
  margin: auto;
  display: grid;
}
.jss18421 {
  height: 100vh;
  display: flex;
  position: relative;
  overflow: hidden;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/2.adfa400c.jpg&quot;);
  background-position: center;
}
.jss18422 {
  color: #FFF;
  background-color: #03a9f4;
}
.jss18423 {
  color: #03a9f4;
  background-color: #fff;
}
.jss18424 {
  color: #03a9f4;
  padding: 0;
}
.jss18425 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss18425 {
    width: calc(100%);
  }
}
.jss18426 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss18426 {
    width: calc(100% - 300px);
  }
}
.jss18427 {
  display: none;
}
@media (min-width:600px) {
  .jss18427 {
    display: block;
  }
}
.jss18428 {
  color: #fff;
  position: relative;
  background: #03a9f4;
}
.jss18429 {
  width: 100%;
  position: relative;
  border-radius: 14px;
  background-color: rgba(255, 255, 255, 0.5);
}
.jss18429:hover {
  background-color: rgba(255, 255, 255, 0.7);
}
@media (min-width:600px) {
  .jss18429 {
    width: auto;
  }
}
.jss18430 {
  width: 72px;
  color: #212121;
  height: 100%;
  display: flex;
  position: absolute;
  align-items: center;
  pointer-events: none;
  justify-content: center;
}
.jss18431 {
  color: #fff;
  width: 100%;
}
.jss18432 {
  color: #212121;
  width: 100%;
  transition: width 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  padding-top: 8px;
  padding-left: 64px;
  padding-right: 8px;
  padding-bottom: 8px;
}
@media (min-width:600px) {
  .jss18432 {
    width: 80px;
  }
  .jss18432:focus {
    width: 200px;
  }
}
.jss18433 {
  color: #212121;
}
.jss18434 {
  background-color: #4fc3f7;
}
.jss18435 {
  margin-left: 12px;
  margin-right: 36px;
}
.jss18436 {
  display: none;
}
@media (min-width:960px) {
  .jss18437 {
    display: none;
  }
}
.jss18438 {
  width: 300px;
}
.jss18439 {
  display: flex;
  padding: 0 8px;
  min-height: 56px;
  align-items: center;
}
@media (min-width:0px) and (orientation: landscape) {
  .jss18439 {
    min-height: 48px;
  }
}
@media (min-width:600px) {
  .jss18439 {
    min-height: 64px;
  }
}
.jss18440 {
  margin-top: 65px;
  justify-content: flex-start;
}
.jss18441 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss18443 {
  width: 100%;
  height: 100%;
  opacity: 0.2;
  content: &quot;&quot;;
  display: block;
  position: absolute;
  background: linear-gradient(rgba(0,130,170,0), #03a9f4));
  background-size: cover;
  background-position: center center;
}
.jss18444 {
  width: 60px;
  overflow-x: hidden;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss18445 {
  color: inherit;
}
.jss18446 {
  margin-left: 56px;
}
.jss18447 {
  bottom: 16px;
}
.jss18448 {
  color: rgba(0, 0, 0, 0.87);
  background-size: cover;
  background-color: #fff;
  background-image: url(https://xdev.equine.co.id/apps/tessa/static/media/pattern2.f069796d.svg);
  background-repeat: no-repeat;
  background-position: inherit;
}
.jss18449 {
  color: #fff;
  background-color: #03a9f4;
}
.jss18450 {
  color: #fff;
  background-color: #ff9800;
}
.jss18451 {
  color: #fff;
  background-color: #f44336;
}
.jss18452 {
  color: #fff;
  background: #4caf50;
}
.jss18453 {
  flex: 1;
}
.jss18454 {
  padding: 16px;
  flex-grow: 1;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss18454 {
    margin-top: 68px;
  }
}
@media (max-width:959.95px) {
  .jss18454 {
    padding: 0;
    margin-top: 56px;
  }
}
@media (max-width:959.95px) {
  .jss18455 {
    margin-top: 68px;
  }
}
@media (min-width:960px) {
  .jss18455 {
    left: 50%;
    width: 982px;
    position: absolute;
    transform: translateX(-50%);
  }
}
@media (min-width:960px) {
  .jss18456 {
    margin-left: 300px;
    margin-right: 0;
  }
}
.jss18457 {
  margin-bottom: 56px;
}
.jss18458 {
  width: calc(100% - 0px);
  right: 0;
  bottom: 0;
  display: flex;
  position: fixed;
}
@media (min-width:960px) {
  .jss18458 {
    width: calc(100% - 300px);
  }
}
.jss18460 {
  background: #fff;
}
.jss18461 {
  margin-bottom: 60px;
}
.jss18462 {
  height: 4px;
}
.jss18463 {
  display: flex;
  align-items: center;
  justify-content: space-between;
}
.jss18464 {
  margin: 0 15px 0 0;
  padding: 16px 0;
  flex-grow: 1;
}
@media (min-width:600px) {
  .jss18464 {
    width: 30%;
    flex-grow: 0;
  }
}
.jss18465 {
  color: inherit;
  font-size: 0.6964285714285714rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: inherit;
  line-height: 1.66;
  letter-spacing: 0.03333em;
}
.jss18466 {
  color: #fff;
  background: #03a9f4;
}
.jss18467 {
  color: #fff;
  min-height: 40px;
  background: #ff9800;
  margin-bottom: 16px;
}
.jss18468 {
  height: 500px;
  display: flex;
  overflow: hidden;
  align-items: center;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss18468 {
    height: 300px;
  }
}
.jss18469 {
  height: 100%;
  margin: 0 auto;
}
.jss18470 {
  width: 25px;
  height: 25px;
  margin-right: 16px;
}
.jss18471 {
  transform: rotate(0deg);
  transition: transform 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
}
.jss18472 {
  transform: rotate(180deg);
}
.jss18473 {
  min-width: 350px;
}
.jss18474 {
  display: flex;
  flex-wrap: wrap;
}
.jss18475 {
  padding: 0;
}
@media (min-width:0px) {
  .jss18475 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss18475 {
    width: calc(100% / 2);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1280px) {
  .jss18475 {
    width: calc(100% / 3);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1920px) {
  .jss18475 {
    width: calc(100% / 4);
    padding: 0 16px 16px 0;
  }
}
.jss18476 {
  padding: 0;
}
@media (min-width:0px) {
  .jss18476 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss18476 {
    width: calc(100% / 2);
  }
}
@media (min-width:1280px) {
  .jss18476 {
    width: calc(100% - (100% / 3));
  }
}
@media (min-width:1920px) {
  .jss18476 {
    width: calc(100% - (100% / 4));
  }
}
.jss18477 {
  padding: 0;
}
@media (min-width:0px) {
  .jss18477 {
    width: 100%;
  }
}
.jss18478 {
  width: 100%;
  margin-bottom: 16px;
  vertical-align: middle;
}
.jss18479 {
  right: 16px;
  display: flex;
  position: absolute;
}
.jss18480 {
  right: 16px;
  bottom: 16px;
  position: fixed;
}
.jss18481 {
  float: right;
}
.jss18482 {
  margin: 0;
}
.jss18483 {
  margin: 8px;
}
.jss18484 {
  margin-left: 8px;
}
.jss18485 {
  margin-top: 8px;
}
.jss18486 {
  margin-right: 8px;
}
.jss18487 {
  margin-bottom: 8px;
}
.jss18488 {
  margin: 16px;
}
.jss18489 {
  margin-left: 16px;
}
.jss18490 {
  margin-top: 16px;
}
.jss18491 {
  margin-right: 16px;
}
.jss18492 {
  margin-bottom: 16px;
}
.jss18493 {
  margin: 24px;
}
.jss18494 {
  margin-left: 24px;
}
.jss18495 {
  margin-top: 24px;
}
.jss18496 {
  margin-right: 24px;
}
.jss18497 {
  margin-bottom: 24px;
}
.jss18498 {
  padding: 0;
}
.jss18499 {
  padding: 8px;
}
.jss18500 {
  padding-left: 8px;
}
.jss18501 {
  padding-top: 8px;
}
.jss18502 {
  padding-right: 8px;
}
.jss18503 {
  padding-bottom: 8px;
}
.jss18504 {
  padding: 16px;
}
.jss18505 {
  padding-left: 16px;
}
.jss18506 {
  padding-top: 16px;
}
.jss18507 {
  padding-right: 16px;
}
.jss18508 {
  padding-bottom: 16px;
}
.jss18509 {
  padding: 24px;
}
.jss18510 {
  padding-left: 24px;
}
.jss18511 {
  padding-top: 24px;
}
.jss18512 {
  padding-right: 24px;
}
.jss18513 {
  padding-bottom: 24px;
}
.jss18514 {
  background-color: #03a9f4;
}
.jss18515 {
  background-color: #ff9800;
}
.jss18516 {
  color: white;
  background-color: #f44336;
}
.jss18517 {
  color: #03a9f4;
}
.jss18518 {
  color: #ff9800;
}
.jss18519 {
  color: #f44336;
}
.jss18520 {
  text-decoration: line-through;
}
.jss18521 {
  padding: 3px;
}
.jss18522 {
  height: calc(100vh - 96px - 72px);
  overflow-x: auto;
}
.jss18523 {
  width: calc(100vw - 32px - 300px);
}
.jss18524 {
  height: calc(100vh - 56px - 68px);
  overflow-x: auto;
}
.jss18525 {
  width: calc(100vw);
}
.jss18526 {
  overflow-x: auto;
}
.jss18527 {
  table-layout: initial;
}
.jss18528 {
  top: 0;
  position: sticky;
  background-color: #fff;
}
.jss18529 {
  width: 3vw;
}
.jss18530 {
  width: 5vw;
}
.jss18531 {
  width: 10vw;
}
.jss18532 {
  width: 15vw;
}
.jss18533 {
  min-width: 15vw;
  max-width: 15vw;
}
@media (min-width:960px) {
  .jss18533 {
    min-width: calc((100vw - 300px - 48px) * 0.15);
    max-width: calc((100vw - 300px - 48px) * 0.15);
  }
}
.jss18534 {
  min-width: 25vw;
  max-width: 25vw;
}
@media (min-width:960px) {
  .jss18534 {
    min-width: calc((100vw - 300px - 48px) * 0.25);
    max-width: calc((100vw - 300px - 48px) * 0.25);
  }
}
.jss18535 {
  width: 25vw;
}
.jss18536 {
  width: 30vw;
}
.jss18537 {
  min-width: 45vw;
  max-width: 45vw;
}
@media (min-width:960px) {
  .jss18537 {
    min-width: calc((100vw - 300px - 48px) * 0.45);
    max-width: calc((100vw - 300px - 48px) * 0.45);
  }
}
.jss18538 {
  width: 100%;
  overflow-x: auto;
}
.jss18539 {
  width: 100%;
  margin-top: 24px;
  overflow-x: auto;
}
.jss18540 {
  min-width: 700px;
}
.jss18541 {
  width: 100%;
}
.jss18542 {
  min-width: 100px;
}
.jss18543 {
  color: rgba(0, 0, 0, 0.54);
  flex-shrink: 0;
}
.jss18544 {
  color: #2196f3;
}
.jss18545 {
  padding-top: 16px;
  padding-bottom: 16px;
}
.jss18547 {
  max-height: 100px;
}
.jss18548 {
  margin-top: -144px;
  margin-left: -56px;
  margin-right: -136px;
  margin-bottom: -64px;
}
.jss18549 {
  margin-top: -80px;
}
.jss18550 {
  width: 500px;
  height: 450px;
}
.jss18551 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss18552 {
  display: flex;
  box-sizing: border-box;
  align-items: center;
}
.jss18554:hover {
  background-color: #eeeeee;
}
.jss18555 {
  flex: 1;
}
.jss18556 {
  cursor: initial;
}
.jss18557 {
  background: #f44336;
}
.jss18558 {
  color: #fff;
}
.jss18559 {
  width: 100%;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
.jss18560 {
  max-height: 75vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss18561 {
  padding: 10px;
  text-align: center;
}
.jss18562 {
  padding: 0;
  vertical-align: top;
}
.jss18563 {
  margin-top: 8px;
  padding-left: 32px;
}
.jss18564 {
  top: 0;
  padding: 5px;
  position: sticky;
  overflow: hidden;
  max-width: 100px;
  max-height: 10px;
  text-align: center;
  background-color: #fff;
}
.jss18565 {
  max-height: 76vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss18566 {
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss18567 {
  padding: 0;
  margin-top: 8px;
}
.jss18568 {
  left: 0;
  width: 500px;
  z-index: 1;
  padding: 8px 16px 0 16px;
  min-width: 300px;
  background: #fff;
  vertical-align: top;
}
.jss18569 {
  padding: 8px 16px 0 16px;
  min-width: 300px;
  vertical-align: top;
}
.jss18570 {
  min-width: 300px;
}
.jss18571 {
  transform: rotate(180deg);
  writing-mode: vertical-rl;
}
.jss18572 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss18573 {
  cursor: pointer;
  transition: all .3s;
}
.jss18573:hover {
  background-color: #ebebeb;
}
.jss18574 {
  background-color: #ebebeb;
}
.jss18575 {
  transition: all .3s;
}
.jss18575:hover {
  transform: scale(1.03);
}
.jss18576 {
  padding: 0;
  transition: all .3s;
}
.jss18576:hover {
  transform: scale(1.03);
}
.jss18577 {
  display: grid;
  align-items: center;
  grid-column-gap: 1.5rem;
  grid-template-columns: 1fr max-content 1fr;
}
.jss18577::after, .jss18577::before {
  height: 1px;
  content: '';
  display: block;
  background-color: currentColor;
}
.jss18578 a {
  text-decoration: none;
}
.jss18579 {
  left: 50%;
  position: absolute;
  transform: ;
}
.jss18580 {
  display: -webkit-box;
  overflow: hidden;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;
}
.jss18581 {
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
.jss18582 {
  display: inline;
  text-transform: capitalize;
}
.jss18583 {
  text-decoration: none;
}
.jss18583 p:hover {
  color: #03a9f4;
}
.jss18584 {
  margin-left: 4px;
}
.jss18585 {
  padding: 0 16px;
  display: inline-flex;
  position: relative;
  vertical-align: middle;
}
.jss18586 {
  top: 0;
  color: #fff;
  right: 0;
  height: 20px;
  display: flex;
  padding: 0 4px;
  z-index: 1;
  position: absolute;
  flex-wrap: wrap;
  min-width: 20px;
  transform: translate(20px, -50%);
  box-sizing: border-box;
  transition: transform 225ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  align-items: center;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  align-content: center;
  border-radius: 10px;
  flex-direction: row;
  justify-content: center;
  background-color: #ff9800;
  transform-origin: 100% 0%;
}
.jss18587 {
  margin: 0;
  padding: 16px;
  border-bottom: 1px solid rgba(0, 0, 0, 0.12);
}
.jss18588 {
  top: 8px;
  right: 8px;
  color: #9e9e9e;
  position: absolute;
}
.jss18589 {
  margin: 0;
  padding: 16px;
}
.jss18590 {
  margin: 0;
  padding: 8px;
  border-top: 1px solid rgba(0, 0, 0, 0.12);
}
.jss18591 {
  font-size: 13px;
}
.jss18592 {
  padding: 0;
  overflow: hidden;
}
.jss18592 span {
  display: inline-flex;
}
.jss18592 span:hover {
  position: relative;
  animation: moveTextHorz 3s linear infinite;
}
@-webkit-keyframes moveTextHorz {
  0% {
    transform: translate(0, 0);
  }
  100% {
    transform: translate(-70%, 0);
  }
}
.jss18593 span {
  color: #f44336;
}
.jss18594 {
  width: 50px;
}
@media (min-width:600px) {
  .jss18594 {
    right: 90px;
  }
}
@media (min-width:960px) {
  .jss18594 {
    right: 15px;
  }
}
.jss18595 {
  cursor: pointer;
}
.jss18595:hover {
  background: #0288d1;
}
.jss18596 {
  margin-left: 10px;
  vertical-align: middle;
}
.jss18597 {
  width: 100%;
}
.jss18598 {
  width: 100%;
  margin: 0;
}
@media (min-width:960px) {
  .jss18599 {
    margin-left: auto;
    margin-right: 16px;
  }
}
@media (min-width:1280px) {
  .jss18599 {
    margin-left: auto;
    margin-right: 32px;
  }
}
@media (min-width:960px) {
  .jss18600 {
    transform: translateY(75%);
    margin-left: 16px;
  }
}
@media (min-width:1280px) {
  .jss18600 {
    transform: translateY(75%);
    margin-left: 32px;
  }
}
.jss18601 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
@media (max-width:1279.95px) {
  .jss18602 {
    display: none;
  }
}
@media (max-width:1919.95px) {
  .jss18603 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss18604 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss18605 {
    text-align: right;
  }
}
.jss18606 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss18606 {
    text-align: right;
  }
}
.jss18607 {
  padding: 0;
}
@media (min-width:1280px) {
  .jss18608 {
    margin-top: 0 !important;
  }
}
@media (min-width:1280px) {
  .jss18609 {
    margin-top: 64px;
  }
}
.jss18610 div textarea {
  overflow: auto;
}
.jss18611 {
  width: 70%;
}
@media (min-width:1280px) {
  .jss18611 {
    float: right;
  }
}
@media (min-width:1280px) {
  .jss18612 {
    float: right;
  }
}
@media (max-width:1279.95px) {
  .jss18612 {
    margin-top: 6px;
  }
}
@media (min-width:1280px) {
  .jss18613 {
    padding: 12px 0;
  }
}
@media (max-width:1279.95px) {
  .jss18613 {
    padding: 6px 0 3px;
  }
}
.jss18614 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss18614 {
    margin-top: 0 !important;
  }
}
.jss18614 div textarea {
  overflow: auto;
}
.jss18615 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss18616 {
  width: 57px;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
  overflow-x: hidden;
}
@media (min-width:600px) {
  .jss18616 {
    width: 73px;
  }
}
@media (min-width:960px) {
  .jss18617 {
    width: calc(100% - 73px);
    margin-left: 73px;
    margin-right: 0;
  }
}
@media (min-width:960px) {
  .jss18618 {
    margin-left: 73px;
    margin-right: 0;
  }
}
.jss18619 {
  bottom: 0px;
  z-index: 1;
  position: sticky;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss18619 {
    display: none;
  }
}
.jss18619:hover {
  background-color: rgb(235, 235, 235);
}
@media (max-width:959.95px) {
  .jss18620 {
    display: none;
  }
}
.jss18621 {
  top: 0;
  left: auto;
  width: 100%;
  right: 0;
  height: 100%;
  z-index: 1301;
  overflow: hidden;
  position: absolute;
  overflow-y: auto;
  transition: transform 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
@media (min-width:960px) {
  .jss18621 {
    width: 24vw;
  }
}
.jss18622 {
  cursor: pointer;
}
.jss18622:hover {
  background-color: #eeeeee;
}
.jss18623 {
  background-color: #e0e0e0 !important;
}
.jss18624 {
  transition: width 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
.jss18625 {
  border-collapse: separate;
}
.jss18626:hover {
  background-color: rgb(235, 235, 235);
}
.jss18627 {
  position: relative;
  min-height: 80vh;
}
.jss18627 .back-icon {
  top: 5px;
  left: 5px;
  z-index: 1;
  position: absolute;
}
.jss18627 > .ninebox-inner {
  width: 100%;
  position: relative;
  min-height: 80vh;
}
.jss18627 > .ninebox-inner .category-paper {
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #424242;
}
.jss18627 > .ninebox-inner .percentage-wrapper {
  width: 100%;
  height: 100%;
  display: flex;
  flex-wrap: wrap;
  box-sizing: border-box;
}
.jss18627 > .ninebox-inner .horizontal-category {
  height: 50px;
  margin: 0 60px;
  display: flex;
  align-items: center;
  justify-content: center;
}
.jss18627 > .ninebox-inner .vertical-category {
  width: 50px;
  height: calc(80vh - 120px);
  display: inline-flex;
  align-items: center;
  justify-content: center;
}
.jss18627 > .ninebox-inner .ninebox-content {
  width: calc(100% - 100px);
  height: calc(80vh - 120px);
  display: inline-block;
  position: relative;
}
.jss18627 > .ninebox-inner .ninebox-content .wrapper {
  width: 100%;
  height: 100%;
  margin: 0;
}
.jss18627 > .ninebox-inner .ninebox-content .wrapper .box {
  height: calc(100% / 3);
  padding: 10px;
}
.jss18627 > .ninebox-inner .ninebox-content .wrapper .box .has-item {
  padding: 26px 10px !important;
}
.jss18627 > .ninebox-inner .ninebox-content .wrapper .box .disabled {
  background: #d1d1d1;
}
.jss18627 > .ninebox-inner .ninebox-content .wrapper .box .card {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 30px;
  position: relative;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  -webkit-transition: all .4s ease-out;
}
.jss18627 > .ninebox-inner .ninebox-content .wrapper .box .card h6 {
  color: rgba(0, 0, 0, 0.87);
}
.jss18627 > .ninebox-inner .ninebox-content .wrapper .box .card .total-person {
  top: 5px;
  right: 5px;
  width: 15px;
  height: 15px;
  position: absolute;
}
.jss18627 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list {
  width: 100%;
  height: 100%;
  display: flex;
  overflow-y: auto;
  align-items: center;
}
.jss18627 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list > ol {
  height: 100%;
  margin: 0;
  padding: 0;
  text-align: left;
  list-style: none;
}
.jss18627 > .ninebox-inner .ninebox-content .wrapper .box .active:hover {
  cursor: pointer;
  transform: scale(1.1);
}
.jss18627 > .ninebox-inner .vertical-category .text-rotate {
  transform: rotate(270deg);
}
.jss18627 > .ninebox-inner .vertical-category.left {
  padding: 10px 0;
  margin-right: 9px;
}
.jss18627 > .ninebox-inner .vertical-category.right {
  margin-left: 9px;
}
.jss18627 > .ninebox-inner .vertical-category .text-rotate.nowrap {
  white-space: nowrap;
}
.jss18627 > .ninebox-inner .horizontal-category.top {
  padding: 0 9px;
  margin-bottom: 9px;
}
.jss18627 > .ninebox-inner .horizontal-category.bottom {
  margin-top: 9px;
}
.jss18627 > .ninebox-inner .percentage-wrapper > .percentage-item {
  padding: 0 10px;
  flex-grow: 0;
  max-width: calc(100% / 3);
  flex-basis: calc(100% / 3);
}
.jss18627 > .ninebox-inner .percentage-wrapper > .percentage-item.horizontal {
  height: calc(100% / 3);
  padding: 10px 0;
  max-width: 100%;
}
.jss18627 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 10px;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  background-color: #424242;
  -webkit-transition: all .4s ease-out;
}
.jss18627 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper > p {
  color: #fff;
}
.jss18627 > .ninebox-inner .category-paper > p {
  color: #fff;
}
.jss18628 {
  max-height: 45vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss18628 .table {
  border-collapse: separate;
}
.jss18628 .table .sticky-head {
  top: 0;
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss18629 {
  height: 44vh;
  overflow: auto;
}
.jss18630 {
  width: 100%;
  height: 50vh;
  overflow: auto;
}
.jss18631 {
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss18632 {
  width: 100%;
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss18633 {
  max-width: 82%;
}

@media (min-width:960px) {
  ::-webkit-scrollbar {
    width: 5px;
    height: 5px;
  }
  ::-webkit-scrollbar-track {
    background: rgba(250, 250, 250, 0);
  }
  ::-webkit-scrollbar-thumb {
    background: #eeeeee;
  }
  ::-webkit-scrollbar-thumb:hover {
    background: #e0e0e0;
  }
}
.jss17938 {
  width: 100%;
  z-index: 1;
  overflow: hidden;
  margin-bottom: 0;
}
.jss17939 {
  background-color: #424242;
}
.jss17940 {
  height: 70px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/etg.9362c8fb.svg&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss17941 {
  height: 150px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/tessa.55f0b81f.svg&quot;);
  background-repeat: no-repeat;
}
.jss17942 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2hJREFUeNrsWtGR2jAQ1TEU4FQQUsGRCuBm+IerAKgguAKgAnMVHFcBvn/PnFwBvg6cCuIOEslacRvFyJJswL74zQgDNrKedvftSoaQDh06dHDAnfGVfjRjr0PWRg3kkbD2RIJJakfMjzbsdd0CQz0yciF/0zf8wRyOKbSmgXuSx1rAmhWxgbSdnJFG4cOj5DhJz7KLrC3i0a8wSwM8QzcRjGCS1U+MkLcbE9uztjQn5kcyEFMsn5q4uxW09+8hQh5rR/buCNY4OtxsBzMpEaLZ/cIm6g6uyZTrtih+d6C8/PxDLuEiT1mhp7jWEH32HIi95sr5kRKe4PiTtRWbuN/5UfS9hXOUEd6gSXgBa/gwpoMyLiMh6ym5gJxmSsyuCxaK71OUAyka0AC+i8H9YzjnofEsNfd5NyWWnCwVTGgF35/CoPbK9ykMXt5nDEQpWIaC1aZwnlvqnrXvLq7YV2Z2CDesijUMCLv0FNxQYgQWkWIl3fOAfrcq+N46xuJTh8I1XOCBi4nyRqjqEMUZVSyWIQvNlZAI4fr4TIyNTIlh93O12gHJ8AxUluMHkI2V+xwhJmVsviniEAOBtbsr8izuRykM7L7G4lRacl2SizxFiRd1llQUOjSx2BYN9lpVvyzjMkgtxsRiIDbIa0Fd5SFyz6apRXCvwGJV46yBxISFUhPVaeOyJQE/HpcsW8aO2wV8sZqgfhZI6m2w1IVKEbE4l2oRZ55mzTN2dNeZUknMHftZ6GK8iJgaZ6FmZ8il9KIFE+mC0M4VuZv4Ec7u4RlVDMs6N4JQ14urojqrrVXGc8TiU+XA4+yTqCJRgnt8RhU9CGAb4mLljAVJbArZlk9pHgYOmznUoIpeEffd4R16/+zo8p7Sj4EriplIkDxfEtklfqfbfkugOh+cVTM/spd7dXUeTB4h2duRwkneklhc6vvVthDq78dAFYnLPkMb5J6AqbO2Eivb4k40cr8i4rGNS/G6R/08O66WH3QuXPa0JS6RW9dVsO6z7baDk8WoJle51om7f5Yxbill706Mm/qjIC6KweoCU1c/lq7YWnU0IUY/K7H3GgTj6ih/oiKq+F+oqk5IyZOOK+MriI98WPnNjJggF5C/Hyg0Faf/edj8M4cn6mlZ/rgRuBe9XqLm7NChw3+GPwIMAA139Xg+K8vEAAAAAElFTkSuQmCC&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss17943 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABANJREFUeNrsWt1x4jAQNpl7P18FcSqIqSBmhvdABYEKiCvwUQFcBfZVEHhnBl8F8VUQSvB1cJJ3jddC/pOlQBg0YwYUIfbT/n27imXdxtcag947+DuPvXqa5TpYq3F0PmD+bsZeQ0OHvmbg/HMB+2CvDntS9iSaADn48PGDgUtVNvmmQQg+fjEBfmqBBaa9x08ue2KVbe6uNXhcOrDDdQJbjQ/XqjHrBuxLAYMIeVZg9xrhuJegsTx5ztjphuyxe2qI58LVJURFSnk4vfpgwr0qmRywmECgVMrAdJBgB095QmaTDPRqHBv7rnFgZUcPCc3igzP0pfTkwewW7LGJWfO168soW9oJXHDJ6gPwVQnv5wCrNrEDgnSFuXlfszPB7utYfw7EFioBOmLy9wuroE8Lz6ACRBPZXfatmvUD83cu+o0r5DiukT+SIpSve0RTtU2Y5kCTlkLl05dreX7unkeIiTkfS+VKGhJ7QDQYsb3mnw+srCludiMmSNKwPq5lE2DSewLOV81rA0VQtC/RFlSIax9q89UpuJGKz6lyRepT9aBg5BzQFnKbrGpOsj0Lgq3U3rtTNEGH+FTSImLSwPDSoiWQZHvn+Q9+07jGgmP0axcoRCAeMpMmcGtStgRmgfm7iUV7ie3GRKjdrEZzpFG20Nqki6hdKdWTQFxFfuhIErFDhHzBuQVbn0i0JAaJDfGxJ/xsBJh35Hh5ZANA7y0430YAu5dYBKxbjacINGVzG9SwZ9LHXFIM0tEEKs9fG8EkZUPc669KL0Q13P8j5gP8riwwfz/FsM3TwYisfTjOFz5Eq+epZM5guIewLf8x4HUjMm8ffUP0G25eMOcJ0S7CQ0grGkaiDNo0lta2yIrEGhFwe6kwwDEDsu8844XNFXSqH1ibjhFoYy6YmFNRiFLmErUIWJ16+V2jYl4RPzas+14KHNBvnGWmCma47RDl7rtqSyV4xCenWJ+UEzTbd+yBcNN8s8qXeU0Uy1MJIl2BbY/+U8UEytzQxnzlCKD3UlOr32trEhjN/ItGnyiESjElrCW5yq3hjouK39YMDKJWRMis7LSfJeY7zAIE/BfAVOIvXoW2ZqSaPpjUWM75ilrp9CLCJVrys+RMhVqNN5ikNw3MJZSQYYPAQMglMTWxEMzD/bCyrIe0MM3WwNpIkudcUvMduorZp+fxRqJfr8ZLTYOIp4ehyjZ9OsFz1Bj4AgSAqXL/HUw6FFLFSFU49fsxAED5IQ8Aqvdjs+y7IqgelxS6OsE8+b4KDCXKck9Vhwki6jOCoeF+jX7V6+ZF9/1YUJFwExJJ7YraKkZAsQ5xTNyPeUiTJla7mxSu2d+6r5LM3I+VQboSgCkhxLdxG2z8F2AAPtd0UKyJkPIAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss17944 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAzRJREFUeNrsWs2Nm0AUnl1RAFIKCK4gdLBwy9GuYL0VePeWm+1bbt5UYLsC7FtueCswqWDdQCTSQd5bfRM9IcADDD+OeNIIbI8f8837f4NSI4000kg16K7uHz99++nTZUkj/f3965PNRRHvFV2+0FgT76QOj/sGz2dQUxpzWkhgEVQgeC/r8mkCzO1Ao9w+gEnyb8bGSCU8qEPRri3Ebycab5bW9JnVG/cXGvuCeSmNA9ngxRgYgeIFv3ekbk2IwU0IXGqqiv4NgNI2mGsGjsGfi9ThkYZXoIoPDRbrCb4siaTOxhsBI1GvctT1QSzgLW9OA3cf42NCfMOcOfx70JZXlDt5atGGapHT4KFrGn+QeVgDxrxIIi9QtV3nwOCJVm2IiXi/NuXhVNB9T8SXV+1iYROB6pY8mxKbi9yNPeKEQHEAj4YYB6oAk+qxg8ocCFzYg8Qer0nNaWpTcBynLlFlQk2rSfDg6L7DXQ5s1m2DAIZqm7OFGPf/jcS2Bfe3Cwz9C1+kRz6+u11gCOo69v1AGvZRpOK32wMGRxGJLH2FWJggD4zadCaOZelMUYsFol5iIKGOhQjoMdSTnckFc44cD4tK/V4kRouL0ErYZPokLKFQlu64D0UmozeEnco78doMSWJuJvX6hUZLWpLFvBCINUBxc/TZNMHt0saehNcLykDlADyIXDMFr2EAg13MRD+iSgd3I8LBxGRDOvWKSIY1uGcTd44sRNd4M1ugrLt7LmNEOT83LD8UvKHVCqGNOHbE1aQFp/PG/WDjmKAq6qSBGcUuqHfQF7CiLOQfcHHmlapqHWeev1AGhyBtANMPdQnQVuUcbND3KXLH1HBjPjIa2hCOfRwOzn3YmCsAzjOpVSLmLMUmXPOg7Fi4UN1C2qe+VZE95BHeMiuBhbAX91og1zkmXc992ZjOAXdFCS2AHhDHpqqk4yuObmfIMWMTG7srYRaLGBP22ZgRUtLrkGd3YV4MdNRACa49AhgtqRj3yTW3bwLM66KUL/GwZwTwNUqb2JaNearBawkWKPt8oy5XkbtPVIOzqQ6p6MSz9K0BX5T6QyQGtK/75s5II400Ui79FWAANjYkpEJzM+AAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss17945 {
  color: #FFF;
  padding: 48px;
}
.jss17946 {
  color: rgba(0, 0, 0, 0.87);
  width: 100%;
  display: flex;
  z-index: 2;
  opacity: 0.5;
  position: fixed;
  align-items: center;
  padding-left: 16px;
  padding-right: 16px;
  background-color: #424242;
}
.jss17947 {
  height: 100vh;
  display: flex;
  position: relative;
  max-height: 1600px;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: linear-gradient(rgba(0,130,170,0), #424242), url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/1.20e79364.jpg&quot;);
}
.jss17948 {
  margin: -200px auto 0 auto;
  padding: 64px 0 48px;
  max-width: 1200px;
}
.jss17949 {
  width: 50%;
  color: #FFF;
}
@media (max-width:1279.95px) {
  .jss17949 {
    width: 100%;
    padding: 24px;
  }
}
.jss17950 {
  margin: -150px auto 0 auto;
  padding: 16px;
  position: relative;
  max-width: 1200px;
  background: #fafafa;
  justify-content: center;
}
.jss17951 {
  margin: 0 auto;
  padding: 24px;
  display: flex;
}
.jss17952 {
  margin-bottom: 24px;
}
.jss17953 {
  text-decoration: none;
}
.jss17954 {
  margin: 24px auto;
  max-width: 1200px;
}
@media (max-width:1279.95px) {
  .jss17954 {
    max-width: 1366px;
  }
}
.jss17955 {
  height: 160px;
  display: flex;
  align-items: center;
}
.jss17956 {
  margin: auto;
  display: grid;
}
.jss17957 {
  height: 100vh;
  display: flex;
  position: relative;
  overflow: hidden;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/2.adfa400c.jpg&quot;);
  background-position: center;
}
.jss17958 {
  color: #FFF;
  background-color: #03a9f4;
}
.jss17959 {
  color: #03a9f4;
  background-color: #fff;
}
.jss17960 {
  color: #03a9f4;
  padding: 0;
}
.jss17961 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss17961 {
    width: calc(100%);
  }
}
.jss17962 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss17962 {
    width: calc(100% - 300px);
  }
}
.jss17963 {
  display: none;
}
@media (min-width:600px) {
  .jss17963 {
    display: block;
  }
}
.jss17964 {
  color: #fff;
  position: relative;
  background: #03a9f4;
}
.jss17965 {
  width: 100%;
  position: relative;
  border-radius: 14px;
  background-color: rgba(255, 255, 255, 0.5);
}
.jss17965:hover {
  background-color: rgba(255, 255, 255, 0.7);
}
@media (min-width:600px) {
  .jss17965 {
    width: auto;
  }
}
.jss17966 {
  width: 72px;
  color: #212121;
  height: 100%;
  display: flex;
  position: absolute;
  align-items: center;
  pointer-events: none;
  justify-content: center;
}
.jss17967 {
  color: #fff;
  width: 100%;
}
.jss17968 {
  color: #212121;
  width: 100%;
  transition: width 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  padding-top: 8px;
  padding-left: 64px;
  padding-right: 8px;
  padding-bottom: 8px;
}
@media (min-width:600px) {
  .jss17968 {
    width: 80px;
  }
  .jss17968:focus {
    width: 200px;
  }
}
.jss17969 {
  color: #212121;
}
.jss17970 {
  background-color: #4fc3f7;
}
.jss17971 {
  margin-left: 12px;
  margin-right: 36px;
}
.jss17972 {
  display: none;
}
@media (min-width:960px) {
  .jss17973 {
    display: none;
  }
}
.jss17974 {
  width: 300px;
}
.jss17975 {
  display: flex;
  padding: 0 8px;
  min-height: 56px;
  align-items: center;
}
@media (min-width:0px) and (orientation: landscape) {
  .jss17975 {
    min-height: 48px;
  }
}
@media (min-width:600px) {
  .jss17975 {
    min-height: 64px;
  }
}
.jss17976 {
  margin-top: 65px;
  justify-content: flex-start;
}
.jss17977 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss17979 {
  width: 100%;
  height: 100%;
  opacity: 0.2;
  content: &quot;&quot;;
  display: block;
  position: absolute;
  background: linear-gradient(rgba(0,130,170,0), #03a9f4));
  background-size: cover;
  background-position: center center;
}
.jss17980 {
  width: 60px;
  overflow-x: hidden;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss17981 {
  color: inherit;
}
.jss17982 {
  margin-left: 56px;
}
.jss17983 {
  bottom: 16px;
}
.jss17984 {
  color: rgba(0, 0, 0, 0.87);
  background-size: cover;
  background-color: #fff;
  background-image: url(https://xdev.equine.co.id/apps/tessa/static/media/pattern2.f069796d.svg);
  background-repeat: no-repeat;
  background-position: inherit;
}
.jss17985 {
  color: #fff;
  background-color: #03a9f4;
}
.jss17986 {
  color: #fff;
  background-color: #ff9800;
}
.jss17987 {
  color: #fff;
  background-color: #f44336;
}
.jss17988 {
  color: #fff;
  background: #4caf50;
}
.jss17989 {
  flex: 1;
}
.jss17990 {
  padding: 16px;
  flex-grow: 1;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss17990 {
    margin-top: 68px;
  }
}
@media (max-width:959.95px) {
  .jss17990 {
    padding: 0;
    margin-top: 56px;
  }
}
@media (max-width:959.95px) {
  .jss17991 {
    margin-top: 68px;
  }
}
@media (min-width:960px) {
  .jss17991 {
    left: 50%;
    width: 982px;
    position: absolute;
    transform: translateX(-50%);
  }
}
@media (min-width:960px) {
  .jss17992 {
    margin-left: 300px;
    margin-right: 0;
  }
}
.jss17993 {
  margin-bottom: 56px;
}
.jss17994 {
  width: calc(100% - 0px);
  right: 0;
  bottom: 0;
  display: flex;
  position: fixed;
}
@media (min-width:960px) {
  .jss17994 {
    width: calc(100% - 300px);
  }
}
.jss17996 {
  background: #fff;
}
.jss17997 {
  margin-bottom: 60px;
}
.jss17998 {
  height: 4px;
}
.jss17999 {
  display: flex;
  align-items: center;
  justify-content: space-between;
}
.jss18000 {
  margin: 0 15px 0 0;
  padding: 16px 0;
  flex-grow: 1;
}
@media (min-width:600px) {
  .jss18000 {
    width: 30%;
    flex-grow: 0;
  }
}
.jss18001 {
  color: inherit;
  font-size: 0.6964285714285714rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: inherit;
  line-height: 1.66;
  letter-spacing: 0.03333em;
}
.jss18002 {
  color: #fff;
  background: #03a9f4;
}
.jss18003 {
  color: #fff;
  min-height: 40px;
  background: #ff9800;
  margin-bottom: 16px;
}
.jss18004 {
  height: 500px;
  display: flex;
  overflow: hidden;
  align-items: center;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss18004 {
    height: 300px;
  }
}
.jss18005 {
  height: 100%;
  margin: 0 auto;
}
.jss18006 {
  width: 25px;
  height: 25px;
  margin-right: 16px;
}
.jss18007 {
  transform: rotate(0deg);
  transition: transform 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
}
.jss18008 {
  transform: rotate(180deg);
}
.jss18009 {
  min-width: 350px;
}
.jss18010 {
  display: flex;
  flex-wrap: wrap;
}
.jss18011 {
  padding: 0;
}
@media (min-width:0px) {
  .jss18011 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss18011 {
    width: calc(100% / 2);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1280px) {
  .jss18011 {
    width: calc(100% / 3);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1920px) {
  .jss18011 {
    width: calc(100% / 4);
    padding: 0 16px 16px 0;
  }
}
.jss18012 {
  padding: 0;
}
@media (min-width:0px) {
  .jss18012 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss18012 {
    width: calc(100% / 2);
  }
}
@media (min-width:1280px) {
  .jss18012 {
    width: calc(100% - (100% / 3));
  }
}
@media (min-width:1920px) {
  .jss18012 {
    width: calc(100% - (100% / 4));
  }
}
.jss18013 {
  padding: 0;
}
@media (min-width:0px) {
  .jss18013 {
    width: 100%;
  }
}
.jss18014 {
  width: 100%;
  margin-bottom: 16px;
  vertical-align: middle;
}
.jss18015 {
  right: 16px;
  display: flex;
  position: absolute;
}
.jss18016 {
  right: 16px;
  bottom: 16px;
  position: fixed;
}
.jss18017 {
  float: right;
}
.jss18018 {
  margin: 0;
}
.jss18019 {
  margin: 8px;
}
.jss18020 {
  margin-left: 8px;
}
.jss18021 {
  margin-top: 8px;
}
.jss18022 {
  margin-right: 8px;
}
.jss18023 {
  margin-bottom: 8px;
}
.jss18024 {
  margin: 16px;
}
.jss18025 {
  margin-left: 16px;
}
.jss18026 {
  margin-top: 16px;
}
.jss18027 {
  margin-right: 16px;
}
.jss18028 {
  margin-bottom: 16px;
}
.jss18029 {
  margin: 24px;
}
.jss18030 {
  margin-left: 24px;
}
.jss18031 {
  margin-top: 24px;
}
.jss18032 {
  margin-right: 24px;
}
.jss18033 {
  margin-bottom: 24px;
}
.jss18034 {
  padding: 0;
}
.jss18035 {
  padding: 8px;
}
.jss18036 {
  padding-left: 8px;
}
.jss18037 {
  padding-top: 8px;
}
.jss18038 {
  padding-right: 8px;
}
.jss18039 {
  padding-bottom: 8px;
}
.jss18040 {
  padding: 16px;
}
.jss18041 {
  padding-left: 16px;
}
.jss18042 {
  padding-top: 16px;
}
.jss18043 {
  padding-right: 16px;
}
.jss18044 {
  padding-bottom: 16px;
}
.jss18045 {
  padding: 24px;
}
.jss18046 {
  padding-left: 24px;
}
.jss18047 {
  padding-top: 24px;
}
.jss18048 {
  padding-right: 24px;
}
.jss18049 {
  padding-bottom: 24px;
}
.jss18050 {
  background-color: #03a9f4;
}
.jss18051 {
  background-color: #ff9800;
}
.jss18052 {
  color: white;
  background-color: #f44336;
}
.jss18053 {
  color: #03a9f4;
}
.jss18054 {
  color: #ff9800;
}
.jss18055 {
  color: #f44336;
}
.jss18056 {
  text-decoration: line-through;
}
.jss18057 {
  padding: 3px;
}
.jss18058 {
  height: calc(100vh - 96px - 72px);
  overflow-x: auto;
}
.jss18059 {
  width: calc(100vw - 32px - 300px);
}
.jss18060 {
  height: calc(100vh - 56px - 68px);
  overflow-x: auto;
}
.jss18061 {
  width: calc(100vw);
}
.jss18062 {
  overflow-x: auto;
}
.jss18063 {
  table-layout: initial;
}
.jss18064 {
  top: 0;
  position: sticky;
  background-color: #fff;
}
.jss18065 {
  width: 3vw;
}
.jss18066 {
  width: 5vw;
}
.jss18067 {
  width: 10vw;
}
.jss18068 {
  width: 15vw;
}
.jss18069 {
  min-width: 15vw;
  max-width: 15vw;
}
@media (min-width:960px) {
  .jss18069 {
    min-width: calc((100vw - 300px - 48px) * 0.15);
    max-width: calc((100vw - 300px - 48px) * 0.15);
  }
}
.jss18070 {
  min-width: 25vw;
  max-width: 25vw;
}
@media (min-width:960px) {
  .jss18070 {
    min-width: calc((100vw - 300px - 48px) * 0.25);
    max-width: calc((100vw - 300px - 48px) * 0.25);
  }
}
.jss18071 {
  width: 25vw;
}
.jss18072 {
  width: 30vw;
}
.jss18073 {
  min-width: 45vw;
  max-width: 45vw;
}
@media (min-width:960px) {
  .jss18073 {
    min-width: calc((100vw - 300px - 48px) * 0.45);
    max-width: calc((100vw - 300px - 48px) * 0.45);
  }
}
.jss18074 {
  width: 100%;
  overflow-x: auto;
}
.jss18075 {
  width: 100%;
  margin-top: 24px;
  overflow-x: auto;
}
.jss18076 {
  min-width: 700px;
}
.jss18077 {
  width: 100%;
}
.jss18078 {
  min-width: 100px;
}
.jss18079 {
  color: rgba(0, 0, 0, 0.54);
  flex-shrink: 0;
}
.jss18080 {
  color: #2196f3;
}
.jss18081 {
  padding-top: 16px;
  padding-bottom: 16px;
}
.jss18083 {
  max-height: 100px;
}
.jss18084 {
  margin-top: -144px;
  margin-left: -56px;
  margin-right: -136px;
  margin-bottom: -64px;
}
.jss18085 {
  margin-top: -80px;
}
.jss18086 {
  width: 500px;
  height: 450px;
}
.jss18087 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss18088 {
  display: flex;
  box-sizing: border-box;
  align-items: center;
}
.jss18090:hover {
  background-color: #eeeeee;
}
.jss18091 {
  flex: 1;
}
.jss18092 {
  cursor: initial;
}
.jss18093 {
  background: #f44336;
}
.jss18094 {
  color: #fff;
}
.jss18095 {
  width: 100%;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
.jss18096 {
  max-height: 75vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss18097 {
  padding: 10px;
  text-align: center;
}
.jss18098 {
  padding: 0;
  vertical-align: top;
}
.jss18099 {
  margin-top: 8px;
  padding-left: 32px;
}
.jss18100 {
  top: 0;
  padding: 5px;
  position: sticky;
  overflow: hidden;
  max-width: 100px;
  max-height: 10px;
  text-align: center;
  background-color: #fff;
}
.jss18101 {
  max-height: 76vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss18102 {
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss18103 {
  padding: 0;
  margin-top: 8px;
}
.jss18104 {
  left: 0;
  width: 500px;
  z-index: 1;
  padding: 8px 16px 0 16px;
  min-width: 300px;
  background: #fff;
  vertical-align: top;
}
.jss18105 {
  padding: 8px 16px 0 16px;
  min-width: 300px;
  vertical-align: top;
}
.jss18106 {
  min-width: 300px;
}
.jss18107 {
  transform: rotate(180deg);
  writing-mode: vertical-rl;
}
.jss18108 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss18109 {
  cursor: pointer;
  transition: all .3s;
}
.jss18109:hover {
  background-color: #ebebeb;
}
.jss18110 {
  background-color: #ebebeb;
}
.jss18111 {
  transition: all .3s;
}
.jss18111:hover {
  transform: scale(1.03);
}
.jss18112 {
  padding: 0;
  transition: all .3s;
}
.jss18112:hover {
  transform: scale(1.03);
}
.jss18113 {
  display: grid;
  align-items: center;
  grid-column-gap: 1.5rem;
  grid-template-columns: 1fr max-content 1fr;
}
.jss18113::after, .jss18113::before {
  height: 1px;
  content: '';
  display: block;
  background-color: currentColor;
}
.jss18114 a {
  text-decoration: none;
}
.jss18115 {
  left: 50%;
  position: absolute;
  transform: ;
}
.jss18116 {
  display: -webkit-box;
  overflow: hidden;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;
}
.jss18117 {
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
.jss18118 {
  display: inline;
  text-transform: capitalize;
}
.jss18119 {
  text-decoration: none;
}
.jss18119 p:hover {
  color: #03a9f4;
}
.jss18120 {
  margin-left: 4px;
}
.jss18121 {
  padding: 0 16px;
  display: inline-flex;
  position: relative;
  vertical-align: middle;
}
.jss18122 {
  top: 0;
  color: #fff;
  right: 0;
  height: 20px;
  display: flex;
  padding: 0 4px;
  z-index: 1;
  position: absolute;
  flex-wrap: wrap;
  min-width: 20px;
  transform: translate(20px, -50%);
  box-sizing: border-box;
  transition: transform 225ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  align-items: center;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  align-content: center;
  border-radius: 10px;
  flex-direction: row;
  justify-content: center;
  background-color: #ff9800;
  transform-origin: 100% 0%;
}
.jss18123 {
  margin: 0;
  padding: 16px;
  border-bottom: 1px solid rgba(0, 0, 0, 0.12);
}
.jss18124 {
  top: 8px;
  right: 8px;
  color: #9e9e9e;
  position: absolute;
}
.jss18125 {
  margin: 0;
  padding: 16px;
}
.jss18126 {
  margin: 0;
  padding: 8px;
  border-top: 1px solid rgba(0, 0, 0, 0.12);
}
.jss18127 {
  font-size: 13px;
}
.jss18128 {
  padding: 0;
  overflow: hidden;
}
.jss18128 span {
  display: inline-flex;
}
.jss18128 span:hover {
  position: relative;
  animation: moveTextHorz 3s linear infinite;
}
@-webkit-keyframes moveTextHorz {
  0% {
    transform: translate(0, 0);
  }
  100% {
    transform: translate(-70%, 0);
  }
}
.jss18129 span {
  color: #f44336;
}
.jss18130 {
  width: 50px;
}
@media (min-width:600px) {
  .jss18130 {
    right: 90px;
  }
}
@media (min-width:960px) {
  .jss18130 {
    right: 15px;
  }
}
.jss18131 {
  cursor: pointer;
}
.jss18131:hover {
  background: #0288d1;
}
.jss18132 {
  margin-left: 10px;
  vertical-align: middle;
}
.jss18133 {
  width: 100%;
}
.jss18134 {
  width: 100%;
  margin: 0;
}
@media (min-width:960px) {
  .jss18135 {
    margin-left: auto;
    margin-right: 16px;
  }
}
@media (min-width:1280px) {
  .jss18135 {
    margin-left: auto;
    margin-right: 32px;
  }
}
@media (min-width:960px) {
  .jss18136 {
    transform: translateY(75%);
    margin-left: 16px;
  }
}
@media (min-width:1280px) {
  .jss18136 {
    transform: translateY(75%);
    margin-left: 32px;
  }
}
.jss18137 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
@media (max-width:1279.95px) {
  .jss18138 {
    display: none;
  }
}
@media (max-width:1919.95px) {
  .jss18139 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss18140 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss18141 {
    text-align: right;
  }
}
.jss18142 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss18142 {
    text-align: right;
  }
}
.jss18143 {
  padding: 0;
}
@media (min-width:1280px) {
  .jss18144 {
    margin-top: 0 !important;
  }
}
@media (min-width:1280px) {
  .jss18145 {
    margin-top: 64px;
  }
}
.jss18146 div textarea {
  overflow: auto;
}
.jss18147 {
  width: 70%;
}
@media (min-width:1280px) {
  .jss18147 {
    float: right;
  }
}
@media (min-width:1280px) {
  .jss18148 {
    float: right;
  }
}
@media (max-width:1279.95px) {
  .jss18148 {
    margin-top: 6px;
  }
}
@media (min-width:1280px) {
  .jss18149 {
    padding: 12px 0;
  }
}
@media (max-width:1279.95px) {
  .jss18149 {
    padding: 6px 0 3px;
  }
}
.jss18150 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss18150 {
    margin-top: 0 !important;
  }
}
.jss18150 div textarea {
  overflow: auto;
}
.jss18151 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss18152 {
  width: 57px;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
  overflow-x: hidden;
}
@media (min-width:600px) {
  .jss18152 {
    width: 73px;
  }
}
@media (min-width:960px) {
  .jss18153 {
    width: calc(100% - 73px);
    margin-left: 73px;
    margin-right: 0;
  }
}
@media (min-width:960px) {
  .jss18154 {
    margin-left: 73px;
    margin-right: 0;
  }
}
.jss18155 {
  bottom: 0px;
  z-index: 1;
  position: sticky;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss18155 {
    display: none;
  }
}
.jss18155:hover {
  background-color: rgb(235, 235, 235);
}
@media (max-width:959.95px) {
  .jss18156 {
    display: none;
  }
}
.jss18157 {
  top: 0;
  left: auto;
  width: 100%;
  right: 0;
  height: 100%;
  z-index: 1301;
  overflow: hidden;
  position: absolute;
  overflow-y: auto;
  transition: transform 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
@media (min-width:960px) {
  .jss18157 {
    width: 24vw;
  }
}
.jss18158 {
  cursor: pointer;
}
.jss18158:hover {
  background-color: #eeeeee;
}
.jss18159 {
  background-color: #e0e0e0 !important;
}
.jss18160 {
  transition: width 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
.jss18161 {
  border-collapse: separate;
}
.jss18162:hover {
  background-color: rgb(235, 235, 235);
}
.jss18163 {
  position: relative;
  min-height: 80vh;
}
.jss18163 .back-icon {
  top: 5px;
  left: 5px;
  z-index: 1;
  position: absolute;
}
.jss18163 > .ninebox-inner {
  width: 100%;
  position: relative;
  min-height: 80vh;
}
.jss18163 > .ninebox-inner .category-paper {
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #424242;
}
.jss18163 > .ninebox-inner .percentage-wrapper {
  width: 100%;
  height: 100%;
  display: flex;
  flex-wrap: wrap;
  box-sizing: border-box;
}
.jss18163 > .ninebox-inner .horizontal-category {
  height: 50px;
  margin: 0 60px;
  display: flex;
  align-items: center;
  justify-content: center;
}
.jss18163 > .ninebox-inner .vertical-category {
  width: 50px;
  height: calc(80vh - 120px);
  display: inline-flex;
  align-items: center;
  justify-content: center;
}
.jss18163 > .ninebox-inner .ninebox-content {
  width: calc(100% - 100px);
  height: calc(80vh - 120px);
  display: inline-block;
  position: relative;
}
.jss18163 > .ninebox-inner .ninebox-content .wrapper {
  width: 100%;
  height: 100%;
  margin: 0;
}
.jss18163 > .ninebox-inner .ninebox-content .wrapper .box {
  height: calc(100% / 3);
  padding: 10px;
}
.jss18163 > .ninebox-inner .ninebox-content .wrapper .box .has-item {
  padding: 26px 10px !important;
}
.jss18163 > .ninebox-inner .ninebox-content .wrapper .box .disabled {
  background: #d1d1d1;
}
.jss18163 > .ninebox-inner .ninebox-content .wrapper .box .card {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 30px;
  position: relative;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  -webkit-transition: all .4s ease-out;
}
.jss18163 > .ninebox-inner .ninebox-content .wrapper .box .card h6 {
  color: rgba(0, 0, 0, 0.87);
}
.jss18163 > .ninebox-inner .ninebox-content .wrapper .box .card .total-person {
  top: 5px;
  right: 5px;
  width: 15px;
  height: 15px;
  position: absolute;
}
.jss18163 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list {
  width: 100%;
  height: 100%;
  display: flex;
  overflow-y: auto;
  align-items: center;
}
.jss18163 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list > ol {
  height: 100%;
  margin: 0;
  padding: 0;
  text-align: left;
  list-style: none;
}
.jss18163 > .ninebox-inner .ninebox-content .wrapper .box .active:hover {
  cursor: pointer;
  transform: scale(1.1);
}
.jss18163 > .ninebox-inner .vertical-category .text-rotate {
  transform: rotate(270deg);
}
.jss18163 > .ninebox-inner .vertical-category.left {
  padding: 10px 0;
  margin-right: 9px;
}
.jss18163 > .ninebox-inner .vertical-category.right {
  margin-left: 9px;
}
.jss18163 > .ninebox-inner .vertical-category .text-rotate.nowrap {
  white-space: nowrap;
}
.jss18163 > .ninebox-inner .horizontal-category.top {
  padding: 0 9px;
  margin-bottom: 9px;
}
.jss18163 > .ninebox-inner .horizontal-category.bottom {
  margin-top: 9px;
}
.jss18163 > .ninebox-inner .percentage-wrapper > .percentage-item {
  padding: 0 10px;
  flex-grow: 0;
  max-width: calc(100% / 3);
  flex-basis: calc(100% / 3);
}
.jss18163 > .ninebox-inner .percentage-wrapper > .percentage-item.horizontal {
  height: calc(100% / 3);
  padding: 10px 0;
  max-width: 100%;
}
.jss18163 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 10px;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  background-color: #424242;
  -webkit-transition: all .4s ease-out;
}
.jss18163 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper > p {
  color: #fff;
}
.jss18163 > .ninebox-inner .category-paper > p {
  color: #fff;
}
.jss18164 {
  max-height: 45vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss18164 .table {
  border-collapse: separate;
}
.jss18164 .table .sticky-head {
  top: 0;
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss18165 {
  height: 44vh;
  overflow: auto;
}
.jss18166 {
  width: 100%;
  height: 50vh;
  overflow: auto;
}
.jss18167 {
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss18168 {
  width: 100%;
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss18169 {
  max-width: 82%;
}

@media (min-width:960px) {
  ::-webkit-scrollbar {
    width: 5px;
    height: 5px;
  }
  ::-webkit-scrollbar-track {
    background: rgba(250, 250, 250, 0);
  }
  ::-webkit-scrollbar-thumb {
    background: #eeeeee;
  }
  ::-webkit-scrollbar-thumb:hover {
    background: #e0e0e0;
  }
}
.jss17706 {
  width: 100%;
  z-index: 1;
  overflow: hidden;
  margin-bottom: 0;
}
.jss17707 {
  background-color: #424242;
}
.jss17708 {
  height: 70px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/etg.9362c8fb.svg&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss17709 {
  height: 150px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/tessa.55f0b81f.svg&quot;);
  background-repeat: no-repeat;
}
.jss17710 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2hJREFUeNrsWtGR2jAQ1TEU4FQQUsGRCuBm+IerAKgguAKgAnMVHFcBvn/PnFwBvg6cCuIOEslacRvFyJJswL74zQgDNrKedvftSoaQDh06dHDAnfGVfjRjr0PWRg3kkbD2RIJJakfMjzbsdd0CQz0yciF/0zf8wRyOKbSmgXuSx1rAmhWxgbSdnJFG4cOj5DhJz7KLrC3i0a8wSwM8QzcRjGCS1U+MkLcbE9uztjQn5kcyEFMsn5q4uxW09+8hQh5rR/buCNY4OtxsBzMpEaLZ/cIm6g6uyZTrtih+d6C8/PxDLuEiT1mhp7jWEH32HIi95sr5kRKe4PiTtRWbuN/5UfS9hXOUEd6gSXgBa/gwpoMyLiMh6ym5gJxmSsyuCxaK71OUAyka0AC+i8H9YzjnofEsNfd5NyWWnCwVTGgF35/CoPbK9ykMXt5nDEQpWIaC1aZwnlvqnrXvLq7YV2Z2CDesijUMCLv0FNxQYgQWkWIl3fOAfrcq+N46xuJTh8I1XOCBi4nyRqjqEMUZVSyWIQvNlZAI4fr4TIyNTIlh93O12gHJ8AxUluMHkI2V+xwhJmVsviniEAOBtbsr8izuRykM7L7G4lRacl2SizxFiRd1llQUOjSx2BYN9lpVvyzjMkgtxsRiIDbIa0Fd5SFyz6apRXCvwGJV46yBxISFUhPVaeOyJQE/HpcsW8aO2wV8sZqgfhZI6m2w1IVKEbE4l2oRZ55mzTN2dNeZUknMHftZ6GK8iJgaZ6FmZ8il9KIFE+mC0M4VuZv4Ec7u4RlVDMs6N4JQ14urojqrrVXGc8TiU+XA4+yTqCJRgnt8RhU9CGAb4mLljAVJbArZlk9pHgYOmznUoIpeEffd4R16/+zo8p7Sj4EriplIkDxfEtklfqfbfkugOh+cVTM/spd7dXUeTB4h2duRwkneklhc6vvVthDq78dAFYnLPkMb5J6AqbO2Eivb4k40cr8i4rGNS/G6R/08O66WH3QuXPa0JS6RW9dVsO6z7baDk8WoJle51om7f5Yxbill706Mm/qjIC6KweoCU1c/lq7YWnU0IUY/K7H3GgTj6ih/oiKq+F+oqk5IyZOOK+MriI98WPnNjJggF5C/Hyg0Faf/edj8M4cn6mlZ/rgRuBe9XqLm7NChw3+GPwIMAA139Xg+K8vEAAAAAElFTkSuQmCC&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss17711 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABANJREFUeNrsWt1x4jAQNpl7P18FcSqIqSBmhvdABYEKiCvwUQFcBfZVEHhnBl8F8VUQSvB1cJJ3jddC/pOlQBg0YwYUIfbT/n27imXdxtcag947+DuPvXqa5TpYq3F0PmD+bsZeQ0OHvmbg/HMB+2CvDntS9iSaADn48PGDgUtVNvmmQQg+fjEBfmqBBaa9x08ue2KVbe6uNXhcOrDDdQJbjQ/XqjHrBuxLAYMIeVZg9xrhuJegsTx5ztjphuyxe2qI58LVJURFSnk4vfpgwr0qmRywmECgVMrAdJBgB095QmaTDPRqHBv7rnFgZUcPCc3igzP0pfTkwewW7LGJWfO168soW9oJXHDJ6gPwVQnv5wCrNrEDgnSFuXlfszPB7utYfw7EFioBOmLy9wuroE8Lz6ACRBPZXfatmvUD83cu+o0r5DiukT+SIpSve0RTtU2Y5kCTlkLl05dreX7unkeIiTkfS+VKGhJ7QDQYsb3mnw+srCludiMmSNKwPq5lE2DSewLOV81rA0VQtC/RFlSIax9q89UpuJGKz6lyRepT9aBg5BzQFnKbrGpOsj0Lgq3U3rtTNEGH+FTSImLSwPDSoiWQZHvn+Q9+07jGgmP0axcoRCAeMpMmcGtStgRmgfm7iUV7ie3GRKjdrEZzpFG20Nqki6hdKdWTQFxFfuhIErFDhHzBuQVbn0i0JAaJDfGxJ/xsBJh35Hh5ZANA7y0430YAu5dYBKxbjacINGVzG9SwZ9LHXFIM0tEEKs9fG8EkZUPc669KL0Q13P8j5gP8riwwfz/FsM3TwYisfTjOFz5Eq+epZM5guIewLf8x4HUjMm8ffUP0G25eMOcJ0S7CQ0grGkaiDNo0lta2yIrEGhFwe6kwwDEDsu8844XNFXSqH1ibjhFoYy6YmFNRiFLmErUIWJ16+V2jYl4RPzas+14KHNBvnGWmCma47RDl7rtqSyV4xCenWJ+UEzTbd+yBcNN8s8qXeU0Uy1MJIl2BbY/+U8UEytzQxnzlCKD3UlOr32trEhjN/ItGnyiESjElrCW5yq3hjouK39YMDKJWRMis7LSfJeY7zAIE/BfAVOIvXoW2ZqSaPpjUWM75ilrp9CLCJVrys+RMhVqNN5ikNw3MJZSQYYPAQMglMTWxEMzD/bCyrIe0MM3WwNpIkudcUvMduorZp+fxRqJfr8ZLTYOIp4ehyjZ9OsFz1Bj4AgSAqXL/HUw6FFLFSFU49fsxAED5IQ8Aqvdjs+y7IqgelxS6OsE8+b4KDCXKck9Vhwki6jOCoeF+jX7V6+ZF9/1YUJFwExJJ7YraKkZAsQ5xTNyPeUiTJla7mxSu2d+6r5LM3I+VQboSgCkhxLdxG2z8F2AAPtd0UKyJkPIAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss17712 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAzRJREFUeNrsWs2Nm0AUnl1RAFIKCK4gdLBwy9GuYL0VePeWm+1bbt5UYLsC7FtueCswqWDdQCTSQd5bfRM9IcADDD+OeNIIbI8f8837f4NSI4000kg16K7uHz99++nTZUkj/f3965PNRRHvFV2+0FgT76QOj/sGz2dQUxpzWkhgEVQgeC/r8mkCzO1Ao9w+gEnyb8bGSCU8qEPRri3Ebycab5bW9JnVG/cXGvuCeSmNA9ngxRgYgeIFv3ekbk2IwU0IXGqqiv4NgNI2mGsGjsGfi9ThkYZXoIoPDRbrCb4siaTOxhsBI1GvctT1QSzgLW9OA3cf42NCfMOcOfx70JZXlDt5atGGapHT4KFrGn+QeVgDxrxIIi9QtV3nwOCJVm2IiXi/NuXhVNB9T8SXV+1iYROB6pY8mxKbi9yNPeKEQHEAj4YYB6oAk+qxg8ocCFzYg8Qer0nNaWpTcBynLlFlQk2rSfDg6L7DXQ5s1m2DAIZqm7OFGPf/jcS2Bfe3Cwz9C1+kRz6+u11gCOo69v1AGvZRpOK32wMGRxGJLH2FWJggD4zadCaOZelMUYsFol5iIKGOhQjoMdSTnckFc44cD4tK/V4kRouL0ErYZPokLKFQlu64D0UmozeEnco78doMSWJuJvX6hUZLWpLFvBCINUBxc/TZNMHt0saehNcLykDlADyIXDMFr2EAg13MRD+iSgd3I8LBxGRDOvWKSIY1uGcTd44sRNd4M1ugrLt7LmNEOT83LD8UvKHVCqGNOHbE1aQFp/PG/WDjmKAq6qSBGcUuqHfQF7CiLOQfcHHmlapqHWeev1AGhyBtANMPdQnQVuUcbND3KXLH1HBjPjIa2hCOfRwOzn3YmCsAzjOpVSLmLMUmXPOg7Fi4UN1C2qe+VZE95BHeMiuBhbAX91og1zkmXc992ZjOAXdFCS2AHhDHpqqk4yuObmfIMWMTG7srYRaLGBP22ZgRUtLrkGd3YV4MdNRACa49AhgtqRj3yTW3bwLM66KUL/GwZwTwNUqb2JaNearBawkWKPt8oy5XkbtPVIOzqQ6p6MSz9K0BX5T6QyQGtK/75s5II400Ui79FWAANjYkpEJzM+AAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss17713 {
  color: #FFF;
  padding: 48px;
}
.jss17714 {
  color: rgba(0, 0, 0, 0.87);
  width: 100%;
  display: flex;
  z-index: 2;
  opacity: 0.5;
  position: fixed;
  align-items: center;
  padding-left: 16px;
  padding-right: 16px;
  background-color: #424242;
}
.jss17715 {
  height: 100vh;
  display: flex;
  position: relative;
  max-height: 1600px;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: linear-gradient(rgba(0,130,170,0), #424242), url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/1.20e79364.jpg&quot;);
}
.jss17716 {
  margin: -200px auto 0 auto;
  padding: 64px 0 48px;
  max-width: 1200px;
}
.jss17717 {
  width: 50%;
  color: #FFF;
}
@media (max-width:1279.95px) {
  .jss17717 {
    width: 100%;
    padding: 24px;
  }
}
.jss17718 {
  margin: -150px auto 0 auto;
  padding: 16px;
  position: relative;
  max-width: 1200px;
  background: #fafafa;
  justify-content: center;
}
.jss17719 {
  margin: 0 auto;
  padding: 24px;
  display: flex;
}
.jss17720 {
  margin-bottom: 24px;
}
.jss17721 {
  text-decoration: none;
}
.jss17722 {
  margin: 24px auto;
  max-width: 1200px;
}
@media (max-width:1279.95px) {
  .jss17722 {
    max-width: 1366px;
  }
}
.jss17723 {
  height: 160px;
  display: flex;
  align-items: center;
}
.jss17724 {
  margin: auto;
  display: grid;
}
.jss17725 {
  height: 100vh;
  display: flex;
  position: relative;
  overflow: hidden;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/2.adfa400c.jpg&quot;);
  background-position: center;
}
.jss17726 {
  color: #FFF;
  background-color: #03a9f4;
}
.jss17727 {
  color: #03a9f4;
  background-color: #fff;
}
.jss17728 {
  color: #03a9f4;
  padding: 0;
}
.jss17729 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss17729 {
    width: calc(100%);
  }
}
.jss17730 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss17730 {
    width: calc(100% - 300px);
  }
}
.jss17731 {
  display: none;
}
@media (min-width:600px) {
  .jss17731 {
    display: block;
  }
}
.jss17732 {
  color: #fff;
  position: relative;
  background: #03a9f4;
}
.jss17733 {
  width: 100%;
  position: relative;
  border-radius: 14px;
  background-color: rgba(255, 255, 255, 0.5);
}
.jss17733:hover {
  background-color: rgba(255, 255, 255, 0.7);
}
@media (min-width:600px) {
  .jss17733 {
    width: auto;
  }
}
.jss17734 {
  width: 72px;
  color: #212121;
  height: 100%;
  display: flex;
  position: absolute;
  align-items: center;
  pointer-events: none;
  justify-content: center;
}
.jss17735 {
  color: #fff;
  width: 100%;
}
.jss17736 {
  color: #212121;
  width: 100%;
  transition: width 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  padding-top: 8px;
  padding-left: 64px;
  padding-right: 8px;
  padding-bottom: 8px;
}
@media (min-width:600px) {
  .jss17736 {
    width: 80px;
  }
  .jss17736:focus {
    width: 200px;
  }
}
.jss17737 {
  color: #212121;
}
.jss17738 {
  background-color: #4fc3f7;
}
.jss17739 {
  margin-left: 12px;
  margin-right: 36px;
}
.jss17740 {
  display: none;
}
@media (min-width:960px) {
  .jss17741 {
    display: none;
  }
}
.jss17742 {
  width: 300px;
}
.jss17743 {
  display: flex;
  padding: 0 8px;
  min-height: 56px;
  align-items: center;
}
@media (min-width:0px) and (orientation: landscape) {
  .jss17743 {
    min-height: 48px;
  }
}
@media (min-width:600px) {
  .jss17743 {
    min-height: 64px;
  }
}
.jss17744 {
  margin-top: 65px;
  justify-content: flex-start;
}
.jss17745 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss17747 {
  width: 100%;
  height: 100%;
  opacity: 0.2;
  content: &quot;&quot;;
  display: block;
  position: absolute;
  background: linear-gradient(rgba(0,130,170,0), #03a9f4));
  background-size: cover;
  background-position: center center;
}
.jss17748 {
  width: 60px;
  overflow-x: hidden;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss17749 {
  color: inherit;
}
.jss17750 {
  margin-left: 56px;
}
.jss17751 {
  bottom: 16px;
}
.jss17752 {
  color: rgba(0, 0, 0, 0.87);
  background-size: cover;
  background-color: #fff;
  background-image: url(https://xdev.equine.co.id/apps/tessa/static/media/pattern2.f069796d.svg);
  background-repeat: no-repeat;
  background-position: inherit;
}
.jss17753 {
  color: #fff;
  background-color: #03a9f4;
}
.jss17754 {
  color: #fff;
  background-color: #ff9800;
}
.jss17755 {
  color: #fff;
  background-color: #f44336;
}
.jss17756 {
  color: #fff;
  background: #4caf50;
}
.jss17757 {
  flex: 1;
}
.jss17758 {
  padding: 16px;
  flex-grow: 1;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss17758 {
    margin-top: 68px;
  }
}
@media (max-width:959.95px) {
  .jss17758 {
    padding: 0;
    margin-top: 56px;
  }
}
@media (max-width:959.95px) {
  .jss17759 {
    margin-top: 68px;
  }
}
@media (min-width:960px) {
  .jss17759 {
    left: 50%;
    width: 982px;
    position: absolute;
    transform: translateX(-50%);
  }
}
@media (min-width:960px) {
  .jss17760 {
    margin-left: 300px;
    margin-right: 0;
  }
}
.jss17761 {
  margin-bottom: 56px;
}
.jss17762 {
  width: calc(100% - 0px);
  right: 0;
  bottom: 0;
  display: flex;
  position: fixed;
}
@media (min-width:960px) {
  .jss17762 {
    width: calc(100% - 300px);
  }
}
.jss17764 {
  background: #fff;
}
.jss17765 {
  margin-bottom: 60px;
}
.jss17766 {
  height: 4px;
}
.jss17767 {
  display: flex;
  align-items: center;
  justify-content: space-between;
}
.jss17768 {
  margin: 0 15px 0 0;
  padding: 16px 0;
  flex-grow: 1;
}
@media (min-width:600px) {
  .jss17768 {
    width: 30%;
    flex-grow: 0;
  }
}
.jss17769 {
  color: inherit;
  font-size: 0.6964285714285714rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: inherit;
  line-height: 1.66;
  letter-spacing: 0.03333em;
}
.jss17770 {
  color: #fff;
  background: #03a9f4;
}
.jss17771 {
  color: #fff;
  min-height: 40px;
  background: #ff9800;
  margin-bottom: 16px;
}
.jss17772 {
  height: 500px;
  display: flex;
  overflow: hidden;
  align-items: center;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss17772 {
    height: 300px;
  }
}
.jss17773 {
  height: 100%;
  margin: 0 auto;
}
.jss17774 {
  width: 25px;
  height: 25px;
  margin-right: 16px;
}
.jss17775 {
  transform: rotate(0deg);
  transition: transform 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
}
.jss17776 {
  transform: rotate(180deg);
}
.jss17777 {
  min-width: 350px;
}
.jss17778 {
  display: flex;
  flex-wrap: wrap;
}
.jss17779 {
  padding: 0;
}
@media (min-width:0px) {
  .jss17779 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss17779 {
    width: calc(100% / 2);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1280px) {
  .jss17779 {
    width: calc(100% / 3);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1920px) {
  .jss17779 {
    width: calc(100% / 4);
    padding: 0 16px 16px 0;
  }
}
.jss17780 {
  padding: 0;
}
@media (min-width:0px) {
  .jss17780 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss17780 {
    width: calc(100% / 2);
  }
}
@media (min-width:1280px) {
  .jss17780 {
    width: calc(100% - (100% / 3));
  }
}
@media (min-width:1920px) {
  .jss17780 {
    width: calc(100% - (100% / 4));
  }
}
.jss17781 {
  padding: 0;
}
@media (min-width:0px) {
  .jss17781 {
    width: 100%;
  }
}
.jss17782 {
  width: 100%;
  margin-bottom: 16px;
  vertical-align: middle;
}
.jss17783 {
  right: 16px;
  display: flex;
  position: absolute;
}
.jss17784 {
  right: 16px;
  bottom: 16px;
  position: fixed;
}
.jss17785 {
  float: right;
}
.jss17786 {
  margin: 0;
}
.jss17787 {
  margin: 8px;
}
.jss17788 {
  margin-left: 8px;
}
.jss17789 {
  margin-top: 8px;
}
.jss17790 {
  margin-right: 8px;
}
.jss17791 {
  margin-bottom: 8px;
}
.jss17792 {
  margin: 16px;
}
.jss17793 {
  margin-left: 16px;
}
.jss17794 {
  margin-top: 16px;
}
.jss17795 {
  margin-right: 16px;
}
.jss17796 {
  margin-bottom: 16px;
}
.jss17797 {
  margin: 24px;
}
.jss17798 {
  margin-left: 24px;
}
.jss17799 {
  margin-top: 24px;
}
.jss17800 {
  margin-right: 24px;
}
.jss17801 {
  margin-bottom: 24px;
}
.jss17802 {
  padding: 0;
}
.jss17803 {
  padding: 8px;
}
.jss17804 {
  padding-left: 8px;
}
.jss17805 {
  padding-top: 8px;
}
.jss17806 {
  padding-right: 8px;
}
.jss17807 {
  padding-bottom: 8px;
}
.jss17808 {
  padding: 16px;
}
.jss17809 {
  padding-left: 16px;
}
.jss17810 {
  padding-top: 16px;
}
.jss17811 {
  padding-right: 16px;
}
.jss17812 {
  padding-bottom: 16px;
}
.jss17813 {
  padding: 24px;
}
.jss17814 {
  padding-left: 24px;
}
.jss17815 {
  padding-top: 24px;
}
.jss17816 {
  padding-right: 24px;
}
.jss17817 {
  padding-bottom: 24px;
}
.jss17818 {
  background-color: #03a9f4;
}
.jss17819 {
  background-color: #ff9800;
}
.jss17820 {
  color: white;
  background-color: #f44336;
}
.jss17821 {
  color: #03a9f4;
}
.jss17822 {
  color: #ff9800;
}
.jss17823 {
  color: #f44336;
}
.jss17824 {
  text-decoration: line-through;
}
.jss17825 {
  padding: 3px;
}
.jss17826 {
  height: calc(100vh - 96px - 72px);
  overflow-x: auto;
}
.jss17827 {
  width: calc(100vw - 32px - 300px);
}
.jss17828 {
  height: calc(100vh - 56px - 68px);
  overflow-x: auto;
}
.jss17829 {
  width: calc(100vw);
}
.jss17830 {
  overflow-x: auto;
}
.jss17831 {
  table-layout: initial;
}
.jss17832 {
  top: 0;
  position: sticky;
  background-color: #fff;
}
.jss17833 {
  width: 3vw;
}
.jss17834 {
  width: 5vw;
}
.jss17835 {
  width: 10vw;
}
.jss17836 {
  width: 15vw;
}
.jss17837 {
  min-width: 15vw;
  max-width: 15vw;
}
@media (min-width:960px) {
  .jss17837 {
    min-width: calc((100vw - 300px - 48px) * 0.15);
    max-width: calc((100vw - 300px - 48px) * 0.15);
  }
}
.jss17838 {
  min-width: 25vw;
  max-width: 25vw;
}
@media (min-width:960px) {
  .jss17838 {
    min-width: calc((100vw - 300px - 48px) * 0.25);
    max-width: calc((100vw - 300px - 48px) * 0.25);
  }
}
.jss17839 {
  width: 25vw;
}
.jss17840 {
  width: 30vw;
}
.jss17841 {
  min-width: 45vw;
  max-width: 45vw;
}
@media (min-width:960px) {
  .jss17841 {
    min-width: calc((100vw - 300px - 48px) * 0.45);
    max-width: calc((100vw - 300px - 48px) * 0.45);
  }
}
.jss17842 {
  width: 100%;
  overflow-x: auto;
}
.jss17843 {
  width: 100%;
  margin-top: 24px;
  overflow-x: auto;
}
.jss17844 {
  min-width: 700px;
}
.jss17845 {
  width: 100%;
}
.jss17846 {
  min-width: 100px;
}
.jss17847 {
  color: rgba(0, 0, 0, 0.54);
  flex-shrink: 0;
}
.jss17848 {
  color: #2196f3;
}
.jss17849 {
  padding-top: 16px;
  padding-bottom: 16px;
}
.jss17851 {
  max-height: 100px;
}
.jss17852 {
  margin-top: -144px;
  margin-left: -56px;
  margin-right: -136px;
  margin-bottom: -64px;
}
.jss17853 {
  margin-top: -80px;
}
.jss17854 {
  width: 500px;
  height: 450px;
}
.jss17855 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss17856 {
  display: flex;
  box-sizing: border-box;
  align-items: center;
}
.jss17858:hover {
  background-color: #eeeeee;
}
.jss17859 {
  flex: 1;
}
.jss17860 {
  cursor: initial;
}
.jss17861 {
  background: #f44336;
}
.jss17862 {
  color: #fff;
}
.jss17863 {
  width: 100%;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
.jss17864 {
  max-height: 75vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss17865 {
  padding: 10px;
  text-align: center;
}
.jss17866 {
  padding: 0;
  vertical-align: top;
}
.jss17867 {
  margin-top: 8px;
  padding-left: 32px;
}
.jss17868 {
  top: 0;
  padding: 5px;
  position: sticky;
  overflow: hidden;
  max-width: 100px;
  max-height: 10px;
  text-align: center;
  background-color: #fff;
}
.jss17869 {
  max-height: 76vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss17870 {
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss17871 {
  padding: 0;
  margin-top: 8px;
}
.jss17872 {
  left: 0;
  width: 500px;
  z-index: 1;
  padding: 8px 16px 0 16px;
  min-width: 300px;
  background: #fff;
  vertical-align: top;
}
.jss17873 {
  padding: 8px 16px 0 16px;
  min-width: 300px;
  vertical-align: top;
}
.jss17874 {
  min-width: 300px;
}
.jss17875 {
  transform: rotate(180deg);
  writing-mode: vertical-rl;
}
.jss17876 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss17877 {
  cursor: pointer;
  transition: all .3s;
}
.jss17877:hover {
  background-color: #ebebeb;
}
.jss17878 {
  background-color: #ebebeb;
}
.jss17879 {
  transition: all .3s;
}
.jss17879:hover {
  transform: scale(1.03);
}
.jss17880 {
  padding: 0;
  transition: all .3s;
}
.jss17880:hover {
  transform: scale(1.03);
}
.jss17881 {
  display: grid;
  align-items: center;
  grid-column-gap: 1.5rem;
  grid-template-columns: 1fr max-content 1fr;
}
.jss17881::after, .jss17881::before {
  height: 1px;
  content: '';
  display: block;
  background-color: currentColor;
}
.jss17882 a {
  text-decoration: none;
}
.jss17883 {
  left: 50%;
  position: absolute;
  transform: ;
}
.jss17884 {
  display: -webkit-box;
  overflow: hidden;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;
}
.jss17885 {
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
.jss17886 {
  display: inline;
  text-transform: capitalize;
}
.jss17887 {
  text-decoration: none;
}
.jss17887 p:hover {
  color: #03a9f4;
}
.jss17888 {
  margin-left: 4px;
}
.jss17889 {
  padding: 0 16px;
  display: inline-flex;
  position: relative;
  vertical-align: middle;
}
.jss17890 {
  top: 0;
  color: #fff;
  right: 0;
  height: 20px;
  display: flex;
  padding: 0 4px;
  z-index: 1;
  position: absolute;
  flex-wrap: wrap;
  min-width: 20px;
  transform: translate(20px, -50%);
  box-sizing: border-box;
  transition: transform 225ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  align-items: center;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  align-content: center;
  border-radius: 10px;
  flex-direction: row;
  justify-content: center;
  background-color: #ff9800;
  transform-origin: 100% 0%;
}
.jss17891 {
  margin: 0;
  padding: 16px;
  border-bottom: 1px solid rgba(0, 0, 0, 0.12);
}
.jss17892 {
  top: 8px;
  right: 8px;
  color: #9e9e9e;
  position: absolute;
}
.jss17893 {
  margin: 0;
  padding: 16px;
}
.jss17894 {
  margin: 0;
  padding: 8px;
  border-top: 1px solid rgba(0, 0, 0, 0.12);
}
.jss17895 {
  font-size: 13px;
}
.jss17896 {
  padding: 0;
  overflow: hidden;
}
.jss17896 span {
  display: inline-flex;
}
.jss17896 span:hover {
  position: relative;
  animation: moveTextHorz 3s linear infinite;
}
@-webkit-keyframes moveTextHorz {
  0% {
    transform: translate(0, 0);
  }
  100% {
    transform: translate(-70%, 0);
  }
}
.jss17897 span {
  color: #f44336;
}
.jss17898 {
  width: 50px;
}
@media (min-width:600px) {
  .jss17898 {
    right: 90px;
  }
}
@media (min-width:960px) {
  .jss17898 {
    right: 15px;
  }
}
.jss17899 {
  cursor: pointer;
}
.jss17899:hover {
  background: #0288d1;
}
.jss17900 {
  margin-left: 10px;
  vertical-align: middle;
}
.jss17901 {
  width: 100%;
}
.jss17902 {
  width: 100%;
  margin: 0;
}
@media (min-width:960px) {
  .jss17903 {
    margin-left: auto;
    margin-right: 16px;
  }
}
@media (min-width:1280px) {
  .jss17903 {
    margin-left: auto;
    margin-right: 32px;
  }
}
@media (min-width:960px) {
  .jss17904 {
    transform: translateY(75%);
    margin-left: 16px;
  }
}
@media (min-width:1280px) {
  .jss17904 {
    transform: translateY(75%);
    margin-left: 32px;
  }
}
.jss17905 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
@media (max-width:1279.95px) {
  .jss17906 {
    display: none;
  }
}
@media (max-width:1919.95px) {
  .jss17907 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss17908 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss17909 {
    text-align: right;
  }
}
.jss17910 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss17910 {
    text-align: right;
  }
}
.jss17911 {
  padding: 0;
}
@media (min-width:1280px) {
  .jss17912 {
    margin-top: 0 !important;
  }
}
@media (min-width:1280px) {
  .jss17913 {
    margin-top: 64px;
  }
}
.jss17914 div textarea {
  overflow: auto;
}
.jss17915 {
  width: 70%;
}
@media (min-width:1280px) {
  .jss17915 {
    float: right;
  }
}
@media (min-width:1280px) {
  .jss17916 {
    float: right;
  }
}
@media (max-width:1279.95px) {
  .jss17916 {
    margin-top: 6px;
  }
}
@media (min-width:1280px) {
  .jss17917 {
    padding: 12px 0;
  }
}
@media (max-width:1279.95px) {
  .jss17917 {
    padding: 6px 0 3px;
  }
}
.jss17918 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss17918 {
    margin-top: 0 !important;
  }
}
.jss17918 div textarea {
  overflow: auto;
}
.jss17919 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss17920 {
  width: 57px;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
  overflow-x: hidden;
}
@media (min-width:600px) {
  .jss17920 {
    width: 73px;
  }
}
@media (min-width:960px) {
  .jss17921 {
    width: calc(100% - 73px);
    margin-left: 73px;
    margin-right: 0;
  }
}
@media (min-width:960px) {
  .jss17922 {
    margin-left: 73px;
    margin-right: 0;
  }
}
.jss17923 {
  bottom: 0px;
  z-index: 1;
  position: sticky;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss17923 {
    display: none;
  }
}
.jss17923:hover {
  background-color: rgb(235, 235, 235);
}
@media (max-width:959.95px) {
  .jss17924 {
    display: none;
  }
}
.jss17925 {
  top: 0;
  left: auto;
  width: 100%;
  right: 0;
  height: 100%;
  z-index: 1301;
  overflow: hidden;
  position: absolute;
  overflow-y: auto;
  transition: transform 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
@media (min-width:960px) {
  .jss17925 {
    width: 24vw;
  }
}
.jss17926 {
  cursor: pointer;
}
.jss17926:hover {
  background-color: #eeeeee;
}
.jss17927 {
  background-color: #e0e0e0 !important;
}
.jss17928 {
  transition: width 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
.jss17929 {
  border-collapse: separate;
}
.jss17930:hover {
  background-color: rgb(235, 235, 235);
}
.jss17931 {
  position: relative;
  min-height: 80vh;
}
.jss17931 .back-icon {
  top: 5px;
  left: 5px;
  z-index: 1;
  position: absolute;
}
.jss17931 > .ninebox-inner {
  width: 100%;
  position: relative;
  min-height: 80vh;
}
.jss17931 > .ninebox-inner .category-paper {
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #424242;
}
.jss17931 > .ninebox-inner .percentage-wrapper {
  width: 100%;
  height: 100%;
  display: flex;
  flex-wrap: wrap;
  box-sizing: border-box;
}
.jss17931 > .ninebox-inner .horizontal-category {
  height: 50px;
  margin: 0 60px;
  display: flex;
  align-items: center;
  justify-content: center;
}
.jss17931 > .ninebox-inner .vertical-category {
  width: 50px;
  height: calc(80vh - 120px);
  display: inline-flex;
  align-items: center;
  justify-content: center;
}
.jss17931 > .ninebox-inner .ninebox-content {
  width: calc(100% - 100px);
  height: calc(80vh - 120px);
  display: inline-block;
  position: relative;
}
.jss17931 > .ninebox-inner .ninebox-content .wrapper {
  width: 100%;
  height: 100%;
  margin: 0;
}
.jss17931 > .ninebox-inner .ninebox-content .wrapper .box {
  height: calc(100% / 3);
  padding: 10px;
}
.jss17931 > .ninebox-inner .ninebox-content .wrapper .box .has-item {
  padding: 26px 10px !important;
}
.jss17931 > .ninebox-inner .ninebox-content .wrapper .box .disabled {
  background: #d1d1d1;
}
.jss17931 > .ninebox-inner .ninebox-content .wrapper .box .card {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 30px;
  position: relative;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  -webkit-transition: all .4s ease-out;
}
.jss17931 > .ninebox-inner .ninebox-content .wrapper .box .card h6 {
  color: rgba(0, 0, 0, 0.87);
}
.jss17931 > .ninebox-inner .ninebox-content .wrapper .box .card .total-person {
  top: 5px;
  right: 5px;
  width: 15px;
  height: 15px;
  position: absolute;
}
.jss17931 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list {
  width: 100%;
  height: 100%;
  display: flex;
  overflow-y: auto;
  align-items: center;
}
.jss17931 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list > ol {
  height: 100%;
  margin: 0;
  padding: 0;
  text-align: left;
  list-style: none;
}
.jss17931 > .ninebox-inner .ninebox-content .wrapper .box .active:hover {
  cursor: pointer;
  transform: scale(1.1);
}
.jss17931 > .ninebox-inner .vertical-category .text-rotate {
  transform: rotate(270deg);
}
.jss17931 > .ninebox-inner .vertical-category.left {
  padding: 10px 0;
  margin-right: 9px;
}
.jss17931 > .ninebox-inner .vertical-category.right {
  margin-left: 9px;
}
.jss17931 > .ninebox-inner .vertical-category .text-rotate.nowrap {
  white-space: nowrap;
}
.jss17931 > .ninebox-inner .horizontal-category.top {
  padding: 0 9px;
  margin-bottom: 9px;
}
.jss17931 > .ninebox-inner .horizontal-category.bottom {
  margin-top: 9px;
}
.jss17931 > .ninebox-inner .percentage-wrapper > .percentage-item {
  padding: 0 10px;
  flex-grow: 0;
  max-width: calc(100% / 3);
  flex-basis: calc(100% / 3);
}
.jss17931 > .ninebox-inner .percentage-wrapper > .percentage-item.horizontal {
  height: calc(100% / 3);
  padding: 10px 0;
  max-width: 100%;
}
.jss17931 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 10px;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  background-color: #424242;
  -webkit-transition: all .4s ease-out;
}
.jss17931 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper > p {
  color: #fff;
}
.jss17931 > .ninebox-inner .category-paper > p {
  color: #fff;
}
.jss17932 {
  max-height: 45vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss17932 .table {
  border-collapse: separate;
}
.jss17932 .table .sticky-head {
  top: 0;
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss17933 {
  height: 44vh;
  overflow: auto;
}
.jss17934 {
  width: 100%;
  height: 50vh;
  overflow: auto;
}
.jss17935 {
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss17936 {
  width: 100%;
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss17937 {
  max-width: 82%;
}

.jss13268 {
  left: 0;
  right: 0;
  z-index: 1400;
  display: flex;
  position: fixed;
  align-items: center;
  justify-content: center;
}
.jss13269 {
  top: 0;
}
@media (min-width:960px) {
  .jss13269 {
    left: 50%;
    right: auto;
    transform: translateX(-50%);
  }
}
.jss13270 {
  bottom: 0;
}
@media (min-width:960px) {
  .jss13270 {
    left: 50%;
    right: auto;
    transform: translateX(-50%);
  }
}
.jss13271 {
  top: 0;
  justify-content: flex-end;
}
@media (min-width:960px) {
  .jss13271 {
    top: 24px;
    left: auto;
    right: 24px;
  }
}
.jss13272 {
  bottom: 0;
  justify-content: flex-end;
}
@media (min-width:960px) {
  .jss13272 {
    left: auto;
    right: 24px;
    bottom: 24px;
  }
}
.jss13273 {
  top: 0;
  justify-content: flex-start;
}
@media (min-width:960px) {
  .jss13273 {
    top: 24px;
    left: 24px;
    right: auto;
  }
}
.jss13274 {
  bottom: 0;
  justify-content: flex-start;
}
@media (min-width:960px) {
  .jss13274 {
    left: 24px;
    right: auto;
    bottom: 24px;
  }
}

.jss13535 {
  flex: 0 0 auto;
}
.jss13536 {
  top: 0;
  flex: 1 0 auto;
  height: 100%;
  display: flex;
  z-index: 1200;
  outline: none;
  position: fixed;
  overflow-y: auto;
  flex-direction: column;
  -webkit-overflow-scrolling: touch;
}
.jss13537 {
  left: 0;
  right: auto;
}
.jss13538 {
  left: auto;
  right: 0;
}
.jss13539 {
  top: 0;
  left: 0;
  right: 0;
  bottom: auto;
  height: auto;
  max-height: 100%;
}
.jss13540 {
  top: auto;
  left: 0;
  right: 0;
  bottom: 0;
  height: auto;
  max-height: 100%;
}
.jss13541 {
  border-right: 1px solid rgba(0, 0, 0, 0.12);
}
.jss13542 {
  border-bottom: 1px solid rgba(0, 0, 0, 0.12);
}
.jss13543 {
  border-left: 1px solid rgba(0, 0, 0, 0.12);
}
.jss13544 {
  border-top: 1px solid rgba(0, 0, 0, 0.12);
}

.jss14265 {
  top: 0;
  left: 0;
  bottom: 0;
  z-index: 1199;
  position: fixed;
}
.jss14266 {
  right: auto;
}
.jss14267 {
  left: auto;
  right: 0;
}
.jss14268 {
  right: 0;
  bottom: auto;
}
.jss14269 {
  top: auto;
  right: 0;
  bottom: 0;
}

.jss14734 {
  color: rgba(0, 0, 0, 0.54);
  font-size: 0.8125rem;
  box-sizing: border-box;
  list-style: none;
  line-height: 48px;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: 500;
}
.jss14735 {
  color: #03a9f4;
}
.jss14736 {
  color: inherit;
}
.jss14737 {
  padding-left: 16px;
  padding-right: 16px;
}
@media (min-width:600px) {
  .jss14737 {
    padding-left: 24px;
    padding-right: 24px;
  }
}
.jss14738 {
  padding-left: 72px;
}
.jss14739 {
  top: 0;
  z-index: 1;
  position: sticky;
  background-color: inherit;
}

@media (min-width:960px) {
  ::-webkit-scrollbar {
    width: 5px;
    height: 5px;
  }
  ::-webkit-scrollbar-track {
    background: rgba(250, 250, 250, 0);
  }
  ::-webkit-scrollbar-thumb {
    background: #eeeeee;
  }
  ::-webkit-scrollbar-thumb:hover {
    background: #e0e0e0;
  }
}
.jss14502 {
  width: 100%;
  z-index: 1;
  overflow: hidden;
  margin-bottom: 0;
}
.jss14503 {
  background-color: #424242;
}
.jss14504 {
  height: 70px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/etg.9362c8fb.svg&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss14505 {
  height: 150px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/tessa.55f0b81f.svg&quot;);
  background-repeat: no-repeat;
}
.jss14506 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2hJREFUeNrsWtGR2jAQ1TEU4FQQUsGRCuBm+IerAKgguAKgAnMVHFcBvn/PnFwBvg6cCuIOEslacRvFyJJswL74zQgDNrKedvftSoaQDh06dHDAnfGVfjRjr0PWRg3kkbD2RIJJakfMjzbsdd0CQz0yciF/0zf8wRyOKbSmgXuSx1rAmhWxgbSdnJFG4cOj5DhJz7KLrC3i0a8wSwM8QzcRjGCS1U+MkLcbE9uztjQn5kcyEFMsn5q4uxW09+8hQh5rR/buCNY4OtxsBzMpEaLZ/cIm6g6uyZTrtih+d6C8/PxDLuEiT1mhp7jWEH32HIi95sr5kRKe4PiTtRWbuN/5UfS9hXOUEd6gSXgBa/gwpoMyLiMh6ym5gJxmSsyuCxaK71OUAyka0AC+i8H9YzjnofEsNfd5NyWWnCwVTGgF35/CoPbK9ykMXt5nDEQpWIaC1aZwnlvqnrXvLq7YV2Z2CDesijUMCLv0FNxQYgQWkWIl3fOAfrcq+N46xuJTh8I1XOCBi4nyRqjqEMUZVSyWIQvNlZAI4fr4TIyNTIlh93O12gHJ8AxUluMHkI2V+xwhJmVsviniEAOBtbsr8izuRykM7L7G4lRacl2SizxFiRd1llQUOjSx2BYN9lpVvyzjMkgtxsRiIDbIa0Fd5SFyz6apRXCvwGJV46yBxISFUhPVaeOyJQE/HpcsW8aO2wV8sZqgfhZI6m2w1IVKEbE4l2oRZ55mzTN2dNeZUknMHftZ6GK8iJgaZ6FmZ8il9KIFE+mC0M4VuZv4Ec7u4RlVDMs6N4JQ14urojqrrVXGc8TiU+XA4+yTqCJRgnt8RhU9CGAb4mLljAVJbArZlk9pHgYOmznUoIpeEffd4R16/+zo8p7Sj4EriplIkDxfEtklfqfbfkugOh+cVTM/spd7dXUeTB4h2duRwkneklhc6vvVthDq78dAFYnLPkMb5J6AqbO2Eivb4k40cr8i4rGNS/G6R/08O66WH3QuXPa0JS6RW9dVsO6z7baDk8WoJle51om7f5Yxbill706Mm/qjIC6KweoCU1c/lq7YWnU0IUY/K7H3GgTj6ih/oiKq+F+oqk5IyZOOK+MriI98WPnNjJggF5C/Hyg0Faf/edj8M4cn6mlZ/rgRuBe9XqLm7NChw3+GPwIMAA139Xg+K8vEAAAAAElFTkSuQmCC&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss14507 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABANJREFUeNrsWt1x4jAQNpl7P18FcSqIqSBmhvdABYEKiCvwUQFcBfZVEHhnBl8F8VUQSvB1cJJ3jddC/pOlQBg0YwYUIfbT/n27imXdxtcag947+DuPvXqa5TpYq3F0PmD+bsZeQ0OHvmbg/HMB+2CvDntS9iSaADn48PGDgUtVNvmmQQg+fjEBfmqBBaa9x08ue2KVbe6uNXhcOrDDdQJbjQ/XqjHrBuxLAYMIeVZg9xrhuJegsTx5ztjphuyxe2qI58LVJURFSnk4vfpgwr0qmRywmECgVMrAdJBgB095QmaTDPRqHBv7rnFgZUcPCc3igzP0pfTkwewW7LGJWfO168soW9oJXHDJ6gPwVQnv5wCrNrEDgnSFuXlfszPB7utYfw7EFioBOmLy9wuroE8Lz6ACRBPZXfatmvUD83cu+o0r5DiukT+SIpSve0RTtU2Y5kCTlkLl05dreX7unkeIiTkfS+VKGhJ7QDQYsb3mnw+srCludiMmSNKwPq5lE2DSewLOV81rA0VQtC/RFlSIax9q89UpuJGKz6lyRepT9aBg5BzQFnKbrGpOsj0Lgq3U3rtTNEGH+FTSImLSwPDSoiWQZHvn+Q9+07jGgmP0axcoRCAeMpMmcGtStgRmgfm7iUV7ie3GRKjdrEZzpFG20Nqki6hdKdWTQFxFfuhIErFDhHzBuQVbn0i0JAaJDfGxJ/xsBJh35Hh5ZANA7y0430YAu5dYBKxbjacINGVzG9SwZ9LHXFIM0tEEKs9fG8EkZUPc669KL0Q13P8j5gP8riwwfz/FsM3TwYisfTjOFz5Eq+epZM5guIewLf8x4HUjMm8ffUP0G25eMOcJ0S7CQ0grGkaiDNo0lta2yIrEGhFwe6kwwDEDsu8844XNFXSqH1ibjhFoYy6YmFNRiFLmErUIWJ16+V2jYl4RPzas+14KHNBvnGWmCma47RDl7rtqSyV4xCenWJ+UEzTbd+yBcNN8s8qXeU0Uy1MJIl2BbY/+U8UEytzQxnzlCKD3UlOr32trEhjN/ItGnyiESjElrCW5yq3hjouK39YMDKJWRMis7LSfJeY7zAIE/BfAVOIvXoW2ZqSaPpjUWM75ilrp9CLCJVrys+RMhVqNN5ikNw3MJZSQYYPAQMglMTWxEMzD/bCyrIe0MM3WwNpIkudcUvMduorZp+fxRqJfr8ZLTYOIp4ehyjZ9OsFz1Bj4AgSAqXL/HUw6FFLFSFU49fsxAED5IQ8Aqvdjs+y7IqgelxS6OsE8+b4KDCXKck9Vhwki6jOCoeF+jX7V6+ZF9/1YUJFwExJJ7YraKkZAsQ5xTNyPeUiTJla7mxSu2d+6r5LM3I+VQboSgCkhxLdxG2z8F2AAPtd0UKyJkPIAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss14508 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAzRJREFUeNrsWs2Nm0AUnl1RAFIKCK4gdLBwy9GuYL0VePeWm+1bbt5UYLsC7FtueCswqWDdQCTSQd5bfRM9IcADDD+OeNIIbI8f8837f4NSI4000kg16K7uHz99++nTZUkj/f3965PNRRHvFV2+0FgT76QOj/sGz2dQUxpzWkhgEVQgeC/r8mkCzO1Ao9w+gEnyb8bGSCU8qEPRri3Ebycab5bW9JnVG/cXGvuCeSmNA9ngxRgYgeIFv3ekbk2IwU0IXGqqiv4NgNI2mGsGjsGfi9ThkYZXoIoPDRbrCb4siaTOxhsBI1GvctT1QSzgLW9OA3cf42NCfMOcOfx70JZXlDt5atGGapHT4KFrGn+QeVgDxrxIIi9QtV3nwOCJVm2IiXi/NuXhVNB9T8SXV+1iYROB6pY8mxKbi9yNPeKEQHEAj4YYB6oAk+qxg8ocCFzYg8Qer0nNaWpTcBynLlFlQk2rSfDg6L7DXQ5s1m2DAIZqm7OFGPf/jcS2Bfe3Cwz9C1+kRz6+u11gCOo69v1AGvZRpOK32wMGRxGJLH2FWJggD4zadCaOZelMUYsFol5iIKGOhQjoMdSTnckFc44cD4tK/V4kRouL0ErYZPokLKFQlu64D0UmozeEnco78doMSWJuJvX6hUZLWpLFvBCINUBxc/TZNMHt0saehNcLykDlADyIXDMFr2EAg13MRD+iSgd3I8LBxGRDOvWKSIY1uGcTd44sRNd4M1ugrLt7LmNEOT83LD8UvKHVCqGNOHbE1aQFp/PG/WDjmKAq6qSBGcUuqHfQF7CiLOQfcHHmlapqHWeev1AGhyBtANMPdQnQVuUcbND3KXLH1HBjPjIa2hCOfRwOzn3YmCsAzjOpVSLmLMUmXPOg7Fi4UN1C2qe+VZE95BHeMiuBhbAX91og1zkmXc992ZjOAXdFCS2AHhDHpqqk4yuObmfIMWMTG7srYRaLGBP22ZgRUtLrkGd3YV4MdNRACa49AhgtqRj3yTW3bwLM66KUL/GwZwTwNUqb2JaNearBawkWKPt8oy5XkbtPVIOzqQ6p6MSz9K0BX5T6QyQGtK/75s5II400Ui79FWAANjYkpEJzM+AAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss14509 {
  color: #FFF;
  padding: 48px;
}
.jss14510 {
  color: rgba(0, 0, 0, 0.87);
  width: 100%;
  display: flex;
  z-index: 2;
  opacity: 0.5;
  position: fixed;
  align-items: center;
  padding-left: 16px;
  padding-right: 16px;
  background-color: #424242;
}
.jss14511 {
  height: 100vh;
  display: flex;
  position: relative;
  max-height: 1600px;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: linear-gradient(rgba(0,130,170,0), #424242), url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/1.20e79364.jpg&quot;);
}
.jss14512 {
  margin: -200px auto 0 auto;
  padding: 64px 0 48px;
  max-width: 1200px;
}
.jss14513 {
  width: 50%;
  color: #FFF;
}
@media (max-width:1279.95px) {
  .jss14513 {
    width: 100%;
    padding: 24px;
  }
}
.jss14514 {
  margin: -150px auto 0 auto;
  padding: 16px;
  position: relative;
  max-width: 1200px;
  background: #fafafa;
  justify-content: center;
}
.jss14515 {
  margin: 0 auto;
  padding: 24px;
  display: flex;
}
.jss14516 {
  margin-bottom: 24px;
}
.jss14517 {
  text-decoration: none;
}
.jss14518 {
  margin: 24px auto;
  max-width: 1200px;
}
@media (max-width:1279.95px) {
  .jss14518 {
    max-width: 1366px;
  }
}
.jss14519 {
  height: 160px;
  display: flex;
  align-items: center;
}
.jss14520 {
  margin: auto;
  display: grid;
}
.jss14521 {
  height: 100vh;
  display: flex;
  position: relative;
  overflow: hidden;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/2.adfa400c.jpg&quot;);
  background-position: center;
}
.jss14522 {
  color: #FFF;
  background-color: #03a9f4;
}
.jss14523 {
  color: #03a9f4;
  background-color: #fff;
}
.jss14524 {
  color: #03a9f4;
  padding: 0;
}
.jss14525 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss14525 {
    width: calc(100%);
  }
}
.jss14526 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss14526 {
    width: calc(100% - 300px);
  }
}
.jss14527 {
  display: none;
}
@media (min-width:600px) {
  .jss14527 {
    display: block;
  }
}
.jss14528 {
  color: #fff;
  position: relative;
  background: #03a9f4;
}
.jss14529 {
  width: 100%;
  position: relative;
  border-radius: 14px;
  background-color: rgba(255, 255, 255, 0.5);
}
.jss14529:hover {
  background-color: rgba(255, 255, 255, 0.7);
}
@media (min-width:600px) {
  .jss14529 {
    width: auto;
  }
}
.jss14530 {
  width: 72px;
  color: #212121;
  height: 100%;
  display: flex;
  position: absolute;
  align-items: center;
  pointer-events: none;
  justify-content: center;
}
.jss14531 {
  color: #fff;
  width: 100%;
}
.jss14532 {
  color: #212121;
  width: 100%;
  transition: width 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  padding-top: 8px;
  padding-left: 64px;
  padding-right: 8px;
  padding-bottom: 8px;
}
@media (min-width:600px) {
  .jss14532 {
    width: 80px;
  }
  .jss14532:focus {
    width: 200px;
  }
}
.jss14533 {
  color: #212121;
}
.jss14534 {
  background-color: #4fc3f7;
}
.jss14535 {
  margin-left: 12px;
  margin-right: 36px;
}
.jss14536 {
  display: none;
}
@media (min-width:960px) {
  .jss14537 {
    display: none;
  }
}
.jss14538 {
  width: 300px;
}
.jss14539 {
  display: flex;
  padding: 0 8px;
  min-height: 56px;
  align-items: center;
}
@media (min-width:0px) and (orientation: landscape) {
  .jss14539 {
    min-height: 48px;
  }
}
@media (min-width:600px) {
  .jss14539 {
    min-height: 64px;
  }
}
.jss14540 {
  margin-top: 65px;
  justify-content: flex-start;
}
.jss14541 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss14543 {
  width: 100%;
  height: 100%;
  opacity: 0.2;
  content: &quot;&quot;;
  display: block;
  position: absolute;
  background: linear-gradient(rgba(0,130,170,0), #03a9f4));
  background-size: cover;
  background-position: center center;
}
.jss14544 {
  width: 60px;
  overflow-x: hidden;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss14545 {
  color: inherit;
}
.jss14546 {
  margin-left: 56px;
}
.jss14547 {
  bottom: 16px;
}
.jss14548 {
  color: rgba(0, 0, 0, 0.87);
  background-size: cover;
  background-color: #fff;
  background-image: url(https://xdev.equine.co.id/apps/tessa/static/media/pattern2.f069796d.svg);
  background-repeat: no-repeat;
  background-position: inherit;
}
.jss14549 {
  color: #fff;
  background-color: #03a9f4;
}
.jss14550 {
  color: #fff;
  background-color: #ff9800;
}
.jss14551 {
  color: #fff;
  background-color: #f44336;
}
.jss14552 {
  color: #fff;
  background: #4caf50;
}
.jss14553 {
  flex: 1;
}
.jss14554 {
  padding: 16px;
  flex-grow: 1;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss14554 {
    margin-top: 68px;
  }
}
@media (max-width:959.95px) {
  .jss14554 {
    padding: 0;
    margin-top: 56px;
  }
}
@media (max-width:959.95px) {
  .jss14555 {
    margin-top: 68px;
  }
}
@media (min-width:960px) {
  .jss14555 {
    left: 50%;
    width: 982px;
    position: absolute;
    transform: translateX(-50%);
  }
}
@media (min-width:960px) {
  .jss14556 {
    margin-left: 300px;
    margin-right: 0;
  }
}
.jss14557 {
  margin-bottom: 56px;
}
.jss14558 {
  width: calc(100% - 0px);
  right: 0;
  bottom: 0;
  display: flex;
  position: fixed;
}
@media (min-width:960px) {
  .jss14558 {
    width: calc(100% - 300px);
  }
}
.jss14560 {
  background: #fff;
}
.jss14561 {
  margin-bottom: 60px;
}
.jss14562 {
  height: 4px;
}
.jss14563 {
  display: flex;
  align-items: center;
  justify-content: space-between;
}
.jss14564 {
  margin: 0 15px 0 0;
  padding: 16px 0;
  flex-grow: 1;
}
@media (min-width:600px) {
  .jss14564 {
    width: 30%;
    flex-grow: 0;
  }
}
.jss14565 {
  color: inherit;
  font-size: 0.6964285714285714rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: inherit;
  line-height: 1.66;
  letter-spacing: 0.03333em;
}
.jss14566 {
  color: #fff;
  background: #03a9f4;
}
.jss14567 {
  color: #fff;
  min-height: 40px;
  background: #ff9800;
  margin-bottom: 16px;
}
.jss14568 {
  height: 500px;
  display: flex;
  overflow: hidden;
  align-items: center;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss14568 {
    height: 300px;
  }
}
.jss14569 {
  height: 100%;
  margin: 0 auto;
}
.jss14570 {
  width: 25px;
  height: 25px;
  margin-right: 16px;
}
.jss14571 {
  transform: rotate(0deg);
  transition: transform 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
}
.jss14572 {
  transform: rotate(180deg);
}
.jss14573 {
  min-width: 350px;
}
.jss14574 {
  display: flex;
  flex-wrap: wrap;
}
.jss14575 {
  padding: 0;
}
@media (min-width:0px) {
  .jss14575 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss14575 {
    width: calc(100% / 2);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1280px) {
  .jss14575 {
    width: calc(100% / 3);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1920px) {
  .jss14575 {
    width: calc(100% / 4);
    padding: 0 16px 16px 0;
  }
}
.jss14576 {
  padding: 0;
}
@media (min-width:0px) {
  .jss14576 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss14576 {
    width: calc(100% / 2);
  }
}
@media (min-width:1280px) {
  .jss14576 {
    width: calc(100% - (100% / 3));
  }
}
@media (min-width:1920px) {
  .jss14576 {
    width: calc(100% - (100% / 4));
  }
}
.jss14577 {
  padding: 0;
}
@media (min-width:0px) {
  .jss14577 {
    width: 100%;
  }
}
.jss14578 {
  width: 100%;
  margin-bottom: 16px;
  vertical-align: middle;
}
.jss14579 {
  right: 16px;
  display: flex;
  position: absolute;
}
.jss14580 {
  right: 16px;
  bottom: 16px;
  position: fixed;
}
.jss14581 {
  float: right;
}
.jss14582 {
  margin: 0;
}
.jss14583 {
  margin: 8px;
}
.jss14584 {
  margin-left: 8px;
}
.jss14585 {
  margin-top: 8px;
}
.jss14586 {
  margin-right: 8px;
}
.jss14587 {
  margin-bottom: 8px;
}
.jss14588 {
  margin: 16px;
}
.jss14589 {
  margin-left: 16px;
}
.jss14590 {
  margin-top: 16px;
}
.jss14591 {
  margin-right: 16px;
}
.jss14592 {
  margin-bottom: 16px;
}
.jss14593 {
  margin: 24px;
}
.jss14594 {
  margin-left: 24px;
}
.jss14595 {
  margin-top: 24px;
}
.jss14596 {
  margin-right: 24px;
}
.jss14597 {
  margin-bottom: 24px;
}
.jss14598 {
  padding: 0;
}
.jss14599 {
  padding: 8px;
}
.jss14600 {
  padding-left: 8px;
}
.jss14601 {
  padding-top: 8px;
}
.jss14602 {
  padding-right: 8px;
}
.jss14603 {
  padding-bottom: 8px;
}
.jss14604 {
  padding: 16px;
}
.jss14605 {
  padding-left: 16px;
}
.jss14606 {
  padding-top: 16px;
}
.jss14607 {
  padding-right: 16px;
}
.jss14608 {
  padding-bottom: 16px;
}
.jss14609 {
  padding: 24px;
}
.jss14610 {
  padding-left: 24px;
}
.jss14611 {
  padding-top: 24px;
}
.jss14612 {
  padding-right: 24px;
}
.jss14613 {
  padding-bottom: 24px;
}
.jss14614 {
  background-color: #03a9f4;
}
.jss14615 {
  background-color: #ff9800;
}
.jss14616 {
  color: white;
  background-color: #f44336;
}
.jss14617 {
  color: #03a9f4;
}
.jss14618 {
  color: #ff9800;
}
.jss14619 {
  color: #f44336;
}
.jss14620 {
  text-decoration: line-through;
}
.jss14621 {
  padding: 3px;
}
.jss14622 {
  height: calc(100vh - 96px - 72px);
  overflow-x: auto;
}
.jss14623 {
  width: calc(100vw - 32px - 300px);
}
.jss14624 {
  height: calc(100vh - 56px - 68px);
  overflow-x: auto;
}
.jss14625 {
  width: calc(100vw);
}
.jss14626 {
  overflow-x: auto;
}
.jss14627 {
  table-layout: initial;
}
.jss14628 {
  top: 0;
  position: sticky;
  background-color: #fff;
}
.jss14629 {
  width: 3vw;
}
.jss14630 {
  width: 5vw;
}
.jss14631 {
  width: 10vw;
}
.jss14632 {
  width: 15vw;
}
.jss14633 {
  min-width: 15vw;
  max-width: 15vw;
}
@media (min-width:960px) {
  .jss14633 {
    min-width: calc((100vw - 300px - 48px) * 0.15);
    max-width: calc((100vw - 300px - 48px) * 0.15);
  }
}
.jss14634 {
  min-width: 25vw;
  max-width: 25vw;
}
@media (min-width:960px) {
  .jss14634 {
    min-width: calc((100vw - 300px - 48px) * 0.25);
    max-width: calc((100vw - 300px - 48px) * 0.25);
  }
}
.jss14635 {
  width: 25vw;
}
.jss14636 {
  width: 30vw;
}
.jss14637 {
  min-width: 45vw;
  max-width: 45vw;
}
@media (min-width:960px) {
  .jss14637 {
    min-width: calc((100vw - 300px - 48px) * 0.45);
    max-width: calc((100vw - 300px - 48px) * 0.45);
  }
}
.jss14638 {
  width: 100%;
  overflow-x: auto;
}
.jss14639 {
  width: 100%;
  margin-top: 24px;
  overflow-x: auto;
}
.jss14640 {
  min-width: 700px;
}
.jss14641 {
  width: 100%;
}
.jss14642 {
  min-width: 100px;
}
.jss14643 {
  color: rgba(0, 0, 0, 0.54);
  flex-shrink: 0;
}
.jss14644 {
  color: #2196f3;
}
.jss14645 {
  padding-top: 16px;
  padding-bottom: 16px;
}
.jss14647 {
  max-height: 100px;
}
.jss14648 {
  margin-top: -144px;
  margin-left: -56px;
  margin-right: -136px;
  margin-bottom: -64px;
}
.jss14649 {
  margin-top: -80px;
}
.jss14650 {
  width: 500px;
  height: 450px;
}
.jss14651 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss14652 {
  display: flex;
  box-sizing: border-box;
  align-items: center;
}
.jss14654:hover {
  background-color: #eeeeee;
}
.jss14655 {
  flex: 1;
}
.jss14656 {
  cursor: initial;
}
.jss14657 {
  background: #f44336;
}
.jss14658 {
  color: #fff;
}
.jss14659 {
  width: 100%;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
.jss14660 {
  max-height: 75vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss14661 {
  padding: 10px;
  text-align: center;
}
.jss14662 {
  padding: 0;
  vertical-align: top;
}
.jss14663 {
  margin-top: 8px;
  padding-left: 32px;
}
.jss14664 {
  top: 0;
  padding: 5px;
  position: sticky;
  overflow: hidden;
  max-width: 100px;
  max-height: 10px;
  text-align: center;
  background-color: #fff;
}
.jss14665 {
  max-height: 76vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss14666 {
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss14667 {
  padding: 0;
  margin-top: 8px;
}
.jss14668 {
  left: 0;
  width: 500px;
  z-index: 1;
  padding: 8px 16px 0 16px;
  min-width: 300px;
  background: #fff;
  vertical-align: top;
}
.jss14669 {
  padding: 8px 16px 0 16px;
  min-width: 300px;
  vertical-align: top;
}
.jss14670 {
  min-width: 300px;
}
.jss14671 {
  transform: rotate(180deg);
  writing-mode: vertical-rl;
}
.jss14672 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss14673 {
  cursor: pointer;
  transition: all .3s;
}
.jss14673:hover {
  background-color: #ebebeb;
}
.jss14674 {
  background-color: #ebebeb;
}
.jss14675 {
  transition: all .3s;
}
.jss14675:hover {
  transform: scale(1.03);
}
.jss14676 {
  padding: 0;
  transition: all .3s;
}
.jss14676:hover {
  transform: scale(1.03);
}
.jss14677 {
  display: grid;
  align-items: center;
  grid-column-gap: 1.5rem;
  grid-template-columns: 1fr max-content 1fr;
}
.jss14677::after, .jss14677::before {
  height: 1px;
  content: '';
  display: block;
  background-color: currentColor;
}
.jss14678 a {
  text-decoration: none;
}
.jss14679 {
  left: 50%;
  position: absolute;
  transform: ;
}
.jss14680 {
  display: -webkit-box;
  overflow: hidden;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;
}
.jss14681 {
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
.jss14682 {
  display: inline;
  text-transform: capitalize;
}
.jss14683 {
  text-decoration: none;
}
.jss14683 p:hover {
  color: #03a9f4;
}
.jss14684 {
  margin-left: 4px;
}
.jss14685 {
  padding: 0 16px;
  display: inline-flex;
  position: relative;
  vertical-align: middle;
}
.jss14686 {
  top: 0;
  color: #fff;
  right: 0;
  height: 20px;
  display: flex;
  padding: 0 4px;
  z-index: 1;
  position: absolute;
  flex-wrap: wrap;
  min-width: 20px;
  transform: translate(20px, -50%);
  box-sizing: border-box;
  transition: transform 225ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  align-items: center;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  align-content: center;
  border-radius: 10px;
  flex-direction: row;
  justify-content: center;
  background-color: #ff9800;
  transform-origin: 100% 0%;
}
.jss14687 {
  margin: 0;
  padding: 16px;
  border-bottom: 1px solid rgba(0, 0, 0, 0.12);
}
.jss14688 {
  top: 8px;
  right: 8px;
  color: #9e9e9e;
  position: absolute;
}
.jss14689 {
  margin: 0;
  padding: 16px;
}
.jss14690 {
  margin: 0;
  padding: 8px;
  border-top: 1px solid rgba(0, 0, 0, 0.12);
}
.jss14691 {
  font-size: 13px;
}
.jss14692 {
  padding: 0;
  overflow: hidden;
}
.jss14692 span {
  display: inline-flex;
}
.jss14692 span:hover {
  position: relative;
  animation: moveTextHorz 3s linear infinite;
}
@-webkit-keyframes moveTextHorz {
  0% {
    transform: translate(0, 0);
  }
  100% {
    transform: translate(-70%, 0);
  }
}
.jss14693 span {
  color: #f44336;
}
.jss14694 {
  width: 50px;
}
@media (min-width:600px) {
  .jss14694 {
    right: 90px;
  }
}
@media (min-width:960px) {
  .jss14694 {
    right: 15px;
  }
}
.jss14695 {
  cursor: pointer;
}
.jss14695:hover {
  background: #0288d1;
}
.jss14696 {
  margin-left: 10px;
  vertical-align: middle;
}
.jss14697 {
  width: 100%;
}
.jss14698 {
  width: 100%;
  margin: 0;
}
@media (min-width:960px) {
  .jss14699 {
    margin-left: auto;
    margin-right: 16px;
  }
}
@media (min-width:1280px) {
  .jss14699 {
    margin-left: auto;
    margin-right: 32px;
  }
}
@media (min-width:960px) {
  .jss14700 {
    transform: translateY(75%);
    margin-left: 16px;
  }
}
@media (min-width:1280px) {
  .jss14700 {
    transform: translateY(75%);
    margin-left: 32px;
  }
}
.jss14701 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
@media (max-width:1279.95px) {
  .jss14702 {
    display: none;
  }
}
@media (max-width:1919.95px) {
  .jss14703 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss14704 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss14705 {
    text-align: right;
  }
}
.jss14706 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss14706 {
    text-align: right;
  }
}
.jss14707 {
  padding: 0;
}
@media (min-width:1280px) {
  .jss14708 {
    margin-top: 0 !important;
  }
}
@media (min-width:1280px) {
  .jss14709 {
    margin-top: 64px;
  }
}
.jss14710 div textarea {
  overflow: auto;
}
.jss14711 {
  width: 70%;
}
@media (min-width:1280px) {
  .jss14711 {
    float: right;
  }
}
@media (min-width:1280px) {
  .jss14712 {
    float: right;
  }
}
@media (max-width:1279.95px) {
  .jss14712 {
    margin-top: 6px;
  }
}
@media (min-width:1280px) {
  .jss14713 {
    padding: 12px 0;
  }
}
@media (max-width:1279.95px) {
  .jss14713 {
    padding: 6px 0 3px;
  }
}
.jss14714 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss14714 {
    margin-top: 0 !important;
  }
}
.jss14714 div textarea {
  overflow: auto;
}
.jss14715 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss14716 {
  width: 57px;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
  overflow-x: hidden;
}
@media (min-width:600px) {
  .jss14716 {
    width: 73px;
  }
}
@media (min-width:960px) {
  .jss14717 {
    width: calc(100% - 73px);
    margin-left: 73px;
    margin-right: 0;
  }
}
@media (min-width:960px) {
  .jss14718 {
    margin-left: 73px;
    margin-right: 0;
  }
}
.jss14719 {
  bottom: 0px;
  z-index: 1;
  position: sticky;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss14719 {
    display: none;
  }
}
.jss14719:hover {
  background-color: rgb(235, 235, 235);
}
@media (max-width:959.95px) {
  .jss14720 {
    display: none;
  }
}
.jss14721 {
  top: 0;
  left: auto;
  width: 100%;
  right: 0;
  height: 100%;
  z-index: 1301;
  overflow: hidden;
  position: absolute;
  overflow-y: auto;
  transition: transform 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
@media (min-width:960px) {
  .jss14721 {
    width: 24vw;
  }
}
.jss14722 {
  cursor: pointer;
}
.jss14722:hover {
  background-color: #eeeeee;
}
.jss14723 {
  background-color: #e0e0e0 !important;
}
.jss14724 {
  transition: width 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
.jss14725 {
  border-collapse: separate;
}
.jss14726:hover {
  background-color: rgb(235, 235, 235);
}
.jss14727 {
  position: relative;
  min-height: 80vh;
}
.jss14727 .back-icon {
  top: 5px;
  left: 5px;
  z-index: 1;
  position: absolute;
}
.jss14727 > .ninebox-inner {
  width: 100%;
  position: relative;
  min-height: 80vh;
}
.jss14727 > .ninebox-inner .category-paper {
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #424242;
}
.jss14727 > .ninebox-inner .percentage-wrapper {
  width: 100%;
  height: 100%;
  display: flex;
  flex-wrap: wrap;
  box-sizing: border-box;
}
.jss14727 > .ninebox-inner .horizontal-category {
  height: 50px;
  margin: 0 60px;
  display: flex;
  align-items: center;
  justify-content: center;
}
.jss14727 > .ninebox-inner .vertical-category {
  width: 50px;
  height: calc(80vh - 120px);
  display: inline-flex;
  align-items: center;
  justify-content: center;
}
.jss14727 > .ninebox-inner .ninebox-content {
  width: calc(100% - 100px);
  height: calc(80vh - 120px);
  display: inline-block;
  position: relative;
}
.jss14727 > .ninebox-inner .ninebox-content .wrapper {
  width: 100%;
  height: 100%;
  margin: 0;
}
.jss14727 > .ninebox-inner .ninebox-content .wrapper .box {
  height: calc(100% / 3);
  padding: 10px;
}
.jss14727 > .ninebox-inner .ninebox-content .wrapper .box .has-item {
  padding: 26px 10px !important;
}
.jss14727 > .ninebox-inner .ninebox-content .wrapper .box .disabled {
  background: #d1d1d1;
}
.jss14727 > .ninebox-inner .ninebox-content .wrapper .box .card {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 30px;
  position: relative;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  -webkit-transition: all .4s ease-out;
}
.jss14727 > .ninebox-inner .ninebox-content .wrapper .box .card h6 {
  color: rgba(0, 0, 0, 0.87);
}
.jss14727 > .ninebox-inner .ninebox-content .wrapper .box .card .total-person {
  top: 5px;
  right: 5px;
  width: 15px;
  height: 15px;
  position: absolute;
}
.jss14727 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list {
  width: 100%;
  height: 100%;
  display: flex;
  overflow-y: auto;
  align-items: center;
}
.jss14727 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list > ol {
  height: 100%;
  margin: 0;
  padding: 0;
  text-align: left;
  list-style: none;
}
.jss14727 > .ninebox-inner .ninebox-content .wrapper .box .active:hover {
  cursor: pointer;
  transform: scale(1.1);
}
.jss14727 > .ninebox-inner .vertical-category .text-rotate {
  transform: rotate(270deg);
}
.jss14727 > .ninebox-inner .vertical-category.left {
  padding: 10px 0;
  margin-right: 9px;
}
.jss14727 > .ninebox-inner .vertical-category.right {
  margin-left: 9px;
}
.jss14727 > .ninebox-inner .vertical-category .text-rotate.nowrap {
  white-space: nowrap;
}
.jss14727 > .ninebox-inner .horizontal-category.top {
  padding: 0 9px;
  margin-bottom: 9px;
}
.jss14727 > .ninebox-inner .horizontal-category.bottom {
  margin-top: 9px;
}
.jss14727 > .ninebox-inner .percentage-wrapper > .percentage-item {
  padding: 0 10px;
  flex-grow: 0;
  max-width: calc(100% / 3);
  flex-basis: calc(100% / 3);
}
.jss14727 > .ninebox-inner .percentage-wrapper > .percentage-item.horizontal {
  height: calc(100% / 3);
  padding: 10px 0;
  max-width: 100%;
}
.jss14727 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 10px;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  background-color: #424242;
  -webkit-transition: all .4s ease-out;
}
.jss14727 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper > p {
  color: #fff;
}
.jss14727 > .ninebox-inner .category-paper > p {
  color: #fff;
}
.jss14728 {
  max-height: 45vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss14728 .table {
  border-collapse: separate;
}
.jss14728 .table .sticky-head {
  top: 0;
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss14729 {
  height: 44vh;
  overflow: auto;
}
.jss14730 {
  width: 100%;
  height: 50vh;
  overflow: auto;
}
.jss14731 {
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss14732 {
  width: 100%;
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss14733 {
  max-width: 82%;
}

@media (min-width:960px) {
  ::-webkit-scrollbar {
    width: 5px;
    height: 5px;
  }
  ::-webkit-scrollbar-track {
    background: rgba(250, 250, 250, 0);
  }
  ::-webkit-scrollbar-thumb {
    background: #eeeeee;
  }
  ::-webkit-scrollbar-thumb:hover {
    background: #e0e0e0;
  }
}
.jss14270 {
  width: 100%;
  z-index: 1;
  overflow: hidden;
  margin-bottom: 0;
}
.jss14271 {
  background-color: #424242;
}
.jss14272 {
  height: 70px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/etg.9362c8fb.svg&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss14273 {
  height: 150px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/tessa.55f0b81f.svg&quot;);
  background-repeat: no-repeat;
}
.jss14274 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2hJREFUeNrsWtGR2jAQ1TEU4FQQUsGRCuBm+IerAKgguAKgAnMVHFcBvn/PnFwBvg6cCuIOEslacRvFyJJswL74zQgDNrKedvftSoaQDh06dHDAnfGVfjRjr0PWRg3kkbD2RIJJakfMjzbsdd0CQz0yciF/0zf8wRyOKbSmgXuSx1rAmhWxgbSdnJFG4cOj5DhJz7KLrC3i0a8wSwM8QzcRjGCS1U+MkLcbE9uztjQn5kcyEFMsn5q4uxW09+8hQh5rR/buCNY4OtxsBzMpEaLZ/cIm6g6uyZTrtih+d6C8/PxDLuEiT1mhp7jWEH32HIi95sr5kRKe4PiTtRWbuN/5UfS9hXOUEd6gSXgBa/gwpoMyLiMh6ym5gJxmSsyuCxaK71OUAyka0AC+i8H9YzjnofEsNfd5NyWWnCwVTGgF35/CoPbK9ykMXt5nDEQpWIaC1aZwnlvqnrXvLq7YV2Z2CDesijUMCLv0FNxQYgQWkWIl3fOAfrcq+N46xuJTh8I1XOCBi4nyRqjqEMUZVSyWIQvNlZAI4fr4TIyNTIlh93O12gHJ8AxUluMHkI2V+xwhJmVsviniEAOBtbsr8izuRykM7L7G4lRacl2SizxFiRd1llQUOjSx2BYN9lpVvyzjMkgtxsRiIDbIa0Fd5SFyz6apRXCvwGJV46yBxISFUhPVaeOyJQE/HpcsW8aO2wV8sZqgfhZI6m2w1IVKEbE4l2oRZ55mzTN2dNeZUknMHftZ6GK8iJgaZ6FmZ8il9KIFE+mC0M4VuZv4Ec7u4RlVDMs6N4JQ14urojqrrVXGc8TiU+XA4+yTqCJRgnt8RhU9CGAb4mLljAVJbArZlk9pHgYOmznUoIpeEffd4R16/+zo8p7Sj4EriplIkDxfEtklfqfbfkugOh+cVTM/spd7dXUeTB4h2duRwkneklhc6vvVthDq78dAFYnLPkMb5J6AqbO2Eivb4k40cr8i4rGNS/G6R/08O66WH3QuXPa0JS6RW9dVsO6z7baDk8WoJle51om7f5Yxbill706Mm/qjIC6KweoCU1c/lq7YWnU0IUY/K7H3GgTj6ih/oiKq+F+oqk5IyZOOK+MriI98WPnNjJggF5C/Hyg0Faf/edj8M4cn6mlZ/rgRuBe9XqLm7NChw3+GPwIMAA139Xg+K8vEAAAAAElFTkSuQmCC&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss14275 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABANJREFUeNrsWt1x4jAQNpl7P18FcSqIqSBmhvdABYEKiCvwUQFcBfZVEHhnBl8F8VUQSvB1cJJ3jddC/pOlQBg0YwYUIfbT/n27imXdxtcag947+DuPvXqa5TpYq3F0PmD+bsZeQ0OHvmbg/HMB+2CvDntS9iSaADn48PGDgUtVNvmmQQg+fjEBfmqBBaa9x08ue2KVbe6uNXhcOrDDdQJbjQ/XqjHrBuxLAYMIeVZg9xrhuJegsTx5ztjphuyxe2qI58LVJURFSnk4vfpgwr0qmRywmECgVMrAdJBgB095QmaTDPRqHBv7rnFgZUcPCc3igzP0pfTkwewW7LGJWfO168soW9oJXHDJ6gPwVQnv5wCrNrEDgnSFuXlfszPB7utYfw7EFioBOmLy9wuroE8Lz6ACRBPZXfatmvUD83cu+o0r5DiukT+SIpSve0RTtU2Y5kCTlkLl05dreX7unkeIiTkfS+VKGhJ7QDQYsb3mnw+srCludiMmSNKwPq5lE2DSewLOV81rA0VQtC/RFlSIax9q89UpuJGKz6lyRepT9aBg5BzQFnKbrGpOsj0Lgq3U3rtTNEGH+FTSImLSwPDSoiWQZHvn+Q9+07jGgmP0axcoRCAeMpMmcGtStgRmgfm7iUV7ie3GRKjdrEZzpFG20Nqki6hdKdWTQFxFfuhIErFDhHzBuQVbn0i0JAaJDfGxJ/xsBJh35Hh5ZANA7y0430YAu5dYBKxbjacINGVzG9SwZ9LHXFIM0tEEKs9fG8EkZUPc669KL0Q13P8j5gP8riwwfz/FsM3TwYisfTjOFz5Eq+epZM5guIewLf8x4HUjMm8ffUP0G25eMOcJ0S7CQ0grGkaiDNo0lta2yIrEGhFwe6kwwDEDsu8844XNFXSqH1ibjhFoYy6YmFNRiFLmErUIWJ16+V2jYl4RPzas+14KHNBvnGWmCma47RDl7rtqSyV4xCenWJ+UEzTbd+yBcNN8s8qXeU0Uy1MJIl2BbY/+U8UEytzQxnzlCKD3UlOr32trEhjN/ItGnyiESjElrCW5yq3hjouK39YMDKJWRMis7LSfJeY7zAIE/BfAVOIvXoW2ZqSaPpjUWM75ilrp9CLCJVrys+RMhVqNN5ikNw3MJZSQYYPAQMglMTWxEMzD/bCyrIe0MM3WwNpIkudcUvMduorZp+fxRqJfr8ZLTYOIp4ehyjZ9OsFz1Bj4AgSAqXL/HUw6FFLFSFU49fsxAED5IQ8Aqvdjs+y7IqgelxS6OsE8+b4KDCXKck9Vhwki6jOCoeF+jX7V6+ZF9/1YUJFwExJJ7YraKkZAsQ5xTNyPeUiTJla7mxSu2d+6r5LM3I+VQboSgCkhxLdxG2z8F2AAPtd0UKyJkPIAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss14276 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAzRJREFUeNrsWs2Nm0AUnl1RAFIKCK4gdLBwy9GuYL0VePeWm+1bbt5UYLsC7FtueCswqWDdQCTSQd5bfRM9IcADDD+OeNIIbI8f8837f4NSI4000kg16K7uHz99++nTZUkj/f3965PNRRHvFV2+0FgT76QOj/sGz2dQUxpzWkhgEVQgeC/r8mkCzO1Ao9w+gEnyb8bGSCU8qEPRri3Ebycab5bW9JnVG/cXGvuCeSmNA9ngxRgYgeIFv3ekbk2IwU0IXGqqiv4NgNI2mGsGjsGfi9ThkYZXoIoPDRbrCb4siaTOxhsBI1GvctT1QSzgLW9OA3cf42NCfMOcOfx70JZXlDt5atGGapHT4KFrGn+QeVgDxrxIIi9QtV3nwOCJVm2IiXi/NuXhVNB9T8SXV+1iYROB6pY8mxKbi9yNPeKEQHEAj4YYB6oAk+qxg8ocCFzYg8Qer0nNaWpTcBynLlFlQk2rSfDg6L7DXQ5s1m2DAIZqm7OFGPf/jcS2Bfe3Cwz9C1+kRz6+u11gCOo69v1AGvZRpOK32wMGRxGJLH2FWJggD4zadCaOZelMUYsFol5iIKGOhQjoMdSTnckFc44cD4tK/V4kRouL0ErYZPokLKFQlu64D0UmozeEnco78doMSWJuJvX6hUZLWpLFvBCINUBxc/TZNMHt0saehNcLykDlADyIXDMFr2EAg13MRD+iSgd3I8LBxGRDOvWKSIY1uGcTd44sRNd4M1ugrLt7LmNEOT83LD8UvKHVCqGNOHbE1aQFp/PG/WDjmKAq6qSBGcUuqHfQF7CiLOQfcHHmlapqHWeev1AGhyBtANMPdQnQVuUcbND3KXLH1HBjPjIa2hCOfRwOzn3YmCsAzjOpVSLmLMUmXPOg7Fi4UN1C2qe+VZE95BHeMiuBhbAX91og1zkmXc992ZjOAXdFCS2AHhDHpqqk4yuObmfIMWMTG7srYRaLGBP22ZgRUtLrkGd3YV4MdNRACa49AhgtqRj3yTW3bwLM66KUL/GwZwTwNUqb2JaNearBawkWKPt8oy5XkbtPVIOzqQ6p6MSz9K0BX5T6QyQGtK/75s5II400Ui79FWAANjYkpEJzM+AAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss14277 {
  color: #FFF;
  padding: 48px;
}
.jss14278 {
  color: rgba(0, 0, 0, 0.87);
  width: 100%;
  display: flex;
  z-index: 2;
  opacity: 0.5;
  position: fixed;
  align-items: center;
  padding-left: 16px;
  padding-right: 16px;
  background-color: #424242;
}
.jss14279 {
  height: 100vh;
  display: flex;
  position: relative;
  max-height: 1600px;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: linear-gradient(rgba(0,130,170,0), #424242), url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/1.20e79364.jpg&quot;);
}
.jss14280 {
  margin: -200px auto 0 auto;
  padding: 64px 0 48px;
  max-width: 1200px;
}
.jss14281 {
  width: 50%;
  color: #FFF;
}
@media (max-width:1279.95px) {
  .jss14281 {
    width: 100%;
    padding: 24px;
  }
}
.jss14282 {
  margin: -150px auto 0 auto;
  padding: 16px;
  position: relative;
  max-width: 1200px;
  background: #fafafa;
  justify-content: center;
}
.jss14283 {
  margin: 0 auto;
  padding: 24px;
  display: flex;
}
.jss14284 {
  margin-bottom: 24px;
}
.jss14285 {
  text-decoration: none;
}
.jss14286 {
  margin: 24px auto;
  max-width: 1200px;
}
@media (max-width:1279.95px) {
  .jss14286 {
    max-width: 1366px;
  }
}
.jss14287 {
  height: 160px;
  display: flex;
  align-items: center;
}
.jss14288 {
  margin: auto;
  display: grid;
}
.jss14289 {
  height: 100vh;
  display: flex;
  position: relative;
  overflow: hidden;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/2.adfa400c.jpg&quot;);
  background-position: center;
}
.jss14290 {
  color: #FFF;
  background-color: #03a9f4;
}
.jss14291 {
  color: #03a9f4;
  background-color: #fff;
}
.jss14292 {
  color: #03a9f4;
  padding: 0;
}
.jss14293 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss14293 {
    width: calc(100%);
  }
}
.jss14294 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss14294 {
    width: calc(100% - 300px);
  }
}
.jss14295 {
  display: none;
}
@media (min-width:600px) {
  .jss14295 {
    display: block;
  }
}
.jss14296 {
  color: #fff;
  position: relative;
  background: #03a9f4;
}
.jss14297 {
  width: 100%;
  position: relative;
  border-radius: 14px;
  background-color: rgba(255, 255, 255, 0.5);
}
.jss14297:hover {
  background-color: rgba(255, 255, 255, 0.7);
}
@media (min-width:600px) {
  .jss14297 {
    width: auto;
  }
}
.jss14298 {
  width: 72px;
  color: #212121;
  height: 100%;
  display: flex;
  position: absolute;
  align-items: center;
  pointer-events: none;
  justify-content: center;
}
.jss14299 {
  color: #fff;
  width: 100%;
}
.jss14300 {
  color: #212121;
  width: 100%;
  transition: width 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  padding-top: 8px;
  padding-left: 64px;
  padding-right: 8px;
  padding-bottom: 8px;
}
@media (min-width:600px) {
  .jss14300 {
    width: 80px;
  }
  .jss14300:focus {
    width: 200px;
  }
}
.jss14301 {
  color: #212121;
}
.jss14302 {
  background-color: #4fc3f7;
}
.jss14303 {
  margin-left: 12px;
  margin-right: 36px;
}
.jss14304 {
  display: none;
}
@media (min-width:960px) {
  .jss14305 {
    display: none;
  }
}
.jss14306 {
  width: 300px;
}
.jss14307 {
  display: flex;
  padding: 0 8px;
  min-height: 56px;
  align-items: center;
}
@media (min-width:0px) and (orientation: landscape) {
  .jss14307 {
    min-height: 48px;
  }
}
@media (min-width:600px) {
  .jss14307 {
    min-height: 64px;
  }
}
.jss14308 {
  margin-top: 65px;
  justify-content: flex-start;
}
.jss14309 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss14311 {
  width: 100%;
  height: 100%;
  opacity: 0.2;
  content: &quot;&quot;;
  display: block;
  position: absolute;
  background: linear-gradient(rgba(0,130,170,0), #03a9f4));
  background-size: cover;
  background-position: center center;
}
.jss14312 {
  width: 60px;
  overflow-x: hidden;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss14313 {
  color: inherit;
}
.jss14314 {
  margin-left: 56px;
}
.jss14315 {
  bottom: 16px;
}
.jss14316 {
  color: rgba(0, 0, 0, 0.87);
  background-size: cover;
  background-color: #fff;
  background-image: url(https://xdev.equine.co.id/apps/tessa/static/media/pattern2.f069796d.svg);
  background-repeat: no-repeat;
  background-position: inherit;
}
.jss14317 {
  color: #fff;
  background-color: #03a9f4;
}
.jss14318 {
  color: #fff;
  background-color: #ff9800;
}
.jss14319 {
  color: #fff;
  background-color: #f44336;
}
.jss14320 {
  color: #fff;
  background: #4caf50;
}
.jss14321 {
  flex: 1;
}
.jss14322 {
  padding: 16px;
  flex-grow: 1;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss14322 {
    margin-top: 68px;
  }
}
@media (max-width:959.95px) {
  .jss14322 {
    padding: 0;
    margin-top: 56px;
  }
}
@media (max-width:959.95px) {
  .jss14323 {
    margin-top: 68px;
  }
}
@media (min-width:960px) {
  .jss14323 {
    left: 50%;
    width: 982px;
    position: absolute;
    transform: translateX(-50%);
  }
}
@media (min-width:960px) {
  .jss14324 {
    margin-left: 300px;
    margin-right: 0;
  }
}
.jss14325 {
  margin-bottom: 56px;
}
.jss14326 {
  width: calc(100% - 0px);
  right: 0;
  bottom: 0;
  display: flex;
  position: fixed;
}
@media (min-width:960px) {
  .jss14326 {
    width: calc(100% - 300px);
  }
}
.jss14328 {
  background: #fff;
}
.jss14329 {
  margin-bottom: 60px;
}
.jss14330 {
  height: 4px;
}
.jss14331 {
  display: flex;
  align-items: center;
  justify-content: space-between;
}
.jss14332 {
  margin: 0 15px 0 0;
  padding: 16px 0;
  flex-grow: 1;
}
@media (min-width:600px) {
  .jss14332 {
    width: 30%;
    flex-grow: 0;
  }
}
.jss14333 {
  color: inherit;
  font-size: 0.6964285714285714rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: inherit;
  line-height: 1.66;
  letter-spacing: 0.03333em;
}
.jss14334 {
  color: #fff;
  background: #03a9f4;
}
.jss14335 {
  color: #fff;
  min-height: 40px;
  background: #ff9800;
  margin-bottom: 16px;
}
.jss14336 {
  height: 500px;
  display: flex;
  overflow: hidden;
  align-items: center;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss14336 {
    height: 300px;
  }
}
.jss14337 {
  height: 100%;
  margin: 0 auto;
}
.jss14338 {
  width: 25px;
  height: 25px;
  margin-right: 16px;
}
.jss14339 {
  transform: rotate(0deg);
  transition: transform 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
}
.jss14340 {
  transform: rotate(180deg);
}
.jss14341 {
  min-width: 350px;
}
.jss14342 {
  display: flex;
  flex-wrap: wrap;
}
.jss14343 {
  padding: 0;
}
@media (min-width:0px) {
  .jss14343 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss14343 {
    width: calc(100% / 2);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1280px) {
  .jss14343 {
    width: calc(100% / 3);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1920px) {
  .jss14343 {
    width: calc(100% / 4);
    padding: 0 16px 16px 0;
  }
}
.jss14344 {
  padding: 0;
}
@media (min-width:0px) {
  .jss14344 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss14344 {
    width: calc(100% / 2);
  }
}
@media (min-width:1280px) {
  .jss14344 {
    width: calc(100% - (100% / 3));
  }
}
@media (min-width:1920px) {
  .jss14344 {
    width: calc(100% - (100% / 4));
  }
}
.jss14345 {
  padding: 0;
}
@media (min-width:0px) {
  .jss14345 {
    width: 100%;
  }
}
.jss14346 {
  width: 100%;
  margin-bottom: 16px;
  vertical-align: middle;
}
.jss14347 {
  right: 16px;
  display: flex;
  position: absolute;
}
.jss14348 {
  right: 16px;
  bottom: 16px;
  position: fixed;
}
.jss14349 {
  float: right;
}
.jss14350 {
  margin: 0;
}
.jss14351 {
  margin: 8px;
}
.jss14352 {
  margin-left: 8px;
}
.jss14353 {
  margin-top: 8px;
}
.jss14354 {
  margin-right: 8px;
}
.jss14355 {
  margin-bottom: 8px;
}
.jss14356 {
  margin: 16px;
}
.jss14357 {
  margin-left: 16px;
}
.jss14358 {
  margin-top: 16px;
}
.jss14359 {
  margin-right: 16px;
}
.jss14360 {
  margin-bottom: 16px;
}
.jss14361 {
  margin: 24px;
}
.jss14362 {
  margin-left: 24px;
}
.jss14363 {
  margin-top: 24px;
}
.jss14364 {
  margin-right: 24px;
}
.jss14365 {
  margin-bottom: 24px;
}
.jss14366 {
  padding: 0;
}
.jss14367 {
  padding: 8px;
}
.jss14368 {
  padding-left: 8px;
}
.jss14369 {
  padding-top: 8px;
}
.jss14370 {
  padding-right: 8px;
}
.jss14371 {
  padding-bottom: 8px;
}
.jss14372 {
  padding: 16px;
}
.jss14373 {
  padding-left: 16px;
}
.jss14374 {
  padding-top: 16px;
}
.jss14375 {
  padding-right: 16px;
}
.jss14376 {
  padding-bottom: 16px;
}
.jss14377 {
  padding: 24px;
}
.jss14378 {
  padding-left: 24px;
}
.jss14379 {
  padding-top: 24px;
}
.jss14380 {
  padding-right: 24px;
}
.jss14381 {
  padding-bottom: 24px;
}
.jss14382 {
  background-color: #03a9f4;
}
.jss14383 {
  background-color: #ff9800;
}
.jss14384 {
  color: white;
  background-color: #f44336;
}
.jss14385 {
  color: #03a9f4;
}
.jss14386 {
  color: #ff9800;
}
.jss14387 {
  color: #f44336;
}
.jss14388 {
  text-decoration: line-through;
}
.jss14389 {
  padding: 3px;
}
.jss14390 {
  height: calc(100vh - 96px - 72px);
  overflow-x: auto;
}
.jss14391 {
  width: calc(100vw - 32px - 300px);
}
.jss14392 {
  height: calc(100vh - 56px - 68px);
  overflow-x: auto;
}
.jss14393 {
  width: calc(100vw);
}
.jss14394 {
  overflow-x: auto;
}
.jss14395 {
  table-layout: initial;
}
.jss14396 {
  top: 0;
  position: sticky;
  background-color: #fff;
}
.jss14397 {
  width: 3vw;
}
.jss14398 {
  width: 5vw;
}
.jss14399 {
  width: 10vw;
}
.jss14400 {
  width: 15vw;
}
.jss14401 {
  min-width: 15vw;
  max-width: 15vw;
}
@media (min-width:960px) {
  .jss14401 {
    min-width: calc((100vw - 300px - 48px) * 0.15);
    max-width: calc((100vw - 300px - 48px) * 0.15);
  }
}
.jss14402 {
  min-width: 25vw;
  max-width: 25vw;
}
@media (min-width:960px) {
  .jss14402 {
    min-width: calc((100vw - 300px - 48px) * 0.25);
    max-width: calc((100vw - 300px - 48px) * 0.25);
  }
}
.jss14403 {
  width: 25vw;
}
.jss14404 {
  width: 30vw;
}
.jss14405 {
  min-width: 45vw;
  max-width: 45vw;
}
@media (min-width:960px) {
  .jss14405 {
    min-width: calc((100vw - 300px - 48px) * 0.45);
    max-width: calc((100vw - 300px - 48px) * 0.45);
  }
}
.jss14406 {
  width: 100%;
  overflow-x: auto;
}
.jss14407 {
  width: 100%;
  margin-top: 24px;
  overflow-x: auto;
}
.jss14408 {
  min-width: 700px;
}
.jss14409 {
  width: 100%;
}
.jss14410 {
  min-width: 100px;
}
.jss14411 {
  color: rgba(0, 0, 0, 0.54);
  flex-shrink: 0;
}
.jss14412 {
  color: #2196f3;
}
.jss14413 {
  padding-top: 16px;
  padding-bottom: 16px;
}
.jss14415 {
  max-height: 100px;
}
.jss14416 {
  margin-top: -144px;
  margin-left: -56px;
  margin-right: -136px;
  margin-bottom: -64px;
}
.jss14417 {
  margin-top: -80px;
}
.jss14418 {
  width: 500px;
  height: 450px;
}
.jss14419 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss14420 {
  display: flex;
  box-sizing: border-box;
  align-items: center;
}
.jss14422:hover {
  background-color: #eeeeee;
}
.jss14423 {
  flex: 1;
}
.jss14424 {
  cursor: initial;
}
.jss14425 {
  background: #f44336;
}
.jss14426 {
  color: #fff;
}
.jss14427 {
  width: 100%;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
.jss14428 {
  max-height: 75vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss14429 {
  padding: 10px;
  text-align: center;
}
.jss14430 {
  padding: 0;
  vertical-align: top;
}
.jss14431 {
  margin-top: 8px;
  padding-left: 32px;
}
.jss14432 {
  top: 0;
  padding: 5px;
  position: sticky;
  overflow: hidden;
  max-width: 100px;
  max-height: 10px;
  text-align: center;
  background-color: #fff;
}
.jss14433 {
  max-height: 76vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss14434 {
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss14435 {
  padding: 0;
  margin-top: 8px;
}
.jss14436 {
  left: 0;
  width: 500px;
  z-index: 1;
  padding: 8px 16px 0 16px;
  min-width: 300px;
  background: #fff;
  vertical-align: top;
}
.jss14437 {
  padding: 8px 16px 0 16px;
  min-width: 300px;
  vertical-align: top;
}
.jss14438 {
  min-width: 300px;
}
.jss14439 {
  transform: rotate(180deg);
  writing-mode: vertical-rl;
}
.jss14440 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss14441 {
  cursor: pointer;
  transition: all .3s;
}
.jss14441:hover {
  background-color: #ebebeb;
}
.jss14442 {
  background-color: #ebebeb;
}
.jss14443 {
  transition: all .3s;
}
.jss14443:hover {
  transform: scale(1.03);
}
.jss14444 {
  padding: 0;
  transition: all .3s;
}
.jss14444:hover {
  transform: scale(1.03);
}
.jss14445 {
  display: grid;
  align-items: center;
  grid-column-gap: 1.5rem;
  grid-template-columns: 1fr max-content 1fr;
}
.jss14445::after, .jss14445::before {
  height: 1px;
  content: '';
  display: block;
  background-color: currentColor;
}
.jss14446 a {
  text-decoration: none;
}
.jss14447 {
  left: 50%;
  position: absolute;
  transform: ;
}
.jss14448 {
  display: -webkit-box;
  overflow: hidden;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;
}
.jss14449 {
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
.jss14450 {
  display: inline;
  text-transform: capitalize;
}
.jss14451 {
  text-decoration: none;
}
.jss14451 p:hover {
  color: #03a9f4;
}
.jss14452 {
  margin-left: 4px;
}
.jss14453 {
  padding: 0 16px;
  display: inline-flex;
  position: relative;
  vertical-align: middle;
}
.jss14454 {
  top: 0;
  color: #fff;
  right: 0;
  height: 20px;
  display: flex;
  padding: 0 4px;
  z-index: 1;
  position: absolute;
  flex-wrap: wrap;
  min-width: 20px;
  transform: translate(20px, -50%);
  box-sizing: border-box;
  transition: transform 225ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  align-items: center;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  align-content: center;
  border-radius: 10px;
  flex-direction: row;
  justify-content: center;
  background-color: #ff9800;
  transform-origin: 100% 0%;
}
.jss14455 {
  margin: 0;
  padding: 16px;
  border-bottom: 1px solid rgba(0, 0, 0, 0.12);
}
.jss14456 {
  top: 8px;
  right: 8px;
  color: #9e9e9e;
  position: absolute;
}
.jss14457 {
  margin: 0;
  padding: 16px;
}
.jss14458 {
  margin: 0;
  padding: 8px;
  border-top: 1px solid rgba(0, 0, 0, 0.12);
}
.jss14459 {
  font-size: 13px;
}
.jss14460 {
  padding: 0;
  overflow: hidden;
}
.jss14460 span {
  display: inline-flex;
}
.jss14460 span:hover {
  position: relative;
  animation: moveTextHorz 3s linear infinite;
}
@-webkit-keyframes moveTextHorz {
  0% {
    transform: translate(0, 0);
  }
  100% {
    transform: translate(-70%, 0);
  }
}
.jss14461 span {
  color: #f44336;
}
.jss14462 {
  width: 50px;
}
@media (min-width:600px) {
  .jss14462 {
    right: 90px;
  }
}
@media (min-width:960px) {
  .jss14462 {
    right: 15px;
  }
}
.jss14463 {
  cursor: pointer;
}
.jss14463:hover {
  background: #0288d1;
}
.jss14464 {
  margin-left: 10px;
  vertical-align: middle;
}
.jss14465 {
  width: 100%;
}
.jss14466 {
  width: 100%;
  margin: 0;
}
@media (min-width:960px) {
  .jss14467 {
    margin-left: auto;
    margin-right: 16px;
  }
}
@media (min-width:1280px) {
  .jss14467 {
    margin-left: auto;
    margin-right: 32px;
  }
}
@media (min-width:960px) {
  .jss14468 {
    transform: translateY(75%);
    margin-left: 16px;
  }
}
@media (min-width:1280px) {
  .jss14468 {
    transform: translateY(75%);
    margin-left: 32px;
  }
}
.jss14469 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
@media (max-width:1279.95px) {
  .jss14470 {
    display: none;
  }
}
@media (max-width:1919.95px) {
  .jss14471 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss14472 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss14473 {
    text-align: right;
  }
}
.jss14474 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss14474 {
    text-align: right;
  }
}
.jss14475 {
  padding: 0;
}
@media (min-width:1280px) {
  .jss14476 {
    margin-top: 0 !important;
  }
}
@media (min-width:1280px) {
  .jss14477 {
    margin-top: 64px;
  }
}
.jss14478 div textarea {
  overflow: auto;
}
.jss14479 {
  width: 70%;
}
@media (min-width:1280px) {
  .jss14479 {
    float: right;
  }
}
@media (min-width:1280px) {
  .jss14480 {
    float: right;
  }
}
@media (max-width:1279.95px) {
  .jss14480 {
    margin-top: 6px;
  }
}
@media (min-width:1280px) {
  .jss14481 {
    padding: 12px 0;
  }
}
@media (max-width:1279.95px) {
  .jss14481 {
    padding: 6px 0 3px;
  }
}
.jss14482 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss14482 {
    margin-top: 0 !important;
  }
}
.jss14482 div textarea {
  overflow: auto;
}
.jss14483 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss14484 {
  width: 57px;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
  overflow-x: hidden;
}
@media (min-width:600px) {
  .jss14484 {
    width: 73px;
  }
}
@media (min-width:960px) {
  .jss14485 {
    width: calc(100% - 73px);
    margin-left: 73px;
    margin-right: 0;
  }
}
@media (min-width:960px) {
  .jss14486 {
    margin-left: 73px;
    margin-right: 0;
  }
}
.jss14487 {
  bottom: 0px;
  z-index: 1;
  position: sticky;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss14487 {
    display: none;
  }
}
.jss14487:hover {
  background-color: rgb(235, 235, 235);
}
@media (max-width:959.95px) {
  .jss14488 {
    display: none;
  }
}
.jss14489 {
  top: 0;
  left: auto;
  width: 100%;
  right: 0;
  height: 100%;
  z-index: 1301;
  overflow: hidden;
  position: absolute;
  overflow-y: auto;
  transition: transform 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
@media (min-width:960px) {
  .jss14489 {
    width: 24vw;
  }
}
.jss14490 {
  cursor: pointer;
}
.jss14490:hover {
  background-color: #eeeeee;
}
.jss14491 {
  background-color: #e0e0e0 !important;
}
.jss14492 {
  transition: width 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
.jss14493 {
  border-collapse: separate;
}
.jss14494:hover {
  background-color: rgb(235, 235, 235);
}
.jss14495 {
  position: relative;
  min-height: 80vh;
}
.jss14495 .back-icon {
  top: 5px;
  left: 5px;
  z-index: 1;
  position: absolute;
}
.jss14495 > .ninebox-inner {
  width: 100%;
  position: relative;
  min-height: 80vh;
}
.jss14495 > .ninebox-inner .category-paper {
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #424242;
}
.jss14495 > .ninebox-inner .percentage-wrapper {
  width: 100%;
  height: 100%;
  display: flex;
  flex-wrap: wrap;
  box-sizing: border-box;
}
.jss14495 > .ninebox-inner .horizontal-category {
  height: 50px;
  margin: 0 60px;
  display: flex;
  align-items: center;
  justify-content: center;
}
.jss14495 > .ninebox-inner .vertical-category {
  width: 50px;
  height: calc(80vh - 120px);
  display: inline-flex;
  align-items: center;
  justify-content: center;
}
.jss14495 > .ninebox-inner .ninebox-content {
  width: calc(100% - 100px);
  height: calc(80vh - 120px);
  display: inline-block;
  position: relative;
}
.jss14495 > .ninebox-inner .ninebox-content .wrapper {
  width: 100%;
  height: 100%;
  margin: 0;
}
.jss14495 > .ninebox-inner .ninebox-content .wrapper .box {
  height: calc(100% / 3);
  padding: 10px;
}
.jss14495 > .ninebox-inner .ninebox-content .wrapper .box .has-item {
  padding: 26px 10px !important;
}
.jss14495 > .ninebox-inner .ninebox-content .wrapper .box .disabled {
  background: #d1d1d1;
}
.jss14495 > .ninebox-inner .ninebox-content .wrapper .box .card {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 30px;
  position: relative;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  -webkit-transition: all .4s ease-out;
}
.jss14495 > .ninebox-inner .ninebox-content .wrapper .box .card h6 {
  color: rgba(0, 0, 0, 0.87);
}
.jss14495 > .ninebox-inner .ninebox-content .wrapper .box .card .total-person {
  top: 5px;
  right: 5px;
  width: 15px;
  height: 15px;
  position: absolute;
}
.jss14495 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list {
  width: 100%;
  height: 100%;
  display: flex;
  overflow-y: auto;
  align-items: center;
}
.jss14495 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list > ol {
  height: 100%;
  margin: 0;
  padding: 0;
  text-align: left;
  list-style: none;
}
.jss14495 > .ninebox-inner .ninebox-content .wrapper .box .active:hover {
  cursor: pointer;
  transform: scale(1.1);
}
.jss14495 > .ninebox-inner .vertical-category .text-rotate {
  transform: rotate(270deg);
}
.jss14495 > .ninebox-inner .vertical-category.left {
  padding: 10px 0;
  margin-right: 9px;
}
.jss14495 > .ninebox-inner .vertical-category.right {
  margin-left: 9px;
}
.jss14495 > .ninebox-inner .vertical-category .text-rotate.nowrap {
  white-space: nowrap;
}
.jss14495 > .ninebox-inner .horizontal-category.top {
  padding: 0 9px;
  margin-bottom: 9px;
}
.jss14495 > .ninebox-inner .horizontal-category.bottom {
  margin-top: 9px;
}
.jss14495 > .ninebox-inner .percentage-wrapper > .percentage-item {
  padding: 0 10px;
  flex-grow: 0;
  max-width: calc(100% / 3);
  flex-basis: calc(100% / 3);
}
.jss14495 > .ninebox-inner .percentage-wrapper > .percentage-item.horizontal {
  height: calc(100% / 3);
  padding: 10px 0;
  max-width: 100%;
}
.jss14495 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 10px;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  background-color: #424242;
  -webkit-transition: all .4s ease-out;
}
.jss14495 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper > p {
  color: #fff;
}
.jss14495 > .ninebox-inner .category-paper > p {
  color: #fff;
}
.jss14496 {
  max-height: 45vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss14496 .table {
  border-collapse: separate;
}
.jss14496 .table .sticky-head {
  top: 0;
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss14497 {
  height: 44vh;
  overflow: auto;
}
.jss14498 {
  width: 100%;
  height: 50vh;
  overflow: auto;
}
.jss14499 {
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss14500 {
  width: 100%;
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss14501 {
  max-width: 82%;
}

@media (min-width:960px) {
  ::-webkit-scrollbar {
    width: 5px;
    height: 5px;
  }
  ::-webkit-scrollbar-track {
    background: rgba(250, 250, 250, 0);
  }
  ::-webkit-scrollbar-thumb {
    background: #eeeeee;
  }
  ::-webkit-scrollbar-thumb:hover {
    background: #e0e0e0;
  }
}
.jss13546 {
  width: 100%;
  z-index: 1;
  overflow: hidden;
  margin-bottom: 0;
}
.jss13547 {
  background-color: #424242;
}
.jss13548 {
  height: 70px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/etg.9362c8fb.svg&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss13549 {
  height: 150px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/tessa.55f0b81f.svg&quot;);
  background-repeat: no-repeat;
}
.jss13550 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2hJREFUeNrsWtGR2jAQ1TEU4FQQUsGRCuBm+IerAKgguAKgAnMVHFcBvn/PnFwBvg6cCuIOEslacRvFyJJswL74zQgDNrKedvftSoaQDh06dHDAnfGVfjRjr0PWRg3kkbD2RIJJakfMjzbsdd0CQz0yciF/0zf8wRyOKbSmgXuSx1rAmhWxgbSdnJFG4cOj5DhJz7KLrC3i0a8wSwM8QzcRjGCS1U+MkLcbE9uztjQn5kcyEFMsn5q4uxW09+8hQh5rR/buCNY4OtxsBzMpEaLZ/cIm6g6uyZTrtih+d6C8/PxDLuEiT1mhp7jWEH32HIi95sr5kRKe4PiTtRWbuN/5UfS9hXOUEd6gSXgBa/gwpoMyLiMh6ym5gJxmSsyuCxaK71OUAyka0AC+i8H9YzjnofEsNfd5NyWWnCwVTGgF35/CoPbK9ykMXt5nDEQpWIaC1aZwnlvqnrXvLq7YV2Z2CDesijUMCLv0FNxQYgQWkWIl3fOAfrcq+N46xuJTh8I1XOCBi4nyRqjqEMUZVSyWIQvNlZAI4fr4TIyNTIlh93O12gHJ8AxUluMHkI2V+xwhJmVsviniEAOBtbsr8izuRykM7L7G4lRacl2SizxFiRd1llQUOjSx2BYN9lpVvyzjMkgtxsRiIDbIa0Fd5SFyz6apRXCvwGJV46yBxISFUhPVaeOyJQE/HpcsW8aO2wV8sZqgfhZI6m2w1IVKEbE4l2oRZ55mzTN2dNeZUknMHftZ6GK8iJgaZ6FmZ8il9KIFE+mC0M4VuZv4Ec7u4RlVDMs6N4JQ14urojqrrVXGc8TiU+XA4+yTqCJRgnt8RhU9CGAb4mLljAVJbArZlk9pHgYOmznUoIpeEffd4R16/+zo8p7Sj4EriplIkDxfEtklfqfbfkugOh+cVTM/spd7dXUeTB4h2duRwkneklhc6vvVthDq78dAFYnLPkMb5J6AqbO2Eivb4k40cr8i4rGNS/G6R/08O66WH3QuXPa0JS6RW9dVsO6z7baDk8WoJle51om7f5Yxbill706Mm/qjIC6KweoCU1c/lq7YWnU0IUY/K7H3GgTj6ih/oiKq+F+oqk5IyZOOK+MriI98WPnNjJggF5C/Hyg0Faf/edj8M4cn6mlZ/rgRuBe9XqLm7NChw3+GPwIMAA139Xg+K8vEAAAAAElFTkSuQmCC&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss13551 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABANJREFUeNrsWt1x4jAQNpl7P18FcSqIqSBmhvdABYEKiCvwUQFcBfZVEHhnBl8F8VUQSvB1cJJ3jddC/pOlQBg0YwYUIfbT/n27imXdxtcag947+DuPvXqa5TpYq3F0PmD+bsZeQ0OHvmbg/HMB+2CvDntS9iSaADn48PGDgUtVNvmmQQg+fjEBfmqBBaa9x08ue2KVbe6uNXhcOrDDdQJbjQ/XqjHrBuxLAYMIeVZg9xrhuJegsTx5ztjphuyxe2qI58LVJURFSnk4vfpgwr0qmRywmECgVMrAdJBgB095QmaTDPRqHBv7rnFgZUcPCc3igzP0pfTkwewW7LGJWfO168soW9oJXHDJ6gPwVQnv5wCrNrEDgnSFuXlfszPB7utYfw7EFioBOmLy9wuroE8Lz6ACRBPZXfatmvUD83cu+o0r5DiukT+SIpSve0RTtU2Y5kCTlkLl05dreX7unkeIiTkfS+VKGhJ7QDQYsb3mnw+srCludiMmSNKwPq5lE2DSewLOV81rA0VQtC/RFlSIax9q89UpuJGKz6lyRepT9aBg5BzQFnKbrGpOsj0Lgq3U3rtTNEGH+FTSImLSwPDSoiWQZHvn+Q9+07jGgmP0axcoRCAeMpMmcGtStgRmgfm7iUV7ie3GRKjdrEZzpFG20Nqki6hdKdWTQFxFfuhIErFDhHzBuQVbn0i0JAaJDfGxJ/xsBJh35Hh5ZANA7y0430YAu5dYBKxbjacINGVzG9SwZ9LHXFIM0tEEKs9fG8EkZUPc669KL0Q13P8j5gP8riwwfz/FsM3TwYisfTjOFz5Eq+epZM5guIewLf8x4HUjMm8ffUP0G25eMOcJ0S7CQ0grGkaiDNo0lta2yIrEGhFwe6kwwDEDsu8844XNFXSqH1ibjhFoYy6YmFNRiFLmErUIWJ16+V2jYl4RPzas+14KHNBvnGWmCma47RDl7rtqSyV4xCenWJ+UEzTbd+yBcNN8s8qXeU0Uy1MJIl2BbY/+U8UEytzQxnzlCKD3UlOr32trEhjN/ItGnyiESjElrCW5yq3hjouK39YMDKJWRMis7LSfJeY7zAIE/BfAVOIvXoW2ZqSaPpjUWM75ilrp9CLCJVrys+RMhVqNN5ikNw3MJZSQYYPAQMglMTWxEMzD/bCyrIe0MM3WwNpIkudcUvMduorZp+fxRqJfr8ZLTYOIp4ehyjZ9OsFz1Bj4AgSAqXL/HUw6FFLFSFU49fsxAED5IQ8Aqvdjs+y7IqgelxS6OsE8+b4KDCXKck9Vhwki6jOCoeF+jX7V6+ZF9/1YUJFwExJJ7YraKkZAsQ5xTNyPeUiTJla7mxSu2d+6r5LM3I+VQboSgCkhxLdxG2z8F2AAPtd0UKyJkPIAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss13552 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAzRJREFUeNrsWs2Nm0AUnl1RAFIKCK4gdLBwy9GuYL0VePeWm+1bbt5UYLsC7FtueCswqWDdQCTSQd5bfRM9IcADDD+OeNIIbI8f8837f4NSI4000kg16K7uHz99++nTZUkj/f3965PNRRHvFV2+0FgT76QOj/sGz2dQUxpzWkhgEVQgeC/r8mkCzO1Ao9w+gEnyb8bGSCU8qEPRri3Ebycab5bW9JnVG/cXGvuCeSmNA9ngxRgYgeIFv3ekbk2IwU0IXGqqiv4NgNI2mGsGjsGfi9ThkYZXoIoPDRbrCb4siaTOxhsBI1GvctT1QSzgLW9OA3cf42NCfMOcOfx70JZXlDt5atGGapHT4KFrGn+QeVgDxrxIIi9QtV3nwOCJVm2IiXi/NuXhVNB9T8SXV+1iYROB6pY8mxKbi9yNPeKEQHEAj4YYB6oAk+qxg8ocCFzYg8Qer0nNaWpTcBynLlFlQk2rSfDg6L7DXQ5s1m2DAIZqm7OFGPf/jcS2Bfe3Cwz9C1+kRz6+u11gCOo69v1AGvZRpOK32wMGRxGJLH2FWJggD4zadCaOZelMUYsFol5iIKGOhQjoMdSTnckFc44cD4tK/V4kRouL0ErYZPokLKFQlu64D0UmozeEnco78doMSWJuJvX6hUZLWpLFvBCINUBxc/TZNMHt0saehNcLykDlADyIXDMFr2EAg13MRD+iSgd3I8LBxGRDOvWKSIY1uGcTd44sRNd4M1ugrLt7LmNEOT83LD8UvKHVCqGNOHbE1aQFp/PG/WDjmKAq6qSBGcUuqHfQF7CiLOQfcHHmlapqHWeev1AGhyBtANMPdQnQVuUcbND3KXLH1HBjPjIa2hCOfRwOzn3YmCsAzjOpVSLmLMUmXPOg7Fi4UN1C2qe+VZE95BHeMiuBhbAX91og1zkmXc992ZjOAXdFCS2AHhDHpqqk4yuObmfIMWMTG7srYRaLGBP22ZgRUtLrkGd3YV4MdNRACa49AhgtqRj3yTW3bwLM66KUL/GwZwTwNUqb2JaNearBawkWKPt8oy5XkbtPVIOzqQ6p6MSz9K0BX5T6QyQGtK/75s5II400Ui79FWAANjYkpEJzM+AAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss13553 {
  color: #FFF;
  padding: 48px;
}
.jss13554 {
  color: rgba(0, 0, 0, 0.87);
  width: 100%;
  display: flex;
  z-index: 2;
  opacity: 0.5;
  position: fixed;
  align-items: center;
  padding-left: 16px;
  padding-right: 16px;
  background-color: #424242;
}
.jss13555 {
  height: 100vh;
  display: flex;
  position: relative;
  max-height: 1600px;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: linear-gradient(rgba(0,130,170,0), #424242), url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/1.20e79364.jpg&quot;);
}
.jss13556 {
  margin: -200px auto 0 auto;
  padding: 64px 0 48px;
  max-width: 1200px;
}
.jss13557 {
  width: 50%;
  color: #FFF;
}
@media (max-width:1279.95px) {
  .jss13557 {
    width: 100%;
    padding: 24px;
  }
}
.jss13558 {
  margin: -150px auto 0 auto;
  padding: 16px;
  position: relative;
  max-width: 1200px;
  background: #fafafa;
  justify-content: center;
}
.jss13559 {
  margin: 0 auto;
  padding: 24px;
  display: flex;
}
.jss13560 {
  margin-bottom: 24px;
}
.jss13561 {
  text-decoration: none;
}
.jss13562 {
  margin: 24px auto;
  max-width: 1200px;
}
@media (max-width:1279.95px) {
  .jss13562 {
    max-width: 1366px;
  }
}
.jss13563 {
  height: 160px;
  display: flex;
  align-items: center;
}
.jss13564 {
  margin: auto;
  display: grid;
}
.jss13565 {
  height: 100vh;
  display: flex;
  position: relative;
  overflow: hidden;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/2.adfa400c.jpg&quot;);
  background-position: center;
}
.jss13566 {
  color: #FFF;
  background-color: #03a9f4;
}
.jss13567 {
  color: #03a9f4;
  background-color: #fff;
}
.jss13568 {
  color: #03a9f4;
  padding: 0;
}
.jss13569 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss13569 {
    width: calc(100%);
  }
}
.jss13570 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss13570 {
    width: calc(100% - 300px);
  }
}
.jss13571 {
  display: none;
}
@media (min-width:600px) {
  .jss13571 {
    display: block;
  }
}
.jss13572 {
  color: #fff;
  position: relative;
  background: #03a9f4;
}
.jss13573 {
  width: 100%;
  position: relative;
  border-radius: 14px;
  background-color: rgba(255, 255, 255, 0.5);
}
.jss13573:hover {
  background-color: rgba(255, 255, 255, 0.7);
}
@media (min-width:600px) {
  .jss13573 {
    width: auto;
  }
}
.jss13574 {
  width: 72px;
  color: #212121;
  height: 100%;
  display: flex;
  position: absolute;
  align-items: center;
  pointer-events: none;
  justify-content: center;
}
.jss13575 {
  color: #fff;
  width: 100%;
}
.jss13576 {
  color: #212121;
  width: 100%;
  transition: width 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  padding-top: 8px;
  padding-left: 64px;
  padding-right: 8px;
  padding-bottom: 8px;
}
@media (min-width:600px) {
  .jss13576 {
    width: 80px;
  }
  .jss13576:focus {
    width: 200px;
  }
}
.jss13577 {
  color: #212121;
}
.jss13578 {
  background-color: #4fc3f7;
}
.jss13579 {
  margin-left: 12px;
  margin-right: 36px;
}
.jss13580 {
  display: none;
}
@media (min-width:960px) {
  .jss13581 {
    display: none;
  }
}
.jss13582 {
  width: 300px;
}
.jss13583 {
  display: flex;
  padding: 0 8px;
  min-height: 56px;
  align-items: center;
}
@media (min-width:0px) and (orientation: landscape) {
  .jss13583 {
    min-height: 48px;
  }
}
@media (min-width:600px) {
  .jss13583 {
    min-height: 64px;
  }
}
.jss13584 {
  margin-top: 65px;
  justify-content: flex-start;
}
.jss13585 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss13587 {
  width: 100%;
  height: 100%;
  opacity: 0.2;
  content: &quot;&quot;;
  display: block;
  position: absolute;
  background: linear-gradient(rgba(0,130,170,0), #03a9f4));
  background-size: cover;
  background-position: center center;
}
.jss13588 {
  width: 60px;
  overflow-x: hidden;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss13589 {
  color: inherit;
}
.jss13590 {
  margin-left: 56px;
}
.jss13591 {
  bottom: 16px;
}
.jss13592 {
  color: rgba(0, 0, 0, 0.87);
  background-size: cover;
  background-color: #fff;
  background-image: url(https://xdev.equine.co.id/apps/tessa/static/media/pattern2.f069796d.svg);
  background-repeat: no-repeat;
  background-position: inherit;
}
.jss13593 {
  color: #fff;
  background-color: #03a9f4;
}
.jss13594 {
  color: #fff;
  background-color: #ff9800;
}
.jss13595 {
  color: #fff;
  background-color: #f44336;
}
.jss13596 {
  color: #fff;
  background: #4caf50;
}
.jss13597 {
  flex: 1;
}
.jss13598 {
  padding: 16px;
  flex-grow: 1;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss13598 {
    margin-top: 68px;
  }
}
@media (max-width:959.95px) {
  .jss13598 {
    padding: 0;
    margin-top: 56px;
  }
}
@media (max-width:959.95px) {
  .jss13599 {
    margin-top: 68px;
  }
}
@media (min-width:960px) {
  .jss13599 {
    left: 50%;
    width: 982px;
    position: absolute;
    transform: translateX(-50%);
  }
}
@media (min-width:960px) {
  .jss13600 {
    margin-left: 300px;
    margin-right: 0;
  }
}
.jss13601 {
  margin-bottom: 56px;
}
.jss13602 {
  width: calc(100% - 0px);
  right: 0;
  bottom: 0;
  display: flex;
  position: fixed;
}
@media (min-width:960px) {
  .jss13602 {
    width: calc(100% - 300px);
  }
}
.jss13604 {
  background: #fff;
}
.jss13605 {
  margin-bottom: 60px;
}
.jss13606 {
  height: 4px;
}
.jss13607 {
  display: flex;
  align-items: center;
  justify-content: space-between;
}
.jss13608 {
  margin: 0 15px 0 0;
  padding: 16px 0;
  flex-grow: 1;
}
@media (min-width:600px) {
  .jss13608 {
    width: 30%;
    flex-grow: 0;
  }
}
.jss13609 {
  color: inherit;
  font-size: 0.6964285714285714rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: inherit;
  line-height: 1.66;
  letter-spacing: 0.03333em;
}
.jss13610 {
  color: #fff;
  background: #03a9f4;
}
.jss13611 {
  color: #fff;
  min-height: 40px;
  background: #ff9800;
  margin-bottom: 16px;
}
.jss13612 {
  height: 500px;
  display: flex;
  overflow: hidden;
  align-items: center;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss13612 {
    height: 300px;
  }
}
.jss13613 {
  height: 100%;
  margin: 0 auto;
}
.jss13614 {
  width: 25px;
  height: 25px;
  margin-right: 16px;
}
.jss13615 {
  transform: rotate(0deg);
  transition: transform 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
}
.jss13616 {
  transform: rotate(180deg);
}
.jss13617 {
  min-width: 350px;
}
.jss13618 {
  display: flex;
  flex-wrap: wrap;
}
.jss13619 {
  padding: 0;
}
@media (min-width:0px) {
  .jss13619 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss13619 {
    width: calc(100% / 2);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1280px) {
  .jss13619 {
    width: calc(100% / 3);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1920px) {
  .jss13619 {
    width: calc(100% / 4);
    padding: 0 16px 16px 0;
  }
}
.jss13620 {
  padding: 0;
}
@media (min-width:0px) {
  .jss13620 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss13620 {
    width: calc(100% / 2);
  }
}
@media (min-width:1280px) {
  .jss13620 {
    width: calc(100% - (100% / 3));
  }
}
@media (min-width:1920px) {
  .jss13620 {
    width: calc(100% - (100% / 4));
  }
}
.jss13621 {
  padding: 0;
}
@media (min-width:0px) {
  .jss13621 {
    width: 100%;
  }
}
.jss13622 {
  width: 100%;
  margin-bottom: 16px;
  vertical-align: middle;
}
.jss13623 {
  right: 16px;
  display: flex;
  position: absolute;
}
.jss13624 {
  right: 16px;
  bottom: 16px;
  position: fixed;
}
.jss13625 {
  float: right;
}
.jss13626 {
  margin: 0;
}
.jss13627 {
  margin: 8px;
}
.jss13628 {
  margin-left: 8px;
}
.jss13629 {
  margin-top: 8px;
}
.jss13630 {
  margin-right: 8px;
}
.jss13631 {
  margin-bottom: 8px;
}
.jss13632 {
  margin: 16px;
}
.jss13633 {
  margin-left: 16px;
}
.jss13634 {
  margin-top: 16px;
}
.jss13635 {
  margin-right: 16px;
}
.jss13636 {
  margin-bottom: 16px;
}
.jss13637 {
  margin: 24px;
}
.jss13638 {
  margin-left: 24px;
}
.jss13639 {
  margin-top: 24px;
}
.jss13640 {
  margin-right: 24px;
}
.jss13641 {
  margin-bottom: 24px;
}
.jss13642 {
  padding: 0;
}
.jss13643 {
  padding: 8px;
}
.jss13644 {
  padding-left: 8px;
}
.jss13645 {
  padding-top: 8px;
}
.jss13646 {
  padding-right: 8px;
}
.jss13647 {
  padding-bottom: 8px;
}
.jss13648 {
  padding: 16px;
}
.jss13649 {
  padding-left: 16px;
}
.jss13650 {
  padding-top: 16px;
}
.jss13651 {
  padding-right: 16px;
}
.jss13652 {
  padding-bottom: 16px;
}
.jss13653 {
  padding: 24px;
}
.jss13654 {
  padding-left: 24px;
}
.jss13655 {
  padding-top: 24px;
}
.jss13656 {
  padding-right: 24px;
}
.jss13657 {
  padding-bottom: 24px;
}
.jss13658 {
  background-color: #03a9f4;
}
.jss13659 {
  background-color: #ff9800;
}
.jss13660 {
  color: white;
  background-color: #f44336;
}
.jss13661 {
  color: #03a9f4;
}
.jss13662 {
  color: #ff9800;
}
.jss13663 {
  color: #f44336;
}
.jss13664 {
  text-decoration: line-through;
}
.jss13665 {
  padding: 3px;
}
.jss13666 {
  height: calc(100vh - 96px - 72px);
  overflow-x: auto;
}
.jss13667 {
  width: calc(100vw - 32px - 300px);
}
.jss13668 {
  height: calc(100vh - 56px - 68px);
  overflow-x: auto;
}
.jss13669 {
  width: calc(100vw);
}
.jss13670 {
  overflow-x: auto;
}
.jss13671 {
  table-layout: initial;
}
.jss13672 {
  top: 0;
  position: sticky;
  background-color: #fff;
}
.jss13673 {
  width: 3vw;
}
.jss13674 {
  width: 5vw;
}
.jss13675 {
  width: 10vw;
}
.jss13676 {
  width: 15vw;
}
.jss13677 {
  min-width: 15vw;
  max-width: 15vw;
}
@media (min-width:960px) {
  .jss13677 {
    min-width: calc((100vw - 300px - 48px) * 0.15);
    max-width: calc((100vw - 300px - 48px) * 0.15);
  }
}
.jss13678 {
  min-width: 25vw;
  max-width: 25vw;
}
@media (min-width:960px) {
  .jss13678 {
    min-width: calc((100vw - 300px - 48px) * 0.25);
    max-width: calc((100vw - 300px - 48px) * 0.25);
  }
}
.jss13679 {
  width: 25vw;
}
.jss13680 {
  width: 30vw;
}
.jss13681 {
  min-width: 45vw;
  max-width: 45vw;
}
@media (min-width:960px) {
  .jss13681 {
    min-width: calc((100vw - 300px - 48px) * 0.45);
    max-width: calc((100vw - 300px - 48px) * 0.45);
  }
}
.jss13682 {
  width: 100%;
  overflow-x: auto;
}
.jss13683 {
  width: 100%;
  margin-top: 24px;
  overflow-x: auto;
}
.jss13684 {
  min-width: 700px;
}
.jss13685 {
  width: 100%;
}
.jss13686 {
  min-width: 100px;
}
.jss13687 {
  color: rgba(0, 0, 0, 0.54);
  flex-shrink: 0;
}
.jss13688 {
  color: #2196f3;
}
.jss13689 {
  padding-top: 16px;
  padding-bottom: 16px;
}
.jss13691 {
  max-height: 100px;
}
.jss13692 {
  margin-top: -144px;
  margin-left: -56px;
  margin-right: -136px;
  margin-bottom: -64px;
}
.jss13693 {
  margin-top: -80px;
}
.jss13694 {
  width: 500px;
  height: 450px;
}
.jss13695 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss13696 {
  display: flex;
  box-sizing: border-box;
  align-items: center;
}
.jss13698:hover {
  background-color: #eeeeee;
}
.jss13699 {
  flex: 1;
}
.jss13700 {
  cursor: initial;
}
.jss13701 {
  background: #f44336;
}
.jss13702 {
  color: #fff;
}
.jss13703 {
  width: 100%;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
.jss13704 {
  max-height: 75vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss13705 {
  padding: 10px;
  text-align: center;
}
.jss13706 {
  padding: 0;
  vertical-align: top;
}
.jss13707 {
  margin-top: 8px;
  padding-left: 32px;
}
.jss13708 {
  top: 0;
  padding: 5px;
  position: sticky;
  overflow: hidden;
  max-width: 100px;
  max-height: 10px;
  text-align: center;
  background-color: #fff;
}
.jss13709 {
  max-height: 76vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss13710 {
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss13711 {
  padding: 0;
  margin-top: 8px;
}
.jss13712 {
  left: 0;
  width: 500px;
  z-index: 1;
  padding: 8px 16px 0 16px;
  min-width: 300px;
  background: #fff;
  vertical-align: top;
}
.jss13713 {
  padding: 8px 16px 0 16px;
  min-width: 300px;
  vertical-align: top;
}
.jss13714 {
  min-width: 300px;
}
.jss13715 {
  transform: rotate(180deg);
  writing-mode: vertical-rl;
}
.jss13716 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss13717 {
  cursor: pointer;
  transition: all .3s;
}
.jss13717:hover {
  background-color: #ebebeb;
}
.jss13718 {
  background-color: #ebebeb;
}
.jss13719 {
  transition: all .3s;
}
.jss13719:hover {
  transform: scale(1.03);
}
.jss13720 {
  padding: 0;
  transition: all .3s;
}
.jss13720:hover {
  transform: scale(1.03);
}
.jss13721 {
  display: grid;
  align-items: center;
  grid-column-gap: 1.5rem;
  grid-template-columns: 1fr max-content 1fr;
}
.jss13721::after, .jss13721::before {
  height: 1px;
  content: '';
  display: block;
  background-color: currentColor;
}
.jss13722 a {
  text-decoration: none;
}
.jss13723 {
  left: 50%;
  position: absolute;
  transform: ;
}
.jss13724 {
  display: -webkit-box;
  overflow: hidden;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;
}
.jss13725 {
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
.jss13726 {
  display: inline;
  text-transform: capitalize;
}
.jss13727 {
  text-decoration: none;
}
.jss13727 p:hover {
  color: #03a9f4;
}
.jss13728 {
  margin-left: 4px;
}
.jss13729 {
  padding: 0 16px;
  display: inline-flex;
  position: relative;
  vertical-align: middle;
}
.jss13730 {
  top: 0;
  color: #fff;
  right: 0;
  height: 20px;
  display: flex;
  padding: 0 4px;
  z-index: 1;
  position: absolute;
  flex-wrap: wrap;
  min-width: 20px;
  transform: translate(20px, -50%);
  box-sizing: border-box;
  transition: transform 225ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  align-items: center;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  align-content: center;
  border-radius: 10px;
  flex-direction: row;
  justify-content: center;
  background-color: #ff9800;
  transform-origin: 100% 0%;
}
.jss13731 {
  margin: 0;
  padding: 16px;
  border-bottom: 1px solid rgba(0, 0, 0, 0.12);
}
.jss13732 {
  top: 8px;
  right: 8px;
  color: #9e9e9e;
  position: absolute;
}
.jss13733 {
  margin: 0;
  padding: 16px;
}
.jss13734 {
  margin: 0;
  padding: 8px;
  border-top: 1px solid rgba(0, 0, 0, 0.12);
}
.jss13735 {
  font-size: 13px;
}
.jss13736 {
  padding: 0;
  overflow: hidden;
}
.jss13736 span {
  display: inline-flex;
}
.jss13736 span:hover {
  position: relative;
  animation: moveTextHorz 3s linear infinite;
}
@-webkit-keyframes moveTextHorz {
  0% {
    transform: translate(0, 0);
  }
  100% {
    transform: translate(-70%, 0);
  }
}
.jss13737 span {
  color: #f44336;
}
.jss13738 {
  width: 50px;
}
@media (min-width:600px) {
  .jss13738 {
    right: 90px;
  }
}
@media (min-width:960px) {
  .jss13738 {
    right: 15px;
  }
}
.jss13739 {
  cursor: pointer;
}
.jss13739:hover {
  background: #0288d1;
}
.jss13740 {
  margin-left: 10px;
  vertical-align: middle;
}
.jss13741 {
  width: 100%;
}
.jss13742 {
  width: 100%;
  margin: 0;
}
@media (min-width:960px) {
  .jss13743 {
    margin-left: auto;
    margin-right: 16px;
  }
}
@media (min-width:1280px) {
  .jss13743 {
    margin-left: auto;
    margin-right: 32px;
  }
}
@media (min-width:960px) {
  .jss13744 {
    transform: translateY(75%);
    margin-left: 16px;
  }
}
@media (min-width:1280px) {
  .jss13744 {
    transform: translateY(75%);
    margin-left: 32px;
  }
}
.jss13745 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
@media (max-width:1279.95px) {
  .jss13746 {
    display: none;
  }
}
@media (max-width:1919.95px) {
  .jss13747 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss13748 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss13749 {
    text-align: right;
  }
}
.jss13750 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss13750 {
    text-align: right;
  }
}
.jss13751 {
  padding: 0;
}
@media (min-width:1280px) {
  .jss13752 {
    margin-top: 0 !important;
  }
}
@media (min-width:1280px) {
  .jss13753 {
    margin-top: 64px;
  }
}
.jss13754 div textarea {
  overflow: auto;
}
.jss13755 {
  width: 70%;
}
@media (min-width:1280px) {
  .jss13755 {
    float: right;
  }
}
@media (min-width:1280px) {
  .jss13756 {
    float: right;
  }
}
@media (max-width:1279.95px) {
  .jss13756 {
    margin-top: 6px;
  }
}
@media (min-width:1280px) {
  .jss13757 {
    padding: 12px 0;
  }
}
@media (max-width:1279.95px) {
  .jss13757 {
    padding: 6px 0 3px;
  }
}
.jss13758 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss13758 {
    margin-top: 0 !important;
  }
}
.jss13758 div textarea {
  overflow: auto;
}
.jss13759 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss13760 {
  width: 57px;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
  overflow-x: hidden;
}
@media (min-width:600px) {
  .jss13760 {
    width: 73px;
  }
}
@media (min-width:960px) {
  .jss13761 {
    width: calc(100% - 73px);
    margin-left: 73px;
    margin-right: 0;
  }
}
@media (min-width:960px) {
  .jss13762 {
    margin-left: 73px;
    margin-right: 0;
  }
}
.jss13763 {
  bottom: 0px;
  z-index: 1;
  position: sticky;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss13763 {
    display: none;
  }
}
.jss13763:hover {
  background-color: rgb(235, 235, 235);
}
@media (max-width:959.95px) {
  .jss13764 {
    display: none;
  }
}
.jss13765 {
  top: 0;
  left: auto;
  width: 100%;
  right: 0;
  height: 100%;
  z-index: 1301;
  overflow: hidden;
  position: absolute;
  overflow-y: auto;
  transition: transform 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
@media (min-width:960px) {
  .jss13765 {
    width: 24vw;
  }
}
.jss13766 {
  cursor: pointer;
}
.jss13766:hover {
  background-color: #eeeeee;
}
.jss13767 {
  background-color: #e0e0e0 !important;
}
.jss13768 {
  transition: width 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
.jss13769 {
  border-collapse: separate;
}
.jss13770:hover {
  background-color: rgb(235, 235, 235);
}
.jss13771 {
  position: relative;
  min-height: 80vh;
}
.jss13771 .back-icon {
  top: 5px;
  left: 5px;
  z-index: 1;
  position: absolute;
}
.jss13771 > .ninebox-inner {
  width: 100%;
  position: relative;
  min-height: 80vh;
}
.jss13771 > .ninebox-inner .category-paper {
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #424242;
}
.jss13771 > .ninebox-inner .percentage-wrapper {
  width: 100%;
  height: 100%;
  display: flex;
  flex-wrap: wrap;
  box-sizing: border-box;
}
.jss13771 > .ninebox-inner .horizontal-category {
  height: 50px;
  margin: 0 60px;
  display: flex;
  align-items: center;
  justify-content: center;
}
.jss13771 > .ninebox-inner .vertical-category {
  width: 50px;
  height: calc(80vh - 120px);
  display: inline-flex;
  align-items: center;
  justify-content: center;
}
.jss13771 > .ninebox-inner .ninebox-content {
  width: calc(100% - 100px);
  height: calc(80vh - 120px);
  display: inline-block;
  position: relative;
}
.jss13771 > .ninebox-inner .ninebox-content .wrapper {
  width: 100%;
  height: 100%;
  margin: 0;
}
.jss13771 > .ninebox-inner .ninebox-content .wrapper .box {
  height: calc(100% / 3);
  padding: 10px;
}
.jss13771 > .ninebox-inner .ninebox-content .wrapper .box .has-item {
  padding: 26px 10px !important;
}
.jss13771 > .ninebox-inner .ninebox-content .wrapper .box .disabled {
  background: #d1d1d1;
}
.jss13771 > .ninebox-inner .ninebox-content .wrapper .box .card {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 30px;
  position: relative;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  -webkit-transition: all .4s ease-out;
}
.jss13771 > .ninebox-inner .ninebox-content .wrapper .box .card h6 {
  color: rgba(0, 0, 0, 0.87);
}
.jss13771 > .ninebox-inner .ninebox-content .wrapper .box .card .total-person {
  top: 5px;
  right: 5px;
  width: 15px;
  height: 15px;
  position: absolute;
}
.jss13771 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list {
  width: 100%;
  height: 100%;
  display: flex;
  overflow-y: auto;
  align-items: center;
}
.jss13771 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list > ol {
  height: 100%;
  margin: 0;
  padding: 0;
  text-align: left;
  list-style: none;
}
.jss13771 > .ninebox-inner .ninebox-content .wrapper .box .active:hover {
  cursor: pointer;
  transform: scale(1.1);
}
.jss13771 > .ninebox-inner .vertical-category .text-rotate {
  transform: rotate(270deg);
}
.jss13771 > .ninebox-inner .vertical-category.left {
  padding: 10px 0;
  margin-right: 9px;
}
.jss13771 > .ninebox-inner .vertical-category.right {
  margin-left: 9px;
}
.jss13771 > .ninebox-inner .vertical-category .text-rotate.nowrap {
  white-space: nowrap;
}
.jss13771 > .ninebox-inner .horizontal-category.top {
  padding: 0 9px;
  margin-bottom: 9px;
}
.jss13771 > .ninebox-inner .horizontal-category.bottom {
  margin-top: 9px;
}
.jss13771 > .ninebox-inner .percentage-wrapper > .percentage-item {
  padding: 0 10px;
  flex-grow: 0;
  max-width: calc(100% / 3);
  flex-basis: calc(100% / 3);
}
.jss13771 > .ninebox-inner .percentage-wrapper > .percentage-item.horizontal {
  height: calc(100% / 3);
  padding: 10px 0;
  max-width: 100%;
}
.jss13771 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 10px;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  background-color: #424242;
  -webkit-transition: all .4s ease-out;
}
.jss13771 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper > p {
  color: #fff;
}
.jss13771 > .ninebox-inner .category-paper > p {
  color: #fff;
}
.jss13772 {
  max-height: 45vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss13772 .table {
  border-collapse: separate;
}
.jss13772 .table .sticky-head {
  top: 0;
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss13773 {
  height: 44vh;
  overflow: auto;
}
.jss13774 {
  width: 100%;
  height: 50vh;
  overflow: auto;
}
.jss13775 {
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss13776 {
  width: 100%;
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss13777 {
  max-width: 82%;
}

@media (min-width:960px) {
  ::-webkit-scrollbar {
    width: 5px;
    height: 5px;
  }
  ::-webkit-scrollbar-track {
    background: rgba(250, 250, 250, 0);
  }
  ::-webkit-scrollbar-thumb {
    background: #eeeeee;
  }
  ::-webkit-scrollbar-thumb:hover {
    background: #e0e0e0;
  }
}
.jss14029 {
  width: 100%;
  z-index: 1;
  overflow: hidden;
  margin-bottom: 0;
}
.jss14030 {
  background-color: #424242;
}
.jss14031 {
  height: 70px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/etg.9362c8fb.svg&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss14032 {
  height: 150px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/tessa.55f0b81f.svg&quot;);
  background-repeat: no-repeat;
}
.jss14033 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2hJREFUeNrsWtGR2jAQ1TEU4FQQUsGRCuBm+IerAKgguAKgAnMVHFcBvn/PnFwBvg6cCuIOEslacRvFyJJswL74zQgDNrKedvftSoaQDh06dHDAnfGVfjRjr0PWRg3kkbD2RIJJakfMjzbsdd0CQz0yciF/0zf8wRyOKbSmgXuSx1rAmhWxgbSdnJFG4cOj5DhJz7KLrC3i0a8wSwM8QzcRjGCS1U+MkLcbE9uztjQn5kcyEFMsn5q4uxW09+8hQh5rR/buCNY4OtxsBzMpEaLZ/cIm6g6uyZTrtih+d6C8/PxDLuEiT1mhp7jWEH32HIi95sr5kRKe4PiTtRWbuN/5UfS9hXOUEd6gSXgBa/gwpoMyLiMh6ym5gJxmSsyuCxaK71OUAyka0AC+i8H9YzjnofEsNfd5NyWWnCwVTGgF35/CoPbK9ykMXt5nDEQpWIaC1aZwnlvqnrXvLq7YV2Z2CDesijUMCLv0FNxQYgQWkWIl3fOAfrcq+N46xuJTh8I1XOCBi4nyRqjqEMUZVSyWIQvNlZAI4fr4TIyNTIlh93O12gHJ8AxUluMHkI2V+xwhJmVsviniEAOBtbsr8izuRykM7L7G4lRacl2SizxFiRd1llQUOjSx2BYN9lpVvyzjMkgtxsRiIDbIa0Fd5SFyz6apRXCvwGJV46yBxISFUhPVaeOyJQE/HpcsW8aO2wV8sZqgfhZI6m2w1IVKEbE4l2oRZ55mzTN2dNeZUknMHftZ6GK8iJgaZ6FmZ8il9KIFE+mC0M4VuZv4Ec7u4RlVDMs6N4JQ14urojqrrVXGc8TiU+XA4+yTqCJRgnt8RhU9CGAb4mLljAVJbArZlk9pHgYOmznUoIpeEffd4R16/+zo8p7Sj4EriplIkDxfEtklfqfbfkugOh+cVTM/spd7dXUeTB4h2duRwkneklhc6vvVthDq78dAFYnLPkMb5J6AqbO2Eivb4k40cr8i4rGNS/G6R/08O66WH3QuXPa0JS6RW9dVsO6z7baDk8WoJle51om7f5Yxbill706Mm/qjIC6KweoCU1c/lq7YWnU0IUY/K7H3GgTj6ih/oiKq+F+oqk5IyZOOK+MriI98WPnNjJggF5C/Hyg0Faf/edj8M4cn6mlZ/rgRuBe9XqLm7NChw3+GPwIMAA139Xg+K8vEAAAAAElFTkSuQmCC&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss14034 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABANJREFUeNrsWt1x4jAQNpl7P18FcSqIqSBmhvdABYEKiCvwUQFcBfZVEHhnBl8F8VUQSvB1cJJ3jddC/pOlQBg0YwYUIfbT/n27imXdxtcag947+DuPvXqa5TpYq3F0PmD+bsZeQ0OHvmbg/HMB+2CvDntS9iSaADn48PGDgUtVNvmmQQg+fjEBfmqBBaa9x08ue2KVbe6uNXhcOrDDdQJbjQ/XqjHrBuxLAYMIeVZg9xrhuJegsTx5ztjphuyxe2qI58LVJURFSnk4vfpgwr0qmRywmECgVMrAdJBgB095QmaTDPRqHBv7rnFgZUcPCc3igzP0pfTkwewW7LGJWfO168soW9oJXHDJ6gPwVQnv5wCrNrEDgnSFuXlfszPB7utYfw7EFioBOmLy9wuroE8Lz6ACRBPZXfatmvUD83cu+o0r5DiukT+SIpSve0RTtU2Y5kCTlkLl05dreX7unkeIiTkfS+VKGhJ7QDQYsb3mnw+srCludiMmSNKwPq5lE2DSewLOV81rA0VQtC/RFlSIax9q89UpuJGKz6lyRepT9aBg5BzQFnKbrGpOsj0Lgq3U3rtTNEGH+FTSImLSwPDSoiWQZHvn+Q9+07jGgmP0axcoRCAeMpMmcGtStgRmgfm7iUV7ie3GRKjdrEZzpFG20Nqki6hdKdWTQFxFfuhIErFDhHzBuQVbn0i0JAaJDfGxJ/xsBJh35Hh5ZANA7y0430YAu5dYBKxbjacINGVzG9SwZ9LHXFIM0tEEKs9fG8EkZUPc669KL0Q13P8j5gP8riwwfz/FsM3TwYisfTjOFz5Eq+epZM5guIewLf8x4HUjMm8ffUP0G25eMOcJ0S7CQ0grGkaiDNo0lta2yIrEGhFwe6kwwDEDsu8844XNFXSqH1ibjhFoYy6YmFNRiFLmErUIWJ16+V2jYl4RPzas+14KHNBvnGWmCma47RDl7rtqSyV4xCenWJ+UEzTbd+yBcNN8s8qXeU0Uy1MJIl2BbY/+U8UEytzQxnzlCKD3UlOr32trEhjN/ItGnyiESjElrCW5yq3hjouK39YMDKJWRMis7LSfJeY7zAIE/BfAVOIvXoW2ZqSaPpjUWM75ilrp9CLCJVrys+RMhVqNN5ikNw3MJZSQYYPAQMglMTWxEMzD/bCyrIe0MM3WwNpIkudcUvMduorZp+fxRqJfr8ZLTYOIp4ehyjZ9OsFz1Bj4AgSAqXL/HUw6FFLFSFU49fsxAED5IQ8Aqvdjs+y7IqgelxS6OsE8+b4KDCXKck9Vhwki6jOCoeF+jX7V6+ZF9/1YUJFwExJJ7YraKkZAsQ5xTNyPeUiTJla7mxSu2d+6r5LM3I+VQboSgCkhxLdxG2z8F2AAPtd0UKyJkPIAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss14035 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAzRJREFUeNrsWs2Nm0AUnl1RAFIKCK4gdLBwy9GuYL0VePeWm+1bbt5UYLsC7FtueCswqWDdQCTSQd5bfRM9IcADDD+OeNIIbI8f8837f4NSI4000kg16K7uHz99++nTZUkj/f3965PNRRHvFV2+0FgT76QOj/sGz2dQUxpzWkhgEVQgeC/r8mkCzO1Ao9w+gEnyb8bGSCU8qEPRri3Ebycab5bW9JnVG/cXGvuCeSmNA9ngxRgYgeIFv3ekbk2IwU0IXGqqiv4NgNI2mGsGjsGfi9ThkYZXoIoPDRbrCb4siaTOxhsBI1GvctT1QSzgLW9OA3cf42NCfMOcOfx70JZXlDt5atGGapHT4KFrGn+QeVgDxrxIIi9QtV3nwOCJVm2IiXi/NuXhVNB9T8SXV+1iYROB6pY8mxKbi9yNPeKEQHEAj4YYB6oAk+qxg8ocCFzYg8Qer0nNaWpTcBynLlFlQk2rSfDg6L7DXQ5s1m2DAIZqm7OFGPf/jcS2Bfe3Cwz9C1+kRz6+u11gCOo69v1AGvZRpOK32wMGRxGJLH2FWJggD4zadCaOZelMUYsFol5iIKGOhQjoMdSTnckFc44cD4tK/V4kRouL0ErYZPokLKFQlu64D0UmozeEnco78doMSWJuJvX6hUZLWpLFvBCINUBxc/TZNMHt0saehNcLykDlADyIXDMFr2EAg13MRD+iSgd3I8LBxGRDOvWKSIY1uGcTd44sRNd4M1ugrLt7LmNEOT83LD8UvKHVCqGNOHbE1aQFp/PG/WDjmKAq6qSBGcUuqHfQF7CiLOQfcHHmlapqHWeev1AGhyBtANMPdQnQVuUcbND3KXLH1HBjPjIa2hCOfRwOzn3YmCsAzjOpVSLmLMUmXPOg7Fi4UN1C2qe+VZE95BHeMiuBhbAX91og1zkmXc992ZjOAXdFCS2AHhDHpqqk4yuObmfIMWMTG7srYRaLGBP22ZgRUtLrkGd3YV4MdNRACa49AhgtqRj3yTW3bwLM66KUL/GwZwTwNUqb2JaNearBawkWKPt8oy5XkbtPVIOzqQ6p6MSz9K0BX5T6QyQGtK/75s5II400Ui79FWAANjYkpEJzM+AAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss14036 {
  color: #FFF;
  padding: 48px;
}
.jss14037 {
  color: rgba(0, 0, 0, 0.87);
  width: 100%;
  display: flex;
  z-index: 2;
  opacity: 0.5;
  position: fixed;
  align-items: center;
  padding-left: 16px;
  padding-right: 16px;
  background-color: #424242;
}
.jss14038 {
  height: 100vh;
  display: flex;
  position: relative;
  max-height: 1600px;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: linear-gradient(rgba(0,130,170,0), #424242), url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/1.20e79364.jpg&quot;);
}
.jss14039 {
  margin: -200px auto 0 auto;
  padding: 64px 0 48px;
  max-width: 1200px;
}
.jss14040 {
  width: 50%;
  color: #FFF;
}
@media (max-width:1279.95px) {
  .jss14040 {
    width: 100%;
    padding: 24px;
  }
}
.jss14041 {
  margin: -150px auto 0 auto;
  padding: 16px;
  position: relative;
  max-width: 1200px;
  background: #fafafa;
  justify-content: center;
}
.jss14042 {
  margin: 0 auto;
  padding: 24px;
  display: flex;
}
.jss14043 {
  margin-bottom: 24px;
}
.jss14044 {
  text-decoration: none;
}
.jss14045 {
  margin: 24px auto;
  max-width: 1200px;
}
@media (max-width:1279.95px) {
  .jss14045 {
    max-width: 1366px;
  }
}
.jss14046 {
  height: 160px;
  display: flex;
  align-items: center;
}
.jss14047 {
  margin: auto;
  display: grid;
}
.jss14048 {
  height: 100vh;
  display: flex;
  position: relative;
  overflow: hidden;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/2.adfa400c.jpg&quot;);
  background-position: center;
}
.jss14049 {
  color: #FFF;
  background-color: #03a9f4;
}
.jss14050 {
  color: #03a9f4;
  background-color: #fff;
}
.jss14051 {
  color: #03a9f4;
  padding: 0;
}
.jss14052 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss14052 {
    width: calc(100%);
  }
}
.jss14053 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss14053 {
    width: calc(100% - 300px);
  }
}
.jss14054 {
  display: none;
}
@media (min-width:600px) {
  .jss14054 {
    display: block;
  }
}
.jss14055 {
  color: #fff;
  position: relative;
  background: #03a9f4;
}
.jss14056 {
  width: 100%;
  position: relative;
  border-radius: 14px;
  background-color: rgba(255, 255, 255, 0.5);
}
.jss14056:hover {
  background-color: rgba(255, 255, 255, 0.7);
}
@media (min-width:600px) {
  .jss14056 {
    width: auto;
  }
}
.jss14057 {
  width: 72px;
  color: #212121;
  height: 100%;
  display: flex;
  position: absolute;
  align-items: center;
  pointer-events: none;
  justify-content: center;
}
.jss14058 {
  color: #fff;
  width: 100%;
}
.jss14059 {
  color: #212121;
  width: 100%;
  transition: width 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  padding-top: 8px;
  padding-left: 64px;
  padding-right: 8px;
  padding-bottom: 8px;
}
@media (min-width:600px) {
  .jss14059 {
    width: 80px;
  }
  .jss14059:focus {
    width: 200px;
  }
}
.jss14060 {
  color: #212121;
}
.jss14061 {
  background-color: #4fc3f7;
}
.jss14062 {
  margin-left: 12px;
  margin-right: 36px;
}
.jss14063 {
  display: none;
}
@media (min-width:960px) {
  .jss14064 {
    display: none;
  }
}
.jss14065 {
  width: 300px;
}
.jss14066 {
  display: flex;
  padding: 0 8px;
  min-height: 56px;
  align-items: center;
}
@media (min-width:0px) and (orientation: landscape) {
  .jss14066 {
    min-height: 48px;
  }
}
@media (min-width:600px) {
  .jss14066 {
    min-height: 64px;
  }
}
.jss14067 {
  margin-top: 65px;
  justify-content: flex-start;
}
.jss14068 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss14070 {
  width: 100%;
  height: 100%;
  opacity: 0.2;
  content: &quot;&quot;;
  display: block;
  position: absolute;
  background: linear-gradient(rgba(0,130,170,0), #03a9f4));
  background-size: cover;
  background-position: center center;
}
.jss14071 {
  width: 60px;
  overflow-x: hidden;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss14072 {
  color: inherit;
}
.jss14073 {
  margin-left: 56px;
}
.jss14074 {
  bottom: 16px;
}
.jss14075 {
  color: rgba(0, 0, 0, 0.87);
  background-size: cover;
  background-color: #fff;
  background-image: url(https://xdev.equine.co.id/apps/tessa/static/media/pattern2.f069796d.svg);
  background-repeat: no-repeat;
  background-position: inherit;
}
.jss14076 {
  color: #fff;
  background-color: #03a9f4;
}
.jss14077 {
  color: #fff;
  background-color: #ff9800;
}
.jss14078 {
  color: #fff;
  background-color: #f44336;
}
.jss14079 {
  color: #fff;
  background: #4caf50;
}
.jss14080 {
  flex: 1;
}
.jss14081 {
  padding: 16px;
  flex-grow: 1;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss14081 {
    margin-top: 68px;
  }
}
@media (max-width:959.95px) {
  .jss14081 {
    padding: 0;
    margin-top: 56px;
  }
}
@media (max-width:959.95px) {
  .jss14082 {
    margin-top: 68px;
  }
}
@media (min-width:960px) {
  .jss14082 {
    left: 50%;
    width: 982px;
    position: absolute;
    transform: translateX(-50%);
  }
}
@media (min-width:960px) {
  .jss14083 {
    margin-left: 300px;
    margin-right: 0;
  }
}
.jss14084 {
  margin-bottom: 56px;
}
.jss14085 {
  width: calc(100% - 0px);
  right: 0;
  bottom: 0;
  display: flex;
  position: fixed;
}
@media (min-width:960px) {
  .jss14085 {
    width: calc(100% - 300px);
  }
}
.jss14087 {
  background: #fff;
}
.jss14088 {
  margin-bottom: 60px;
}
.jss14089 {
  height: 4px;
}
.jss14090 {
  display: flex;
  align-items: center;
  justify-content: space-between;
}
.jss14091 {
  margin: 0 15px 0 0;
  padding: 16px 0;
  flex-grow: 1;
}
@media (min-width:600px) {
  .jss14091 {
    width: 30%;
    flex-grow: 0;
  }
}
.jss14092 {
  color: inherit;
  font-size: 0.6964285714285714rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: inherit;
  line-height: 1.66;
  letter-spacing: 0.03333em;
}
.jss14093 {
  color: #fff;
  background: #03a9f4;
}
.jss14094 {
  color: #fff;
  min-height: 40px;
  background: #ff9800;
  margin-bottom: 16px;
}
.jss14095 {
  height: 500px;
  display: flex;
  overflow: hidden;
  align-items: center;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss14095 {
    height: 300px;
  }
}
.jss14096 {
  height: 100%;
  margin: 0 auto;
}
.jss14097 {
  width: 25px;
  height: 25px;
  margin-right: 16px;
}
.jss14098 {
  transform: rotate(0deg);
  transition: transform 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
}
.jss14099 {
  transform: rotate(180deg);
}
.jss14100 {
  min-width: 350px;
}
.jss14101 {
  display: flex;
  flex-wrap: wrap;
}
.jss14102 {
  padding: 0;
}
@media (min-width:0px) {
  .jss14102 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss14102 {
    width: calc(100% / 2);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1280px) {
  .jss14102 {
    width: calc(100% / 3);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1920px) {
  .jss14102 {
    width: calc(100% / 4);
    padding: 0 16px 16px 0;
  }
}
.jss14103 {
  padding: 0;
}
@media (min-width:0px) {
  .jss14103 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss14103 {
    width: calc(100% / 2);
  }
}
@media (min-width:1280px) {
  .jss14103 {
    width: calc(100% - (100% / 3));
  }
}
@media (min-width:1920px) {
  .jss14103 {
    width: calc(100% - (100% / 4));
  }
}
.jss14104 {
  padding: 0;
}
@media (min-width:0px) {
  .jss14104 {
    width: 100%;
  }
}
.jss14105 {
  width: 100%;
  margin-bottom: 16px;
  vertical-align: middle;
}
.jss14106 {
  right: 16px;
  display: flex;
  position: absolute;
}
.jss14107 {
  right: 16px;
  bottom: 16px;
  position: fixed;
}
.jss14108 {
  float: right;
}
.jss14109 {
  margin: 0;
}
.jss14110 {
  margin: 8px;
}
.jss14111 {
  margin-left: 8px;
}
.jss14112 {
  margin-top: 8px;
}
.jss14113 {
  margin-right: 8px;
}
.jss14114 {
  margin-bottom: 8px;
}
.jss14115 {
  margin: 16px;
}
.jss14116 {
  margin-left: 16px;
}
.jss14117 {
  margin-top: 16px;
}
.jss14118 {
  margin-right: 16px;
}
.jss14119 {
  margin-bottom: 16px;
}
.jss14120 {
  margin: 24px;
}
.jss14121 {
  margin-left: 24px;
}
.jss14122 {
  margin-top: 24px;
}
.jss14123 {
  margin-right: 24px;
}
.jss14124 {
  margin-bottom: 24px;
}
.jss14125 {
  padding: 0;
}
.jss14126 {
  padding: 8px;
}
.jss14127 {
  padding-left: 8px;
}
.jss14128 {
  padding-top: 8px;
}
.jss14129 {
  padding-right: 8px;
}
.jss14130 {
  padding-bottom: 8px;
}
.jss14131 {
  padding: 16px;
}
.jss14132 {
  padding-left: 16px;
}
.jss14133 {
  padding-top: 16px;
}
.jss14134 {
  padding-right: 16px;
}
.jss14135 {
  padding-bottom: 16px;
}
.jss14136 {
  padding: 24px;
}
.jss14137 {
  padding-left: 24px;
}
.jss14138 {
  padding-top: 24px;
}
.jss14139 {
  padding-right: 24px;
}
.jss14140 {
  padding-bottom: 24px;
}
.jss14141 {
  background-color: #03a9f4;
}
.jss14142 {
  background-color: #ff9800;
}
.jss14143 {
  color: white;
  background-color: #f44336;
}
.jss14144 {
  color: #03a9f4;
}
.jss14145 {
  color: #ff9800;
}
.jss14146 {
  color: #f44336;
}
.jss14147 {
  text-decoration: line-through;
}
.jss14148 {
  padding: 3px;
}
.jss14149 {
  height: calc(100vh - 96px - 72px);
  overflow-x: auto;
}
.jss14150 {
  width: calc(100vw - 32px - 300px);
}
.jss14151 {
  height: calc(100vh - 56px - 68px);
  overflow-x: auto;
}
.jss14152 {
  width: calc(100vw);
}
.jss14153 {
  overflow-x: auto;
}
.jss14154 {
  table-layout: initial;
}
.jss14155 {
  top: 0;
  position: sticky;
  background-color: #fff;
}
.jss14156 {
  width: 3vw;
}
.jss14157 {
  width: 5vw;
}
.jss14158 {
  width: 10vw;
}
.jss14159 {
  width: 15vw;
}
.jss14160 {
  min-width: 15vw;
  max-width: 15vw;
}
@media (min-width:960px) {
  .jss14160 {
    min-width: calc((100vw - 300px - 48px) * 0.15);
    max-width: calc((100vw - 300px - 48px) * 0.15);
  }
}
.jss14161 {
  min-width: 25vw;
  max-width: 25vw;
}
@media (min-width:960px) {
  .jss14161 {
    min-width: calc((100vw - 300px - 48px) * 0.25);
    max-width: calc((100vw - 300px - 48px) * 0.25);
  }
}
.jss14162 {
  width: 25vw;
}
.jss14163 {
  width: 30vw;
}
.jss14164 {
  min-width: 45vw;
  max-width: 45vw;
}
@media (min-width:960px) {
  .jss14164 {
    min-width: calc((100vw - 300px - 48px) * 0.45);
    max-width: calc((100vw - 300px - 48px) * 0.45);
  }
}
.jss14165 {
  width: 100%;
  overflow-x: auto;
}
.jss14166 {
  width: 100%;
  margin-top: 24px;
  overflow-x: auto;
}
.jss14167 {
  min-width: 700px;
}
.jss14168 {
  width: 100%;
}
.jss14169 {
  min-width: 100px;
}
.jss14170 {
  color: rgba(0, 0, 0, 0.54);
  flex-shrink: 0;
}
.jss14171 {
  color: #2196f3;
}
.jss14172 {
  padding-top: 16px;
  padding-bottom: 16px;
}
.jss14174 {
  max-height: 100px;
}
.jss14175 {
  margin-top: -144px;
  margin-left: -56px;
  margin-right: -136px;
  margin-bottom: -64px;
}
.jss14176 {
  margin-top: -80px;
}
.jss14177 {
  width: 500px;
  height: 450px;
}
.jss14178 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss14179 {
  display: flex;
  box-sizing: border-box;
  align-items: center;
}
.jss14181:hover {
  background-color: #eeeeee;
}
.jss14182 {
  flex: 1;
}
.jss14183 {
  cursor: initial;
}
.jss14184 {
  background: #f44336;
}
.jss14185 {
  color: #fff;
}
.jss14186 {
  width: 100%;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
.jss14187 {
  max-height: 75vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss14188 {
  padding: 10px;
  text-align: center;
}
.jss14189 {
  padding: 0;
  vertical-align: top;
}
.jss14190 {
  margin-top: 8px;
  padding-left: 32px;
}
.jss14191 {
  top: 0;
  padding: 5px;
  position: sticky;
  overflow: hidden;
  max-width: 100px;
  max-height: 10px;
  text-align: center;
  background-color: #fff;
}
.jss14192 {
  max-height: 76vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss14193 {
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss14194 {
  padding: 0;
  margin-top: 8px;
}
.jss14195 {
  left: 0;
  width: 500px;
  z-index: 1;
  padding: 8px 16px 0 16px;
  min-width: 300px;
  background: #fff;
  vertical-align: top;
}
.jss14196 {
  padding: 8px 16px 0 16px;
  min-width: 300px;
  vertical-align: top;
}
.jss14197 {
  min-width: 300px;
}
.jss14198 {
  transform: rotate(180deg);
  writing-mode: vertical-rl;
}
.jss14199 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss14200 {
  cursor: pointer;
  transition: all .3s;
}
.jss14200:hover {
  background-color: #ebebeb;
}
.jss14201 {
  background-color: #ebebeb;
}
.jss14202 {
  transition: all .3s;
}
.jss14202:hover {
  transform: scale(1.03);
}
.jss14203 {
  padding: 0;
  transition: all .3s;
}
.jss14203:hover {
  transform: scale(1.03);
}
.jss14204 {
  display: grid;
  align-items: center;
  grid-column-gap: 1.5rem;
  grid-template-columns: 1fr max-content 1fr;
}
.jss14204::after, .jss14204::before {
  height: 1px;
  content: '';
  display: block;
  background-color: currentColor;
}
.jss14205 a {
  text-decoration: none;
}
.jss14206 {
  left: 50%;
  position: absolute;
  transform: ;
}
.jss14207 {
  display: -webkit-box;
  overflow: hidden;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;
}
.jss14208 {
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
.jss14209 {
  display: inline;
  text-transform: capitalize;
}
.jss14210 {
  text-decoration: none;
}
.jss14210 p:hover {
  color: #03a9f4;
}
.jss14211 {
  margin-left: 4px;
}
.jss14212 {
  padding: 0 16px;
  display: inline-flex;
  position: relative;
  vertical-align: middle;
}
.jss14213 {
  top: 0;
  color: #fff;
  right: 0;
  height: 20px;
  display: flex;
  padding: 0 4px;
  z-index: 1;
  position: absolute;
  flex-wrap: wrap;
  min-width: 20px;
  transform: translate(20px, -50%);
  box-sizing: border-box;
  transition: transform 225ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  align-items: center;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  align-content: center;
  border-radius: 10px;
  flex-direction: row;
  justify-content: center;
  background-color: #ff9800;
  transform-origin: 100% 0%;
}
.jss14214 {
  margin: 0;
  padding: 16px;
  border-bottom: 1px solid rgba(0, 0, 0, 0.12);
}
.jss14215 {
  top: 8px;
  right: 8px;
  color: #9e9e9e;
  position: absolute;
}
.jss14216 {
  margin: 0;
  padding: 16px;
}
.jss14217 {
  margin: 0;
  padding: 8px;
  border-top: 1px solid rgba(0, 0, 0, 0.12);
}
.jss14218 {
  font-size: 13px;
}
.jss14219 {
  padding: 0;
  overflow: hidden;
}
.jss14219 span {
  display: inline-flex;
}
.jss14219 span:hover {
  position: relative;
  animation: moveTextHorz 3s linear infinite;
}
@-webkit-keyframes moveTextHorz {
  0% {
    transform: translate(0, 0);
  }
  100% {
    transform: translate(-70%, 0);
  }
}
.jss14220 span {
  color: #f44336;
}
.jss14221 {
  width: 50px;
}
@media (min-width:600px) {
  .jss14221 {
    right: 90px;
  }
}
@media (min-width:960px) {
  .jss14221 {
    right: 15px;
  }
}
.jss14222 {
  cursor: pointer;
}
.jss14222:hover {
  background: #0288d1;
}
.jss14223 {
  margin-left: 10px;
  vertical-align: middle;
}
.jss14224 {
  width: 100%;
}
.jss14225 {
  width: 100%;
  margin: 0;
}
@media (min-width:960px) {
  .jss14226 {
    margin-left: auto;
    margin-right: 16px;
  }
}
@media (min-width:1280px) {
  .jss14226 {
    margin-left: auto;
    margin-right: 32px;
  }
}
@media (min-width:960px) {
  .jss14227 {
    transform: translateY(75%);
    margin-left: 16px;
  }
}
@media (min-width:1280px) {
  .jss14227 {
    transform: translateY(75%);
    margin-left: 32px;
  }
}
.jss14228 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
@media (max-width:1279.95px) {
  .jss14229 {
    display: none;
  }
}
@media (max-width:1919.95px) {
  .jss14230 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss14231 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss14232 {
    text-align: right;
  }
}
.jss14233 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss14233 {
    text-align: right;
  }
}
.jss14234 {
  padding: 0;
}
@media (min-width:1280px) {
  .jss14235 {
    margin-top: 0 !important;
  }
}
@media (min-width:1280px) {
  .jss14236 {
    margin-top: 64px;
  }
}
.jss14237 div textarea {
  overflow: auto;
}
.jss14238 {
  width: 70%;
}
@media (min-width:1280px) {
  .jss14238 {
    float: right;
  }
}
@media (min-width:1280px) {
  .jss14239 {
    float: right;
  }
}
@media (max-width:1279.95px) {
  .jss14239 {
    margin-top: 6px;
  }
}
@media (min-width:1280px) {
  .jss14240 {
    padding: 12px 0;
  }
}
@media (max-width:1279.95px) {
  .jss14240 {
    padding: 6px 0 3px;
  }
}
.jss14241 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss14241 {
    margin-top: 0 !important;
  }
}
.jss14241 div textarea {
  overflow: auto;
}
.jss14242 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss14243 {
  width: 57px;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
  overflow-x: hidden;
}
@media (min-width:600px) {
  .jss14243 {
    width: 73px;
  }
}
@media (min-width:960px) {
  .jss14244 {
    width: calc(100% - 73px);
    margin-left: 73px;
    margin-right: 0;
  }
}
@media (min-width:960px) {
  .jss14245 {
    margin-left: 73px;
    margin-right: 0;
  }
}
.jss14246 {
  bottom: 0px;
  z-index: 1;
  position: sticky;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss14246 {
    display: none;
  }
}
.jss14246:hover {
  background-color: rgb(235, 235, 235);
}
@media (max-width:959.95px) {
  .jss14247 {
    display: none;
  }
}
.jss14248 {
  top: 0;
  left: auto;
  width: 100%;
  right: 0;
  height: 100%;
  z-index: 1301;
  overflow: hidden;
  position: absolute;
  overflow-y: auto;
  transition: transform 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
@media (min-width:960px) {
  .jss14248 {
    width: 24vw;
  }
}
.jss14249 {
  cursor: pointer;
}
.jss14249:hover {
  background-color: #eeeeee;
}
.jss14250 {
  background-color: #e0e0e0 !important;
}
.jss14251 {
  transition: width 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
.jss14252 {
  border-collapse: separate;
}
.jss14253:hover {
  background-color: rgb(235, 235, 235);
}
.jss14254 {
  position: relative;
  min-height: 80vh;
}
.jss14254 .back-icon {
  top: 5px;
  left: 5px;
  z-index: 1;
  position: absolute;
}
.jss14254 > .ninebox-inner {
  width: 100%;
  position: relative;
  min-height: 80vh;
}
.jss14254 > .ninebox-inner .category-paper {
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #424242;
}
.jss14254 > .ninebox-inner .percentage-wrapper {
  width: 100%;
  height: 100%;
  display: flex;
  flex-wrap: wrap;
  box-sizing: border-box;
}
.jss14254 > .ninebox-inner .horizontal-category {
  height: 50px;
  margin: 0 60px;
  display: flex;
  align-items: center;
  justify-content: center;
}
.jss14254 > .ninebox-inner .vertical-category {
  width: 50px;
  height: calc(80vh - 120px);
  display: inline-flex;
  align-items: center;
  justify-content: center;
}
.jss14254 > .ninebox-inner .ninebox-content {
  width: calc(100% - 100px);
  height: calc(80vh - 120px);
  display: inline-block;
  position: relative;
}
.jss14254 > .ninebox-inner .ninebox-content .wrapper {
  width: 100%;
  height: 100%;
  margin: 0;
}
.jss14254 > .ninebox-inner .ninebox-content .wrapper .box {
  height: calc(100% / 3);
  padding: 10px;
}
.jss14254 > .ninebox-inner .ninebox-content .wrapper .box .has-item {
  padding: 26px 10px !important;
}
.jss14254 > .ninebox-inner .ninebox-content .wrapper .box .disabled {
  background: #d1d1d1;
}
.jss14254 > .ninebox-inner .ninebox-content .wrapper .box .card {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 30px;
  position: relative;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  -webkit-transition: all .4s ease-out;
}
.jss14254 > .ninebox-inner .ninebox-content .wrapper .box .card h6 {
  color: rgba(0, 0, 0, 0.87);
}
.jss14254 > .ninebox-inner .ninebox-content .wrapper .box .card .total-person {
  top: 5px;
  right: 5px;
  width: 15px;
  height: 15px;
  position: absolute;
}
.jss14254 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list {
  width: 100%;
  height: 100%;
  display: flex;
  overflow-y: auto;
  align-items: center;
}
.jss14254 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list > ol {
  height: 100%;
  margin: 0;
  padding: 0;
  text-align: left;
  list-style: none;
}
.jss14254 > .ninebox-inner .ninebox-content .wrapper .box .active:hover {
  cursor: pointer;
  transform: scale(1.1);
}
.jss14254 > .ninebox-inner .vertical-category .text-rotate {
  transform: rotate(270deg);
}
.jss14254 > .ninebox-inner .vertical-category.left {
  padding: 10px 0;
  margin-right: 9px;
}
.jss14254 > .ninebox-inner .vertical-category.right {
  margin-left: 9px;
}
.jss14254 > .ninebox-inner .vertical-category .text-rotate.nowrap {
  white-space: nowrap;
}
.jss14254 > .ninebox-inner .horizontal-category.top {
  padding: 0 9px;
  margin-bottom: 9px;
}
.jss14254 > .ninebox-inner .horizontal-category.bottom {
  margin-top: 9px;
}
.jss14254 > .ninebox-inner .percentage-wrapper > .percentage-item {
  padding: 0 10px;
  flex-grow: 0;
  max-width: calc(100% / 3);
  flex-basis: calc(100% / 3);
}
.jss14254 > .ninebox-inner .percentage-wrapper > .percentage-item.horizontal {
  height: calc(100% / 3);
  padding: 10px 0;
  max-width: 100%;
}
.jss14254 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 10px;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  background-color: #424242;
  -webkit-transition: all .4s ease-out;
}
.jss14254 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper > p {
  color: #fff;
}
.jss14254 > .ninebox-inner .category-paper > p {
  color: #fff;
}
.jss14255 {
  max-height: 45vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss14255 .table {
  border-collapse: separate;
}
.jss14255 .table .sticky-head {
  top: 0;
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss14256 {
  height: 44vh;
  overflow: auto;
}
.jss14257 {
  width: 100%;
  height: 50vh;
  overflow: auto;
}
.jss14258 {
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss14259 {
  width: 100%;
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss14260 {
  max-width: 82%;
}

@media (min-width:960px) {
  ::-webkit-scrollbar {
    width: 5px;
    height: 5px;
  }
  ::-webkit-scrollbar-track {
    background: rgba(250, 250, 250, 0);
  }
  ::-webkit-scrollbar-thumb {
    background: #eeeeee;
  }
  ::-webkit-scrollbar-thumb:hover {
    background: #e0e0e0;
  }
}
.jss13302 {
  width: 100%;
  z-index: 1;
  overflow: hidden;
  margin-bottom: 0;
}
.jss13303 {
  background-color: #424242;
}
.jss13304 {
  height: 70px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/etg.9362c8fb.svg&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss13305 {
  height: 150px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/tessa.55f0b81f.svg&quot;);
  background-repeat: no-repeat;
}
.jss13306 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2hJREFUeNrsWtGR2jAQ1TEU4FQQUsGRCuBm+IerAKgguAKgAnMVHFcBvn/PnFwBvg6cCuIOEslacRvFyJJswL74zQgDNrKedvftSoaQDh06dHDAnfGVfjRjr0PWRg3kkbD2RIJJakfMjzbsdd0CQz0yciF/0zf8wRyOKbSmgXuSx1rAmhWxgbSdnJFG4cOj5DhJz7KLrC3i0a8wSwM8QzcRjGCS1U+MkLcbE9uztjQn5kcyEFMsn5q4uxW09+8hQh5rR/buCNY4OtxsBzMpEaLZ/cIm6g6uyZTrtih+d6C8/PxDLuEiT1mhp7jWEH32HIi95sr5kRKe4PiTtRWbuN/5UfS9hXOUEd6gSXgBa/gwpoMyLiMh6ym5gJxmSsyuCxaK71OUAyka0AC+i8H9YzjnofEsNfd5NyWWnCwVTGgF35/CoPbK9ykMXt5nDEQpWIaC1aZwnlvqnrXvLq7YV2Z2CDesijUMCLv0FNxQYgQWkWIl3fOAfrcq+N46xuJTh8I1XOCBi4nyRqjqEMUZVSyWIQvNlZAI4fr4TIyNTIlh93O12gHJ8AxUluMHkI2V+xwhJmVsviniEAOBtbsr8izuRykM7L7G4lRacl2SizxFiRd1llQUOjSx2BYN9lpVvyzjMkgtxsRiIDbIa0Fd5SFyz6apRXCvwGJV46yBxISFUhPVaeOyJQE/HpcsW8aO2wV8sZqgfhZI6m2w1IVKEbE4l2oRZ55mzTN2dNeZUknMHftZ6GK8iJgaZ6FmZ8il9KIFE+mC0M4VuZv4Ec7u4RlVDMs6N4JQ14urojqrrVXGc8TiU+XA4+yTqCJRgnt8RhU9CGAb4mLljAVJbArZlk9pHgYOmznUoIpeEffd4R16/+zo8p7Sj4EriplIkDxfEtklfqfbfkugOh+cVTM/spd7dXUeTB4h2duRwkneklhc6vvVthDq78dAFYnLPkMb5J6AqbO2Eivb4k40cr8i4rGNS/G6R/08O66WH3QuXPa0JS6RW9dVsO6z7baDk8WoJle51om7f5Yxbill706Mm/qjIC6KweoCU1c/lq7YWnU0IUY/K7H3GgTj6ih/oiKq+F+oqk5IyZOOK+MriI98WPnNjJggF5C/Hyg0Faf/edj8M4cn6mlZ/rgRuBe9XqLm7NChw3+GPwIMAA139Xg+K8vEAAAAAElFTkSuQmCC&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss13307 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABANJREFUeNrsWt1x4jAQNpl7P18FcSqIqSBmhvdABYEKiCvwUQFcBfZVEHhnBl8F8VUQSvB1cJJ3jddC/pOlQBg0YwYUIfbT/n27imXdxtcag947+DuPvXqa5TpYq3F0PmD+bsZeQ0OHvmbg/HMB+2CvDntS9iSaADn48PGDgUtVNvmmQQg+fjEBfmqBBaa9x08ue2KVbe6uNXhcOrDDdQJbjQ/XqjHrBuxLAYMIeVZg9xrhuJegsTx5ztjphuyxe2qI58LVJURFSnk4vfpgwr0qmRywmECgVMrAdJBgB095QmaTDPRqHBv7rnFgZUcPCc3igzP0pfTkwewW7LGJWfO168soW9oJXHDJ6gPwVQnv5wCrNrEDgnSFuXlfszPB7utYfw7EFioBOmLy9wuroE8Lz6ACRBPZXfatmvUD83cu+o0r5DiukT+SIpSve0RTtU2Y5kCTlkLl05dreX7unkeIiTkfS+VKGhJ7QDQYsb3mnw+srCludiMmSNKwPq5lE2DSewLOV81rA0VQtC/RFlSIax9q89UpuJGKz6lyRepT9aBg5BzQFnKbrGpOsj0Lgq3U3rtTNEGH+FTSImLSwPDSoiWQZHvn+Q9+07jGgmP0axcoRCAeMpMmcGtStgRmgfm7iUV7ie3GRKjdrEZzpFG20Nqki6hdKdWTQFxFfuhIErFDhHzBuQVbn0i0JAaJDfGxJ/xsBJh35Hh5ZANA7y0430YAu5dYBKxbjacINGVzG9SwZ9LHXFIM0tEEKs9fG8EkZUPc669KL0Q13P8j5gP8riwwfz/FsM3TwYisfTjOFz5Eq+epZM5guIewLf8x4HUjMm8ffUP0G25eMOcJ0S7CQ0grGkaiDNo0lta2yIrEGhFwe6kwwDEDsu8844XNFXSqH1ibjhFoYy6YmFNRiFLmErUIWJ16+V2jYl4RPzas+14KHNBvnGWmCma47RDl7rtqSyV4xCenWJ+UEzTbd+yBcNN8s8qXeU0Uy1MJIl2BbY/+U8UEytzQxnzlCKD3UlOr32trEhjN/ItGnyiESjElrCW5yq3hjouK39YMDKJWRMis7LSfJeY7zAIE/BfAVOIvXoW2ZqSaPpjUWM75ilrp9CLCJVrys+RMhVqNN5ikNw3MJZSQYYPAQMglMTWxEMzD/bCyrIe0MM3WwNpIkudcUvMduorZp+fxRqJfr8ZLTYOIp4ehyjZ9OsFz1Bj4AgSAqXL/HUw6FFLFSFU49fsxAED5IQ8Aqvdjs+y7IqgelxS6OsE8+b4KDCXKck9Vhwki6jOCoeF+jX7V6+ZF9/1YUJFwExJJ7YraKkZAsQ5xTNyPeUiTJla7mxSu2d+6r5LM3I+VQboSgCkhxLdxG2z8F2AAPtd0UKyJkPIAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss13308 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAzRJREFUeNrsWs2Nm0AUnl1RAFIKCK4gdLBwy9GuYL0VePeWm+1bbt5UYLsC7FtueCswqWDdQCTSQd5bfRM9IcADDD+OeNIIbI8f8837f4NSI4000kg16K7uHz99++nTZUkj/f3965PNRRHvFV2+0FgT76QOj/sGz2dQUxpzWkhgEVQgeC/r8mkCzO1Ao9w+gEnyb8bGSCU8qEPRri3Ebycab5bW9JnVG/cXGvuCeSmNA9ngxRgYgeIFv3ekbk2IwU0IXGqqiv4NgNI2mGsGjsGfi9ThkYZXoIoPDRbrCb4siaTOxhsBI1GvctT1QSzgLW9OA3cf42NCfMOcOfx70JZXlDt5atGGapHT4KFrGn+QeVgDxrxIIi9QtV3nwOCJVm2IiXi/NuXhVNB9T8SXV+1iYROB6pY8mxKbi9yNPeKEQHEAj4YYB6oAk+qxg8ocCFzYg8Qer0nNaWpTcBynLlFlQk2rSfDg6L7DXQ5s1m2DAIZqm7OFGPf/jcS2Bfe3Cwz9C1+kRz6+u11gCOo69v1AGvZRpOK32wMGRxGJLH2FWJggD4zadCaOZelMUYsFol5iIKGOhQjoMdSTnckFc44cD4tK/V4kRouL0ErYZPokLKFQlu64D0UmozeEnco78doMSWJuJvX6hUZLWpLFvBCINUBxc/TZNMHt0saehNcLykDlADyIXDMFr2EAg13MRD+iSgd3I8LBxGRDOvWKSIY1uGcTd44sRNd4M1ugrLt7LmNEOT83LD8UvKHVCqGNOHbE1aQFp/PG/WDjmKAq6qSBGcUuqHfQF7CiLOQfcHHmlapqHWeev1AGhyBtANMPdQnQVuUcbND3KXLH1HBjPjIa2hCOfRwOzn3YmCsAzjOpVSLmLMUmXPOg7Fi4UN1C2qe+VZE95BHeMiuBhbAX91og1zkmXc992ZjOAXdFCS2AHhDHpqqk4yuObmfIMWMTG7srYRaLGBP22ZgRUtLrkGd3YV4MdNRACa49AhgtqRj3yTW3bwLM66KUL/GwZwTwNUqb2JaNearBawkWKPt8oy5XkbtPVIOzqQ6p6MSz9K0BX5T6QyQGtK/75s5II400Ui79FWAANjYkpEJzM+AAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss13309 {
  color: #FFF;
  padding: 48px;
}
.jss13310 {
  color: rgba(0, 0, 0, 0.87);
  width: 100%;
  display: flex;
  z-index: 2;
  opacity: 0.5;
  position: fixed;
  align-items: center;
  padding-left: 16px;
  padding-right: 16px;
  background-color: #424242;
}
.jss13311 {
  height: 100vh;
  display: flex;
  position: relative;
  max-height: 1600px;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: linear-gradient(rgba(0,130,170,0), #424242), url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/1.20e79364.jpg&quot;);
}
.jss13312 {
  margin: -200px auto 0 auto;
  padding: 64px 0 48px;
  max-width: 1200px;
}
.jss13313 {
  width: 50%;
  color: #FFF;
}
@media (max-width:1279.95px) {
  .jss13313 {
    width: 100%;
    padding: 24px;
  }
}
.jss13314 {
  margin: -150px auto 0 auto;
  padding: 16px;
  position: relative;
  max-width: 1200px;
  background: #fafafa;
  justify-content: center;
}
.jss13315 {
  margin: 0 auto;
  padding: 24px;
  display: flex;
}
.jss13316 {
  margin-bottom: 24px;
}
.jss13317 {
  text-decoration: none;
}
.jss13318 {
  margin: 24px auto;
  max-width: 1200px;
}
@media (max-width:1279.95px) {
  .jss13318 {
    max-width: 1366px;
  }
}
.jss13319 {
  height: 160px;
  display: flex;
  align-items: center;
}
.jss13320 {
  margin: auto;
  display: grid;
}
.jss13321 {
  height: 100vh;
  display: flex;
  position: relative;
  overflow: hidden;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/2.adfa400c.jpg&quot;);
  background-position: center;
}
.jss13322 {
  color: #FFF;
  background-color: #03a9f4;
}
.jss13323 {
  color: #03a9f4;
  background-color: #fff;
}
.jss13324 {
  color: #03a9f4;
  padding: 0;
}
.jss13325 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss13325 {
    width: calc(100%);
  }
}
.jss13326 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss13326 {
    width: calc(100% - 300px);
  }
}
.jss13327 {
  display: none;
}
@media (min-width:600px) {
  .jss13327 {
    display: block;
  }
}
.jss13328 {
  color: #fff;
  position: relative;
  background: #03a9f4;
}
.jss13329 {
  width: 100%;
  position: relative;
  border-radius: 14px;
  background-color: rgba(255, 255, 255, 0.5);
}
.jss13329:hover {
  background-color: rgba(255, 255, 255, 0.7);
}
@media (min-width:600px) {
  .jss13329 {
    width: auto;
  }
}
.jss13330 {
  width: 72px;
  color: #212121;
  height: 100%;
  display: flex;
  position: absolute;
  align-items: center;
  pointer-events: none;
  justify-content: center;
}
.jss13331 {
  color: #fff;
  width: 100%;
}
.jss13332 {
  color: #212121;
  width: 100%;
  transition: width 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  padding-top: 8px;
  padding-left: 64px;
  padding-right: 8px;
  padding-bottom: 8px;
}
@media (min-width:600px) {
  .jss13332 {
    width: 80px;
  }
  .jss13332:focus {
    width: 200px;
  }
}
.jss13333 {
  color: #212121;
}
.jss13334 {
  background-color: #4fc3f7;
}
.jss13335 {
  margin-left: 12px;
  margin-right: 36px;
}
.jss13336 {
  display: none;
}
@media (min-width:960px) {
  .jss13337 {
    display: none;
  }
}
.jss13338 {
  width: 300px;
}
.jss13339 {
  display: flex;
  padding: 0 8px;
  min-height: 56px;
  align-items: center;
}
@media (min-width:0px) and (orientation: landscape) {
  .jss13339 {
    min-height: 48px;
  }
}
@media (min-width:600px) {
  .jss13339 {
    min-height: 64px;
  }
}
.jss13340 {
  margin-top: 65px;
  justify-content: flex-start;
}
.jss13341 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss13343 {
  width: 100%;
  height: 100%;
  opacity: 0.2;
  content: &quot;&quot;;
  display: block;
  position: absolute;
  background: linear-gradient(rgba(0,130,170,0), #03a9f4));
  background-size: cover;
  background-position: center center;
}
.jss13344 {
  width: 60px;
  overflow-x: hidden;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss13345 {
  color: inherit;
}
.jss13346 {
  margin-left: 56px;
}
.jss13347 {
  bottom: 16px;
}
.jss13348 {
  color: rgba(0, 0, 0, 0.87);
  background-size: cover;
  background-color: #fff;
  background-image: url(https://xdev.equine.co.id/apps/tessa/static/media/pattern2.f069796d.svg);
  background-repeat: no-repeat;
  background-position: inherit;
}
.jss13349 {
  color: #fff;
  background-color: #03a9f4;
}
.jss13350 {
  color: #fff;
  background-color: #ff9800;
}
.jss13351 {
  color: #fff;
  background-color: #f44336;
}
.jss13352 {
  color: #fff;
  background: #4caf50;
}
.jss13353 {
  flex: 1;
}
.jss13354 {
  padding: 16px;
  flex-grow: 1;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss13354 {
    margin-top: 68px;
  }
}
@media (max-width:959.95px) {
  .jss13354 {
    padding: 0;
    margin-top: 56px;
  }
}
@media (max-width:959.95px) {
  .jss13355 {
    margin-top: 68px;
  }
}
@media (min-width:960px) {
  .jss13355 {
    left: 50%;
    width: 982px;
    position: absolute;
    transform: translateX(-50%);
  }
}
@media (min-width:960px) {
  .jss13356 {
    margin-left: 300px;
    margin-right: 0;
  }
}
.jss13357 {
  margin-bottom: 56px;
}
.jss13358 {
  width: calc(100% - 0px);
  right: 0;
  bottom: 0;
  display: flex;
  position: fixed;
}
@media (min-width:960px) {
  .jss13358 {
    width: calc(100% - 300px);
  }
}
.jss13360 {
  background: #fff;
}
.jss13361 {
  margin-bottom: 60px;
}
.jss13362 {
  height: 4px;
}
.jss13363 {
  display: flex;
  align-items: center;
  justify-content: space-between;
}
.jss13364 {
  margin: 0 15px 0 0;
  padding: 16px 0;
  flex-grow: 1;
}
@media (min-width:600px) {
  .jss13364 {
    width: 30%;
    flex-grow: 0;
  }
}
.jss13365 {
  color: inherit;
  font-size: 0.6964285714285714rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: inherit;
  line-height: 1.66;
  letter-spacing: 0.03333em;
}
.jss13366 {
  color: #fff;
  background: #03a9f4;
}
.jss13367 {
  color: #fff;
  min-height: 40px;
  background: #ff9800;
  margin-bottom: 16px;
}
.jss13368 {
  height: 500px;
  display: flex;
  overflow: hidden;
  align-items: center;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss13368 {
    height: 300px;
  }
}
.jss13369 {
  height: 100%;
  margin: 0 auto;
}
.jss13370 {
  width: 25px;
  height: 25px;
  margin-right: 16px;
}
.jss13371 {
  transform: rotate(0deg);
  transition: transform 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
}
.jss13372 {
  transform: rotate(180deg);
}
.jss13373 {
  min-width: 350px;
}
.jss13374 {
  display: flex;
  flex-wrap: wrap;
}
.jss13375 {
  padding: 0;
}
@media (min-width:0px) {
  .jss13375 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss13375 {
    width: calc(100% / 2);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1280px) {
  .jss13375 {
    width: calc(100% / 3);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1920px) {
  .jss13375 {
    width: calc(100% / 4);
    padding: 0 16px 16px 0;
  }
}
.jss13376 {
  padding: 0;
}
@media (min-width:0px) {
  .jss13376 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss13376 {
    width: calc(100% / 2);
  }
}
@media (min-width:1280px) {
  .jss13376 {
    width: calc(100% - (100% / 3));
  }
}
@media (min-width:1920px) {
  .jss13376 {
    width: calc(100% - (100% / 4));
  }
}
.jss13377 {
  padding: 0;
}
@media (min-width:0px) {
  .jss13377 {
    width: 100%;
  }
}
.jss13378 {
  width: 100%;
  margin-bottom: 16px;
  vertical-align: middle;
}
.jss13379 {
  right: 16px;
  display: flex;
  position: absolute;
}
.jss13380 {
  right: 16px;
  bottom: 16px;
  position: fixed;
}
.jss13381 {
  float: right;
}
.jss13382 {
  margin: 0;
}
.jss13383 {
  margin: 8px;
}
.jss13384 {
  margin-left: 8px;
}
.jss13385 {
  margin-top: 8px;
}
.jss13386 {
  margin-right: 8px;
}
.jss13387 {
  margin-bottom: 8px;
}
.jss13388 {
  margin: 16px;
}
.jss13389 {
  margin-left: 16px;
}
.jss13390 {
  margin-top: 16px;
}
.jss13391 {
  margin-right: 16px;
}
.jss13392 {
  margin-bottom: 16px;
}
.jss13393 {
  margin: 24px;
}
.jss13394 {
  margin-left: 24px;
}
.jss13395 {
  margin-top: 24px;
}
.jss13396 {
  margin-right: 24px;
}
.jss13397 {
  margin-bottom: 24px;
}
.jss13398 {
  padding: 0;
}
.jss13399 {
  padding: 8px;
}
.jss13400 {
  padding-left: 8px;
}
.jss13401 {
  padding-top: 8px;
}
.jss13402 {
  padding-right: 8px;
}
.jss13403 {
  padding-bottom: 8px;
}
.jss13404 {
  padding: 16px;
}
.jss13405 {
  padding-left: 16px;
}
.jss13406 {
  padding-top: 16px;
}
.jss13407 {
  padding-right: 16px;
}
.jss13408 {
  padding-bottom: 16px;
}
.jss13409 {
  padding: 24px;
}
.jss13410 {
  padding-left: 24px;
}
.jss13411 {
  padding-top: 24px;
}
.jss13412 {
  padding-right: 24px;
}
.jss13413 {
  padding-bottom: 24px;
}
.jss13414 {
  background-color: #03a9f4;
}
.jss13415 {
  background-color: #ff9800;
}
.jss13416 {
  color: white;
  background-color: #f44336;
}
.jss13417 {
  color: #03a9f4;
}
.jss13418 {
  color: #ff9800;
}
.jss13419 {
  color: #f44336;
}
.jss13420 {
  text-decoration: line-through;
}
.jss13421 {
  padding: 3px;
}
.jss13422 {
  height: calc(100vh - 96px - 72px);
  overflow-x: auto;
}
.jss13423 {
  width: calc(100vw - 32px - 300px);
}
.jss13424 {
  height: calc(100vh - 56px - 68px);
  overflow-x: auto;
}
.jss13425 {
  width: calc(100vw);
}
.jss13426 {
  overflow-x: auto;
}
.jss13427 {
  table-layout: initial;
}
.jss13428 {
  top: 0;
  position: sticky;
  background-color: #fff;
}
.jss13429 {
  width: 3vw;
}
.jss13430 {
  width: 5vw;
}
.jss13431 {
  width: 10vw;
}
.jss13432 {
  width: 15vw;
}
.jss13433 {
  min-width: 15vw;
  max-width: 15vw;
}
@media (min-width:960px) {
  .jss13433 {
    min-width: calc((100vw - 300px - 48px) * 0.15);
    max-width: calc((100vw - 300px - 48px) * 0.15);
  }
}
.jss13434 {
  min-width: 25vw;
  max-width: 25vw;
}
@media (min-width:960px) {
  .jss13434 {
    min-width: calc((100vw - 300px - 48px) * 0.25);
    max-width: calc((100vw - 300px - 48px) * 0.25);
  }
}
.jss13435 {
  width: 25vw;
}
.jss13436 {
  width: 30vw;
}
.jss13437 {
  min-width: 45vw;
  max-width: 45vw;
}
@media (min-width:960px) {
  .jss13437 {
    min-width: calc((100vw - 300px - 48px) * 0.45);
    max-width: calc((100vw - 300px - 48px) * 0.45);
  }
}
.jss13438 {
  width: 100%;
  overflow-x: auto;
}
.jss13439 {
  width: 100%;
  margin-top: 24px;
  overflow-x: auto;
}
.jss13440 {
  min-width: 700px;
}
.jss13441 {
  width: 100%;
}
.jss13442 {
  min-width: 100px;
}
.jss13443 {
  color: rgba(0, 0, 0, 0.54);
  flex-shrink: 0;
}
.jss13444 {
  color: #2196f3;
}
.jss13445 {
  padding-top: 16px;
  padding-bottom: 16px;
}
.jss13447 {
  max-height: 100px;
}
.jss13448 {
  margin-top: -144px;
  margin-left: -56px;
  margin-right: -136px;
  margin-bottom: -64px;
}
.jss13449 {
  margin-top: -80px;
}
.jss13450 {
  width: 500px;
  height: 450px;
}
.jss13451 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss13452 {
  display: flex;
  box-sizing: border-box;
  align-items: center;
}
.jss13454:hover {
  background-color: #eeeeee;
}
.jss13455 {
  flex: 1;
}
.jss13456 {
  cursor: initial;
}
.jss13457 {
  background: #f44336;
}
.jss13458 {
  color: #fff;
}
.jss13459 {
  width: 100%;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
.jss13460 {
  max-height: 75vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss13461 {
  padding: 10px;
  text-align: center;
}
.jss13462 {
  padding: 0;
  vertical-align: top;
}
.jss13463 {
  margin-top: 8px;
  padding-left: 32px;
}
.jss13464 {
  top: 0;
  padding: 5px;
  position: sticky;
  overflow: hidden;
  max-width: 100px;
  max-height: 10px;
  text-align: center;
  background-color: #fff;
}
.jss13465 {
  max-height: 76vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss13466 {
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss13467 {
  padding: 0;
  margin-top: 8px;
}
.jss13468 {
  left: 0;
  width: 500px;
  z-index: 1;
  padding: 8px 16px 0 16px;
  min-width: 300px;
  background: #fff;
  vertical-align: top;
}
.jss13469 {
  padding: 8px 16px 0 16px;
  min-width: 300px;
  vertical-align: top;
}
.jss13470 {
  min-width: 300px;
}
.jss13471 {
  transform: rotate(180deg);
  writing-mode: vertical-rl;
}
.jss13472 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss13473 {
  cursor: pointer;
  transition: all .3s;
}
.jss13473:hover {
  background-color: #ebebeb;
}
.jss13474 {
  background-color: #ebebeb;
}
.jss13475 {
  transition: all .3s;
}
.jss13475:hover {
  transform: scale(1.03);
}
.jss13476 {
  padding: 0;
  transition: all .3s;
}
.jss13476:hover {
  transform: scale(1.03);
}
.jss13477 {
  display: grid;
  align-items: center;
  grid-column-gap: 1.5rem;
  grid-template-columns: 1fr max-content 1fr;
}
.jss13477::after, .jss13477::before {
  height: 1px;
  content: '';
  display: block;
  background-color: currentColor;
}
.jss13478 a {
  text-decoration: none;
}
.jss13479 {
  left: 50%;
  position: absolute;
  transform: ;
}
.jss13480 {
  display: -webkit-box;
  overflow: hidden;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;
}
.jss13481 {
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
.jss13482 {
  display: inline;
  text-transform: capitalize;
}
.jss13483 {
  text-decoration: none;
}
.jss13483 p:hover {
  color: #03a9f4;
}
.jss13484 {
  margin-left: 4px;
}
.jss13485 {
  padding: 0 16px;
  display: inline-flex;
  position: relative;
  vertical-align: middle;
}
.jss13486 {
  top: 0;
  color: #fff;
  right: 0;
  height: 20px;
  display: flex;
  padding: 0 4px;
  z-index: 1;
  position: absolute;
  flex-wrap: wrap;
  min-width: 20px;
  transform: translate(20px, -50%);
  box-sizing: border-box;
  transition: transform 225ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  align-items: center;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  align-content: center;
  border-radius: 10px;
  flex-direction: row;
  justify-content: center;
  background-color: #ff9800;
  transform-origin: 100% 0%;
}
.jss13487 {
  margin: 0;
  padding: 16px;
  border-bottom: 1px solid rgba(0, 0, 0, 0.12);
}
.jss13488 {
  top: 8px;
  right: 8px;
  color: #9e9e9e;
  position: absolute;
}
.jss13489 {
  margin: 0;
  padding: 16px;
}
.jss13490 {
  margin: 0;
  padding: 8px;
  border-top: 1px solid rgba(0, 0, 0, 0.12);
}
.jss13491 {
  font-size: 13px;
}
.jss13492 {
  padding: 0;
  overflow: hidden;
}
.jss13492 span {
  display: inline-flex;
}
.jss13492 span:hover {
  position: relative;
  animation: moveTextHorz 3s linear infinite;
}
@-webkit-keyframes moveTextHorz {
  0% {
    transform: translate(0, 0);
  }
  100% {
    transform: translate(-70%, 0);
  }
}
.jss13493 span {
  color: #f44336;
}
.jss13494 {
  width: 50px;
}
@media (min-width:600px) {
  .jss13494 {
    right: 90px;
  }
}
@media (min-width:960px) {
  .jss13494 {
    right: 15px;
  }
}
.jss13495 {
  cursor: pointer;
}
.jss13495:hover {
  background: #0288d1;
}
.jss13496 {
  margin-left: 10px;
  vertical-align: middle;
}
.jss13497 {
  width: 100%;
}
.jss13498 {
  width: 100%;
  margin: 0;
}
@media (min-width:960px) {
  .jss13499 {
    margin-left: auto;
    margin-right: 16px;
  }
}
@media (min-width:1280px) {
  .jss13499 {
    margin-left: auto;
    margin-right: 32px;
  }
}
@media (min-width:960px) {
  .jss13500 {
    transform: translateY(75%);
    margin-left: 16px;
  }
}
@media (min-width:1280px) {
  .jss13500 {
    transform: translateY(75%);
    margin-left: 32px;
  }
}
.jss13501 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
@media (max-width:1279.95px) {
  .jss13502 {
    display: none;
  }
}
@media (max-width:1919.95px) {
  .jss13503 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss13504 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss13505 {
    text-align: right;
  }
}
.jss13506 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss13506 {
    text-align: right;
  }
}
.jss13507 {
  padding: 0;
}
@media (min-width:1280px) {
  .jss13508 {
    margin-top: 0 !important;
  }
}
@media (min-width:1280px) {
  .jss13509 {
    margin-top: 64px;
  }
}
.jss13510 div textarea {
  overflow: auto;
}
.jss13511 {
  width: 70%;
}
@media (min-width:1280px) {
  .jss13511 {
    float: right;
  }
}
@media (min-width:1280px) {
  .jss13512 {
    float: right;
  }
}
@media (max-width:1279.95px) {
  .jss13512 {
    margin-top: 6px;
  }
}
@media (min-width:1280px) {
  .jss13513 {
    padding: 12px 0;
  }
}
@media (max-width:1279.95px) {
  .jss13513 {
    padding: 6px 0 3px;
  }
}
.jss13514 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss13514 {
    margin-top: 0 !important;
  }
}
.jss13514 div textarea {
  overflow: auto;
}
.jss13515 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss13516 {
  width: 57px;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
  overflow-x: hidden;
}
@media (min-width:600px) {
  .jss13516 {
    width: 73px;
  }
}
@media (min-width:960px) {
  .jss13517 {
    width: calc(100% - 73px);
    margin-left: 73px;
    margin-right: 0;
  }
}
@media (min-width:960px) {
  .jss13518 {
    margin-left: 73px;
    margin-right: 0;
  }
}
.jss13519 {
  bottom: 0px;
  z-index: 1;
  position: sticky;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss13519 {
    display: none;
  }
}
.jss13519:hover {
  background-color: rgb(235, 235, 235);
}
@media (max-width:959.95px) {
  .jss13520 {
    display: none;
  }
}
.jss13521 {
  top: 0;
  left: auto;
  width: 100%;
  right: 0;
  height: 100%;
  z-index: 1301;
  overflow: hidden;
  position: absolute;
  overflow-y: auto;
  transition: transform 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
@media (min-width:960px) {
  .jss13521 {
    width: 24vw;
  }
}
.jss13522 {
  cursor: pointer;
}
.jss13522:hover {
  background-color: #eeeeee;
}
.jss13523 {
  background-color: #e0e0e0 !important;
}
.jss13524 {
  transition: width 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
.jss13525 {
  border-collapse: separate;
}
.jss13526:hover {
  background-color: rgb(235, 235, 235);
}
.jss13527 {
  position: relative;
  min-height: 80vh;
}
.jss13527 .back-icon {
  top: 5px;
  left: 5px;
  z-index: 1;
  position: absolute;
}
.jss13527 > .ninebox-inner {
  width: 100%;
  position: relative;
  min-height: 80vh;
}
.jss13527 > .ninebox-inner .category-paper {
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #424242;
}
.jss13527 > .ninebox-inner .percentage-wrapper {
  width: 100%;
  height: 100%;
  display: flex;
  flex-wrap: wrap;
  box-sizing: border-box;
}
.jss13527 > .ninebox-inner .horizontal-category {
  height: 50px;
  margin: 0 60px;
  display: flex;
  align-items: center;
  justify-content: center;
}
.jss13527 > .ninebox-inner .vertical-category {
  width: 50px;
  height: calc(80vh - 120px);
  display: inline-flex;
  align-items: center;
  justify-content: center;
}
.jss13527 > .ninebox-inner .ninebox-content {
  width: calc(100% - 100px);
  height: calc(80vh - 120px);
  display: inline-block;
  position: relative;
}
.jss13527 > .ninebox-inner .ninebox-content .wrapper {
  width: 100%;
  height: 100%;
  margin: 0;
}
.jss13527 > .ninebox-inner .ninebox-content .wrapper .box {
  height: calc(100% / 3);
  padding: 10px;
}
.jss13527 > .ninebox-inner .ninebox-content .wrapper .box .has-item {
  padding: 26px 10px !important;
}
.jss13527 > .ninebox-inner .ninebox-content .wrapper .box .disabled {
  background: #d1d1d1;
}
.jss13527 > .ninebox-inner .ninebox-content .wrapper .box .card {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 30px;
  position: relative;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  -webkit-transition: all .4s ease-out;
}
.jss13527 > .ninebox-inner .ninebox-content .wrapper .box .card h6 {
  color: rgba(0, 0, 0, 0.87);
}
.jss13527 > .ninebox-inner .ninebox-content .wrapper .box .card .total-person {
  top: 5px;
  right: 5px;
  width: 15px;
  height: 15px;
  position: absolute;
}
.jss13527 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list {
  width: 100%;
  height: 100%;
  display: flex;
  overflow-y: auto;
  align-items: center;
}
.jss13527 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list > ol {
  height: 100%;
  margin: 0;
  padding: 0;
  text-align: left;
  list-style: none;
}
.jss13527 > .ninebox-inner .ninebox-content .wrapper .box .active:hover {
  cursor: pointer;
  transform: scale(1.1);
}
.jss13527 > .ninebox-inner .vertical-category .text-rotate {
  transform: rotate(270deg);
}
.jss13527 > .ninebox-inner .vertical-category.left {
  padding: 10px 0;
  margin-right: 9px;
}
.jss13527 > .ninebox-inner .vertical-category.right {
  margin-left: 9px;
}
.jss13527 > .ninebox-inner .vertical-category .text-rotate.nowrap {
  white-space: nowrap;
}
.jss13527 > .ninebox-inner .horizontal-category.top {
  padding: 0 9px;
  margin-bottom: 9px;
}
.jss13527 > .ninebox-inner .horizontal-category.bottom {
  margin-top: 9px;
}
.jss13527 > .ninebox-inner .percentage-wrapper > .percentage-item {
  padding: 0 10px;
  flex-grow: 0;
  max-width: calc(100% / 3);
  flex-basis: calc(100% / 3);
}
.jss13527 > .ninebox-inner .percentage-wrapper > .percentage-item.horizontal {
  height: calc(100% / 3);
  padding: 10px 0;
  max-width: 100%;
}
.jss13527 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 10px;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  background-color: #424242;
  -webkit-transition: all .4s ease-out;
}
.jss13527 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper > p {
  color: #fff;
}
.jss13527 > .ninebox-inner .category-paper > p {
  color: #fff;
}
.jss13528 {
  max-height: 45vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss13528 .table {
  border-collapse: separate;
}
.jss13528 .table .sticky-head {
  top: 0;
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss13529 {
  height: 44vh;
  overflow: auto;
}
.jss13530 {
  width: 100%;
  height: 50vh;
  overflow: auto;
}
.jss13531 {
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss13532 {
  width: 100%;
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss13533 {
  max-width: 82%;
}

@media (min-width:960px) {
  ::-webkit-scrollbar {
    width: 5px;
    height: 5px;
  }
  ::-webkit-scrollbar-track {
    background: rgba(250, 250, 250, 0);
  }
  ::-webkit-scrollbar-thumb {
    background: #eeeeee;
  }
  ::-webkit-scrollbar-thumb:hover {
    background: #e0e0e0;
  }
}
.jss11432 {
  width: 100%;
  z-index: 1;
  overflow: hidden;
  margin-bottom: 0;
}
.jss11433 {
  background-color: #424242;
}
.jss11434 {
  height: 70px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/etg.9362c8fb.svg&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss11435 {
  height: 150px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/tessa.55f0b81f.svg&quot;);
  background-repeat: no-repeat;
}
.jss11436 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2hJREFUeNrsWtGR2jAQ1TEU4FQQUsGRCuBm+IerAKgguAKgAnMVHFcBvn/PnFwBvg6cCuIOEslacRvFyJJswL74zQgDNrKedvftSoaQDh06dHDAnfGVfjRjr0PWRg3kkbD2RIJJakfMjzbsdd0CQz0yciF/0zf8wRyOKbSmgXuSx1rAmhWxgbSdnJFG4cOj5DhJz7KLrC3i0a8wSwM8QzcRjGCS1U+MkLcbE9uztjQn5kcyEFMsn5q4uxW09+8hQh5rR/buCNY4OtxsBzMpEaLZ/cIm6g6uyZTrtih+d6C8/PxDLuEiT1mhp7jWEH32HIi95sr5kRKe4PiTtRWbuN/5UfS9hXOUEd6gSXgBa/gwpoMyLiMh6ym5gJxmSsyuCxaK71OUAyka0AC+i8H9YzjnofEsNfd5NyWWnCwVTGgF35/CoPbK9ykMXt5nDEQpWIaC1aZwnlvqnrXvLq7YV2Z2CDesijUMCLv0FNxQYgQWkWIl3fOAfrcq+N46xuJTh8I1XOCBi4nyRqjqEMUZVSyWIQvNlZAI4fr4TIyNTIlh93O12gHJ8AxUluMHkI2V+xwhJmVsviniEAOBtbsr8izuRykM7L7G4lRacl2SizxFiRd1llQUOjSx2BYN9lpVvyzjMkgtxsRiIDbIa0Fd5SFyz6apRXCvwGJV46yBxISFUhPVaeOyJQE/HpcsW8aO2wV8sZqgfhZI6m2w1IVKEbE4l2oRZ55mzTN2dNeZUknMHftZ6GK8iJgaZ6FmZ8il9KIFE+mC0M4VuZv4Ec7u4RlVDMs6N4JQ14urojqrrVXGc8TiU+XA4+yTqCJRgnt8RhU9CGAb4mLljAVJbArZlk9pHgYOmznUoIpeEffd4R16/+zo8p7Sj4EriplIkDxfEtklfqfbfkugOh+cVTM/spd7dXUeTB4h2duRwkneklhc6vvVthDq78dAFYnLPkMb5J6AqbO2Eivb4k40cr8i4rGNS/G6R/08O66WH3QuXPa0JS6RW9dVsO6z7baDk8WoJle51om7f5Yxbill706Mm/qjIC6KweoCU1c/lq7YWnU0IUY/K7H3GgTj6ih/oiKq+F+oqk5IyZOOK+MriI98WPnNjJggF5C/Hyg0Faf/edj8M4cn6mlZ/rgRuBe9XqLm7NChw3+GPwIMAA139Xg+K8vEAAAAAElFTkSuQmCC&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss11437 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABANJREFUeNrsWt1x4jAQNpl7P18FcSqIqSBmhvdABYEKiCvwUQFcBfZVEHhnBl8F8VUQSvB1cJJ3jddC/pOlQBg0YwYUIfbT/n27imXdxtcag947+DuPvXqa5TpYq3F0PmD+bsZeQ0OHvmbg/HMB+2CvDntS9iSaADn48PGDgUtVNvmmQQg+fjEBfmqBBaa9x08ue2KVbe6uNXhcOrDDdQJbjQ/XqjHrBuxLAYMIeVZg9xrhuJegsTx5ztjphuyxe2qI58LVJURFSnk4vfpgwr0qmRywmECgVMrAdJBgB095QmaTDPRqHBv7rnFgZUcPCc3igzP0pfTkwewW7LGJWfO168soW9oJXHDJ6gPwVQnv5wCrNrEDgnSFuXlfszPB7utYfw7EFioBOmLy9wuroE8Lz6ACRBPZXfatmvUD83cu+o0r5DiukT+SIpSve0RTtU2Y5kCTlkLl05dreX7unkeIiTkfS+VKGhJ7QDQYsb3mnw+srCludiMmSNKwPq5lE2DSewLOV81rA0VQtC/RFlSIax9q89UpuJGKz6lyRepT9aBg5BzQFnKbrGpOsj0Lgq3U3rtTNEGH+FTSImLSwPDSoiWQZHvn+Q9+07jGgmP0axcoRCAeMpMmcGtStgRmgfm7iUV7ie3GRKjdrEZzpFG20Nqki6hdKdWTQFxFfuhIErFDhHzBuQVbn0i0JAaJDfGxJ/xsBJh35Hh5ZANA7y0430YAu5dYBKxbjacINGVzG9SwZ9LHXFIM0tEEKs9fG8EkZUPc669KL0Q13P8j5gP8riwwfz/FsM3TwYisfTjOFz5Eq+epZM5guIewLf8x4HUjMm8ffUP0G25eMOcJ0S7CQ0grGkaiDNo0lta2yIrEGhFwe6kwwDEDsu8844XNFXSqH1ibjhFoYy6YmFNRiFLmErUIWJ16+V2jYl4RPzas+14KHNBvnGWmCma47RDl7rtqSyV4xCenWJ+UEzTbd+yBcNN8s8qXeU0Uy1MJIl2BbY/+U8UEytzQxnzlCKD3UlOr32trEhjN/ItGnyiESjElrCW5yq3hjouK39YMDKJWRMis7LSfJeY7zAIE/BfAVOIvXoW2ZqSaPpjUWM75ilrp9CLCJVrys+RMhVqNN5ikNw3MJZSQYYPAQMglMTWxEMzD/bCyrIe0MM3WwNpIkudcUvMduorZp+fxRqJfr8ZLTYOIp4ehyjZ9OsFz1Bj4AgSAqXL/HUw6FFLFSFU49fsxAED5IQ8Aqvdjs+y7IqgelxS6OsE8+b4KDCXKck9Vhwki6jOCoeF+jX7V6+ZF9/1YUJFwExJJ7YraKkZAsQ5xTNyPeUiTJla7mxSu2d+6r5LM3I+VQboSgCkhxLdxG2z8F2AAPtd0UKyJkPIAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss11438 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAzRJREFUeNrsWs2Nm0AUnl1RAFIKCK4gdLBwy9GuYL0VePeWm+1bbt5UYLsC7FtueCswqWDdQCTSQd5bfRM9IcADDD+OeNIIbI8f8837f4NSI4000kg16K7uHz99++nTZUkj/f3965PNRRHvFV2+0FgT76QOj/sGz2dQUxpzWkhgEVQgeC/r8mkCzO1Ao9w+gEnyb8bGSCU8qEPRri3Ebycab5bW9JnVG/cXGvuCeSmNA9ngxRgYgeIFv3ekbk2IwU0IXGqqiv4NgNI2mGsGjsGfi9ThkYZXoIoPDRbrCb4siaTOxhsBI1GvctT1QSzgLW9OA3cf42NCfMOcOfx70JZXlDt5atGGapHT4KFrGn+QeVgDxrxIIi9QtV3nwOCJVm2IiXi/NuXhVNB9T8SXV+1iYROB6pY8mxKbi9yNPeKEQHEAj4YYB6oAk+qxg8ocCFzYg8Qer0nNaWpTcBynLlFlQk2rSfDg6L7DXQ5s1m2DAIZqm7OFGPf/jcS2Bfe3Cwz9C1+kRz6+u11gCOo69v1AGvZRpOK32wMGRxGJLH2FWJggD4zadCaOZelMUYsFol5iIKGOhQjoMdSTnckFc44cD4tK/V4kRouL0ErYZPokLKFQlu64D0UmozeEnco78doMSWJuJvX6hUZLWpLFvBCINUBxc/TZNMHt0saehNcLykDlADyIXDMFr2EAg13MRD+iSgd3I8LBxGRDOvWKSIY1uGcTd44sRNd4M1ugrLt7LmNEOT83LD8UvKHVCqGNOHbE1aQFp/PG/WDjmKAq6qSBGcUuqHfQF7CiLOQfcHHmlapqHWeev1AGhyBtANMPdQnQVuUcbND3KXLH1HBjPjIa2hCOfRwOzn3YmCsAzjOpVSLmLMUmXPOg7Fi4UN1C2qe+VZE95BHeMiuBhbAX91og1zkmXc992ZjOAXdFCS2AHhDHpqqk4yuObmfIMWMTG7srYRaLGBP22ZgRUtLrkGd3YV4MdNRACa49AhgtqRj3yTW3bwLM66KUL/GwZwTwNUqb2JaNearBawkWKPt8oy5XkbtPVIOzqQ6p6MSz9K0BX5T6QyQGtK/75s5II400Ui79FWAANjYkpEJzM+AAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss11439 {
  color: #FFF;
  padding: 48px;
}
.jss11440 {
  color: rgba(0, 0, 0, 0.87);
  width: 100%;
  display: flex;
  z-index: 2;
  opacity: 0.5;
  position: fixed;
  align-items: center;
  padding-left: 16px;
  padding-right: 16px;
  background-color: #424242;
}
.jss11441 {
  height: 100vh;
  display: flex;
  position: relative;
  max-height: 1600px;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: linear-gradient(rgba(0,130,170,0), #424242), url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/1.20e79364.jpg&quot;);
}
.jss11442 {
  margin: -200px auto 0 auto;
  padding: 64px 0 48px;
  max-width: 1200px;
}
.jss11443 {
  width: 50%;
  color: #FFF;
}
@media (max-width:1279.95px) {
  .jss11443 {
    width: 100%;
    padding: 24px;
  }
}
.jss11444 {
  margin: -150px auto 0 auto;
  padding: 16px;
  position: relative;
  max-width: 1200px;
  background: #fafafa;
  justify-content: center;
}
.jss11445 {
  margin: 0 auto;
  padding: 24px;
  display: flex;
}
.jss11446 {
  margin-bottom: 24px;
}
.jss11447 {
  text-decoration: none;
}
.jss11448 {
  margin: 24px auto;
  max-width: 1200px;
}
@media (max-width:1279.95px) {
  .jss11448 {
    max-width: 1366px;
  }
}
.jss11449 {
  height: 160px;
  display: flex;
  align-items: center;
}
.jss11450 {
  margin: auto;
  display: grid;
}
.jss11451 {
  height: 100vh;
  display: flex;
  position: relative;
  overflow: hidden;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/2.adfa400c.jpg&quot;);
  background-position: center;
}
.jss11452 {
  color: #FFF;
  background-color: #03a9f4;
}
.jss11453 {
  color: #03a9f4;
  background-color: #fff;
}
.jss11454 {
  color: #03a9f4;
  padding: 0;
}
.jss11455 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss11455 {
    width: calc(100%);
  }
}
.jss11456 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss11456 {
    width: calc(100% - 300px);
  }
}
.jss11457 {
  display: none;
}
@media (min-width:600px) {
  .jss11457 {
    display: block;
  }
}
.jss11458 {
  color: #fff;
  position: relative;
  background: #03a9f4;
}
.jss11459 {
  width: 100%;
  position: relative;
  border-radius: 14px;
  background-color: rgba(255, 255, 255, 0.5);
}
.jss11459:hover {
  background-color: rgba(255, 255, 255, 0.7);
}
@media (min-width:600px) {
  .jss11459 {
    width: auto;
  }
}
.jss11460 {
  width: 72px;
  color: #212121;
  height: 100%;
  display: flex;
  position: absolute;
  align-items: center;
  pointer-events: none;
  justify-content: center;
}
.jss11461 {
  color: #fff;
  width: 100%;
}
.jss11462 {
  color: #212121;
  width: 100%;
  transition: width 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  padding-top: 8px;
  padding-left: 64px;
  padding-right: 8px;
  padding-bottom: 8px;
}
@media (min-width:600px) {
  .jss11462 {
    width: 80px;
  }
  .jss11462:focus {
    width: 200px;
  }
}
.jss11463 {
  color: #212121;
}
.jss11464 {
  background-color: #4fc3f7;
}
.jss11465 {
  margin-left: 12px;
  margin-right: 36px;
}
.jss11466 {
  display: none;
}
@media (min-width:960px) {
  .jss11467 {
    display: none;
  }
}
.jss11468 {
  width: 300px;
}
.jss11469 {
  display: flex;
  padding: 0 8px;
  min-height: 56px;
  align-items: center;
}
@media (min-width:0px) and (orientation: landscape) {
  .jss11469 {
    min-height: 48px;
  }
}
@media (min-width:600px) {
  .jss11469 {
    min-height: 64px;
  }
}
.jss11470 {
  margin-top: 65px;
  justify-content: flex-start;
}
.jss11471 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss11473 {
  width: 100%;
  height: 100%;
  opacity: 0.2;
  content: &quot;&quot;;
  display: block;
  position: absolute;
  background: linear-gradient(rgba(0,130,170,0), #03a9f4));
  background-size: cover;
  background-position: center center;
}
.jss11474 {
  width: 60px;
  overflow-x: hidden;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss11475 {
  color: inherit;
}
.jss11476 {
  margin-left: 56px;
}
.jss11477 {
  bottom: 16px;
}
.jss11478 {
  color: rgba(0, 0, 0, 0.87);
  background-size: cover;
  background-color: #fff;
  background-image: url(https://xdev.equine.co.id/apps/tessa/static/media/pattern2.f069796d.svg);
  background-repeat: no-repeat;
  background-position: inherit;
}
.jss11479 {
  color: #fff;
  background-color: #03a9f4;
}
.jss11480 {
  color: #fff;
  background-color: #ff9800;
}
.jss11481 {
  color: #fff;
  background-color: #f44336;
}
.jss11482 {
  color: #fff;
  background: #4caf50;
}
.jss11483 {
  flex: 1;
}
.jss11484 {
  padding: 16px;
  flex-grow: 1;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss11484 {
    margin-top: 68px;
  }
}
@media (max-width:959.95px) {
  .jss11484 {
    padding: 0;
    margin-top: 56px;
  }
}
@media (max-width:959.95px) {
  .jss11485 {
    margin-top: 68px;
  }
}
@media (min-width:960px) {
  .jss11485 {
    left: 50%;
    width: 982px;
    position: absolute;
    transform: translateX(-50%);
  }
}
@media (min-width:960px) {
  .jss11486 {
    margin-left: 300px;
    margin-right: 0;
  }
}
.jss11487 {
  margin-bottom: 56px;
}
.jss11488 {
  width: calc(100% - 0px);
  right: 0;
  bottom: 0;
  display: flex;
  position: fixed;
}
@media (min-width:960px) {
  .jss11488 {
    width: calc(100% - 300px);
  }
}
.jss11490 {
  background: #fff;
}
.jss11491 {
  margin-bottom: 60px;
}
.jss11492 {
  height: 4px;
}
.jss11493 {
  display: flex;
  align-items: center;
  justify-content: space-between;
}
.jss11494 {
  margin: 0 15px 0 0;
  padding: 16px 0;
  flex-grow: 1;
}
@media (min-width:600px) {
  .jss11494 {
    width: 30%;
    flex-grow: 0;
  }
}
.jss11495 {
  color: inherit;
  font-size: 0.6964285714285714rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: inherit;
  line-height: 1.66;
  letter-spacing: 0.03333em;
}
.jss11496 {
  color: #fff;
  background: #03a9f4;
}
.jss11497 {
  color: #fff;
  min-height: 40px;
  background: #ff9800;
  margin-bottom: 16px;
}
.jss11498 {
  height: 500px;
  display: flex;
  overflow: hidden;
  align-items: center;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss11498 {
    height: 300px;
  }
}
.jss11499 {
  height: 100%;
  margin: 0 auto;
}
.jss11500 {
  width: 25px;
  height: 25px;
  margin-right: 16px;
}
.jss11501 {
  transform: rotate(0deg);
  transition: transform 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
}
.jss11502 {
  transform: rotate(180deg);
}
.jss11503 {
  min-width: 350px;
}
.jss11504 {
  display: flex;
  flex-wrap: wrap;
}
.jss11505 {
  padding: 0;
}
@media (min-width:0px) {
  .jss11505 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss11505 {
    width: calc(100% / 2);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1280px) {
  .jss11505 {
    width: calc(100% / 3);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1920px) {
  .jss11505 {
    width: calc(100% / 4);
    padding: 0 16px 16px 0;
  }
}
.jss11506 {
  padding: 0;
}
@media (min-width:0px) {
  .jss11506 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss11506 {
    width: calc(100% / 2);
  }
}
@media (min-width:1280px) {
  .jss11506 {
    width: calc(100% - (100% / 3));
  }
}
@media (min-width:1920px) {
  .jss11506 {
    width: calc(100% - (100% / 4));
  }
}
.jss11507 {
  padding: 0;
}
@media (min-width:0px) {
  .jss11507 {
    width: 100%;
  }
}
.jss11508 {
  width: 100%;
  margin-bottom: 16px;
  vertical-align: middle;
}
.jss11509 {
  right: 16px;
  display: flex;
  position: absolute;
}
.jss11510 {
  right: 16px;
  bottom: 16px;
  position: fixed;
}
.jss11511 {
  float: right;
}
.jss11512 {
  margin: 0;
}
.jss11513 {
  margin: 8px;
}
.jss11514 {
  margin-left: 8px;
}
.jss11515 {
  margin-top: 8px;
}
.jss11516 {
  margin-right: 8px;
}
.jss11517 {
  margin-bottom: 8px;
}
.jss11518 {
  margin: 16px;
}
.jss11519 {
  margin-left: 16px;
}
.jss11520 {
  margin-top: 16px;
}
.jss11521 {
  margin-right: 16px;
}
.jss11522 {
  margin-bottom: 16px;
}
.jss11523 {
  margin: 24px;
}
.jss11524 {
  margin-left: 24px;
}
.jss11525 {
  margin-top: 24px;
}
.jss11526 {
  margin-right: 24px;
}
.jss11527 {
  margin-bottom: 24px;
}
.jss11528 {
  padding: 0;
}
.jss11529 {
  padding: 8px;
}
.jss11530 {
  padding-left: 8px;
}
.jss11531 {
  padding-top: 8px;
}
.jss11532 {
  padding-right: 8px;
}
.jss11533 {
  padding-bottom: 8px;
}
.jss11534 {
  padding: 16px;
}
.jss11535 {
  padding-left: 16px;
}
.jss11536 {
  padding-top: 16px;
}
.jss11537 {
  padding-right: 16px;
}
.jss11538 {
  padding-bottom: 16px;
}
.jss11539 {
  padding: 24px;
}
.jss11540 {
  padding-left: 24px;
}
.jss11541 {
  padding-top: 24px;
}
.jss11542 {
  padding-right: 24px;
}
.jss11543 {
  padding-bottom: 24px;
}
.jss11544 {
  background-color: #03a9f4;
}
.jss11545 {
  background-color: #ff9800;
}
.jss11546 {
  color: white;
  background-color: #f44336;
}
.jss11547 {
  color: #03a9f4;
}
.jss11548 {
  color: #ff9800;
}
.jss11549 {
  color: #f44336;
}
.jss11550 {
  text-decoration: line-through;
}
.jss11551 {
  padding: 3px;
}
.jss11552 {
  height: calc(100vh - 96px - 72px);
  overflow-x: auto;
}
.jss11553 {
  width: calc(100vw - 32px - 300px);
}
.jss11554 {
  height: calc(100vh - 56px - 68px);
  overflow-x: auto;
}
.jss11555 {
  width: calc(100vw);
}
.jss11556 {
  overflow-x: auto;
}
.jss11557 {
  table-layout: initial;
}
.jss11558 {
  top: 0;
  position: sticky;
  background-color: #fff;
}
.jss11559 {
  width: 3vw;
}
.jss11560 {
  width: 5vw;
}
.jss11561 {
  width: 10vw;
}
.jss11562 {
  width: 15vw;
}
.jss11563 {
  min-width: 15vw;
  max-width: 15vw;
}
@media (min-width:960px) {
  .jss11563 {
    min-width: calc((100vw - 300px - 48px) * 0.15);
    max-width: calc((100vw - 300px - 48px) * 0.15);
  }
}
.jss11564 {
  min-width: 25vw;
  max-width: 25vw;
}
@media (min-width:960px) {
  .jss11564 {
    min-width: calc((100vw - 300px - 48px) * 0.25);
    max-width: calc((100vw - 300px - 48px) * 0.25);
  }
}
.jss11565 {
  width: 25vw;
}
.jss11566 {
  width: 30vw;
}
.jss11567 {
  min-width: 45vw;
  max-width: 45vw;
}
@media (min-width:960px) {
  .jss11567 {
    min-width: calc((100vw - 300px - 48px) * 0.45);
    max-width: calc((100vw - 300px - 48px) * 0.45);
  }
}
.jss11568 {
  width: 100%;
  overflow-x: auto;
}
.jss11569 {
  width: 100%;
  margin-top: 24px;
  overflow-x: auto;
}
.jss11570 {
  min-width: 700px;
}
.jss11571 {
  width: 100%;
}
.jss11572 {
  min-width: 100px;
}
.jss11573 {
  color: rgba(0, 0, 0, 0.54);
  flex-shrink: 0;
}
.jss11574 {
  color: #2196f3;
}
.jss11575 {
  padding-top: 16px;
  padding-bottom: 16px;
}
.jss11577 {
  max-height: 100px;
}
.jss11578 {
  margin-top: -144px;
  margin-left: -56px;
  margin-right: -136px;
  margin-bottom: -64px;
}
.jss11579 {
  margin-top: -80px;
}
.jss11580 {
  width: 500px;
  height: 450px;
}
.jss11581 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss11582 {
  display: flex;
  box-sizing: border-box;
  align-items: center;
}
.jss11584:hover {
  background-color: #eeeeee;
}
.jss11585 {
  flex: 1;
}
.jss11586 {
  cursor: initial;
}
.jss11587 {
  background: #f44336;
}
.jss11588 {
  color: #fff;
}
.jss11589 {
  width: 100%;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
.jss11590 {
  max-height: 75vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss11591 {
  padding: 10px;
  text-align: center;
}
.jss11592 {
  padding: 0;
  vertical-align: top;
}
.jss11593 {
  margin-top: 8px;
  padding-left: 32px;
}
.jss11594 {
  top: 0;
  padding: 5px;
  position: sticky;
  overflow: hidden;
  max-width: 100px;
  max-height: 10px;
  text-align: center;
  background-color: #fff;
}
.jss11595 {
  max-height: 76vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss11596 {
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss11597 {
  padding: 0;
  margin-top: 8px;
}
.jss11598 {
  left: 0;
  width: 500px;
  z-index: 1;
  padding: 8px 16px 0 16px;
  min-width: 300px;
  background: #fff;
  vertical-align: top;
}
.jss11599 {
  padding: 8px 16px 0 16px;
  min-width: 300px;
  vertical-align: top;
}
.jss11600 {
  min-width: 300px;
}
.jss11601 {
  transform: rotate(180deg);
  writing-mode: vertical-rl;
}
.jss11602 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss11603 {
  cursor: pointer;
  transition: all .3s;
}
.jss11603:hover {
  background-color: #ebebeb;
}
.jss11604 {
  background-color: #ebebeb;
}
.jss11605 {
  transition: all .3s;
}
.jss11605:hover {
  transform: scale(1.03);
}
.jss11606 {
  padding: 0;
  transition: all .3s;
}
.jss11606:hover {
  transform: scale(1.03);
}
.jss11607 {
  display: grid;
  align-items: center;
  grid-column-gap: 1.5rem;
  grid-template-columns: 1fr max-content 1fr;
}
.jss11607::after, .jss11607::before {
  height: 1px;
  content: '';
  display: block;
  background-color: currentColor;
}
.jss11608 a {
  text-decoration: none;
}
.jss11609 {
  left: 50%;
  position: absolute;
  transform: ;
}
.jss11610 {
  display: -webkit-box;
  overflow: hidden;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;
}
.jss11611 {
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
.jss11612 {
  display: inline;
  text-transform: capitalize;
}
.jss11613 {
  text-decoration: none;
}
.jss11613 p:hover {
  color: #03a9f4;
}
.jss11614 {
  margin-left: 4px;
}
.jss11615 {
  padding: 0 16px;
  display: inline-flex;
  position: relative;
  vertical-align: middle;
}
.jss11616 {
  top: 0;
  color: #fff;
  right: 0;
  height: 20px;
  display: flex;
  padding: 0 4px;
  z-index: 1;
  position: absolute;
  flex-wrap: wrap;
  min-width: 20px;
  transform: translate(20px, -50%);
  box-sizing: border-box;
  transition: transform 225ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  align-items: center;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  align-content: center;
  border-radius: 10px;
  flex-direction: row;
  justify-content: center;
  background-color: #ff9800;
  transform-origin: 100% 0%;
}
.jss11617 {
  margin: 0;
  padding: 16px;
  border-bottom: 1px solid rgba(0, 0, 0, 0.12);
}
.jss11618 {
  top: 8px;
  right: 8px;
  color: #9e9e9e;
  position: absolute;
}
.jss11619 {
  margin: 0;
  padding: 16px;
}
.jss11620 {
  margin: 0;
  padding: 8px;
  border-top: 1px solid rgba(0, 0, 0, 0.12);
}
.jss11621 {
  font-size: 13px;
}
.jss11622 {
  padding: 0;
  overflow: hidden;
}
.jss11622 span {
  display: inline-flex;
}
.jss11622 span:hover {
  position: relative;
  animation: moveTextHorz 3s linear infinite;
}
@-webkit-keyframes moveTextHorz {
  0% {
    transform: translate(0, 0);
  }
  100% {
    transform: translate(-70%, 0);
  }
}
.jss11623 span {
  color: #f44336;
}
.jss11624 {
  width: 50px;
}
@media (min-width:600px) {
  .jss11624 {
    right: 90px;
  }
}
@media (min-width:960px) {
  .jss11624 {
    right: 15px;
  }
}
.jss11625 {
  cursor: pointer;
}
.jss11625:hover {
  background: #0288d1;
}
.jss11626 {
  margin-left: 10px;
  vertical-align: middle;
}
.jss11627 {
  width: 100%;
}
.jss11628 {
  width: 100%;
  margin: 0;
}
@media (min-width:960px) {
  .jss11629 {
    margin-left: auto;
    margin-right: 16px;
  }
}
@media (min-width:1280px) {
  .jss11629 {
    margin-left: auto;
    margin-right: 32px;
  }
}
@media (min-width:960px) {
  .jss11630 {
    transform: translateY(75%);
    margin-left: 16px;
  }
}
@media (min-width:1280px) {
  .jss11630 {
    transform: translateY(75%);
    margin-left: 32px;
  }
}
.jss11631 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
@media (max-width:1279.95px) {
  .jss11632 {
    display: none;
  }
}
@media (max-width:1919.95px) {
  .jss11633 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss11634 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss11635 {
    text-align: right;
  }
}
.jss11636 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss11636 {
    text-align: right;
  }
}
.jss11637 {
  padding: 0;
}
@media (min-width:1280px) {
  .jss11638 {
    margin-top: 0 !important;
  }
}
@media (min-width:1280px) {
  .jss11639 {
    margin-top: 64px;
  }
}
.jss11640 div textarea {
  overflow: auto;
}
.jss11641 {
  width: 70%;
}
@media (min-width:1280px) {
  .jss11641 {
    float: right;
  }
}
@media (min-width:1280px) {
  .jss11642 {
    float: right;
  }
}
@media (max-width:1279.95px) {
  .jss11642 {
    margin-top: 6px;
  }
}
@media (min-width:1280px) {
  .jss11643 {
    padding: 12px 0;
  }
}
@media (max-width:1279.95px) {
  .jss11643 {
    padding: 6px 0 3px;
  }
}
.jss11644 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss11644 {
    margin-top: 0 !important;
  }
}
.jss11644 div textarea {
  overflow: auto;
}
.jss11645 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss11646 {
  width: 57px;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
  overflow-x: hidden;
}
@media (min-width:600px) {
  .jss11646 {
    width: 73px;
  }
}
@media (min-width:960px) {
  .jss11647 {
    width: calc(100% - 73px);
    margin-left: 73px;
    margin-right: 0;
  }
}
@media (min-width:960px) {
  .jss11648 {
    margin-left: 73px;
    margin-right: 0;
  }
}
.jss11649 {
  bottom: 0px;
  z-index: 1;
  position: sticky;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss11649 {
    display: none;
  }
}
.jss11649:hover {
  background-color: rgb(235, 235, 235);
}
@media (max-width:959.95px) {
  .jss11650 {
    display: none;
  }
}
.jss11651 {
  top: 0;
  left: auto;
  width: 100%;
  right: 0;
  height: 100%;
  z-index: 1301;
  overflow: hidden;
  position: absolute;
  overflow-y: auto;
  transition: transform 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
@media (min-width:960px) {
  .jss11651 {
    width: 24vw;
  }
}
.jss11652 {
  cursor: pointer;
}
.jss11652:hover {
  background-color: #eeeeee;
}
.jss11653 {
  background-color: #e0e0e0 !important;
}
.jss11654 {
  transition: width 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
.jss11655 {
  border-collapse: separate;
}
.jss11656:hover {
  background-color: rgb(235, 235, 235);
}
.jss11657 {
  position: relative;
  min-height: 80vh;
}
.jss11657 .back-icon {
  top: 5px;
  left: 5px;
  z-index: 1;
  position: absolute;
}
.jss11657 > .ninebox-inner {
  width: 100%;
  position: relative;
  min-height: 80vh;
}
.jss11657 > .ninebox-inner .category-paper {
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #424242;
}
.jss11657 > .ninebox-inner .percentage-wrapper {
  width: 100%;
  height: 100%;
  display: flex;
  flex-wrap: wrap;
  box-sizing: border-box;
}
.jss11657 > .ninebox-inner .horizontal-category {
  height: 50px;
  margin: 0 60px;
  display: flex;
  align-items: center;
  justify-content: center;
}
.jss11657 > .ninebox-inner .vertical-category {
  width: 50px;
  height: calc(80vh - 120px);
  display: inline-flex;
  align-items: center;
  justify-content: center;
}
.jss11657 > .ninebox-inner .ninebox-content {
  width: calc(100% - 100px);
  height: calc(80vh - 120px);
  display: inline-block;
  position: relative;
}
.jss11657 > .ninebox-inner .ninebox-content .wrapper {
  width: 100%;
  height: 100%;
  margin: 0;
}
.jss11657 > .ninebox-inner .ninebox-content .wrapper .box {
  height: calc(100% / 3);
  padding: 10px;
}
.jss11657 > .ninebox-inner .ninebox-content .wrapper .box .has-item {
  padding: 26px 10px !important;
}
.jss11657 > .ninebox-inner .ninebox-content .wrapper .box .disabled {
  background: #d1d1d1;
}
.jss11657 > .ninebox-inner .ninebox-content .wrapper .box .card {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 30px;
  position: relative;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  -webkit-transition: all .4s ease-out;
}
.jss11657 > .ninebox-inner .ninebox-content .wrapper .box .card h6 {
  color: rgba(0, 0, 0, 0.87);
}
.jss11657 > .ninebox-inner .ninebox-content .wrapper .box .card .total-person {
  top: 5px;
  right: 5px;
  width: 15px;
  height: 15px;
  position: absolute;
}
.jss11657 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list {
  width: 100%;
  height: 100%;
  display: flex;
  overflow-y: auto;
  align-items: center;
}
.jss11657 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list > ol {
  height: 100%;
  margin: 0;
  padding: 0;
  text-align: left;
  list-style: none;
}
.jss11657 > .ninebox-inner .ninebox-content .wrapper .box .active:hover {
  cursor: pointer;
  transform: scale(1.1);
}
.jss11657 > .ninebox-inner .vertical-category .text-rotate {
  transform: rotate(270deg);
}
.jss11657 > .ninebox-inner .vertical-category.left {
  padding: 10px 0;
  margin-right: 9px;
}
.jss11657 > .ninebox-inner .vertical-category.right {
  margin-left: 9px;
}
.jss11657 > .ninebox-inner .vertical-category .text-rotate.nowrap {
  white-space: nowrap;
}
.jss11657 > .ninebox-inner .horizontal-category.top {
  padding: 0 9px;
  margin-bottom: 9px;
}
.jss11657 > .ninebox-inner .horizontal-category.bottom {
  margin-top: 9px;
}
.jss11657 > .ninebox-inner .percentage-wrapper > .percentage-item {
  padding: 0 10px;
  flex-grow: 0;
  max-width: calc(100% / 3);
  flex-basis: calc(100% / 3);
}
.jss11657 > .ninebox-inner .percentage-wrapper > .percentage-item.horizontal {
  height: calc(100% / 3);
  padding: 10px 0;
  max-width: 100%;
}
.jss11657 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 10px;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  background-color: #424242;
  -webkit-transition: all .4s ease-out;
}
.jss11657 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper > p {
  color: #fff;
}
.jss11657 > .ninebox-inner .category-paper > p {
  color: #fff;
}
.jss11658 {
  max-height: 45vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss11658 .table {
  border-collapse: separate;
}
.jss11658 .table .sticky-head {
  top: 0;
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss11659 {
  height: 44vh;
  overflow: auto;
}
.jss11660 {
  width: 100%;
  height: 50vh;
  overflow: auto;
}
.jss11661 {
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss11662 {
  width: 100%;
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss11663 {
  max-width: 82%;
}

@media (min-width:960px) {
  ::-webkit-scrollbar {
    width: 5px;
    height: 5px;
  }
  ::-webkit-scrollbar-track {
    background: rgba(250, 250, 250, 0);
  }
  ::-webkit-scrollbar-thumb {
    background: #eeeeee;
  }
  ::-webkit-scrollbar-thumb:hover {
    background: #e0e0e0;
  }
}
.jss11664 {
  width: 100%;
  z-index: 1;
  overflow: hidden;
  margin-bottom: 0;
}
.jss11665 {
  background-color: #424242;
}
.jss11666 {
  height: 70px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/etg.9362c8fb.svg&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss11667 {
  height: 150px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/tessa.55f0b81f.svg&quot;);
  background-repeat: no-repeat;
}
.jss11668 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2hJREFUeNrsWtGR2jAQ1TEU4FQQUsGRCuBm+IerAKgguAKgAnMVHFcBvn/PnFwBvg6cCuIOEslacRvFyJJswL74zQgDNrKedvftSoaQDh06dHDAnfGVfjRjr0PWRg3kkbD2RIJJakfMjzbsdd0CQz0yciF/0zf8wRyOKbSmgXuSx1rAmhWxgbSdnJFG4cOj5DhJz7KLrC3i0a8wSwM8QzcRjGCS1U+MkLcbE9uztjQn5kcyEFMsn5q4uxW09+8hQh5rR/buCNY4OtxsBzMpEaLZ/cIm6g6uyZTrtih+d6C8/PxDLuEiT1mhp7jWEH32HIi95sr5kRKe4PiTtRWbuN/5UfS9hXOUEd6gSXgBa/gwpoMyLiMh6ym5gJxmSsyuCxaK71OUAyka0AC+i8H9YzjnofEsNfd5NyWWnCwVTGgF35/CoPbK9ykMXt5nDEQpWIaC1aZwnlvqnrXvLq7YV2Z2CDesijUMCLv0FNxQYgQWkWIl3fOAfrcq+N46xuJTh8I1XOCBi4nyRqjqEMUZVSyWIQvNlZAI4fr4TIyNTIlh93O12gHJ8AxUluMHkI2V+xwhJmVsviniEAOBtbsr8izuRykM7L7G4lRacl2SizxFiRd1llQUOjSx2BYN9lpVvyzjMkgtxsRiIDbIa0Fd5SFyz6apRXCvwGJV46yBxISFUhPVaeOyJQE/HpcsW8aO2wV8sZqgfhZI6m2w1IVKEbE4l2oRZ55mzTN2dNeZUknMHftZ6GK8iJgaZ6FmZ8il9KIFE+mC0M4VuZv4Ec7u4RlVDMs6N4JQ14urojqrrVXGc8TiU+XA4+yTqCJRgnt8RhU9CGAb4mLljAVJbArZlk9pHgYOmznUoIpeEffd4R16/+zo8p7Sj4EriplIkDxfEtklfqfbfkugOh+cVTM/spd7dXUeTB4h2duRwkneklhc6vvVthDq78dAFYnLPkMb5J6AqbO2Eivb4k40cr8i4rGNS/G6R/08O66WH3QuXPa0JS6RW9dVsO6z7baDk8WoJle51om7f5Yxbill706Mm/qjIC6KweoCU1c/lq7YWnU0IUY/K7H3GgTj6ih/oiKq+F+oqk5IyZOOK+MriI98WPnNjJggF5C/Hyg0Faf/edj8M4cn6mlZ/rgRuBe9XqLm7NChw3+GPwIMAA139Xg+K8vEAAAAAElFTkSuQmCC&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss11669 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABANJREFUeNrsWt1x4jAQNpl7P18FcSqIqSBmhvdABYEKiCvwUQFcBfZVEHhnBl8F8VUQSvB1cJJ3jddC/pOlQBg0YwYUIfbT/n27imXdxtcag947+DuPvXqa5TpYq3F0PmD+bsZeQ0OHvmbg/HMB+2CvDntS9iSaADn48PGDgUtVNvmmQQg+fjEBfmqBBaa9x08ue2KVbe6uNXhcOrDDdQJbjQ/XqjHrBuxLAYMIeVZg9xrhuJegsTx5ztjphuyxe2qI58LVJURFSnk4vfpgwr0qmRywmECgVMrAdJBgB095QmaTDPRqHBv7rnFgZUcPCc3igzP0pfTkwewW7LGJWfO168soW9oJXHDJ6gPwVQnv5wCrNrEDgnSFuXlfszPB7utYfw7EFioBOmLy9wuroE8Lz6ACRBPZXfatmvUD83cu+o0r5DiukT+SIpSve0RTtU2Y5kCTlkLl05dreX7unkeIiTkfS+VKGhJ7QDQYsb3mnw+srCludiMmSNKwPq5lE2DSewLOV81rA0VQtC/RFlSIax9q89UpuJGKz6lyRepT9aBg5BzQFnKbrGpOsj0Lgq3U3rtTNEGH+FTSImLSwPDSoiWQZHvn+Q9+07jGgmP0axcoRCAeMpMmcGtStgRmgfm7iUV7ie3GRKjdrEZzpFG20Nqki6hdKdWTQFxFfuhIErFDhHzBuQVbn0i0JAaJDfGxJ/xsBJh35Hh5ZANA7y0430YAu5dYBKxbjacINGVzG9SwZ9LHXFIM0tEEKs9fG8EkZUPc669KL0Q13P8j5gP8riwwfz/FsM3TwYisfTjOFz5Eq+epZM5guIewLf8x4HUjMm8ffUP0G25eMOcJ0S7CQ0grGkaiDNo0lta2yIrEGhFwe6kwwDEDsu8844XNFXSqH1ibjhFoYy6YmFNRiFLmErUIWJ16+V2jYl4RPzas+14KHNBvnGWmCma47RDl7rtqSyV4xCenWJ+UEzTbd+yBcNN8s8qXeU0Uy1MJIl2BbY/+U8UEytzQxnzlCKD3UlOr32trEhjN/ItGnyiESjElrCW5yq3hjouK39YMDKJWRMis7LSfJeY7zAIE/BfAVOIvXoW2ZqSaPpjUWM75ilrp9CLCJVrys+RMhVqNN5ikNw3MJZSQYYPAQMglMTWxEMzD/bCyrIe0MM3WwNpIkudcUvMduorZp+fxRqJfr8ZLTYOIp4ehyjZ9OsFz1Bj4AgSAqXL/HUw6FFLFSFU49fsxAED5IQ8Aqvdjs+y7IqgelxS6OsE8+b4KDCXKck9Vhwki6jOCoeF+jX7V6+ZF9/1YUJFwExJJ7YraKkZAsQ5xTNyPeUiTJla7mxSu2d+6r5LM3I+VQboSgCkhxLdxG2z8F2AAPtd0UKyJkPIAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss11670 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAzRJREFUeNrsWs2Nm0AUnl1RAFIKCK4gdLBwy9GuYL0VePeWm+1bbt5UYLsC7FtueCswqWDdQCTSQd5bfRM9IcADDD+OeNIIbI8f8837f4NSI4000kg16K7uHz99++nTZUkj/f3965PNRRHvFV2+0FgT76QOj/sGz2dQUxpzWkhgEVQgeC/r8mkCzO1Ao9w+gEnyb8bGSCU8qEPRri3Ebycab5bW9JnVG/cXGvuCeSmNA9ngxRgYgeIFv3ekbk2IwU0IXGqqiv4NgNI2mGsGjsGfi9ThkYZXoIoPDRbrCb4siaTOxhsBI1GvctT1QSzgLW9OA3cf42NCfMOcOfx70JZXlDt5atGGapHT4KFrGn+QeVgDxrxIIi9QtV3nwOCJVm2IiXi/NuXhVNB9T8SXV+1iYROB6pY8mxKbi9yNPeKEQHEAj4YYB6oAk+qxg8ocCFzYg8Qer0nNaWpTcBynLlFlQk2rSfDg6L7DXQ5s1m2DAIZqm7OFGPf/jcS2Bfe3Cwz9C1+kRz6+u11gCOo69v1AGvZRpOK32wMGRxGJLH2FWJggD4zadCaOZelMUYsFol5iIKGOhQjoMdSTnckFc44cD4tK/V4kRouL0ErYZPokLKFQlu64D0UmozeEnco78doMSWJuJvX6hUZLWpLFvBCINUBxc/TZNMHt0saehNcLykDlADyIXDMFr2EAg13MRD+iSgd3I8LBxGRDOvWKSIY1uGcTd44sRNd4M1ugrLt7LmNEOT83LD8UvKHVCqGNOHbE1aQFp/PG/WDjmKAq6qSBGcUuqHfQF7CiLOQfcHHmlapqHWeev1AGhyBtANMPdQnQVuUcbND3KXLH1HBjPjIa2hCOfRwOzn3YmCsAzjOpVSLmLMUmXPOg7Fi4UN1C2qe+VZE95BHeMiuBhbAX91og1zkmXc992ZjOAXdFCS2AHhDHpqqk4yuObmfIMWMTG7srYRaLGBP22ZgRUtLrkGd3YV4MdNRACa49AhgtqRj3yTW3bwLM66KUL/GwZwTwNUqb2JaNearBawkWKPt8oy5XkbtPVIOzqQ6p6MSz9K0BX5T6QyQGtK/75s5II400Ui79FWAANjYkpEJzM+AAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss11671 {
  color: #FFF;
  padding: 48px;
}
.jss11672 {
  color: rgba(0, 0, 0, 0.87);
  width: 100%;
  display: flex;
  z-index: 2;
  opacity: 0.5;
  position: fixed;
  align-items: center;
  padding-left: 16px;
  padding-right: 16px;
  background-color: #424242;
}
.jss11673 {
  height: 100vh;
  display: flex;
  position: relative;
  max-height: 1600px;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: linear-gradient(rgba(0,130,170,0), #424242), url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/1.20e79364.jpg&quot;);
}
.jss11674 {
  margin: -200px auto 0 auto;
  padding: 64px 0 48px;
  max-width: 1200px;
}
.jss11675 {
  width: 50%;
  color: #FFF;
}
@media (max-width:1279.95px) {
  .jss11675 {
    width: 100%;
    padding: 24px;
  }
}
.jss11676 {
  margin: -150px auto 0 auto;
  padding: 16px;
  position: relative;
  max-width: 1200px;
  background: #fafafa;
  justify-content: center;
}
.jss11677 {
  margin: 0 auto;
  padding: 24px;
  display: flex;
}
.jss11678 {
  margin-bottom: 24px;
}
.jss11679 {
  text-decoration: none;
}
.jss11680 {
  margin: 24px auto;
  max-width: 1200px;
}
@media (max-width:1279.95px) {
  .jss11680 {
    max-width: 1366px;
  }
}
.jss11681 {
  height: 160px;
  display: flex;
  align-items: center;
}
.jss11682 {
  margin: auto;
  display: grid;
}
.jss11683 {
  height: 100vh;
  display: flex;
  position: relative;
  overflow: hidden;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/2.adfa400c.jpg&quot;);
  background-position: center;
}
.jss11684 {
  color: #FFF;
  background-color: #03a9f4;
}
.jss11685 {
  color: #03a9f4;
  background-color: #fff;
}
.jss11686 {
  color: #03a9f4;
  padding: 0;
}
.jss11687 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss11687 {
    width: calc(100%);
  }
}
.jss11688 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss11688 {
    width: calc(100% - 300px);
  }
}
.jss11689 {
  display: none;
}
@media (min-width:600px) {
  .jss11689 {
    display: block;
  }
}
.jss11690 {
  color: #fff;
  position: relative;
  background: #03a9f4;
}
.jss11691 {
  width: 100%;
  position: relative;
  border-radius: 14px;
  background-color: rgba(255, 255, 255, 0.5);
}
.jss11691:hover {
  background-color: rgba(255, 255, 255, 0.7);
}
@media (min-width:600px) {
  .jss11691 {
    width: auto;
  }
}
.jss11692 {
  width: 72px;
  color: #212121;
  height: 100%;
  display: flex;
  position: absolute;
  align-items: center;
  pointer-events: none;
  justify-content: center;
}
.jss11693 {
  color: #fff;
  width: 100%;
}
.jss11694 {
  color: #212121;
  width: 100%;
  transition: width 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  padding-top: 8px;
  padding-left: 64px;
  padding-right: 8px;
  padding-bottom: 8px;
}
@media (min-width:600px) {
  .jss11694 {
    width: 80px;
  }
  .jss11694:focus {
    width: 200px;
  }
}
.jss11695 {
  color: #212121;
}
.jss11696 {
  background-color: #4fc3f7;
}
.jss11697 {
  margin-left: 12px;
  margin-right: 36px;
}
.jss11698 {
  display: none;
}
@media (min-width:960px) {
  .jss11699 {
    display: none;
  }
}
.jss11700 {
  width: 300px;
}
.jss11701 {
  display: flex;
  padding: 0 8px;
  min-height: 56px;
  align-items: center;
}
@media (min-width:0px) and (orientation: landscape) {
  .jss11701 {
    min-height: 48px;
  }
}
@media (min-width:600px) {
  .jss11701 {
    min-height: 64px;
  }
}
.jss11702 {
  margin-top: 65px;
  justify-content: flex-start;
}
.jss11703 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss11705 {
  width: 100%;
  height: 100%;
  opacity: 0.2;
  content: &quot;&quot;;
  display: block;
  position: absolute;
  background: linear-gradient(rgba(0,130,170,0), #03a9f4));
  background-size: cover;
  background-position: center center;
}
.jss11706 {
  width: 60px;
  overflow-x: hidden;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss11707 {
  color: inherit;
}
.jss11708 {
  margin-left: 56px;
}
.jss11709 {
  bottom: 16px;
}
.jss11710 {
  color: rgba(0, 0, 0, 0.87);
  background-size: cover;
  background-color: #fff;
  background-image: url(https://xdev.equine.co.id/apps/tessa/static/media/pattern2.f069796d.svg);
  background-repeat: no-repeat;
  background-position: inherit;
}
.jss11711 {
  color: #fff;
  background-color: #03a9f4;
}
.jss11712 {
  color: #fff;
  background-color: #ff9800;
}
.jss11713 {
  color: #fff;
  background-color: #f44336;
}
.jss11714 {
  color: #fff;
  background: #4caf50;
}
.jss11715 {
  flex: 1;
}
.jss11716 {
  padding: 16px;
  flex-grow: 1;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss11716 {
    margin-top: 68px;
  }
}
@media (max-width:959.95px) {
  .jss11716 {
    padding: 0;
    margin-top: 56px;
  }
}
@media (max-width:959.95px) {
  .jss11717 {
    margin-top: 68px;
  }
}
@media (min-width:960px) {
  .jss11717 {
    left: 50%;
    width: 982px;
    position: absolute;
    transform: translateX(-50%);
  }
}
@media (min-width:960px) {
  .jss11718 {
    margin-left: 300px;
    margin-right: 0;
  }
}
.jss11719 {
  margin-bottom: 56px;
}
.jss11720 {
  width: calc(100% - 0px);
  right: 0;
  bottom: 0;
  display: flex;
  position: fixed;
}
@media (min-width:960px) {
  .jss11720 {
    width: calc(100% - 300px);
  }
}
.jss11722 {
  background: #fff;
}
.jss11723 {
  margin-bottom: 60px;
}
.jss11724 {
  height: 4px;
}
.jss11725 {
  display: flex;
  align-items: center;
  justify-content: space-between;
}
.jss11726 {
  margin: 0 15px 0 0;
  padding: 16px 0;
  flex-grow: 1;
}
@media (min-width:600px) {
  .jss11726 {
    width: 30%;
    flex-grow: 0;
  }
}
.jss11727 {
  color: inherit;
  font-size: 0.6964285714285714rem;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  font-weight: inherit;
  line-height: 1.66;
  letter-spacing: 0.03333em;
}
.jss11728 {
  color: #fff;
  background: #03a9f4;
}
.jss11729 {
  color: #fff;
  min-height: 40px;
  background: #ff9800;
  margin-bottom: 16px;
}
.jss11730 {
  height: 500px;
  display: flex;
  overflow: hidden;
  align-items: center;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss11730 {
    height: 300px;
  }
}
.jss11731 {
  height: 100%;
  margin: 0 auto;
}
.jss11732 {
  width: 25px;
  height: 25px;
  margin-right: 16px;
}
.jss11733 {
  transform: rotate(0deg);
  transition: transform 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
}
.jss11734 {
  transform: rotate(180deg);
}
.jss11735 {
  min-width: 350px;
}
.jss11736 {
  display: flex;
  flex-wrap: wrap;
}
.jss11737 {
  padding: 0;
}
@media (min-width:0px) {
  .jss11737 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss11737 {
    width: calc(100% / 2);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1280px) {
  .jss11737 {
    width: calc(100% / 3);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1920px) {
  .jss11737 {
    width: calc(100% / 4);
    padding: 0 16px 16px 0;
  }
}
.jss11738 {
  padding: 0;
}
@media (min-width:0px) {
  .jss11738 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss11738 {
    width: calc(100% / 2);
  }
}
@media (min-width:1280px) {
  .jss11738 {
    width: calc(100% - (100% / 3));
  }
}
@media (min-width:1920px) {
  .jss11738 {
    width: calc(100% - (100% / 4));
  }
}
.jss11739 {
  padding: 0;
}
@media (min-width:0px) {
  .jss11739 {
    width: 100%;
  }
}
.jss11740 {
  width: 100%;
  margin-bottom: 16px;
  vertical-align: middle;
}
.jss11741 {
  right: 16px;
  display: flex;
  position: absolute;
}
.jss11742 {
  right: 16px;
  bottom: 16px;
  position: fixed;
}
.jss11743 {
  float: right;
}
.jss11744 {
  margin: 0;
}
.jss11745 {
  margin: 8px;
}
.jss11746 {
  margin-left: 8px;
}
.jss11747 {
  margin-top: 8px;
}
.jss11748 {
  margin-right: 8px;
}
.jss11749 {
  margin-bottom: 8px;
}
.jss11750 {
  margin: 16px;
}
.jss11751 {
  margin-left: 16px;
}
.jss11752 {
  margin-top: 16px;
}
.jss11753 {
  margin-right: 16px;
}
.jss11754 {
  margin-bottom: 16px;
}
.jss11755 {
  margin: 24px;
}
.jss11756 {
  margin-left: 24px;
}
.jss11757 {
  margin-top: 24px;
}
.jss11758 {
  margin-right: 24px;
}
.jss11759 {
  margin-bottom: 24px;
}
.jss11760 {
  padding: 0;
}
.jss11761 {
  padding: 8px;
}
.jss11762 {
  padding-left: 8px;
}
.jss11763 {
  padding-top: 8px;
}
.jss11764 {
  padding-right: 8px;
}
.jss11765 {
  padding-bottom: 8px;
}
.jss11766 {
  padding: 16px;
}
.jss11767 {
  padding-left: 16px;
}
.jss11768 {
  padding-top: 16px;
}
.jss11769 {
  padding-right: 16px;
}
.jss11770 {
  padding-bottom: 16px;
}
.jss11771 {
  padding: 24px;
}
.jss11772 {
  padding-left: 24px;
}
.jss11773 {
  padding-top: 24px;
}
.jss11774 {
  padding-right: 24px;
}
.jss11775 {
  padding-bottom: 24px;
}
.jss11776 {
  background-color: #03a9f4;
}
.jss11777 {
  background-color: #ff9800;
}
.jss11778 {
  color: white;
  background-color: #f44336;
}
.jss11779 {
  color: #03a9f4;
}
.jss11780 {
  color: #ff9800;
}
.jss11781 {
  color: #f44336;
}
.jss11782 {
  text-decoration: line-through;
}
.jss11783 {
  padding: 3px;
}
.jss11784 {
  height: calc(100vh - 96px - 72px);
  overflow-x: auto;
}
.jss11785 {
  width: calc(100vw - 32px - 300px);
}
.jss11786 {
  height: calc(100vh - 56px - 68px);
  overflow-x: auto;
}
.jss11787 {
  width: calc(100vw);
}
.jss11788 {
  overflow-x: auto;
}
.jss11789 {
  table-layout: initial;
}
.jss11790 {
  top: 0;
  position: sticky;
  background-color: #fff;
}
.jss11791 {
  width: 3vw;
}
.jss11792 {
  width: 5vw;
}
.jss11793 {
  width: 10vw;
}
.jss11794 {
  width: 15vw;
}
.jss11795 {
  min-width: 15vw;
  max-width: 15vw;
}
@media (min-width:960px) {
  .jss11795 {
    min-width: calc((100vw - 300px - 48px) * 0.15);
    max-width: calc((100vw - 300px - 48px) * 0.15);
  }
}
.jss11796 {
  min-width: 25vw;
  max-width: 25vw;
}
@media (min-width:960px) {
  .jss11796 {
    min-width: calc((100vw - 300px - 48px) * 0.25);
    max-width: calc((100vw - 300px - 48px) * 0.25);
  }
}
.jss11797 {
  width: 25vw;
}
.jss11798 {
  width: 30vw;
}
.jss11799 {
  min-width: 45vw;
  max-width: 45vw;
}
@media (min-width:960px) {
  .jss11799 {
    min-width: calc((100vw - 300px - 48px) * 0.45);
    max-width: calc((100vw - 300px - 48px) * 0.45);
  }
}
.jss11800 {
  width: 100%;
  overflow-x: auto;
}
.jss11801 {
  width: 100%;
  margin-top: 24px;
  overflow-x: auto;
}
.jss11802 {
  min-width: 700px;
}
.jss11803 {
  width: 100%;
}
.jss11804 {
  min-width: 100px;
}
.jss11805 {
  color: rgba(0, 0, 0, 0.54);
  flex-shrink: 0;
}
.jss11806 {
  color: #2196f3;
}
.jss11807 {
  padding-top: 16px;
  padding-bottom: 16px;
}
.jss11809 {
  max-height: 100px;
}
.jss11810 {
  margin-top: -144px;
  margin-left: -56px;
  margin-right: -136px;
  margin-bottom: -64px;
}
.jss11811 {
  margin-top: -80px;
}
.jss11812 {
  width: 500px;
  height: 450px;
}
.jss11813 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss11814 {
  display: flex;
  box-sizing: border-box;
  align-items: center;
}
.jss11816:hover {
  background-color: #eeeeee;
}
.jss11817 {
  flex: 1;
}
.jss11818 {
  cursor: initial;
}
.jss11819 {
  background: #f44336;
}
.jss11820 {
  color: #fff;
}
.jss11821 {
  width: 100%;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
.jss11822 {
  max-height: 75vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss11823 {
  padding: 10px;
  text-align: center;
}
.jss11824 {
  padding: 0;
  vertical-align: top;
}
.jss11825 {
  margin-top: 8px;
  padding-left: 32px;
}
.jss11826 {
  top: 0;
  padding: 5px;
  position: sticky;
  overflow: hidden;
  max-width: 100px;
  max-height: 10px;
  text-align: center;
  background-color: #fff;
}
.jss11827 {
  max-height: 76vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss11828 {
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss11829 {
  padding: 0;
  margin-top: 8px;
}
.jss11830 {
  left: 0;
  width: 500px;
  z-index: 1;
  padding: 8px 16px 0 16px;
  min-width: 300px;
  background: #fff;
  vertical-align: top;
}
.jss11831 {
  padding: 8px 16px 0 16px;
  min-width: 300px;
  vertical-align: top;
}
.jss11832 {
  min-width: 300px;
}
.jss11833 {
  transform: rotate(180deg);
  writing-mode: vertical-rl;
}
.jss11834 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss11835 {
  cursor: pointer;
  transition: all .3s;
}
.jss11835:hover {
  background-color: #ebebeb;
}
.jss11836 {
  background-color: #ebebeb;
}
.jss11837 {
  transition: all .3s;
}
.jss11837:hover {
  transform: scale(1.03);
}
.jss11838 {
  padding: 0;
  transition: all .3s;
}
.jss11838:hover {
  transform: scale(1.03);
}
.jss11839 {
  display: grid;
  align-items: center;
  grid-column-gap: 1.5rem;
  grid-template-columns: 1fr max-content 1fr;
}
.jss11839::after, .jss11839::before {
  height: 1px;
  content: '';
  display: block;
  background-color: currentColor;
}
.jss11840 a {
  text-decoration: none;
}
.jss11841 {
  left: 50%;
  position: absolute;
  transform: ;
}
.jss11842 {
  display: -webkit-box;
  overflow: hidden;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;
}
.jss11843 {
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
.jss11844 {
  display: inline;
  text-transform: capitalize;
}
.jss11845 {
  text-decoration: none;
}
.jss11845 p:hover {
  color: #03a9f4;
}
.jss11846 {
  margin-left: 4px;
}
.jss11847 {
  padding: 0 16px;
  display: inline-flex;
  position: relative;
  vertical-align: middle;
}
.jss11848 {
  top: 0;
  color: #fff;
  right: 0;
  height: 20px;
  display: flex;
  padding: 0 4px;
  z-index: 1;
  position: absolute;
  flex-wrap: wrap;
  min-width: 20px;
  transform: translate(20px, -50%);
  box-sizing: border-box;
  transition: transform 225ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  align-items: center;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  align-content: center;
  border-radius: 10px;
  flex-direction: row;
  justify-content: center;
  background-color: #ff9800;
  transform-origin: 100% 0%;
}
.jss11849 {
  margin: 0;
  padding: 16px;
  border-bottom: 1px solid rgba(0, 0, 0, 0.12);
}
.jss11850 {
  top: 8px;
  right: 8px;
  color: #9e9e9e;
  position: absolute;
}
.jss11851 {
  margin: 0;
  padding: 16px;
}
.jss11852 {
  margin: 0;
  padding: 8px;
  border-top: 1px solid rgba(0, 0, 0, 0.12);
}
.jss11853 {
  font-size: 13px;
}
.jss11854 {
  padding: 0;
  overflow: hidden;
}
.jss11854 span {
  display: inline-flex;
}
.jss11854 span:hover {
  position: relative;
  animation: moveTextHorz 3s linear infinite;
}
@-webkit-keyframes moveTextHorz {
  0% {
    transform: translate(0, 0);
  }
  100% {
    transform: translate(-70%, 0);
  }
}
.jss11855 span {
  color: #f44336;
}
.jss11856 {
  width: 50px;
}
@media (min-width:600px) {
  .jss11856 {
    right: 90px;
  }
}
@media (min-width:960px) {
  .jss11856 {
    right: 15px;
  }
}
.jss11857 {
  cursor: pointer;
}
.jss11857:hover {
  background: #0288d1;
}
.jss11858 {
  margin-left: 10px;
  vertical-align: middle;
}
.jss11859 {
  width: 100%;
}
.jss11860 {
  width: 100%;
  margin: 0;
}
@media (min-width:960px) {
  .jss11861 {
    margin-left: auto;
    margin-right: 16px;
  }
}
@media (min-width:1280px) {
  .jss11861 {
    margin-left: auto;
    margin-right: 32px;
  }
}
@media (min-width:960px) {
  .jss11862 {
    transform: translateY(75%);
    margin-left: 16px;
  }
}
@media (min-width:1280px) {
  .jss11862 {
    transform: translateY(75%);
    margin-left: 32px;
  }
}
.jss11863 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
@media (max-width:1279.95px) {
  .jss11864 {
    display: none;
  }
}
@media (max-width:1919.95px) {
  .jss11865 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss11866 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss11867 {
    text-align: right;
  }
}
.jss11868 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss11868 {
    text-align: right;
  }
}
.jss11869 {
  padding: 0;
}
@media (min-width:1280px) {
  .jss11870 {
    margin-top: 0 !important;
  }
}
@media (min-width:1280px) {
  .jss11871 {
    margin-top: 64px;
  }
}
.jss11872 div textarea {
  overflow: auto;
}
.jss11873 {
  width: 70%;
}
@media (min-width:1280px) {
  .jss11873 {
    float: right;
  }
}
@media (min-width:1280px) {
  .jss11874 {
    float: right;
  }
}
@media (max-width:1279.95px) {
  .jss11874 {
    margin-top: 6px;
  }
}
@media (min-width:1280px) {
  .jss11875 {
    padding: 12px 0;
  }
}
@media (max-width:1279.95px) {
  .jss11875 {
    padding: 6px 0 3px;
  }
}
.jss11876 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss11876 {
    margin-top: 0 !important;
  }
}
.jss11876 div textarea {
  overflow: auto;
}
.jss11877 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss11878 {
  width: 57px;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
  overflow-x: hidden;
}
@media (min-width:600px) {
  .jss11878 {
    width: 73px;
  }
}
@media (min-width:960px) {
  .jss11879 {
    width: calc(100% - 73px);
    margin-left: 73px;
    margin-right: 0;
  }
}
@media (min-width:960px) {
  .jss11880 {
    margin-left: 73px;
    margin-right: 0;
  }
}
.jss11881 {
  bottom: 0px;
  z-index: 1;
  position: sticky;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss11881 {
    display: none;
  }
}
.jss11881:hover {
  background-color: rgb(235, 235, 235);
}
@media (max-width:959.95px) {
  .jss11882 {
    display: none;
  }
}
.jss11883 {
  top: 0;
  left: auto;
  width: 100%;
  right: 0;
  height: 100%;
  z-index: 1301;
  overflow: hidden;
  position: absolute;
  overflow-y: auto;
  transition: transform 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
@media (min-width:960px) {
  .jss11883 {
    width: 24vw;
  }
}
.jss11884 {
  cursor: pointer;
}
.jss11884:hover {
  background-color: #eeeeee;
}
.jss11885 {
  background-color: #e0e0e0 !important;
}
.jss11886 {
  transition: width 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
.jss11887 {
  border-collapse: separate;
}
.jss11888:hover {
  background-color: rgb(235, 235, 235);
}
.jss11889 {
  position: relative;
  min-height: 80vh;
}
.jss11889 .back-icon {
  top: 5px;
  left: 5px;
  z-index: 1;
  position: absolute;
}
.jss11889 > .ninebox-inner {
  width: 100%;
  position: relative;
  min-height: 80vh;
}
.jss11889 > .ninebox-inner .category-paper {
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #424242;
}
.jss11889 > .ninebox-inner .percentage-wrapper {
  width: 100%;
  height: 100%;
  display: flex;
  flex-wrap: wrap;
  box-sizing: border-box;
}
.jss11889 > .ninebox-inner .horizontal-category {
  height: 50px;
  margin: 0 60px;
  display: flex;
  align-items: center;
  justify-content: center;
}
.jss11889 > .ninebox-inner .vertical-category {
  width: 50px;
  height: calc(80vh - 120px);
  display: inline-flex;
  align-items: center;
  justify-content: center;
}
.jss11889 > .ninebox-inner .ninebox-content {
  width: calc(100% - 100px);
  height: calc(80vh - 120px);
  display: inline-block;
  position: relative;
}
.jss11889 > .ninebox-inner .ninebox-content .wrapper {
  width: 100%;
  height: 100%;
  margin: 0;
}
.jss11889 > .ninebox-inner .ninebox-content .wrapper .box {
  height: calc(100% / 3);
  padding: 10px;
}
.jss11889 > .ninebox-inner .ninebox-content .wrapper .box .has-item {
  padding: 26px 10px !important;
}
.jss11889 > .ninebox-inner .ninebox-content .wrapper .box .disabled {
  background: #d1d1d1;
}
.jss11889 > .ninebox-inner .ninebox-content .wrapper .box .card {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 30px;
  position: relative;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  -webkit-transition: all .4s ease-out;
}
.jss11889 > .ninebox-inner .ninebox-content .wrapper .box .card h6 {
  color: rgba(0, 0, 0, 0.87);
}
.jss11889 > .ninebox-inner .ninebox-content .wrapper .box .card .total-person {
  top: 5px;
  right: 5px;
  width: 15px;
  height: 15px;
  position: absolute;
}
.jss11889 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list {
  width: 100%;
  height: 100%;
  display: flex;
  overflow-y: auto;
  align-items: center;
}
.jss11889 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list > ol {
  height: 100%;
  margin: 0;
  padding: 0;
  text-align: left;
  list-style: none;
}
.jss11889 > .ninebox-inner .ninebox-content .wrapper .box .active:hover {
  cursor: pointer;
  transform: scale(1.1);
}
.jss11889 > .ninebox-inner .vertical-category .text-rotate {
  transform: rotate(270deg);
}
.jss11889 > .ninebox-inner .vertical-category.left {
  padding: 10px 0;
  margin-right: 9px;
}
.jss11889 > .ninebox-inner .vertical-category.right {
  margin-left: 9px;
}
.jss11889 > .ninebox-inner .vertical-category .text-rotate.nowrap {
  white-space: nowrap;
}
.jss11889 > .ninebox-inner .horizontal-category.top {
  padding: 0 9px;
  margin-bottom: 9px;
}
.jss11889 > .ninebox-inner .horizontal-category.bottom {
  margin-top: 9px;
}
.jss11889 > .ninebox-inner .percentage-wrapper > .percentage-item {
  padding: 0 10px;
  flex-grow: 0;
  max-width: calc(100% / 3);
  flex-basis: calc(100% / 3);
}
.jss11889 > .ninebox-inner .percentage-wrapper > .percentage-item.horizontal {
  height: calc(100% / 3);
  padding: 10px 0;
  max-width: 100%;
}
.jss11889 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 10px;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  background-color: #424242;
  -webkit-transition: all .4s ease-out;
}
.jss11889 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper > p {
  color: #fff;
}
.jss11889 > .ninebox-inner .category-paper > p {
  color: #fff;
}
.jss11890 {
  max-height: 45vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss11890 .table {
  border-collapse: separate;
}
.jss11890 .table .sticky-head {
  top: 0;
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss11891 {
  height: 44vh;
  overflow: auto;
}
.jss11892 {
  width: 100%;
  height: 50vh;
  overflow: auto;
}
.jss11893 {
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss11894 {
  width: 100%;
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss11895 {
  max-width: 82%;
}

@media (min-width:960px) {
  ::-webkit-scrollbar {
    width: 5px;
    height: 5px;
  }
  ::-webkit-scrollbar-track {
    background: rgba(250, 250, 250, 0);
  }
  ::-webkit-scrollbar-thumb {
    background: #eeeeee;
  }
  ::-webkit-scrollbar-thumb:hover {
    background: #e0e0e0;
  }
}
.jss11200 {
  width: 100%;
  z-index: 1;
  overflow: hidden;
  margin-bottom: 0;
}
.jss11201 {
  background-color: #424242;
}
.jss11202 {
  height: 70px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/etg.9362c8fb.svg&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss11203 {
  height: 150px;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/tessa.55f0b81f.svg&quot;);
  background-repeat: no-repeat;
}
.jss11204 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2hJREFUeNrsWtGR2jAQ1TEU4FQQUsGRCuBm+IerAKgguAKgAnMVHFcBvn/PnFwBvg6cCuIOEslacRvFyJJswL74zQgDNrKedvftSoaQDh06dHDAnfGVfjRjr0PWRg3kkbD2RIJJakfMjzbsdd0CQz0yciF/0zf8wRyOKbSmgXuSx1rAmhWxgbSdnJFG4cOj5DhJz7KLrC3i0a8wSwM8QzcRjGCS1U+MkLcbE9uztjQn5kcyEFMsn5q4uxW09+8hQh5rR/buCNY4OtxsBzMpEaLZ/cIm6g6uyZTrtih+d6C8/PxDLuEiT1mhp7jWEH32HIi95sr5kRKe4PiTtRWbuN/5UfS9hXOUEd6gSXgBa/gwpoMyLiMh6ym5gJxmSsyuCxaK71OUAyka0AC+i8H9YzjnofEsNfd5NyWWnCwVTGgF35/CoPbK9ykMXt5nDEQpWIaC1aZwnlvqnrXvLq7YV2Z2CDesijUMCLv0FNxQYgQWkWIl3fOAfrcq+N46xuJTh8I1XOCBi4nyRqjqEMUZVSyWIQvNlZAI4fr4TIyNTIlh93O12gHJ8AxUluMHkI2V+xwhJmVsviniEAOBtbsr8izuRykM7L7G4lRacl2SizxFiRd1llQUOjSx2BYN9lpVvyzjMkgtxsRiIDbIa0Fd5SFyz6apRXCvwGJV46yBxISFUhPVaeOyJQE/HpcsW8aO2wV8sZqgfhZI6m2w1IVKEbE4l2oRZ55mzTN2dNeZUknMHftZ6GK8iJgaZ6FmZ8il9KIFE+mC0M4VuZv4Ec7u4RlVDMs6N4JQ14urojqrrVXGc8TiU+XA4+yTqCJRgnt8RhU9CGAb4mLljAVJbArZlk9pHgYOmznUoIpeEffd4R16/+zo8p7Sj4EriplIkDxfEtklfqfbfkugOh+cVTM/spd7dXUeTB4h2duRwkneklhc6vvVthDq78dAFYnLPkMb5J6AqbO2Eivb4k40cr8i4rGNS/G6R/08O66WH3QuXPa0JS6RW9dVsO6z7baDk8WoJle51om7f5Yxbill706Mm/qjIC6KweoCU1c/lq7YWnU0IUY/K7H3GgTj6ih/oiKq+F+oqk5IyZOOK+MriI98WPnNjJggF5C/Hyg0Faf/edj8M4cn6mlZ/rgRuBe9XqLm7NChw3+GPwIMAA139Xg+K8vEAAAAAElFTkSuQmCC&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss11205 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABANJREFUeNrsWt1x4jAQNpl7P18FcSqIqSBmhvdABYEKiCvwUQFcBfZVEHhnBl8F8VUQSvB1cJJ3jddC/pOlQBg0YwYUIfbT/n27imXdxtcag947+DuPvXqa5TpYq3F0PmD+bsZeQ0OHvmbg/HMB+2CvDntS9iSaADn48PGDgUtVNvmmQQg+fjEBfmqBBaa9x08ue2KVbe6uNXhcOrDDdQJbjQ/XqjHrBuxLAYMIeVZg9xrhuJegsTx5ztjphuyxe2qI58LVJURFSnk4vfpgwr0qmRywmECgVMrAdJBgB095QmaTDPRqHBv7rnFgZUcPCc3igzP0pfTkwewW7LGJWfO168soW9oJXHDJ6gPwVQnv5wCrNrEDgnSFuXlfszPB7utYfw7EFioBOmLy9wuroE8Lz6ACRBPZXfatmvUD83cu+o0r5DiukT+SIpSve0RTtU2Y5kCTlkLl05dreX7unkeIiTkfS+VKGhJ7QDQYsb3mnw+srCludiMmSNKwPq5lE2DSewLOV81rA0VQtC/RFlSIax9q89UpuJGKz6lyRepT9aBg5BzQFnKbrGpOsj0Lgq3U3rtTNEGH+FTSImLSwPDSoiWQZHvn+Q9+07jGgmP0axcoRCAeMpMmcGtStgRmgfm7iUV7ie3GRKjdrEZzpFG20Nqki6hdKdWTQFxFfuhIErFDhHzBuQVbn0i0JAaJDfGxJ/xsBJh35Hh5ZANA7y0430YAu5dYBKxbjacINGVzG9SwZ9LHXFIM0tEEKs9fG8EkZUPc669KL0Q13P8j5gP8riwwfz/FsM3TwYisfTjOFz5Eq+epZM5guIewLf8x4HUjMm8ffUP0G25eMOcJ0S7CQ0grGkaiDNo0lta2yIrEGhFwe6kwwDEDsu8844XNFXSqH1ibjhFoYy6YmFNRiFLmErUIWJ16+V2jYl4RPzas+14KHNBvnGWmCma47RDl7rtqSyV4xCenWJ+UEzTbd+yBcNN8s8qXeU0Uy1MJIl2BbY/+U8UEytzQxnzlCKD3UlOr32trEhjN/ItGnyiESjElrCW5yq3hjouK39YMDKJWRMis7LSfJeY7zAIE/BfAVOIvXoW2ZqSaPpjUWM75ilrp9CLCJVrys+RMhVqNN5ikNw3MJZSQYYPAQMglMTWxEMzD/bCyrIe0MM3WwNpIkudcUvMduorZp+fxRqJfr8ZLTYOIp4ehyjZ9OsFz1Bj4AgSAqXL/HUw6FFLFSFU49fsxAED5IQ8Aqvdjs+y7IqgelxS6OsE8+b4KDCXKck9Vhwki6jOCoeF+jX7V6+ZF9/1YUJFwExJJ7YraKkZAsQ5xTNyPeUiTJla7mxSu2d+6r5LM3I+VQboSgCkhxLdxG2z8F2AAPtd0UKyJkPIAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss11206 {
  height: 80px;
  background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA1CAYAAAAK0RhzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAzRJREFUeNrsWs2Nm0AUnl1RAFIKCK4gdLBwy9GuYL0VePeWm+1bbt5UYLsC7FtueCswqWDdQCTSQd5bfRM9IcADDD+OeNIIbI8f8837f4NSI4000kg16K7uHz99++nTZUkj/f3965PNRRHvFV2+0FgT76QOj/sGz2dQUxpzWkhgEVQgeC/r8mkCzO1Ao9w+gEnyb8bGSCU8qEPRri3Ebycab5bW9JnVG/cXGvuCeSmNA9ngxRgYgeIFv3ekbk2IwU0IXGqqiv4NgNI2mGsGjsGfi9ThkYZXoIoPDRbrCb4siaTOxhsBI1GvctT1QSzgLW9OA3cf42NCfMOcOfx70JZXlDt5atGGapHT4KFrGn+QeVgDxrxIIi9QtV3nwOCJVm2IiXi/NuXhVNB9T8SXV+1iYROB6pY8mxKbi9yNPeKEQHEAj4YYB6oAk+qxg8ocCFzYg8Qer0nNaWpTcBynLlFlQk2rSfDg6L7DXQ5s1m2DAIZqm7OFGPf/jcS2Bfe3Cwz9C1+kRz6+u11gCOo69v1AGvZRpOK32wMGRxGJLH2FWJggD4zadCaOZelMUYsFol5iIKGOhQjoMdSTnckFc44cD4tK/V4kRouL0ErYZPokLKFQlu64D0UmozeEnco78doMSWJuJvX6hUZLWpLFvBCINUBxc/TZNMHt0saehNcLykDlADyIXDMFr2EAg13MRD+iSgd3I8LBxGRDOvWKSIY1uGcTd44sRNd4M1ugrLt7LmNEOT83LD8UvKHVCqGNOHbE1aQFp/PG/WDjmKAq6qSBGcUuqHfQF7CiLOQfcHHmlapqHWeev1AGhyBtANMPdQnQVuUcbND3KXLH1HBjPjIa2hCOfRwOzn3YmCsAzjOpVSLmLMUmXPOg7Fi4UN1C2qe+VZE95BHeMiuBhbAX91og1zkmXc992ZjOAXdFCS2AHhDHpqqk4yuObmfIMWMTG7srYRaLGBP22ZgRUtLrkGd3YV4MdNRACa49AhgtqRj3yTW3bwLM66KUL/GwZwTwNUqb2JaNearBawkWKPt8oy5XkbtPVIOzqQ6p6MSz9K0BX5T6QyQGtK/75s5II400Ui79FWAANjYkpEJzM+AAAAAASUVORK5CYII=&quot;);
  background-repeat: no-repeat;
  background-position: center;
}
.jss11207 {
  color: #FFF;
  padding: 48px;
}
.jss11208 {
  color: #fff;
  width: 100%;
  display: flex;
  z-index: 2;
  opacity: 0.5;
  position: fixed;
  align-items: center;
  padding-left: 16px;
  padding-right: 16px;
  background-color: #424242;
}
.jss11209 {
  height: 100vh;
  display: flex;
  position: relative;
  max-height: 1600px;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: linear-gradient(rgba(0,130,170,0), #424242), url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/1.20e79364.jpg&quot;);
}
.jss11210 {
  margin: -200px auto 0 auto;
  padding: 64px 0 48px;
  max-width: 1200px;
}
.jss11211 {
  width: 50%;
  color: #FFF;
}
@media (max-width:1279.95px) {
  .jss11211 {
    width: 100%;
    padding: 24px;
  }
}
.jss11212 {
  margin: -150px auto 0 auto;
  padding: 16px;
  position: relative;
  max-width: 1200px;
  background: #fafafa;
  justify-content: center;
}
.jss11213 {
  margin: 0 auto;
  padding: 24px;
  display: flex;
}
.jss11214 {
  margin-bottom: 24px;
}
.jss11215 {
  text-decoration: none;
}
.jss11216 {
  margin: 24px auto;
  max-width: 1200px;
}
@media (max-width:1279.95px) {
  .jss11216 {
    max-width: 1366px;
  }
}
.jss11217 {
  height: 160px;
  display: flex;
  align-items: center;
}
.jss11218 {
  margin: auto;
  display: grid;
}
.jss11219 {
  height: 100vh;
  display: flex;
  position: relative;
  overflow: hidden;
  align-items: center;
  background-size: cover;
  background-color: #333;
  background-image: url(&quot;https://xdev.equine.co.id/apps/tessa/static/media/2.adfa400c.jpg&quot;);
  background-position: center;
}
.jss11220 {
  color: #FFF;
  background-color: #03a9f4;
}
.jss11221 {
  color: #03a9f4;
  background-color: #fff;
}
.jss11222 {
  color: #03a9f4;
  padding: 0;
}
.jss11223 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss11223 {
    width: calc(100%);
  }
}
.jss11224 {
  color: #fff;
  z-index: 1201;
  background: #03a9f4;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss11224 {
    width: calc(100% - 300px);
  }
}
.jss11225 {
  display: none;
}
@media (min-width:600px) {
  .jss11225 {
    display: block;
  }
}
.jss11226 {
  color: #fff;
  position: relative;
  background: #03a9f4;
}
.jss11227 {
  width: 100%;
  position: relative;
  border-radius: 14px;
  background-color: rgba(255, 255, 255, 0.5);
}
.jss11227:hover {
  background-color: rgba(255, 255, 255, 0.7);
}
@media (min-width:600px) {
  .jss11227 {
    width: auto;
  }
}
.jss11228 {
  width: 72px;
  color: #212121;
  height: 100%;
  display: flex;
  position: absolute;
  align-items: center;
  pointer-events: none;
  justify-content: center;
}
.jss11229 {
  color: #fff;
  width: 100%;
}
.jss11230 {
  color: #212121;
  width: 100%;
  transition: width 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  padding-top: 8px;
  padding-left: 64px;
  padding-right: 8px;
  padding-bottom: 8px;
}
@media (min-width:600px) {
  .jss11230 {
    width: 80px;
  }
  .jss11230:focus {
    width: 200px;
  }
}
.jss11231 {
  color: #212121;
}
.jss11232 {
  background-color: #7986cb;
}
.jss11233 {
  margin-left: 12px;
  margin-right: 36px;
}
.jss11234 {
  display: none;
}
@media (min-width:960px) {
  .jss11235 {
    display: none;
  }
}
.jss11236 {
  width: 300px;
}
.jss11237 {
  display: flex;
  padding: 0 8px;
  min-height: 56px;
  align-items: center;
}
@media (min-width:0px) and (orientation: landscape) {
  .jss11237 {
    min-height: 48px;
  }
}
@media (min-width:600px) {
  .jss11237 {
    min-height: 64px;
  }
}
.jss11238 {
  margin-top: 65px;
  justify-content: flex-start;
}
.jss11239 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss11241 {
  width: 100%;
  height: 100%;
  opacity: 0.2;
  content: &quot;&quot;;
  display: block;
  position: absolute;
  background: linear-gradient(rgba(0,130,170,0), #3f51b5));
  background-size: cover;
  background-position: center center;
}
.jss11242 {
  width: 60px;
  overflow-x: hidden;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss11243 {
  color: inherit;
}
.jss11244 {
  margin-left: 56px;
}
.jss11245 {
  bottom: 16px;
}
.jss11246 {
  color: rgba(0, 0, 0, 0.87);
  background-size: cover;
  background-color: #fff;
  background-image: url(https://xdev.equine.co.id/apps/tessa/static/media/pattern2.f069796d.svg);
  background-repeat: no-repeat;
  background-position: inherit;
}
.jss11247 {
  color: #fff;
  background-color: #03a9f4;
}
.jss11248 {
  color: #fff;
  background-color: #ff9800;
}
.jss11249 {
  color: #fff;
  background-color: #f44336;
}
.jss11250 {
  color: #fff;
  background: #4caf50;
}
.jss11251 {
  flex: 1;
}
.jss11252 {
  padding: 16px;
  flex-grow: 1;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
@media (min-width:960px) {
  .jss11252 {
    margin-top: 68px;
  }
}
@media (max-width:959.95px) {
  .jss11252 {
    padding: 0;
    margin-top: 56px;
  }
}
@media (max-width:959.95px) {
  .jss11253 {
    margin-top: 68px;
  }
}
@media (min-width:960px) {
  .jss11253 {
    left: 50%;
    width: 982px;
    position: absolute;
    transform: translateX(-50%);
  }
}
@media (min-width:960px) {
  .jss11254 {
    margin-left: 300px;
    margin-right: 0;
  }
}
.jss11255 {
  margin-bottom: 56px;
}
.jss11256 {
  width: calc(100% - 0px);
  right: 0;
  bottom: 0;
  display: flex;
  position: fixed;
}
@media (min-width:960px) {
  .jss11256 {
    width: calc(100% - 300px);
  }
}
.jss11258 {
  background: #fff;
}
.jss11259 {
  margin-bottom: 60px;
}
.jss11260 {
  height: 4px;
}
.jss11261 {
  display: flex;
  align-items: center;
  justify-content: space-between;
}
.jss11262 {
  margin: 0 15px 0 0;
  padding: 16px 0;
  flex-grow: 1;
}
@media (min-width:600px) {
  .jss11262 {
    width: 30%;
    flex-grow: 0;
  }
}
.jss11263 {
  color: inherit;
  font-size: 0.75rem;
  font-weight: inherit;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  line-height: 1.375em;
}
.jss11264 {
  color: #fff;
  background: #03a9f4;
}
.jss11265 {
  color: #fff;
  min-height: 40px;
  background: #ff9800;
  margin-bottom: 16px;
}
.jss11266 {
  height: 500px;
  display: flex;
  overflow: hidden;
  align-items: center;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss11266 {
    height: 300px;
  }
}
.jss11267 {
  height: 100%;
  margin: 0 auto;
}
.jss11268 {
  width: 25px;
  height: 25px;
  margin-right: 16px;
}
.jss11269 {
  transform: rotate(0deg);
  transition: transform 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
}
.jss11270 {
  transform: rotate(180deg);
}
.jss11271 {
  min-width: 350px;
}
.jss11272 {
  display: flex;
  flex-wrap: wrap;
}
.jss11273 {
  padding: 0;
}
@media (min-width:0px) {
  .jss11273 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss11273 {
    width: calc(100% / 2);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1280px) {
  .jss11273 {
    width: calc(100% / 3);
    padding: 0 16px 16px 0;
  }
}
@media (min-width:1920px) {
  .jss11273 {
    width: calc(100% / 4);
    padding: 0 16px 16px 0;
  }
}
.jss11274 {
  padding: 0;
}
@media (min-width:0px) {
  .jss11274 {
    width: 100%;
  }
}
@media (min-width:960px) {
  .jss11274 {
    width: calc(100% / 2);
  }
}
@media (min-width:1280px) {
  .jss11274 {
    width: calc(100% - (100% / 3));
  }
}
@media (min-width:1920px) {
  .jss11274 {
    width: calc(100% - (100% / 4));
  }
}
.jss11275 {
  padding: 0;
}
@media (min-width:0px) {
  .jss11275 {
    width: 100%;
  }
}
.jss11276 {
  width: 100%;
  margin-bottom: 16px;
  vertical-align: middle;
}
.jss11277 {
  right: 16px;
  display: flex;
  position: absolute;
}
.jss11278 {
  right: 16px;
  bottom: 16px;
  position: fixed;
}
.jss11279 {
  float: right;
}
.jss11280 {
  margin: 0;
}
.jss11281 {
  margin: 8px;
}
.jss11282 {
  margin-left: 8px;
}
.jss11283 {
  margin-top: 8px;
}
.jss11284 {
  margin-right: 8px;
}
.jss11285 {
  margin-bottom: 8px;
}
.jss11286 {
  margin: 16px;
}
.jss11287 {
  margin-left: 16px;
}
.jss11288 {
  margin-top: 16px;
}
.jss11289 {
  margin-right: 16px;
}
.jss11290 {
  margin-bottom: 16px;
}
.jss11291 {
  margin: 24px;
}
.jss11292 {
  margin-left: 24px;
}
.jss11293 {
  margin-top: 24px;
}
.jss11294 {
  margin-right: 24px;
}
.jss11295 {
  margin-bottom: 24px;
}
.jss11296 {
  padding: 0;
}
.jss11297 {
  padding: 8px;
}
.jss11298 {
  padding-left: 8px;
}
.jss11299 {
  padding-top: 8px;
}
.jss11300 {
  padding-right: 8px;
}
.jss11301 {
  padding-bottom: 8px;
}
.jss11302 {
  padding: 16px;
}
.jss11303 {
  padding-left: 16px;
}
.jss11304 {
  padding-top: 16px;
}
.jss11305 {
  padding-right: 16px;
}
.jss11306 {
  padding-bottom: 16px;
}
.jss11307 {
  padding: 24px;
}
.jss11308 {
  padding-left: 24px;
}
.jss11309 {
  padding-top: 24px;
}
.jss11310 {
  padding-right: 24px;
}
.jss11311 {
  padding-bottom: 24px;
}
.jss11312 {
  background-color: #03a9f4;
}
.jss11313 {
  background-color: #ff9800;
}
.jss11314 {
  color: white;
  background-color: #f44336;
}
.jss11315 {
  color: #3f51b5;
}
.jss11316 {
  color: #f50057;
}
.jss11317 {
  color: #f44336;
}
.jss11318 {
  text-decoration: line-through;
}
.jss11319 {
  padding: 3px;
}
.jss11320 {
  height: calc(100vh - 96px - 72px);
  overflow-x: auto;
}
.jss11321 {
  width: calc(100vw - 32px - 300px);
}
.jss11322 {
  height: calc(100vh - 56px - 68px);
  overflow-x: auto;
}
.jss11323 {
  width: calc(100vw);
}
.jss11324 {
  overflow-x: auto;
}
.jss11325 {
  table-layout: initial;
}
.jss11326 {
  top: 0;
  position: sticky;
  background-color: #fff;
}
.jss11327 {
  width: 3vw;
}
.jss11328 {
  width: 5vw;
}
.jss11329 {
  width: 10vw;
}
.jss11330 {
  width: 15vw;
}
.jss11331 {
  min-width: 15vw;
  max-width: 15vw;
}
@media (min-width:960px) {
  .jss11331 {
    min-width: calc((100vw - 300px - 48px) * 0.15);
    max-width: calc((100vw - 300px - 48px) * 0.15);
  }
}
.jss11332 {
  min-width: 25vw;
  max-width: 25vw;
}
@media (min-width:960px) {
  .jss11332 {
    min-width: calc((100vw - 300px - 48px) * 0.25);
    max-width: calc((100vw - 300px - 48px) * 0.25);
  }
}
.jss11333 {
  width: 25vw;
}
.jss11334 {
  width: 30vw;
}
.jss11335 {
  min-width: 45vw;
  max-width: 45vw;
}
@media (min-width:960px) {
  .jss11335 {
    min-width: calc((100vw - 300px - 48px) * 0.45);
    max-width: calc((100vw - 300px - 48px) * 0.45);
  }
}
.jss11336 {
  width: 100%;
  overflow-x: auto;
}
.jss11337 {
  width: 100%;
  margin-top: 24px;
  overflow-x: auto;
}
.jss11338 {
  min-width: 700px;
}
.jss11339 {
  width: 100%;
}
.jss11340 {
  min-width: 100px;
}
.jss11341 {
  color: rgba(0, 0, 0, 0.54);
  flex-shrink: 0;
}
.jss11342 {
  color: #2196f3;
}
.jss11343 {
  padding-top: 16px;
  padding-bottom: 16px;
}
.jss11345 {
  max-height: 100px;
}
.jss11346 {
  margin-top: -144px;
  margin-left: -56px;
  margin-right: -136px;
  margin-bottom: -64px;
}
.jss11347 {
  margin-top: -80px;
}
.jss11348 {
  width: 500px;
  height: 450px;
}
.jss11349 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss11350 {
  display: flex;
  box-sizing: border-box;
  align-items: center;
}
.jss11352:hover {
  background-color: #eeeeee;
}
.jss11353 {
  flex: 1;
}
.jss11354 {
  cursor: initial;
}
.jss11355 {
  background: #f44336;
}
.jss11356 {
  color: #fff;
}
.jss11357 {
  width: 100%;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
.jss11358 {
  max-height: 75vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss11359 {
  padding: 10px;
  text-align: center;
}
.jss11360 {
  padding: 0;
  vertical-align: top;
}
.jss11361 {
  margin-top: 8px;
  padding-left: 32px;
}
.jss11362 {
  top: 0;
  padding: 5px;
  position: sticky;
  overflow: hidden;
  max-width: 100px;
  max-height: 10px;
  text-align: center;
  background-color: #fff;
}
.jss11363 {
  max-height: 76vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss11364 {
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss11365 {
  padding: 0;
  margin-top: 8px;
}
.jss11366 {
  left: 0;
  width: 500px;
  z-index: 1;
  padding: 8px 16px 0 16px;
  min-width: 300px;
  background: #fff;
  vertical-align: top;
}
.jss11367 {
  padding: 8px 16px 0 16px;
  min-width: 300px;
  vertical-align: top;
}
.jss11368 {
  min-width: 300px;
}
.jss11369 {
  transform: rotate(180deg);
  writing-mode: vertical-rl;
}
.jss11370 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
}
.jss11371 {
  cursor: pointer;
  transition: all .3s;
}
.jss11371:hover {
  background-color: #ebebeb;
}
.jss11372 {
  background-color: #ebebeb;
}
.jss11373 {
  transition: all .3s;
}
.jss11373:hover {
  transform: scale(1.03);
}
.jss11374 {
  padding: 0;
  transition: all .3s;
}
.jss11374:hover {
  transform: scale(1.03);
}
.jss11375 {
  display: grid;
  align-items: center;
  grid-column-gap: 1.5rem;
  grid-template-columns: 1fr max-content 1fr;
}
.jss11375::after, .jss11375::before {
  height: 1px;
  content: '';
  display: block;
  background-color: currentColor;
}
.jss11376 a {
  text-decoration: none;
}
.jss11377 {
  left: 50%;
  position: absolute;
  transform: ;
}
.jss11378 {
  display: -webkit-box;
  overflow: hidden;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;
}
.jss11379 {
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
.jss11380 {
  display: inline;
  text-transform: capitalize;
}
.jss11381 {
  text-decoration: none;
}
.jss11381 p:hover {
  color: #03a9f4;
}
.jss11382 {
  margin-left: 4px;
}
.jss11383 {
  padding: 0 16px;
  display: inline-flex;
  position: relative;
  vertical-align: middle;
}
.jss11384 {
  top: 0;
  color: #fff;
  right: 0;
  height: 20px;
  display: flex;
  padding: 0 4px;
  z-index: 1;
  position: absolute;
  flex-wrap: wrap;
  min-width: 20px;
  transform: translate(20px, -50%);
  box-sizing: border-box;
  transition: transform 225ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
  align-items: center;
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  align-content: center;
  border-radius: 10px;
  flex-direction: row;
  justify-content: center;
  background-color: #ff9800;
  transform-origin: 100% 0%;
}
.jss11385 {
  margin: 0;
  padding: 16px;
  border-bottom: 1px solid rgba(0, 0, 0, 0.12);
}
.jss11386 {
  top: 8px;
  right: 8px;
  color: #9e9e9e;
  position: absolute;
}
.jss11387 {
  margin: 0;
  padding: 16px;
}
.jss11388 {
  margin: 0;
  padding: 8px;
  border-top: 1px solid rgba(0, 0, 0, 0.12);
}
.jss11389 {
  font-size: 14px;
}
.jss11390 {
  padding: 0;
  overflow: hidden;
}
.jss11390 span {
  display: inline-flex;
}
.jss11390 span:hover {
  position: relative;
  animation: moveTextHorz 3s linear infinite;
}
@-webkit-keyframes moveTextHorz {
  0% {
    transform: translate(0, 0);
  }
  100% {
    transform: translate(-70%, 0);
  }
}
.jss11391 span {
  color: #f44336;
}
.jss11392 {
  width: 50px;
}
@media (min-width:600px) {
  .jss11392 {
    right: 90px;
  }
}
@media (min-width:960px) {
  .jss11392 {
    right: 15px;
  }
}
.jss11393 {
  cursor: pointer;
}
.jss11393:hover {
  background: #0288d1;
}
.jss11394 {
  margin-left: 10px;
  vertical-align: middle;
}
.jss11395 {
  width: 100%;
}
.jss11396 {
  width: 100%;
  margin: 0;
}
@media (min-width:960px) {
  .jss11397 {
    margin-left: auto;
    margin-right: 16px;
  }
}
@media (min-width:1280px) {
  .jss11397 {
    margin-left: auto;
    margin-right: 32px;
  }
}
@media (min-width:960px) {
  .jss11398 {
    transform: translateY(75%);
    margin-left: 16px;
  }
}
@media (min-width:1280px) {
  .jss11398 {
    transform: translateY(75%);
    margin-left: 32px;
  }
}
.jss11399 {
  font-family: &quot;Roboto&quot;, &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;
  text-transform: capitalize;
}
@media (max-width:1279.95px) {
  .jss11400 {
    display: none;
  }
}
@media (max-width:1919.95px) {
  .jss11401 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss11402 {
    display: none;
  }
}
@media (min-width:1280px) {
  .jss11403 {
    text-align: right;
  }
}
.jss11404 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss11404 {
    text-align: right;
  }
}
.jss11405 {
  padding: 0;
}
@media (min-width:1280px) {
  .jss11406 {
    margin-top: 0 !important;
  }
}
@media (min-width:1280px) {
  .jss11407 {
    margin-top: 64px;
  }
}
.jss11408 div textarea {
  overflow: auto;
}
.jss11409 {
  width: 70%;
}
@media (min-width:1280px) {
  .jss11409 {
    float: right;
  }
}
@media (min-width:1280px) {
  .jss11410 {
    float: right;
  }
}
@media (max-width:1279.95px) {
  .jss11410 {
    margin-top: 6px;
  }
}
@media (min-width:1280px) {
  .jss11411 {
    padding: 12px 0;
  }
}
@media (max-width:1279.95px) {
  .jss11411 {
    padding: 6px 0 3px;
  }
}
.jss11412 {
  cursor: pointer;
}
@media (min-width:1280px) {
  .jss11412 {
    margin-top: 0 !important;
  }
}
.jss11412 div textarea {
  overflow: auto;
}
.jss11413 {
  width: 300px;
  overflow: hidden;
  transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
}
.jss11414 {
  width: 57px;
  transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
  overflow-x: hidden;
}
@media (min-width:600px) {
  .jss11414 {
    width: 73px;
  }
}
@media (min-width:960px) {
  .jss11415 {
    width: calc(100% - 73px);
    margin-left: 73px;
    margin-right: 0;
  }
}
@media (min-width:960px) {
  .jss11416 {
    margin-left: 73px;
    margin-right: 0;
  }
}
.jss11417 {
  bottom: 0px;
  z-index: 1;
  position: sticky;
  background-color: #fff;
}
@media (max-width:959.95px) {
  .jss11417 {
    display: none;
  }
}
.jss11417:hover {
  background-color: rgb(235, 235, 235);
}
@media (max-width:959.95px) {
  .jss11418 {
    display: none;
  }
}
.jss11419 {
  top: 0;
  left: auto;
  width: 100%;
  right: 0;
  height: 100%;
  z-index: 1301;
  overflow: hidden;
  position: absolute;
  overflow-y: auto;
  transition: transform 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
@media (min-width:960px) {
  .jss11419 {
    width: 24vw;
  }
}
.jss11420 {
  cursor: pointer;
}
.jss11420:hover {
  background-color: #eeeeee;
}
.jss11421 {
  background-color: #e0e0e0 !important;
}
.jss11422 {
  transition: width 225ms cubic-bezier(0, 0, 0.2, 1) 0ms;
}
.jss11423 {
  border-collapse: separate;
}
.jss11424:hover {
  background-color: rgb(235, 235, 235);
}
.jss11425 {
  position: relative;
  min-height: 80vh;
}
.jss11425 .back-icon {
  top: 5px;
  left: 5px;
  z-index: 1;
  position: absolute;
}
.jss11425 > .ninebox-inner {
  width: 100%;
  position: relative;
  min-height: 80vh;
}
.jss11425 > .ninebox-inner .category-paper {
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #424242;
}
.jss11425 > .ninebox-inner .percentage-wrapper {
  width: 100%;
  height: 100%;
  display: flex;
  flex-wrap: wrap;
  box-sizing: border-box;
}
.jss11425 > .ninebox-inner .horizontal-category {
  height: 50px;
  margin: 0 60px;
  display: flex;
  align-items: center;
  justify-content: center;
}
.jss11425 > .ninebox-inner .vertical-category {
  width: 50px;
  height: calc(80vh - 120px);
  display: inline-flex;
  align-items: center;
  justify-content: center;
}
.jss11425 > .ninebox-inner .ninebox-content {
  width: calc(100% - 100px);
  height: calc(80vh - 120px);
  display: inline-block;
  position: relative;
}
.jss11425 > .ninebox-inner .ninebox-content .wrapper {
  width: 100%;
  height: 100%;
  margin: 0;
}
.jss11425 > .ninebox-inner .ninebox-content .wrapper .box {
  height: calc(100% / 3);
  padding: 10px;
}
.jss11425 > .ninebox-inner .ninebox-content .wrapper .box .has-item {
  padding: 26px 10px !important;
}
.jss11425 > .ninebox-inner .ninebox-content .wrapper .box .disabled {
  background: #d1d1d1;
}
.jss11425 > .ninebox-inner .ninebox-content .wrapper .box .card {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 30px;
  position: relative;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  -webkit-transition: all .4s ease-out;
}
.jss11425 > .ninebox-inner .ninebox-content .wrapper .box .card h6 {
  color: rgba(0, 0, 0, 0.87);
}
.jss11425 > .ninebox-inner .ninebox-content .wrapper .box .card .total-person {
  top: 5px;
  right: 5px;
  width: 15px;
  height: 15px;
  position: absolute;
}
.jss11425 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list {
  width: 100%;
  height: 100%;
  display: flex;
  overflow-y: auto;
  align-items: center;
}
.jss11425 > .ninebox-inner .ninebox-content .wrapper .box .card .emp-list > ol {
  height: 100%;
  margin: 0;
  padding: 0;
  text-align: left;
  list-style: none;
}
.jss11425 > .ninebox-inner .ninebox-content .wrapper .box .active:hover {
  cursor: pointer;
  transform: scale(1.1);
}
.jss11425 > .ninebox-inner .vertical-category .text-rotate {
  transform: rotate(270deg);
}
.jss11425 > .ninebox-inner .vertical-category.left {
  padding: 10px 0;
  margin-right: 9px;
}
.jss11425 > .ninebox-inner .vertical-category.right {
  margin-left: 9px;
}
.jss11425 > .ninebox-inner .vertical-category .text-rotate.nowrap {
  white-space: nowrap;
}
.jss11425 > .ninebox-inner .horizontal-category.top {
  padding: 0 9px;
  margin-bottom: 9px;
}
.jss11425 > .ninebox-inner .horizontal-category.bottom {
  margin-top: 9px;
}
.jss11425 > .ninebox-inner .percentage-wrapper > .percentage-item {
  padding: 0 10px;
  flex-grow: 0;
  max-width: calc(100% / 3);
  flex-basis: calc(100% / 3);
}
.jss11425 > .ninebox-inner .percentage-wrapper > .percentage-item.horizontal {
  height: calc(100% / 3);
  padding: 10px 0;
  max-width: 100%;
}
.jss11425 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper {
  width: 100%;
  height: 100%;
  display: inline-flex;
  padding: 10px;
  transition: all .4s ease-out;
  text-align: center;
  align-items: center;
  justify-content: center;
  background-color: #424242;
  -webkit-transition: all .4s ease-out;
}
.jss11425 > .ninebox-inner .percentage-wrapper > .percentage-item > .paper > p {
  color: #fff;
}
.jss11425 > .ninebox-inner .category-paper > p {
  color: #fff;
}
.jss11426 {
  max-height: 45vh;
  overflow-x: auto;
  overflow-y: auto;
}
.jss11426 .table {
  border-collapse: separate;
}
.jss11426 .table .sticky-head {
  top: 0;
  color: #fff;
  position: sticky;
  background: #03a9f4;
}
.jss11427 {
  height: 44vh;
  overflow: auto;
}
.jss11428 {
  width: 100%;
  height: 50vh;
  overflow: auto;
}
.jss11429 {
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss11430 {
  width: 100%;
  height: calc(80vh - 185px);
  overflow: auto;
}
.jss11431 {
  max-width: 82%;
}
#katalon{font-family:monospace;font-size:13px;background-color:rgba(0,0,0,.7);position:fixed;top:0;left:0;right:0;display:block;z-index:999999999;line-height: normal} #katalon div{padding:0;margin:0;color:#fff;} #katalon kbd{display:inline-block;padding:3px 5px;font:13px Consolas,&quot;Liberation Mono&quot;,Menlo,Courier,monospace;line-height:10px;color:#555;vertical-align:middle;background-color:#fcfcfc;border:1px solid #ccc;border-bottom-color:#bbb;border-radius:3px;box-shadow:inset 0 -1px 0 #bbb;font-weight: bold} div#katalon-spy_elementInfoDiv {color: lightblue; padding: 0px 5px 5px} div#katalon-spy_instructionDiv {padding: 5px 5px 2.5px}/* Chart.js */
/*
 * DOM element rendering detection
 * https://davidwalsh.name/detect-node-insertion
 */
@keyframes chartjs-render-animation {
	from { opacity: 0.99; }
	to { opacity: 1; }
}

.chartjs-render-monitor {
	animation: chartjs-render-animation 0.001s;
}

/*
 * DOM element resizing detection
 * https://github.com/marcj/css-element-queries
 */
.chartjs-size-monitor,
.chartjs-size-monitor-expand,
.chartjs-size-monitor-shrink {
	position: absolute;
	direction: ltr;
	left: 0;
	top: 0;
	right: 0;
	bottom: 0;
	overflow: hidden;
	pointer-events: none;
	visibility: hidden;
	z-index: -1;
}

.chartjs-size-monitor-expand > div {
	position: absolute;
	width: 1000000px;
	height: 1000000px;
	left: 0;
	top: 0;
}

.chartjs-size-monitor-shrink > div {
	position: absolute;
	width: 200%;
	height: 200%;
	left: 0;
	top: 0;
}
#katalon{font-family:monospace;font-size:13px;background-color:rgba(0,0,0,.7);position:fixed;top:0;left:0;right:0;display:block;z-index:999999999;line-height: normal} #katalon div{padding:0;margin:0;color:#fff;} #katalon kbd{display:inline-block;padding:3px 5px;font:13px Consolas,&quot;Liberation Mono&quot;,Menlo,Courier,monospace;line-height:10px;color:#555;vertical-align:middle;background-color:#fcfcfc;border:1px solid #ccc;border-bottom-color:#bbb;border-radius:3px;box-shadow:inset 0 -1px 0 #bbb;font-weight: bold} div#katalon-spy_elementInfoDiv {color: lightblue; padding: 0px 5px 5px} div#katalon-spy_instructionDiv {padding: 5px 5px 2.5px}#katalon{font-family:monospace;font-size:13px;background-color:rgba(0,0,0,.7);position:fixed;top:0;left:0;right:0;display:block;z-index:999999999;line-height: normal} #katalon div{padding:0;margin:0;color:#fff;} #katalon kbd{display:inline-block;padding:3px 5px;font:13px Consolas,&quot;Liberation Mono&quot;,Menlo,Courier,monospace;line-height:10px;color:#555;vertical-align:middle;background-color:#fcfcfc;border:1px solid #ccc;border-bottom-color:#bbb;border-radius:3px;box-shadow:inset 0 -1px 0 #bbb;font-weight: bold} div#katalon-spy_elementInfoDiv {color: lightblue; padding: 0px 5px 5px} div#katalon-spy_instructionDiv {padding: 5px 5px 2.5px}You need to enable JavaScript to run this app.Expenses1DMDELLA MUTIARAXMU - Automation Test SubmitterDashboardCustomer eXperienceEmployee ExpenseCreate / Edit ExpenseExpense ApprovalFinanceFinance ApprovalHuman Resource360 Assessment360 Assessment Input360 Assessment ResultCompetencies StandardEmployee ProfileEmployee Request FormHR Form ProgressHR KPI InputManager KPI InputSubordinates ProfileTraining Request FormTraining Request SettlementLeave RequestCreate / Edit LeaveLeave ApprovalLeave CancellationMileageMileage ApprovalSubmit MileageMy AccountMy ProfileProject AssignmentAssignment AcceptanceCreate / Edit AssignmentProject RegistrationAll ProjectsCreate / Edit ProjectProject ApprovalReportsBillable UtilizationProject ProfitabilityProject ProgressProject Resource MappingResource EffectivenessWinning RatioRequest for Purchase ApprovalCreate / Edit RFPAPurchase Request SettlementPurchase Settlement ApprovalRFPA ApprovalSetupAchievement ChartApproval HierarchyCompanyCurrencyCustomerDiem ValueEmployeeEmployee LeaveEmployee LevelFile UploadHolidayHR Competency ClusterHR Competency LevelHR Competency MappingHR Corner PageHR KPI CategoriesHR KPI Employee AssignHR KPI Open DateHR KPI TemplateHR Notification PeriodHR Notification SettingHR Notification TemplateImage GalleryLeaveMileage ExceptionOrganizationPositionProject Notification SettingProject Notification TemplateRolesSystem SetupTime LimitWork FlowTime ReportTime Approval HistoryTime Report ApprovalTime Report EntryTime Report HistoryTravelTravel RequestTravel Request ApprovalTravel SettlementTravel Settlement ApprovalWeb JobCollapse Sidebar1 Record(s)EX21000006Uang makan ditanggung perusahaanAryasa IndahIDR 1,500,000.00Submitted3 days agoStatusExpense IDProject NameXMU.PS1806.0079 - cocobi yeuhXMU.PS1806.0079 - cocobi yeuhCustomerAryasa IndahAryasa IndahNotesUang makan ditanggung perusahaanUang makan ditanggung perusahaanExpense TypeBid RegistrationBid RegistrationLocationAddressJalan Pahlawan No. 22Jalan Pahlawan No. 22Customer NameCustomer Job TitleExpense DateExpense ValueCreated ByFebruary 12, 2021, 11:22:08Updated ByFebruary 13, 2021, 13:57:34ModifyDetailsPage 1 of 1© 2020 | Equine Technologies GroupVersion master.3596!function(c){function e(e){for(var t,r,n=e[0],o=e[1],u=e[2],i=0,a=[];i&lt;n.length;i++)r=n[i],f[r]&amp;&amp;a.push(f[r][0]),f[r]=0;for(t in o)Object.prototype.hasOwnProperty.call(o,t)&amp;&amp;(c[t]=o[t]);for(d&amp;&amp;d(e);a.length;)a.shift()();return s.push.apply(s,u||[]),l()}function l(){for(var e,t=0;t&lt;s.length;t++){for(var r=s[t],n=!0,o=1;o&lt;r.length;o++){var u=r[o];0!==f[u]&amp;&amp;(n=!1)}n&amp;&amp;(s.splice(t--,1),e=p(p.s=r[0]))}return e}var r={},f={6:0},s=[];function p(e){if(r[e])return r[e].exports;var t=r[e]={i:e,l:!1,exports:{}};return c[e].call(t.exports,t,t.exports,p),t.l=!0,t.exports}p.e=function(u){var e=[],r=f[u];if(0!==r)if(r)e.push(r[2]);else{var t=new Promise(function(e,t){r=f[u]=[e,t]});e.push(r[2]=t);var n,o=document.getElementsByTagName(&quot;head&quot;)[0],i=document.createElement(&quot;script&quot;);i.charset=&quot;utf-8&quot;,i.timeout=120,p.nc&amp;&amp;i.setAttribute(&quot;nonce&quot;,p.nc),i.src=p.p+&quot;static/js/&quot;+({1:&quot;xlsx&quot;}[u]||u)+&quot;.&quot;+{1:&quot;6ebfb548&quot;,3:&quot;9030a774&quot;,4:&quot;5b1341d9&quot;,5:&quot;78aaeceb&quot;}[u]+&quot;.chunk.js&quot;,n=function(e){i.onerror=i.onload=null,clearTimeout(a);var t=f[u];if(0!==t){if(t){var r=e&amp;&amp;(&quot;load&quot;===e.type?&quot;missing&quot;:e.type),n=e&amp;&amp;e.target&amp;&amp;e.target.src,o=new Error(&quot;Loading chunk &quot;+u+&quot; failed.\n(&quot;+r+&quot;: &quot;+n+&quot;)&quot;);o.type=r,o.request=n,t[1](o)}f[u]=void 0}};var a=setTimeout(function(){n({type:&quot;timeout&quot;,target:i})},12e4);i.onerror=i.onload=n,o.appendChild(i)}return Promise.all(e)},p.m=c,p.c=r,p.d=function(e,t,r){p.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:r})},p.r=function(e){&quot;undefined&quot;!=typeof Symbol&amp;&amp;Symbol.toStringTag&amp;&amp;Object.defineProperty(e,Symbol.toStringTag,{value:&quot;Module&quot;}),Object.defineProperty(e,&quot;__esModule&quot;,{value:!0})},p.t=function(t,e){if(1&amp;e&amp;&amp;(t=p(t)),8&amp;e)return t;if(4&amp;e&amp;&amp;&quot;object&quot;==typeof t&amp;&amp;t&amp;&amp;t.__esModule)return t;var r=Object.create(null);if(p.r(r),Object.defineProperty(r,&quot;default&quot;,{enumerable:!0,value:t}),2&amp;e&amp;&amp;&quot;string&quot;!=typeof t)for(var n in t)p.d(r,n,function(e){return t[e]}.bind(null,n));return r},p.n=function(e){var t=e&amp;&amp;e.__esModule?function(){return e.default}:function(){return e};return p.d(t,&quot;a&quot;,t),t},p.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},p.p=&quot;https://xdev.equine.co.id/apps/tessa/&quot;,p.oe=function(e){throw console.error(e),e};var t=window.webpackJsonp=window.webpackJsonp||[],n=t.push.bind(t);t.push=e,t=t.slice();for(var o=0;o&lt;t.length;o++)e(t[o]);var d=n;l()}([])NotificationsTravel/html[1]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//html</value>
   </webElementXpaths>
</WebElementEntity>
