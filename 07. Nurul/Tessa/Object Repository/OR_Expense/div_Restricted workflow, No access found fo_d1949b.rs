<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Restricted workflow, No access found fo_d1949b</name>
   <tag></tag>
   <elementGuidId>d100b688-4e6f-4f8b-ac0a-147bcddcfa7d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.jss24348.jss25878.jss24357.jss25885.jss24349.jss25879.jss24355.jss24356.jss25884</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/form/div/div[2]/div/div/div[2]/div[3]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss24348 jss25878 jss24357 jss25885 jss24349 jss25879 jss24355 jss24356 jss25884</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Restricted workflow, No access found for MNU30 CP002 P0353Restricted workflow, No access found for MNU30 CP002 P0353</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss5283&quot;]/main[@class=&quot;jss5799 jss5801&quot;]/form[1]/div[@class=&quot;jss25224&quot;]/div[@class=&quot;jss25225&quot;]/div[@class=&quot;jss25228&quot;]/div[@class=&quot;jss7085 jss7088 jss25848&quot;]/div[@class=&quot;jss25855&quot;]/div[@class=&quot;jss25856 jss25858 jss25859&quot;]/div[@class=&quot;jss24348 jss25878 jss24357 jss25885 jss24349 jss25879 jss24355 jss24356 jss25884&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div/div[2]/div/div/div[2]/div[3]/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Status'])[1]/following::div[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Correlation ID'])[1]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reset'])[1]/preceding::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div[2]/div[3]/div</value>
   </webElementXpaths>
</WebElementEntity>
