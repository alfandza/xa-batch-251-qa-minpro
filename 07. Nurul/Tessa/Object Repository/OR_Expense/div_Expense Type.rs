<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Expense Type</name>
   <tag></tag>
   <elementGuidId>05f0502d-158b-4553-8c6d-b442501a8c72</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.css-10nd86i > div.jss25856.jss25857.jss25859</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[3]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss25856 jss25857 jss25859</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Expense Type *</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss5283&quot;]/main[@class=&quot;jss5799 jss5801&quot;]/form[1]/div[@class=&quot;jss25224&quot;]/div[@class=&quot;jss25225&quot;]/div[@class=&quot;jss25228&quot;]/div[@class=&quot;jss7085 jss7088 jss25848&quot;]/div[@class=&quot;jss25855&quot;]/div[@class=&quot;css-10nd86i&quot;]/div[@class=&quot;jss25856 jss25857 jss25859&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[3]/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[3]/div</value>
   </webElementXpaths>
</WebElementEntity>
