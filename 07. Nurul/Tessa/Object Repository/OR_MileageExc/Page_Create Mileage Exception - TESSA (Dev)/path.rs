<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>path</name>
   <tag></tag>
   <elementGuidId>753b0ba7-cb35-4a68-956d-d2364d3348ee</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.css-1q3fsek > svg.css-19bqh2r > path</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>path</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>d</name>
      <type>Main</type>
      <value>M4.516 7.548c0.436-0.446 1.043-0.481 1.576 0l3.908 3.747 3.908-3.747c0.533-0.481 1.141-0.446 1.574 0 0.436 0.445 0.408 1.197 0 1.615-0.406 0.418-4.695 4.502-4.695 4.502-0.217 0.223-0.502 0.335-0.787 0.335s-0.57-0.112-0.789-0.335c0 0-4.287-4.084-4.695-4.502s-0.436-1.17 0-1.615z</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss38895&quot;]/main[@class=&quot;jss39411 jss39413&quot;]/form[1]/div[@class=&quot;jss45952&quot;]/div[@class=&quot;jss45953&quot;]/div[@class=&quot;jss45956&quot;]/div[@class=&quot;jss40122 jss40125 jss46576&quot;]/div[@class=&quot;jss46583&quot;]/div[@class=&quot;css-10nd86i&quot;]/div[@class=&quot;jss46584 jss46585 jss46587&quot;]/div[@class=&quot;jss44986 jss46606 jss46610 jss44995 jss46613 jss44988 jss46608 jss44987 jss46607&quot;]/div[@class=&quot;jss44996 jss46614 jss46627&quot;]/div[@class=&quot;jss46628&quot;]/div[@class=&quot;css-1q3fsek&quot;]/svg[@class=&quot;css-19bqh2r&quot;]/path[1]</value>
   </webElementProperties>
</WebElementEntity>
