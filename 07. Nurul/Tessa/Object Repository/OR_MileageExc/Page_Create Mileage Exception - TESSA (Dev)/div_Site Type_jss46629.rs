<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Site Type_jss46629</name>
   <tag></tag>
   <elementGuidId>4ddea6b2-697c-4223-9f37-76726b06976c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[4]/div/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.jss44986.jss46606.jss46610.jss44995.jss46613.jss44988.jss46608.jss44987.jss46607 > div.jss44996.jss46614.jss46627 > div.jss46629</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss46629</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss38895&quot;]/main[@class=&quot;jss39411 jss39413&quot;]/form[1]/div[@class=&quot;jss45952&quot;]/div[@class=&quot;jss45953&quot;]/div[@class=&quot;jss45956&quot;]/div[@class=&quot;jss40122 jss40125 jss46576&quot;]/div[@class=&quot;jss46583&quot;]/div[@class=&quot;css-10nd86i&quot;]/div[@class=&quot;jss46584 jss46585 jss46587&quot;]/div[@class=&quot;jss44986 jss46606 jss46610 jss44995 jss46613 jss44988 jss46608 jss44987 jss46607&quot;]/div[@class=&quot;jss44996 jss46614 jss46627&quot;]/div[@class=&quot;jss46629&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[4]/div/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Site Type'])[1]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='|'])[2]/following::div[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='In Jakarta'])[1]/preceding::div[7]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='In Somalia'])[1]/preceding::div[8]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[4]/div/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
