import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Mileage Exception/TC_MILEXC_002'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/OR_MileageExc/Page_Mileage Exception - TESSA (Dev)/button_Mileage Exception_jss40159 jss40153 _7f2258'))

WebUI.click(findTestObject('Object Repository/OR_MileageExc/Page_Create Mileage Exception - TESSA (Dev)/div__jss58113 jss59731 jss59744'))

WebUI.click(findTestObject('Object Repository/OR_MileageExc/Page_Create Mileage Exception - TESSA (Dev)/div_Equine Global'))

WebUI.setText(findTestObject('Object Repository/OR_MileageExc/Page_Create Mileage Exception - TESSA (Dev)/input__description'), 
    'hanya description')

WebUI.click(findTestObject('Object Repository/OR_SetupDepartment/button_Submit'))

WebUI.click(findTestObject('Object Repository/OR_SetupDepartment/button_Continue'))

