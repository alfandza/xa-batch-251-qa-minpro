import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Employee Expanse/TC_EMXPANSE_067'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/OR_Expense/button_Expenses_jss18948 jss18942 jss18943'))

WebUI.click(findTestObject('Object Repository/OR_Expense/div_Expense Date'))

WebUI.click(findTestObject('Object Repository/OR_Expense/button_1'))

WebUI.click(findTestObject('Object Repository/OR_Expense/button_OK'))

WebUI.click(findTestObject('Object Repository/OR_Expense/div_Expense Type'))

WebUI.click(findTestObject('Object Repository/OR_Expense/div_Bid Registration'))

WebUI.click(findTestObject('Object Repository/OR_Expense/div_Customer'))

WebUI.click(findTestObject('Object Repository/OR_Expense/div_CV. Anindya'))

WebUI.click(findTestObject('Object Repository/OR_Expense/div_Project Name'))

WebUI.click(findTestObject('Object Repository/OR_Expense/div_GNLPS00002 - General Purpose'))

WebUI.setText(findTestObject('Object Repository/OR_Expense/input__value'), '2,500,000')

WebUI.setText(findTestObject('Object Repository/OR_Expense/input__location'), 'Jakarta')

WebUI.setText(findTestObject('Object Repository/OR_Expense/input__address'), 'Jalan Petojo VIJ No. 60')

WebUI.setText(findTestObject('Object Repository/OR_Expense/input__client.name'), 'Robby Maulana Turnip')

WebUI.setText(findTestObject('Object Repository/OR_Expense/input__client.title'), 'Programmer')

WebUI.setText(findTestObject('Object Repository/OR_Expense/input__notes'), 'Uang makan diterima diakhir project')

WebUI.scrollToPosition(0, 0)

WebUI.click(findTestObject('Object Repository/OR_Expense/button_Reset'))

