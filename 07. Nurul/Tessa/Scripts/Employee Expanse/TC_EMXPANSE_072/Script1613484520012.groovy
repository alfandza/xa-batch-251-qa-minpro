import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://xdev.equine.co.id/apps/tessa/expense/requests')

WebUI.setText(findTestObject('Object Repository/OR_Expense/Page_Expenses - TESSA (Dev)/input_Expenses_jss18876 jss18664'), 
    'Habli')

WebUI.click(findTestObject('Object Repository/OR_Expense/Page_Expenses - TESSA (Dev)/button_Expenses_search.fields'))

WebUI.click(findTestObject('Object Repository/OR_Expense/Page_Expenses - TESSA (Dev)/li_Habli in Client Name'))

WebUI.rightClick(findTestObject('Object Repository/OR_Expense/Page_Expenses - TESSA (Dev)/div_EX21000006Uang makan ditanggung perusah_573ebf'))

WebUI.click(findTestObject('Object Repository/OR_Expense/Page_Expenses - TESSA (Dev)/html_Expenses - TESSA (Dev).jss13778   top _844c90'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_Expense/Page_Expenses - TESSA (Dev)/div_EX21000006Uang makan ditanggung perusah_573ebf'), 
    0)

WebUI.doubleClick(findTestObject('Object Repository/OR_Expense/Page_Expenses - TESSA (Dev)/input_Expenses_jss18876 jss18664'))

WebUI.setText(findTestObject('Object Repository/OR_Expense/Page_Expenses - TESSA (Dev)/input_Expenses_jss18876 jss18664'), 
    'HABLI')

WebUI.click(findTestObject('Object Repository/OR_Expense/Page_Expenses - TESSA (Dev)/svg_Expenses_jss12176 jss12183'))

WebUI.click(findTestObject('Object Repository/OR_Expense/Page_Expenses - TESSA (Dev)/li_HABLI in Client Name (1)'))

WebUI.click(findTestObject('Object Repository/OR_Expense/Page_Expenses - TESSA (Dev)/button_Expenses_jss12173 jss12167 jss18665'))

WebUI.setText(findTestObject('Object Repository/OR_Expense/Page_Expenses - TESSA (Dev)/input_Expenses_jss18876 jss18664'), 
    'habli')

WebUI.click(findTestObject('Object Repository/OR_Expense/Page_Expenses - TESSA (Dev)/button_Expenses_search.fields'))

WebUI.click(findTestObject('Object Repository/OR_Expense/Page_Expenses - TESSA (Dev)/li_habli in Client Name (2)'))

