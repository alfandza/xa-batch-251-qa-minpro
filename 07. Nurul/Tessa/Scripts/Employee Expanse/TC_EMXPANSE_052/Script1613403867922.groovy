import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Employee Expanse/TC_EMXPANSE_004'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/OR_Expense/Page_Expenses - TESSA (Dev)/button_Expenses_jss100935 jss100929 jss100930'))

WebUI.click(findTestObject('Object Repository/OR_Expense/Page_New Expense - TESSA (Dev)/input__date'))

WebUI.click(findTestObject('Object Repository/OR_Expense/Page_New Expense - TESSA (Dev)/button_2'))

WebUI.click(findTestObject('Object Repository/OR_Expense/Page_New Expense - TESSA (Dev)/button_OK'))

WebUI.click(findTestObject('Object Repository/OR_Expense/Page_New Expense - TESSA (Dev)/div_Expense TypeExpense Type is a required field'))

WebUI.click(findTestObject('Object Repository/OR_Expense/Page_New Expense - TESSA (Dev)/div_Business Trip in City(Dalam Kota)'))

WebUI.click(findTestObject('Object Repository/OR_Expense/Page_New Expense - TESSA (Dev)/div_Customer'))

WebUI.click(findTestObject('Object Repository/OR_Expense/Page_New Expense - TESSA (Dev)/div_Barima Handal R4jas'))

WebUI.click(findTestObject('Object Repository/OR_Expense/Page_New Expense - TESSA (Dev)/div_Project NameProject Name is a required field'))

WebUI.click(findTestObject('Object Repository/OR_Expense/Page_New Expense - TESSA (Dev)/div_XMU.P1805.0041 - TESSA demo'))

WebUI.setText(findTestObject('Object Repository/OR_Expense/Page_New Expense - TESSA (Dev)/input__value'), '1,500,000')

WebUI.setText(findTestObject('Object Repository/OR_Expense/Page_New Expense - TESSA (Dev)/input__location'), 'Cimahi')

WebUI.setText(findTestObject('Object Repository/OR_Expense/Page_New Expense - TESSA (Dev)/input__address'), 'Jalan Baros Selatan Blok III No. 011')

WebUI.setText(findTestObject('Object Repository/OR_Expense/Page_New Expense - TESSA (Dev)/input__client.name'), 'Sahara Maharani')

WebUI.setText(findTestObject('Object Repository/OR_Expense/Page_New Expense - TESSA (Dev)/input__notes'), 'Selesaikan tugas dengan baik')

WebUI.scrollToPosition(0, 0)

WebUI.click(findTestObject('Object Repository/OR_Expense/Page_New Expense - TESSA (Dev)/button_Submit'))

WebUI.click(findTestObject('Object Repository/OR_Expense/Page_New Expense - TESSA (Dev)/div_Submit ConfirmationYou are about to sub_3af10f'))

WebUI.click(findTestObject('Object Repository/OR_Expense/Page_New Expense - TESSA (Dev)/button_Continue'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_Expense/Page_New Expense - TESSA (Dev)/div_Some fields has invalid value, please c_a06eb9'), 
    0)

