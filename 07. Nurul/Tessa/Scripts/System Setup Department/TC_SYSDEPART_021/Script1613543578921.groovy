import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('System Setup Department/TC_SYSDEPART_002'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/OR_SetupDepartment/button_System Setup Department_jss102575 js_3dcae7'))

WebUI.click(findTestObject('Object Repository/OR_SetupDepartment/div_Company'))

WebUI.click(findTestObject('Object Repository/OR_SetupDepartment/div_Optima Data Internasional'))

WebUI.click(findTestObject('Object Repository/OR_SetupDepartment/div_Parent_jss24110'))

WebUI.click(findTestObject('Object Repository/OR_SetupDepartment/div_DO - CEO'))

WebUI.setText(findTestObject('Object Repository/OR_SetupDepartment/input__name'), 'Sales Support')

WebUI.setText(findTestObject('Object Repository/OR_SetupDepartment/input_Description_description'), 'Department in EQG')

WebUI.click(findTestObject('Object Repository/OR_SetupDepartment/input_Description_isActive'))

WebUI.scrollToPosition(0, 0)

WebUI.mouseOver(findTestObject('Object Repository/OR_SetupDepartment/button_Submit'))

WebUI.click(findTestObject('Object Repository/OR_SetupDepartment/button_Submit'))

WebUI.mouseOver(findTestObject('Object Repository/OR_SetupDepartment/button_Discard'))

WebUI.click(findTestObject('Object Repository/OR_SetupDepartment/button_Discard'))

WebUI.click(findTestObject('Object Repository/OR_SetupDepartment/button_Reset'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_SetupDepartment/div_New Type0'), 0)

