import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Employee/TC_EMPLOYEE_002'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/OR_Employee/input_Employee_jss56706 jss56494'), 'E0180')

WebUI.click(findTestObject('Object Repository/OR_Employee/button_Employee_search.fields'))

WebUI.mouseOver(findTestObject('Object Repository/OR_Employee/li_E0180 in Employee ID'))

WebUI.click(findTestObject('Object Repository/OR_Employee/li_E0180 in Employee ID'))

WebUI.click(findTestObject('Object Repository/OR_Employee/div_RUMIYATIAutomation TesterApril 26, 2016_3c477a'))

WebUI.mouseOver(findTestObject('Object Repository/OR_Employee/button_Modify'))

WebUI.click(findTestObject('Object Repository/OR_Employee/button_Modify'))

WebUI.setText(findTestObject('OR_Employee/input__employmentNumber'), Keys.chord(Keys.CONTROL, 'a', Keys.DELETE))

WebUI.setText(findTestObject('Object Repository/OR_Employee/input__employmentNumber'), '12345')

WebUI.setText(findTestObject('OR_Employee/input__fullName'), Keys.chord(Keys.CONTROL, 'a', Keys.DELETE))

WebUI.setText(findTestObject('Object Repository/OR_Employee/input__fullName'), '12345')

WebUI.setText(findTestObject('OR_Employee/input_BPJS Employment Number_bpjsEmploymentNumber'), Keys.chord(Keys.CONTROL, 
        'a', Keys.DELETE))

WebUI.setText(findTestObject('OR_Employee/input_BPJS Employment Number_bpjsEmploymentNumber'), '12345')

WebUI.setText(findTestObject('OR_Employee/input_Phone_phone'), Keys.chord(Keys.CONTROL, 'a', Keys.DELETE))

WebUI.setText(findTestObject('Object Repository/OR_Employee/input_Phone_phone'), '12345')

WebUI.click(findTestObject('Object Repository/OR_Employee/button_Submit'))

WebUI.click(findTestObject('Object Repository/OR_Employee/button_Continue'))

WebUI.rightClick(findTestObject('Object Repository/OR_Employee/div_Some fields has invalid value, please c_a06eb9'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_Employee/div_Some fields has invalid value, please c_a06eb9'), 
    0)

