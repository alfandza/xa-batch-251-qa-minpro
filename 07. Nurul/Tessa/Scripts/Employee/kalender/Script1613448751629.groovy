import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Employee/TC_EMPLOYEE_002'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.mouseOver(findTestObject('Object Repository/OR_Employee/span_Dashboard'))

WebUI.click(findTestObject('Object Repository/OR_Employee/span_Dashboard'))

WebUI.mouseOver(findTestObject('Object Repository/OR_Employee/span_Setup (1)'))

WebUI.click(findTestObject('Object Repository/OR_Employee/span_Setup (1)'))

WebUI.mouseOver(findTestObject('Object Repository/OR_Employee/span_Employee (1)'))

WebUI.click(findTestObject('Object Repository/OR_Employee/div_Employee'))

WebUI.mouseOver(findTestObject('Object Repository/OR_Employee/button_Employee_jss1265 jss1259 jss1260'))

WebUI.click(findTestObject('Object Repository/OR_Employee/button_Employee_jss1265 jss1259 jss1260'))

WebUI.mouseOver(findTestObject('Object Repository/OR_Employee/input__dateOfBirth'))

WebUI.setText(findTestObject('Object Repository/OR_Employee/input__dateOfBirth'), '02/22/2021')

WebUI.sendKeys(findTestObject('Object Repository/OR_Employee/input__dateOfBirth'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/OR_Employee/div__jss10016'))

WebUI.click(findTestObject('Object Repository/OR_Employee/div_Xsis Mitra Utama'))

