import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Employee/TC_EMPLOYEE_002'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/OR_Employee/button_XMU - Curriculum Specialist_user-nav-option'))

WebUI.mouseOver(findTestObject('Object Repository/OR_Employee/li_Switch Access'))

WebUI.click(findTestObject('Object Repository/OR_Employee/li_Switch Access'))

WebUI.mouseOver(findTestObject('Object Repository/OR_Employee/span_Continue'))

WebUI.click(findTestObject('Object Repository/OR_Employee/span_Continue'))

WebUI.click(findTestObject('Object Repository/OR_Employee/div_Xsis Mitra Utama'))

WebUI.mouseOver(findTestObject('Object Repository/OR_Employee/span_Automation Test Submitter'))

WebUI.click(findTestObject('Object Repository/OR_Employee/span_Automation Test Submitter'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_Employee/span_XMU - Automation Test Submitter'), 0)

