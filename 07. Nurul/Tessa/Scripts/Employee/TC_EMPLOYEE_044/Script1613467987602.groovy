import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Employee/TC_EMPLOYEE_002'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/OR_Employee/input_Employee_jss56706 jss56494'), 'E0180')

WebUI.click(findTestObject('Object Repository/OR_Employee/button_Employee_search.fields'))

WebUI.mouseOver(findTestObject('Object Repository/OR_Employee/li_E0180 in Employee ID'))

WebUI.click(findTestObject('Object Repository/OR_Employee/li_E0180 in Employee ID'))

WebUI.click(findTestObject('Object Repository/OR_Employee/div_RUMIYATIAutomation TesterApril 26, 2016_3c477a'))

WebUI.mouseOver(findTestObject('Object Repository/OR_Employee/button_Modify'))

WebUI.click(findTestObject('Object Repository/OR_Employee/button_Modify'))

WebUI.setText(findTestObject('OR_Employee/input__employmentNumber'), Keys.chord(Keys.CONTROL, 'a', Keys.DELETE))

WebUI.setText(findTestObject('Object Repository/OR_Employee/input__employmentNumber'), '')

WebUI.setText(findTestObject('OR_Employee/input__fullName'), Keys.chord(Keys.CONTROL, 'a', Keys.DELETE))

WebUI.setText(findTestObject('Object Repository/OR_Employee/input__fullName'), '')

WebUI.setText(findTestObject('OR_Employee/input__birthPlace'), Keys.chord(Keys.CONTROL, 'a', Keys.DELETE))

WebUI.click(findTestObject('Object Repository/OR_Employee/input__birthPlace'))

WebUI.setText(findTestObject('OR_Employee/input__familyCardNumber'), Keys.chord(Keys.CONTROL, 'a', Keys.DELETE))

WebUI.setText(findTestObject('Object Repository/OR_Employee/input__familyCardNumber'), '')

WebUI.setText(findTestObject('OR_Employee/input__citizenNumber'), Keys.chord(Keys.CONTROL, 'a', Keys.DELETE))

WebUI.setText(findTestObject('Object Repository/OR_Employee/input__citizenNumber'), '')

WebUI.setText(findTestObject('OR_Employee/input__taxNumber'), Keys.chord(Keys.CONTROL, 'a', Keys.DELETE))

WebUI.setText(findTestObject('Object Repository/OR_Employee/input__taxNumber'), '')

WebUI.setText(findTestObject('OR_Employee/input_BPJS Health Care Number_bpjsHealthCareNumber'), Keys.chord(Keys.CONTROL, 
        'a', Keys.DELETE))

WebUI.click(findTestObject('Object Repository/OR_Employee/input_BPJS Health Care Number_bpjsHealthCareNumber'))

WebUI.setText(findTestObject('OR_Employee/input__bankAccount'), Keys.chord(Keys.CONTROL, 'a', Keys.DELETE))

WebUI.click(findTestObject('Object Repository/OR_Employee/input__bankAccount'))

WebUI.setText(findTestObject('OR_Employee/input__bankAccountName'), Keys.chord(Keys.CONTROL, 'a', Keys.DELETE))

WebUI.click(findTestObject('Object Repository/OR_Employee/input__bankAccountName'))

WebUI.setText(findTestObject('OR_Employee/input_BCA Account Branch_bankAccountBranch'), Keys.chord(Keys.CONTROL, 'a', Keys.DELETE))

WebUI.click(findTestObject('Object Repository/OR_Employee/input_BCA Account Branch_bankAccountBranch'))

WebUI.setText(findTestObject('OR_Employee/input_Phone_phone'), Keys.chord(Keys.CONTROL, 'a', Keys.DELETE))

WebUI.click(findTestObject('Object Repository/OR_Employee/input_Phone_phone'))

WebUI.setText(findTestObject('OR_Employee/input__mobilePhone'), Keys.chord(Keys.CONTROL, 'a', Keys.DELETE))

WebUI.setText(findTestObject('Object Repository/OR_Employee/input__mobilePhone'), '')

WebUI.setText(findTestObject('OR_Employee/div_Company Email'), Keys.chord(Keys.CONTROL, 'a', Keys.DELETE))

WebUI.click(findTestObject('Object Repository/OR_Employee/div_Company Email'))

WebUI.click(findTestObject('Object Repository/OR_Employee/input__emailPersonal'))

WebUI.click(findTestObject('Object Repository/OR_Employee/input__addressCurrent'))

WebUI.click(findTestObject('Object Repository/OR_Employee/input__address'))

WebUI.click(findTestObject('Object Repository/OR_Employee/input_Address (NPWP)_addressAdditional'))

WebUI.click(findTestObject('Object Repository/OR_Employee/input_Emergency Contact Name_emergencyContactName'))

WebUI.click(findTestObject('Object Repository/OR_Employee/input_Emergency Contact Relation_emergencyC_f2a908'))

WebUI.click(findTestObject('Object Repository/OR_Employee/input_Emergency Phone 1_emergencyContactPhone'))

WebUI.click(findTestObject('Object Repository/OR_Employee/div_Employee Identity and BCA AccountFamily_0de46f'))

WebUI.click(findTestObject('Object Repository/OR_Employee/input_Emergency Phone 2_emergencyContactPho_8a4f00'))

WebUI.click(findTestObject('Object Repository/OR_Employee/button_Submit'))

WebUI.click(findTestObject('Object Repository/OR_Employee/button_Continue'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_Employee/div_Some fields has invalid value, please c_a06eb9'), 
    0)

