import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Employee/TC_EMPLOYEE_002'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/OR_Employee/Page_Employee - TESSA (Dev)/button_Employee_jss1265 jss1259 jss1260'))

WebUI.setText(findTestObject('Object Repository/OR_Employee/input__employmentNumber'), '321.12.11')

WebUI.setText(findTestObject('Object Repository/OR_Employee/input__fullName'), 'Muhammad Habli')

WebUI.click(findTestObject('Object Repository/OR_Employee/div_Gender'))

WebUI.click(findTestObject('Object Repository/OR_Employee/div_Male'))

WebUI.setText(findTestObject('Object Repository/OR_Employee/input__birthPlace'), 'Ciamis')

WebUI.mouseOver(findTestObject('Object Repository/OR_Employee/input__dateOfBirth'))

WebUI.setText(findTestObject('Object Repository/OR_Employee/input__dateOfBirth'), '02/03/1999')

WebUI.sendKeys(findTestObject('Object Repository/OR_Employee/input__dateOfBirth'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/OR_Employee/div_Company'))

WebUI.click(findTestObject('Object Repository/OR_Employee/div_Equine Global'))

WebUI.click(findTestObject('Object Repository/OR_Employee/div_Employment StatusEmployment Status is a_916094'))

WebUI.click(findTestObject('Object Repository/OR_Employee/div_Trainer'))

WebUI.click(findTestObject('Object Repository/OR_Employee/input__joinDate'))

WebUI.click(findTestObject('Object Repository/OR_Employee/button_1'))

WebUI.click(findTestObject('Object Repository/OR_Employee/button_OK'))

WebUI.click(findTestObject('Object Repository/OR_Employee/input_Date inActive_inactiveDate'))

WebUI.click(findTestObject('Object Repository/OR_Employee/div_10'))

WebUI.click(findTestObject('Object Repository/OR_Employee/button_OK'))

WebUI.click(findTestObject('Object Repository/OR_Employee/div__jss7424'))

WebUI.click(findTestObject('Object Repository/OR_Employee/div_K0'))

WebUI.click(findTestObject('Object Repository/OR_Employee/div_Blood Type'))

WebUI.click(findTestObject('Object Repository/OR_Employee/div_A'))

WebUI.click(findTestObject('Object Repository/OR_Employee/div_Religion'))

WebUI.click(findTestObject('Object Repository/OR_Employee/div_Islam'))

WebUI.scrollToPosition(0, 0)

WebUI.setText(findTestObject('Object Repository/OR_Employee/input__familyCardNumber'), '-')

WebUI.setText(findTestObject('Object Repository/OR_Employee/input__citizenNumber'), '-')

WebUI.setText(findTestObject('Object Repository/OR_Employee/input__taxNumber'), '-')

WebUI.click(findTestObject('Object Repository/OR_Employee/input_BPJS Employment Number_bpjsEmploymentNumber'))

WebUI.click(findTestObject('Object Repository/OR_Employee/input_BPJS Health Care Number_bpjsHealthCareNumber'))

WebUI.click(findTestObject('Object Repository/OR_Employee/div_BCA Account Number'))

WebUI.setText(findTestObject('Object Repository/OR_Employee/input__bankAccount'), '1234321')

WebUI.setText(findTestObject('Object Repository/OR_Employee/input__bankAccountName'), 'Habli')

WebUI.click(findTestObject('Object Repository/OR_Employee/input_BCA Account Branch_bankAccountBranch'))

WebUI.scrollToPosition(0, 0)

WebUI.setText(findTestObject('Object Repository/OR_Employee/input_Phone_phone'), '')

WebUI.setText(findTestObject('Object Repository/OR_Employee/input__mobilePhone'), '0832123')

WebUI.setText(findTestObject('Object Repository/OR_Employee/input__email'), 'habli@xsis.net')

WebUI.setText(findTestObject('Object Repository/OR_Employee/input__emailPersonal'), 'habli@gmail.com')

WebUI.setText(findTestObject('Object Repository/OR_Employee/input__addressCurrent'), 'Yogyakarta')

WebUI.setText(findTestObject('Object Repository/OR_Employee/input__address'), 'Ciamis')

WebUI.click(findTestObject('Object Repository/OR_Employee/input_Address (NPWP)_addressAdditional'))

WebUI.click(findTestObject('Object Repository/OR_Employee/input_Emergency Contact Name_emergencyContactName'))

WebUI.click(findTestObject('Object Repository/OR_Employee/input_Emergency Contact Relation_emergencyC_f2a908'))

WebUI.click(findTestObject('Object Repository/OR_Employee/input_Emergency Phone 1_emergencyContactPhone'))

WebUI.click(findTestObject('Object Repository/OR_Employee/input_Emergency Phone 2_emergencyContactPho_8a4f00'))

