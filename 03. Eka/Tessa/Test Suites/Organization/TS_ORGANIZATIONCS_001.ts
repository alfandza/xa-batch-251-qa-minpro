<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_ORGANIZATIONCS_001</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>0bb91461-a7f3-43d4-8622-d9beae829f66</testSuiteGuid>
   <testCaseLink>
      <guid>6847d63c-ab9a-45db-add6-4560d632a012</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_001</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>137a0757-4907-4862-a951-054bc02bc614</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_002</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>81f039aa-9905-4af0-a29a-9eab6901afc3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_003</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>802c86be-17bd-4e69-ba97-ffd3683a9062</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_004</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5f5148d3-83d5-4660-bd6e-fca9454ed963</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_005</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>47c73132-0ccd-4d5a-8213-6e05cc7464da</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_006</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>731883f2-5d0e-4807-a35e-214734c9e568</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_007</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1134c93a-6c45-4d28-9555-2fe7c5f90264</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_008</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9af5578c-a617-44d0-8c81-9d6a98585d7d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_009</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7da1e7bf-5ac8-4042-922e-de9bc0a5ac23</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_010</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1a1daff5-6211-4c77-bc61-b5add787f130</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_011</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7ba7fd9e-3088-471d-a178-a787a6c38077</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_012</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9435d40b-6f36-461b-b15e-17c89f6838ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_013</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f99d3f63-d45d-4e05-b8c2-c310abafcd36</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_014</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e820fce7-0f48-43b5-a815-01a939f113d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_015</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>17ddcb65-82cd-4931-8004-1398a03eb7c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_016</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7c530bb7-4ffc-403b-b9a6-b57e15dae365</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_017</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a68583f8-f6c0-4c69-b129-b28f1fbef2ae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_018</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7856ed46-4d41-4347-867e-86082b44427a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_019</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0a638338-06c3-4d4f-81c9-c2712dc96a3a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_020</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>80657654-e1c5-4a35-85a9-e7677c108f45</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_021</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>02f5d44d-75ba-47e5-8848-b3d6b396d2db</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_022</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>fc3dc8a4-2fa3-411a-97bf-8cd8a2ed9e85</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>f811c418-d8cf-4c9a-bf07-a26d7300cd84</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_023</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>411ced7c-6ea3-4d0d-a548-b252b1793c7f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>2fdcb32d-a369-4ff9-a3f0-5748e164aaaa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_024</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2ac14620-5e49-4529-828c-fa8cbb07cb80</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Negative/TC_ORGANIZATIONCS_025</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>adbe554f-734e-4c08-a3d6-3bda936761bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Negative/TC_ORGANIZATIONCS_026</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cb5dae7b-180c-45f0-aaa6-3fa50a7ec6dc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_027</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>fefc661f-4577-4b83-86d7-766ce52a8a39</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>7c46643f-66c9-4162-abbf-fe4eed5985be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_028</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>caa56517-f787-4bab-88f6-6d14390361f0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_029</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a0fe5758-a237-4a97-b0d9-eb905ab26f39</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_030</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3719ec08-7968-4ea6-b161-972f9e5bd0c8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_031</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b8d6dffd-c0e1-4eb3-9183-a6b21c0a70fb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_032</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fe838675-0620-4f85-a925-1fb7e6375af4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_033</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5c3c2f65-a8c2-4a64-a313-23da2c76395a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_034</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>618534bc-660d-4697-abd8-0ee1a7220392</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_035</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d5396e1a-6285-4ffa-be6c-164ef8d9eb21</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_036</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3d51bd4b-66b3-4826-933b-b5b78529d6b7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_037</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7a641623-59cb-4d91-b59d-71fc6d0318bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_038</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0c9826e9-f7e2-45f2-8692-0d1dfcc3fd7b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_039</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9422dcd6-66b1-4d22-9243-fb2459edfefc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_040</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>45e03bd4-9087-43e2-856a-db777d03d223</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_041</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>62f6e652-15ec-498d-a5a2-af9d45a18370</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_042</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bcd4a5bb-ae71-4fa9-b1f4-984725ce57e3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_043</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b33e6ac4-09b1-43d8-af4e-a142e54676fb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_044</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f1dd18d8-59b6-4490-a42f-aed2076e22e2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_045</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>40b610f6-dd6b-47cd-a7f5-1a1b2e538e7b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_046</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1b2e5772-c668-4f4e-aa09-20ead1b43609</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_047</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>44bfbb55-941b-4201-bc61-77d75459262b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_048</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9e245aa1-fa17-42b5-af4d-078b3b8b224f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_049</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3be0daa2-254b-4284-8726-6b025a40b857</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_050</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1c9f5f2d-20c3-4807-91b4-ef2f04457da6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_051</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d5065ac1-8490-4df7-9408-d083fef74e31</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_052</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1dec7246-b163-4201-bc5a-612c73dafa85</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_053</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a20c5c66-109d-45f9-bc79-01fdeed16513</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_054</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>01664a14-5802-46df-9f6e-659827c766b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_055</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bd6eacae-9872-4c76-816d-f4801ad90297</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_056</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7250ac22-ffca-4c4e-84bf-6c9202943272</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_057</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>88f3ed98-2e45-495c-a2eb-60ac982a96fb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_058</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5a68d286-9715-4813-9fb1-407487b506d1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_059</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>861b3a03-46d2-40d6-85f3-0478558640b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_060</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>355e2560-05df-4e58-b8e4-3dcebb14e8dd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_061</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eee025b3-4b44-413b-af46-7d37332aeca2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_062</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>29215404-1e0c-41fc-b1b5-26b6476a3195</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Negative/TC_ORGANIZATIONCS_063</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ccc3f7d7-2ce6-4011-a05d-3c81b1059aaf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Negative/TC_ORGANIZATIONCS_064</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2c73adc4-1e02-452a-a0eb-968d26c44489</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Negative/TC_ORGANIZATIONCS_065</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>65566342-074b-497e-9542-1d8c58d6d681</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Negative/TC_ORGANIZATIONCS_066</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>990a0a85-cd3c-4759-974f-69645b346548</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_067</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8e947a42-4639-4d7b-862d-52074571cbae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Negative/TC_ORGANIZATIONCS_068</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>434ccaba-1e91-4e02-b5f0-3a2c48b133d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Negative/TC_ORGANIZATIONCS_069</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>80637597-99b4-4efd-9ef7-3df414fa0c13</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Negative/TC_ORGANIZATIONCS_070</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d79817c8-76c2-4ace-af13-447c9b91b75e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_071</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>af99a3fb-da9f-4485-8e97-714205e370ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_072</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
