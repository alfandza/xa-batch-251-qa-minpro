<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_ORGANIZATIONATS_001</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>13de7288-4dd3-4b24-ae95-71f011d0c1aa</testSuiteGuid>
   <testCaseLink>
      <guid>104da0bf-9c6e-4c13-be75-e69c44f14497</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_001</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c018f409-8905-4009-a851-1236caaf1415</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_002</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2fff0708-1757-4c9b-80d7-f9708b1cf0a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_003</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>35440232-ff99-4bcf-847b-05ef6feb4ab9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_004</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e4319c8-6f35-4f22-a2e1-aff2dbce7545</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_005</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eb0977d7-8e7e-4c5d-aa1f-4e2a7e7b309a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_006</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a426314a-a6c2-44f7-a8e9-0f14ba821503</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_007</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2ffa0fe3-dc91-4db4-96ae-8a81818c9e61</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_008</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a014f340-2ea1-4ea8-bae9-235d011c05bf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_009</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>77418309-9cbb-4be2-8036-4f516e101416</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_010</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa44f7f1-e0d3-4fda-91e9-c9416448669a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_011</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cc93e36b-9459-44d2-a40b-3c26833f0b2b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_012</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e5390946-91cb-4959-bd1b-1dcb39c3df92</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_013</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a33dbb15-49e9-431d-9f3b-0a4c9502c205</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_014</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5650c041-fb53-4d38-985d-e1dcc74440fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_015</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>59791f9c-9274-4d2d-b790-cf56ac4a6053</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_016</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>39da837d-05ab-4e6a-95fc-9d46e34f9c89</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_017</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a9f5edf1-2e55-4d88-a877-a00fc2bb5b23</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_018</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0e1cd8ad-5184-4168-98ce-7d1dd0698c08</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_019</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>833af11a-d179-4f39-934c-6ee74a205800</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_020</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>012a45f6-b929-4b80-9f0a-14914b705a8a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_021</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c45ab9d6-4b9e-4593-92c5-f09d62ff8294</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_022</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2e06437a-70ce-4e47-8b66-084103808737</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>0c2de6fa-f38d-4534-bf28-ddc1bd0c117b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_023</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>90ea7e10-e95e-42eb-bb3b-e3c38517d1e5</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>83ecbaee-8f9d-433d-b0fa-8cd850d27b1b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_024</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>64cf2f8c-9010-4dc2-a234-bb2b23618357</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_027</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>0d868039-5cc7-43e5-9f85-237ac0087b40</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a56259c7-6599-4978-91c2-1829f807bd21</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_028</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3daa379e-95b6-4fd8-9d0c-fef5772f7958</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_029</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0ee3a88f-0fc6-4bb4-a263-aa511faab4d7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_030</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a296c638-9667-4a9a-b034-12c99c8cfad9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_031</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e6e0e775-bbe6-4e72-b9d3-2f1848448ab1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_032</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>90a6519f-e84c-4899-823d-3e1c23173f55</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_033</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>87edb698-e25c-4284-acc9-0abc8a960d9a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_034</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>929b4944-f114-4a41-87aa-e7cc9139acb5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_035</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa1a389a-ac49-449f-b909-ce1daba7e97f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_037</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6d0ab67a-a019-400a-99ab-88fcecf3c6b7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_036</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>15e7791e-4eb1-4c92-9b6c-42ca32280fcd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_038</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2136ab0e-d8a5-48da-ac7c-b54ceb838d98</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_039</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>878fc5fd-6f5d-4a74-a21e-c5e5868ac674</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_040</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>52e18907-330e-443f-9bec-6110d294217e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_041</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a712a953-4bef-4f98-b1bf-35ad30b4a523</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_042</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ce56a60a-b5fd-4c80-a6d0-dbc1ae07a632</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_043</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>86779655-72f1-42f9-bfd0-bfd11f9d38f4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_044</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b4b4b564-abc8-4aa1-819c-a6b63458953f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_045</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f9e77917-72ac-4799-b0b9-6bf91a5fbb2a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_046</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3e400c0b-3e26-4cc6-8819-6088bbc14023</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_047</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ced00999-f92f-4f07-bd0a-1ba32c5df60e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_048</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a1211c19-d155-48e0-8777-c6fec6745f75</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_049</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ee4f83a5-b63c-4b8c-aca9-ca7cd8adad64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_051</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f27dcc2-f94e-4ef3-aa9c-e2bf6835f78d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_050</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a428fc87-55f9-4768-a6fc-8caff3ba534e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_052</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9921204c-196e-4385-a17a-5a22c0e6dc30</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_053</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>67cd0917-5842-42f8-8e86-fdf526b93ca6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_054</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>49fa8709-f479-4103-ac1b-afdaf26dd729</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_055</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5a569bfb-78d9-4b9d-8285-b4a347a690ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_056</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a34d9dcf-8527-4ccd-b47e-1b0424def321</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_057</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>81823fdf-02b2-48f7-bbf1-ac622b27b766</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_058</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>058260cc-9b74-41cc-b5b7-4f568023056e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_059</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e3cec208-c190-4bae-9e6f-ba71dd6c7f0c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_060</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>95d3fa85-9030-41d5-9fa8-10094f51077a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_061</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>90d924e1-2b39-41bd-823a-df51a303caaf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_062</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2c675d9e-c6db-47d6-9016-bbcde1bc209f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Negative/TC_ORGANIZATIONATS_063</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>be36dbc7-42b3-421f-800f-890ef20de433</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Negative/TC_ORGANIZATIONATS_064</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bb98c127-6542-44f8-927a-34600b552863</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Negative/TC_ORGANIZATIONATS_065</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9322e923-bd2a-4564-af05-c71dc037bb9d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Negative/TC_ORGANIZATIONATS_066</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>58001d26-0aac-43bd-9681-dcca0e8ec14c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_067</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>19da6a9e-95a5-4526-bade-474d3828d4d9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Negative/TC_ORGANIZATIONATS_068</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0f4f1db1-4f87-4aaf-97c4-daf535f5db45</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Negative/TC_ORGANIZATIONATS_069</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>45763da8-55d2-4ca5-a5d1-fb836502a473</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Negative/TC_ORGANIZATIONATS_070</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf7c8c7e-6d25-42b6-9aea-7e59760c46f8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_071</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>41312216-e783-4917-8a83-549f43a413a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_072</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
