<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_APPROVHIERCS_001</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>d791939c-0d05-4c63-835f-6d0bb1b4d685</testSuiteGuid>
   <testCaseLink>
      <guid>41b30424-63a2-4f8f-becb-4676e843321e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_001</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5bf13417-be21-4935-a0c4-96edb8db017d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_002</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>675505de-ad03-4191-9241-99a66bff39b6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_003</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c58fe7d7-f3c6-4161-8fc7-70eed395a138</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_004</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fd6e74f3-5d44-44a3-b77d-15598fab4549</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_005</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0363574c-ad4b-4863-af57-84a48245c8f1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_006</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ca711a06-bac0-4dc8-8276-7de61b2cfea4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_007</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f76ff723-b239-4a8f-9f49-e657cc58f447</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_008</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e5941d21-c701-4625-ad1d-964e8c3aa08d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_009</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e4e0a427-0e7d-4fac-b505-89829247e9dd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_010</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>65ad6a9b-6762-4eef-ab72-153d3a038432</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_011</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>584f414f-ef33-4820-8368-fad93b8f6d77</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_012</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>04d8c5f8-0ca1-45e4-99de-6da179f48bb3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_013</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f6668fc9-d564-4257-97c8-1679ca691b75</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_014</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b4addf4e-2e43-415c-bb9c-1354c61ba16a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_015</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>93868e72-c9cf-455a-850b-e53f1b438880</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_016</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>787b62e8-d056-486e-8ef5-038041e934fb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_017</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f6f792b7-9640-4191-8132-800a2e633d69</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_018</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>913d64d5-dade-44da-83de-3bdd35d10f05</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_019</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c3a1eac3-c5c8-4b3f-974c-aa0ce6da9d13</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_020</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>599e8479-3dca-4520-983c-985022796b24</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_021</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>44f24d37-a8c1-4f7b-a89b-a29e50f95b42</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Negative/TC_APPROVHIERCS_022</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>52397b2b-2519-44f1-aa49-9aef95246f42</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Negative/TC_APPROVHIERCS_023</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf5f391e-89d9-45cc-9a71-00df83f55728</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_024</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1500787c-0af6-46cc-a08c-6f9689e62588</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_025</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>95c77e96-a534-442c-b502-97f5b751ae16</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_026</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1c7c96fa-c7e6-4b6f-ae24-3a44406948b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_027</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b256113d-9775-4d71-bfb5-8fdeb8fcda24</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_028</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a2b398da-0ab9-499e-9362-42c3fed32e7f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_029</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f1e74e3e-5134-49c8-95ef-d06e00ed859b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_030</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cdd3d8e5-a5fc-4a35-8939-e0ed155a4482</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_031</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>88729372-92f0-4dd3-bb0c-ed926d308def</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_032</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1609a6d1-831a-4092-86b5-fef3ab7a7b8c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_033</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4cd9b508-d832-4a1d-b934-7b0b0944b9ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_034</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>33467939-8a5a-4a09-b3f4-b784a42cc51e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_035</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7c088ad6-34ff-4c71-bc2a-6ab2b72b94ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_036</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>52898ea9-cfc7-4db3-b36e-d05afe7f2d00</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_037</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9ae09b3d-c391-4c4a-ae99-0333647d987d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_038</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9192e8ca-8703-4d28-a10c-5e3fec8f3314</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_039</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>62b62262-b9b1-47c6-bd95-8be10c27c83b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_040</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>27c64c37-4ffb-41a0-a7df-44d25056b8f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_041</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>26c92b2e-4f5c-4234-8c5c-eefa3a2b9c99</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_042</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>867728dc-d47f-4476-8de0-cd6cf2efce7d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Negative/TC_APPROVHIERCS_043</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>59aaabdb-8d71-463c-a72e-a20482c79c7d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Negative/TC_APPROVHIERCS_044</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b8dda5e7-f9a2-4837-a31c-ab98c42dbd07</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Negative/TC_APPROVHIERCS_045</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>64022772-395c-49ca-8c21-4359f8b3c444</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Negative/TC_APPROVHIERCS_046</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>620d4e3f-da07-4dc0-b1d5-c234d338958e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Negative/TC_APPROVHIERCS_047</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3efdbd9c-10d3-4b1e-8e13-820fbd6fac15</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Negative/TC_APPROVHIERCS_048</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>62b2852f-0f1f-48a6-9146-0245aa32b599</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_049</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3686b3f6-8866-43db-9ef5-500cc316ffa3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_050</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
