<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_GENERAL</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>368fb6ae-6234-4d5a-b5c7-b7b8b3a45966</testSuiteGuid>
   <testCaseLink>
      <guid>652cc304-6f9e-44e4-b24c-ae3259a304be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/General/TC_GENERAL_001</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>563ead3f-900b-4491-a1d1-c94cfaade6e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/General/TC_GENERAL_002</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6dd0a26f-87dd-4014-95ab-3edd7bc49790</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/General/TC_GENERAL_003</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>beefe128-4924-44c6-b073-2697bf445b01</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/General/TC_GENERAL_004</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4d051c9e-05ae-4a86-9153-867d4fb3681e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/General/TC_GENERAL_005</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f6f8eeec-d905-4497-81ac-8413752136f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/General/TC_GENERAL_006</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>82fec5bc-1b97-4344-b9ef-ca1d51ebda2b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/General/TC_GENERAL_007</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
