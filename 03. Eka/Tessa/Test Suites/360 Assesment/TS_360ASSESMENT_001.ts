<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_360ASSESMENT_001</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>c567b480-199b-453e-8d20-18776e462748</testSuiteGuid>
   <testCaseLink>
      <guid>de8879e0-cf2f-4775-8479-901a5d3aaabf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_001</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ad99de76-c1b6-45cd-bc43-65f5f01fe7d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_002</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d3467690-964a-47d0-a597-175629f8b71e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_003</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6eb55ed8-d627-49f0-aa99-a3811eacd442</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_004</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>82d8fa25-957f-47ae-84dd-26f9d3400c62</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_005</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a06ef9c0-f2c2-4eec-b810-6636e3a15e78</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_006</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8f62294d-3639-41bc-b4b4-a360a55f2cc7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_007</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bfebe948-31bc-41ff-b2a6-467ec2d3f300</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_008</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eb52744a-ad2d-4010-a576-093b311af883</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_009</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>16d19d7a-e3fc-47f9-a00b-b3856acb7dd3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_010</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e840798-b5e7-45d9-b2eb-28d6f7feb7b0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_011</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>079e946a-2af8-484e-8687-4da6d856d42a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_012</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1aa5be4a-73af-4c1b-b661-c7172bf9b6e5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_013</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b131967e-c7f5-4ffb-b1b0-bf46d1fb1bf4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_014</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>32741395-3772-43bd-80b9-92aadcde2fa5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_015</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bc85bf87-51e9-45b7-af5c-36249d996a72</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_016</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ab24fe2b-36cd-43e7-acb3-1be4dec26992</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_017</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>58fa1149-d029-4ac0-98a9-6f41d1ca1af5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_018</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>da9afdda-5b8c-4bab-bbc4-e201eea93ed9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_019</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fba6dc21-87f3-456f-806d-5fccab6f0971</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_020</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d6a9c6d8-e319-4d0b-a6fc-16eb6ac5d5cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_021</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>38e6f4df-ad80-4cc0-a708-cbfdbac256d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_022</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d8d75b03-bf56-412c-b5a5-2de51861b8ab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_023</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>667464e8-cfab-4069-8216-0612e71e1c40</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_024</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0984e52d-bdef-4b77-9044-5825baf22d70</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_025</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bd3d0de0-3a62-4ee5-898b-c11c57660f24</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Negative/TC_360ASSESMENT_026</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7c1ac345-bc96-427c-969c-1208364fb768</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Negative/TC_360ASSESMENT_027</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bedd0640-02d0-4ca3-8d16-270cc2e027af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_028</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2d7ca28c-5dcf-4faf-a07c-1b4bac1bb8b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_029</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dac2ed60-0c96-4188-a455-bfb656482781</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_030</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3b775edc-f177-45e7-bdc7-f649244806e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_031</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fd9a70fe-36aa-4f93-a82b-bed40c77a59c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_032</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b468aa9c-46eb-4fd9-9059-70b0798d1671</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_033</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>626362db-87fc-4ab1-8d58-4e4aebdd71a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_034</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cc2878ff-e48f-4f5e-aee2-cf791d4e9510</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_035</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b092fa0e-465f-472f-b01a-1d37f2632d50</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_036</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>870a348b-c579-414d-bc29-10e73f60a995</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_037</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ccd4d8d4-22af-4092-9b5a-e47149503ec7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_038</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5c7966d6-ac7c-415e-b184-f6da1748ca32</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_039</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1888f2af-7bae-4584-9525-dfc308461e98</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_040</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>200dc949-cdcb-43ea-96e4-2997dd59728a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_041</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e18ae0c6-0f33-4e8f-bce2-9959c85e81cc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_042</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f9d5699-8fa6-481c-b65e-eeb393606764</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_043</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7177e9de-3bb1-42d2-a8fc-366f096ee161</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_044</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5bf396b0-f3bd-4eab-98b1-54d2d91973f4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_045</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>59c09bf1-1106-427b-9cd2-a724ef3dc79b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_046</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>741f873a-2a6e-4f4c-9542-d0ce79303715</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Negative/TC_360ASSESMENT_047</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>72c8dfb7-f3f0-44aa-aec2-80df45711232</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Negative/TC_360ASSESMENT_048</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>23af86f5-381f-4943-b749-233051348d87</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Negative/TC_360ASSESMENT_049</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>07c9270b-e5fa-4ad6-bfb4-cac76f0992ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Negative/TC_360ASSESMENT_050</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a91131ea-f08b-49ae-8399-7bd97add8be2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Negative/TC_360ASSESMENT_051</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c2946679-3f7d-4b89-8b5e-4ac1834d9abf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Negative/TC_360ASSESMENT_052</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>15a31a52-c721-4d37-98f4-1dfd742d3690</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Human Resource - 360 Assessment/Automation Test Submitter/Negative/TC_360ASSESMENT_053</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
