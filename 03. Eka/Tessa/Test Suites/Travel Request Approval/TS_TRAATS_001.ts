<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_TRAATS_001</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>3222c540-0688-4831-9be4-c50e74c8ebae</testSuiteGuid>
   <testCaseLink>
      <guid>49fbac20-f448-40e9-8616-d370fccfc4e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Submitter/Positive/TC_TRAATS_001</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f7571f74-ff06-4722-bc35-733c27c7840a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Submitter/Positive/TC_TRAATS_002</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>199aea8c-7e07-4d67-9577-a77185070eb4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Submitter/Positive/TC_TRAATS_003</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7467190b-d22c-49dd-b7de-6791cd5770d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Submitter/Positive/TC_TRAATS_004</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b01af7d8-4a1a-4775-a1b6-367a14d3f7b6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Submitter/Positive/TC_TRAATS_005</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b9484e74-dbd1-4ccf-85d2-9b4213e1d992</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Submitter/Positive/TC_TRAATS_006</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0685ced8-01f3-4eda-8cbc-97d28abc8d61</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Submitter/Positive/TC_TRAATS_007</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3d7ed64f-c58f-4dab-a8cd-7f65e35fb980</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Submitter/Positive/TC_TRAATS_008</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9a334e79-e3b6-4f63-ac70-d9984aba8d26</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Submitter/Positive/TC_TRAATS_009</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>401594aa-ccbd-4603-a131-0a13689be52d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Submitter/Positive/TC_TRAATS_010</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f1f73c34-5b61-4d67-880a-38f208ef206f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Submitter/Positive/TC_TRAATS_011</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1bb85a20-e9bf-499f-85d1-533d7d56b7cb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Submitter/Positive/TC_TRAATS_012</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bb4712ec-556f-415d-a44b-efffdc27a6e5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Submitter/Positive/TC_TRAATS_013</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>12f37e77-7e77-4126-a642-59fe61beeed2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Submitter/Positive/TC_TRAATS_014</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dbf88430-b558-4424-9c7f-98ebc8ba1373</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Submitter/Positive/TC_TRAATS_015</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>114585d0-4d5c-44b3-87fd-90b7e11c5fe3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Submitter/Positive/TC_TRAATS_016</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8f9cea97-f8fd-40aa-b96b-63fbf6c8e723</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Submitter/Positive/TC_TRAATS_017</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f153fbe-7265-4797-9f71-c2493ec23567</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Submitter/Positive/TC_TRAATS_018</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>15501fd6-fa2a-4863-a432-3cf2971eb860</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Submitter/Positive/TC_TRAATS_019</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f1a509fb-cc3d-4919-9a14-bdf244851f72</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Submitter/Positive/TC_TRAATS_020</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d2cf827a-3796-4973-ab8f-53fd94a3cc23</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Submitter/Positive/TC_TRAATS_021</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fe2d2775-7806-4ac0-ac70-5cfdc1401683</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Submitter/Positive/TC_TRAATS_022</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fe5ecfe0-250b-47ca-b475-2875745d3a64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Submitter/Negative/TC_TRAATS_023</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dfa42f68-c36f-4fd8-8090-83cf4593a5b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Submitter/Negative/TC_TRAATS_024</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>40a87771-80c1-4091-a851-e5f583f6abb7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Submitter/Positive/TC_TRAATS_025</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
