<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_TRAATA_001</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>e441bd79-9c74-4916-95eb-1eab12261bb1</testSuiteGuid>
   <testCaseLink>
      <guid>bfb33de4-3d16-4c29-874e-93d03ea94202</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_001</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a7c4f958-c897-4187-ac4a-e2e183278f65</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_002</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6624abe0-0411-4822-8a8d-489747e5d09e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_003</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>109c9b7b-fdcc-4999-970f-959feb470cfe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_004</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1131e337-191a-4b8d-918b-351ab015e724</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_005</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a65c4fa1-aa9e-40af-bf68-129d7e09803b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_006</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cebece9b-e9b1-4047-a1ca-bcd5ba867af5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_007</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6b1322b1-9702-4435-b9c2-ed474b470ca3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_008</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>18a13616-ac53-42bc-a003-c01eaff9ede4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_009</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6cfedda2-a39a-4d3d-b312-a707eed08bbc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_010</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4d2ce84a-86fc-4fe5-9d5b-f832697c9b24</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_011</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>483dba06-0822-443f-b6c3-86074197604e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_012</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>31b07680-16c5-4cc7-ab47-9cf84924f2fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_013</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>73f03cc7-2ea1-4047-94d0-89623b77ed34</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_014</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e27ee69d-6f0a-4329-949f-42f27beabbc0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_015</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cae6066d-22af-4c1f-ab88-6ecba43bf823</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_016</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>af1b556e-1f64-4d61-acd6-3cf88b36336d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_017</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7f3c48c1-6974-4ad3-852e-a4e470fd1d01</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_018</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8227d64-5965-4370-adcf-29dacb00ff96</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_019</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8eb696be-f36a-492d-83ad-be90a83d74f4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_020</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>84b9170d-4949-45d2-b7a5-d357c3282e8b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_021</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4890798e-1777-4e5a-8da4-1d1512dd012e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_022</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>20126f03-2c68-45c4-8bae-a0281c08435e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Negative/TC_TRAATA_023</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dd493474-1eea-4bcb-88c9-2c6f4aaa6cef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Negative/TC_TRAATA_024</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5ca3a1dd-8f18-4302-9973-2f802d754498</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_025</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9dea2d6a-9fa2-44c9-aca3-731a4b59f16e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_026</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e7ee8f83-f501-4d3c-92e5-5e4cc7279f0f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_027</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f6dcd6c6-c984-4e28-81b0-c2002e6b87ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_028</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7cbe54fd-8e08-4d12-a461-79200cb40e3f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_029</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9b6a4676-661b-474a-8668-9f3179338199</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_030</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>65cf58a1-d36f-4b38-a143-5ab08fcded32</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Negative/TC_TRAATA_031</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f1d70eeb-2e61-41f4-b634-59afa9309cea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_032</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>927101ad-a3d4-4536-a15e-a917a593da6b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Negative/TC_TRAATA_033</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>075e44f0-7c66-43db-8eb6-bbb9e380218e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Negative/TC_TRAATA_034</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d49471ed-b9d1-4c2c-9b21-720970c1d9df</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_035</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
