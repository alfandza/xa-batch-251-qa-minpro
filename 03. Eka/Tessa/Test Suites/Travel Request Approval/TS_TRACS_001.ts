<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_TRACS_001</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>6c1ef9c0-e292-408d-bfee-3f258f3d0ddc</testSuiteGuid>
   <testCaseLink>
      <guid>b637b163-8b01-4fc3-918d-c0043c416481</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Curriculum Specialist/Positive/TC_TRACS_001</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0c05ebab-189c-4cd2-8ee2-a5ab3b06ff0c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Curriculum Specialist/Positive/TC_TRACS_002</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2104731f-cd38-4eb0-9480-ee0374f5960f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Curriculum Specialist/Positive/TC_TRACS_003</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>44d5c527-11e5-4c4b-afce-af6b9d9213e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Curriculum Specialist/Positive/TC_TRACS_004</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b2e66432-e2b7-4600-aa7f-3a0df1c26f78</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Curriculum Specialist/Positive/TC_TRACS_005</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>50beda34-9147-4154-abe1-793cc686490b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Curriculum Specialist/Positive/TC_TRACS_006</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1572db89-dfc6-41a5-8f20-0ef51bcab0ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Curriculum Specialist/Positive/TC_TRACS_007</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>065b1448-d72e-4025-9a45-d0e1cd9812d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Curriculum Specialist/Positive/TC_TRACS_008</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4ced0ed3-337f-4f9f-8e72-3df484857dbe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Curriculum Specialist/Positive/TC_TRACS_009</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7c4c86ff-2112-4206-baff-2ba8dbdb5665</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Curriculum Specialist/Positive/TC_TRACS_010</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ff1933c4-b9ac-48fa-9438-c425625713e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Curriculum Specialist/Positive/TC_TRACS_011</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>285fe043-159b-49d0-a177-f38b05cbdf47</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Curriculum Specialist/Positive/TC_TRACS_012</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bb50976f-5998-4831-96f2-34bf75299876</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Curriculum Specialist/Positive/TC_TRACS_013</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f6a3200f-2cc7-4a39-9274-898ca96e2991</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Curriculum Specialist/Positive/TC_TRACS_014</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c0fe887a-058f-4767-a4ec-64384ca3a612</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Curriculum Specialist/Positive/TC_TRACS_015</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b8b5d77d-e962-4171-bf4f-96fd034baf31</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Curriculum Specialist/Positive/TC_TRACS_016</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1e7f390a-9162-43fb-a655-b8571ddc7ca5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Curriculum Specialist/Positive/TC_TRACS_017</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>90431e36-b968-41ee-88f7-e873f13e253d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Curriculum Specialist/Positive/TC_TRACS_018</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3d793887-7849-4304-a8cd-f1a6da3245df</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Curriculum Specialist/Positive/TC_TRACS_019</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b0dd5757-338f-4061-923e-3ffdf3161309</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Curriculum Specialist/Positive/TC_TRACS_020</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1f5a8f8e-6865-4209-93c8-b0b87a42ee96</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Curriculum Specialist/Positive/TC_TRACS_021</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>012ac862-b675-44e6-acc4-b0060b97dc83</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Curriculum Specialist/Positive/TC_TRACS_022</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fbf8054c-49e2-4dd5-b0cc-d081c8dfb6e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Curriculum Specialist/Negative/TC_TRACS_023</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f1b2e9ca-3fe3-4715-b406-29e3a57b3f81</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Curriculum Specialist/Negative/TC_TRACS_024</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a6401cc6-964b-4ab1-b9fd-7b77f9da46b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Travel Request Approval/Curriculum Specialist/Positive/TC_TRACS_025</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
