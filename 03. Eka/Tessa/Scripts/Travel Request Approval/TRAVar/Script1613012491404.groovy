import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://xdev.equine.co.id/apps/tessa/travel/approvals')

WebUI.rightClick(findTestObject('Object Repository/OR_TRA/button_Collapse Sidebar_option-filter'))

WebUI.rightClick(findTestObject('Object Repository/OR_TRA/button_Collapse Sidebar_option-field'))

WebUI.rightClick(findTestObject('Object Repository/OR_TRA/button_Collapse Sidebar_option-order'))

WebUI.rightClick(findTestObject('Object Repository/OR_TRA/button_Collapse Sidebar_option-size'))

WebUI.rightClick(findTestObject('Object Repository/OR_TRA/button_Collapse Sidebar_option-sync'))

WebUI.click(findTestObject('Object Repository/OR_TRA/span_Customer'))

WebUI.click(findTestObject('Object Repository/OR_TRA/span_CV. Abiyyu'))

WebUI.click(findTestObject('Object Repository/OR_TRA/span_Status'))

WebUI.click(findTestObject('Object Repository/OR_TRA/span_Approved'))

WebUI.click(findTestObject('Object Repository/OR_TRA/span_Approval Completion'))

WebUI.click(findTestObject('Object Repository/OR_TRA/span_All'))

WebUI.click(findTestObject('Object Repository/OR_TRA/input_Notified Only_jss28668'))

WebUI.rightClick(findTestObject('Object Repository/OR_TRA/button_Reset'))

WebUI.rightClick(findTestObject('Object Repository/OR_TRA/button_Apply'))

WebUI.rightClick(findTestObject('Object Repository/OR_TRA/button_Customer_jss13440 jss13434'))

WebUI.rightClick(findTestObject('Object Repository/OR_TRA/svg_Status_jss13443'))

WebUI.rightClick(findTestObject('Object Repository/OR_TRA/path'))

WebUI.click(findTestObject('Object Repository/OR_TRA/button_Collapse Sidebar_option-field'))

WebUI.rightClick(findTestObject('Object Repository/OR_TRA/li_Travel ID'))

WebUI.rightClick(findTestObject('Object Repository/OR_TRA/li_Objective'))

WebUI.rightClick(findTestObject('Object Repository/OR_TRA/li_Target'))

WebUI.rightClick(findTestObject('Object Repository/OR_TRA/li_Comment'))

WebUI.rightClick(findTestObject('Object Repository/OR_TRA/li_Last Changes'))

WebUI.click(findTestObject('Object Repository/OR_TRA/button_Collapse Sidebar_option-order'))

WebUI.rightClick(findTestObject('Object Repository/OR_TRA/li_Ascending'))

WebUI.rightClick(findTestObject('Object Repository/OR_TRA/li_Descending'))

WebUI.click(findTestObject('Object Repository/OR_TRA/button_Collapse Sidebar_option-size'))

WebUI.rightClick(findTestObject('Object Repository/OR_TRA/li_10'))

WebUI.rightClick(findTestObject('Object Repository/OR_TRA/li_20'))

WebUI.rightClick(findTestObject('Object Repository/OR_TRA/li_30'))

WebUI.rightClick(findTestObject('Object Repository/OR_TRA/button_Collapse Sidebar_option-sync'))

