import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Travel Request Approval/Automation Test Approver/Positive/TC_TRAATA_027'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.scrollToElement(findTestObject('OR_TRA/Page_Travel Approval Detail - TESSA (Dev)/div_Approval'), 0)

WebUI.click(findTestObject('Object Repository/OR_TRA/Page_Travel Approval Detail - TESSA (Dev)/label_Approve'))

WebUI.setText(findTestObject('Object Repository/OR_TRA/Page_Travel Approval Detail - TESSA (Dev)/textarea_1'), 'Pastikan Project Selesai')

WebUI.click(findTestObject('Object Repository/OR_TRA/Page_Travel Approval Detail - TESSA (Dev)/button_Submit'))

WebUI.click(findTestObject('Object Repository/OR_TRA/Page_Travel Approval Detail - TESSA (Dev)/span_Continue'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_TRA/Page_Travel Approval Detail - TESSA (Dev)/div_Successfully processing approval submis_eed62d'), 
    0)

WebUI.verifyElementText(findTestObject('Object Repository/OR_TRA/Page_Travel Approval Detail - TESSA (Dev)/span_Successfully processing approval submission'), 
    'Successfully processing approval submission')

