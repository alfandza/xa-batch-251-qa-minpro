import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_016'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.comment('Company Name')

WebUI.click(findTestObject('Object Repository/OR_APPROVHIER/div__jss8260'))

WebUI.click(findTestObject('OR_APPROVHIER/VariableHelp/Modify/company'))

WebUI.comment('Hierarchy Name')

WebUI.click(findTestObject('Object Repository/OR_APPROVHIER/CRUD/input__name'), FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('Object Repository/OR_APPROVHIER/CRUD/input__name'), Keys.chord(Keys.END))

WebUI.sendKeys(findTestObject('Object Repository/OR_APPROVHIER/CRUD/input__name'), Keys.chord(Keys.SHIFT, Keys.HOME))

WebUI.sendKeys(findTestObject('Object Repository/OR_APPROVHIER/CRUD/input__name'), Keys.chord(Keys.DELETE))

WebUI.setText(findTestObject('Object Repository/OR_APPROVHIER/CRUD/input__name'), 'Project Automation Tester 64th')

WebUI.comment('Description')

WebUI.click(findTestObject('Object Repository/OR_APPROVHIER/CRUD/input_Description_description'), FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('Object Repository/OR_APPROVHIER/CRUD/input_Description_description'), Keys.chord(Keys.END))

WebUI.sendKeys(findTestObject('Object Repository/OR_APPROVHIER/CRUD/input_Description_description'), Keys.chord(Keys.SHIFT, 
        Keys.HOME))

WebUI.sendKeys(findTestObject('Object Repository/OR_APPROVHIER/CRUD/input_Description_description'), Keys.chord(Keys.DELETE))

WebUI.setText(findTestObject('Object Repository/OR_APPROVHIER/CRUD/input_Description_description'), 'Project Automation Testing ke 64')

WebUI.comment('Date Inactive')

WebUI.click(findTestObject('Object Repository/OR_APPROVHIER/input_Date Inactive_inactiveDate'))

WebUI.click(findTestObject('OR_APPROVHIER/DateOnly/button_right button date'))

WebUI.click(findTestObject('OR_APPROVHIER/DateOnly/button_21'))

WebUI.click(findTestObject('OR_APPROVHIER/DateOnly/button_OK'))

WebUI.comment('Position')

WebUI.click(findTestObject('Object Repository/OR_APPROVHIER/div__jss8260 (1)'))

WebUI.click(findTestObject('OR_APPROVHIER/VariableHelp/Modify/position'))

WebUI.comment('Relation')

WebUI.click(findTestObject('Object Repository/OR_APPROVHIER/div_Relation_jss8260'))

WebUI.click(findTestObject('OR_APPROVHIER/VariableHelp/Modify/relation'))

WebUI.comment('Submit')

WebUI.click(findTestObject('OR_APPROVHIER/CRUD/button_Submit'))

WebUI.click(findTestObject('OR_APPROVHIER/CRUD/button_Continue'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_APPROVHIER/CRUD/div_A new Hierarchy H0600 has been created'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_APPROVHIER/CRUD/div_Hierarchy IDCompany NameHierarchy NameD_d99a44'), 
    0)

