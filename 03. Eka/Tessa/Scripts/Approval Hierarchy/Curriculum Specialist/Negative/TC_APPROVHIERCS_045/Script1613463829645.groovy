import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_025'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.comment('Position')

WebUI.verifyElementNotClickable(findTestObject('Object Repository/OR_APPROVHIER/div__jss8260 (1)'))

WebUI.comment('Submit')

WebUI.click(findTestObject('OR_APPROVHIER/CRUD/button_Submit'))

WebUI.click(findTestObject('OR_APPROVHIER/CRUD/button_Continue'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_APPROVHIER/CRUD/Error/p_Company Name is a required field'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_APPROVHIER/CRUD/Error/p_Hierarchy Name is a required field'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_APPROVHIER/CRUD/Error/p_Position is a required field'), 
    0)

