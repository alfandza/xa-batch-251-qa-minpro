import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Approval Hierarchy/Curriculum Specialist/Positive/TC_APPROVHIERCS_025'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.comment('Company Name')

WebUI.click(findTestObject('Object Repository/OR_APPROVHIER/div__jss8260'))

WebUI.click(findTestObject('OR_APPROVHIER/VariableHelp/Create/company'))

WebUI.comment('Hierarchy Name')

WebUI.setText(findTestObject('Object Repository/OR_APPROVHIER/CRUD/input__name'), 'Project Automation Tester')

WebUI.comment('Description')

WebUI.setText(findTestObject('Object Repository/OR_APPROVHIER/CRUD/input_Description_description'), 'New Project Automation Testing')

WebUI.comment('Date Inactive')

WebUI.click(findTestObject('Object Repository/OR_APPROVHIER/input_Date Inactive_inactiveDate'))

WebUI.click(findTestObject('OR_APPROVHIER/DateOnly/button_OK'))

WebUI.comment('Level')

WebUI.click(findTestObject('OR_APPROVHIER/CRUD/input__items.0.sequence'))

WebUI.sendKeys(findTestObject('Object Repository/OR_APPROVHIER/CRUD/input__items.0.sequence'), Keys.chord(Keys.END))

WebUI.sendKeys(findTestObject('Object Repository/OR_APPROVHIER/CRUD/input__items.0.sequence'), Keys.chord(Keys.BACK_SPACE))

WebUI.setText(findTestObject('Object Repository/OR_APPROVHIER/CRUD/input__items.0.sequence'), '1')

WebUI.comment('Position')

WebUI.click(findTestObject('Object Repository/OR_APPROVHIER/div__jss8260 (1)'))

WebUI.click(findTestObject('OR_APPROVHIER/VariableHelp/Create/position'))

WebUI.comment('Relation')

WebUI.click(findTestObject('Object Repository/OR_APPROVHIER/div_Relation_jss8260'))

WebUI.click(findTestObject('OR_APPROVHIER/VariableHelp/Create/relation'))

WebUI.comment('Submit')

WebUI.click(findTestObject('OR_APPROVHIER/CRUD/button_Submit'))

WebUI.click(findTestObject('OR_APPROVHIER/CRUD/button_Continue'))

WebUI.verifyElementPresent(findTestObject('OR_APPROVHIER/DateOnly/p_date must be over than today'), 0, FailureHandling.CONTINUE_ON_FAILURE)

