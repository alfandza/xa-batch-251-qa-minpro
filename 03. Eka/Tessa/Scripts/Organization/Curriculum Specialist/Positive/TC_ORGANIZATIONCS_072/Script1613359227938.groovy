import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Organization/Curriculum Specialist/Positive/TC_ORGANIZATIONCS_002'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_Niagaprima ParamitraAdministratorAdmini_75b97d'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/span_Details'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/h6_Structure Details'), 0)

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Structure Details_organization-struc_77ae73'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/li_Refresh'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/li_Modify'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/li_Delete'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Agree'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/div_Structure with ID O0350 has been deleted'), 
    0)

