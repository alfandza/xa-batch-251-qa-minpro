import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Organization/Automation Test Submitter/Positive/TC_ORGANIZATIONATS_028'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.comment('Input Company')

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div__jss38111'))

WebUI.click(findTestObject('OR_ORGANIZATION/company'))

WebUI.comment('Input Position')

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div__jss23567 (2)'))

WebUI.click(findTestObject('OR_ORGANIZATION/position'))

WebUI.comment('Description')

WebUI.setText(findTestObject('OR_ORGANIZATION/input_Description_description (1)'), 'Project Automation Tester 10th')

WebUI.comment('Input Super Ordinate')

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div__jss38111 (1)'))

WebUI.click(findTestObject('OR_ORGANIZATION/superordinate'))

WebUI.comment('Input Date Inactive')

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/input_Date Inactive_inactiveDate (1)'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/div_2021Sun, Feb 14'), 0)

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Sun, Feb 14_jss13440 jss13434 jss24664'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_15'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_OK'))

WebUI.comment('Input Start Date')

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/input__start'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/div_2021Sun, Feb 14'), 0)

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Sun, Feb 14_jss13440 jss13434 jss24664'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_15'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_OK'))

WebUI.comment('Input End Date')

WebUI.click(findTestObject('OR_ORGANIZATION/input_End Date_end'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/div_2021Sun, Feb 14'), 0)

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_15'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_OK'))

WebUI.comment('Submit')

WebUI.click(findTestObject('OR_ORGANIZATION/span_Submit'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Continue'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/p_date must be over than today'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/p_date must be over than start date'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

