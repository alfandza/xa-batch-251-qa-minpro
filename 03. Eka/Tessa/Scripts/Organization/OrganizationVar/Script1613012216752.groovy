import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://xdev.equine.co.id/apps/tessa/')

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/svg_Setup_jss2708 jss2711 jss4649 jss4664'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/div_Setup_jss5057'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/div_Setup'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/div_Setup'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_Setup'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/div_Approval Hierarchy'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/div_Approval Hierarchy'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/div_Organization'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/div_Organization'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_Organization'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/h6_Organization Structure'), 0)

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/svg_Collapse Sidebar_jss2708'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/div_Collapse Sidebar_jss2705 jss2699 jss6969'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/div_Optima Data InternasionalCloud Delivery_65a313'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/svg_Collapse Sidebar_jss2708'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/button_Modify'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/button_Details'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/button_Collapse Sidebar_jss2705 jss2699 jss2702'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Collapse Sidebar_jss2705 jss2699 jss2702'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/h6_Organization Chart'), 0)

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Expand All'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Collapse All'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Collapse Sidebar_jss2705 jss2699 jss2702'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Collapse Sidebar_jss2705 jss2699 jss2702'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Collapse Sidebar_jss2705 jss2699 jss2702'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Collapse Sidebar_jss2705 jss2699 jss2702'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_Organization ChartExpand AllDirektur CO_ed7d0e'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/button_Collapse Sidebar_option-filter'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/button_Collapse Sidebar_option-field'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/button_Collapse Sidebar_option-order'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/button_Collapse Sidebar_option-size'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/button_Collapse Sidebar_option-sync'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/button_Collapse Sidebar_jss2705 jss2699 jss2702'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Collapse Sidebar_option-filter'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/button_Collapse Sidebar_jss2705 jss2699 jss2702'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/button_Apply'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Collapse Sidebar_jss2705 jss2699 jss2702'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/div_antar niaga daya'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/div_Equine Global'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/div_Niagaprima Paramitra'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/div_Optima Data Internasional'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/div_Test Comp'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_Xsis Mitra Utama'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/svg_Company_jss2708'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/button_Reset'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/p_Xsis Mitra Utama'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/span_Company'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Apply'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/path'), 0)

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Collapse Sidebar_option-filter'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Reset'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/span_Apply'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Collapse Sidebar_option-filter'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/span_Company'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_Test Comp'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/svg_Company_jss2708_1'))

WebUI.verifyElementText(findTestObject('Object Repository/OR_ORGANIZATION/p_None'), 'None')

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/span_Apply'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Collapse Sidebar_option-field'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/li_Structure ID'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/li_Description'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/li_Last Changes'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/li_Structure ID'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Collapse Sidebar_option-field'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/li_Structure ID'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_Notifications_jss4319 jss4320'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_Notifications_jss4319 jss4320'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Collapse Sidebar_option-order'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/li_Ascending'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/li_Descending'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Collapse Sidebar_option-size'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/li_10'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/li_20'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/li_30'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Collapse Sidebar_option-sync'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/div_Xsis Mitra UtamaDirektur Utamates Expir_d7416b'))

WebUI.setText(findTestObject('Object Repository/OR_ORGANIZATION/input_Organization Structure_jss6248 jss6036'), 'Xsis')

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/button_Organization Structure_search.fields'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/svg_Organization Structure_jss2708 jss2715'), 
    0)

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/svg_Organization Structure_jss2708 jss2715_1'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/li_Xsis in Any'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/li_Xsis in Structure ID'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/li_Xsis in Description'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/li_Xsis in Any'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/svg_Organization Structure_jss2708 jss2715'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Collapse Sidebar_jss2705 jss2699 jss2702'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div__jss9403'))

WebUI.setText(findTestObject('Object Repository/OR_ORGANIZATION/input__react-select-2-input'), '')

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_antar niaga daya (1)'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/svg__css-19bqh2r_1'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_Equine Global (1)'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/svg__css-19bqh2r_1'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_Niagaprima Paramitra (1)'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/svg__css-19bqh2r_1'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_Optima Data Internasional (1)'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/svg__css-19bqh2r_1'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_Test Comp (1)'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/svg__css-19bqh2r_1'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_staff'))

WebUI.setText(findTestObject('Object Repository/OR_ORGANIZATION/input_Description_description'), 'Test')

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/input_Date Inactive_inactiveDate'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/h6_2021'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/div_190019011902190319041905190619071908190_9eb598'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_2022'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/svg_Fri, Feb 11_jss2708'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/svg_Fri, Feb 11_jss2708'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/svg_Fri, Feb 11_jss2708'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/svg_Fri, Feb 11_jss2708'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/button_OK'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/button_Cancel'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/button_Clear'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_OK'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/svg__css-19bqh2r_1'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_staff_1'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/input__start'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/button_Cancel'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/button_OK'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/svg_Thu, Feb 11_jss2708'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/svg_Thu, Feb 11_jss2708'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/svg_Thu, Feb 11_jss2708'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/svg_Thu, Feb 11_jss2708'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/h6_2021'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_2020'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_9'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Today'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/span_OK'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/input_End Date_end'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/button_Clear'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/button_Cancel'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/button_OK'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/svg_Thu, Feb 11_jss2708_1'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/svg_Thu, Feb 11_jss2708_1'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/path (1)'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/svg_Thu, Feb 11_jss2708_1'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/h6_2021'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_2022'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_10'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_12'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/span_11'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_OK'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/span_Add'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/span_Reset'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/span_Submit'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Submit'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/h6_Submit Confirmation'), 0)

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/button_Discard'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/span_Continue'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/div_DateCorrelation IDStatusInsufficient ac_f60fd0'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/div_StatusInsufficient access roles, No acc_1614f9'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/textarea_Insufficient access roles, No acce_a2e523'), 
    0)

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/span_Reset'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/span_Submit'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/span_Continue'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/p_Company is a required field'), 0)

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_Structure IDThis field value will be au_eef75c'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/p_Start Date is a required field'), 0)

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_Super OrdinatePosition is a required fi_0f4148'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_Super OrdinatePosition is a required fi_0f4148'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_Super OrdinatePosition is a required fi_0f4148'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/div_Super OrdinatePosition is a required fi_0f4148'), 
    0)

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_Structure IDThis field value will be au_eef75c'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/div_Structure IDThis field value will be au_eef75c'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/span_Structure of Organization'), 0)

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_New Structure_jss2705 jss2699 jss2700'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_Details_jss2705 jss2699 jss11335'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Modify'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/path (2)'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_BS 2 - Business Support Analyst Manager'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/svg__css-19bqh2r (1)'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_BS 2 - Business Support Analyst'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/span_Submit (1)'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/h6_Modify Confirmation'), 0)

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/span_Discard'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/span_Continue (1)'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/div_DateCorrelation IDStatusInsufficient ac_f60fd0 (1)'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/div_StatusInsufficient access roles, No acc_1614f9 (1)'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/textarea_Insufficient access roles, No acce_a2e523 (1)'), 
    0)

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/input__start (1)'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Cancel (1)'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_Structure IDCompanyXsis Mitra UtamaPosi_7300d4'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/p_Position is a required field'), 0)

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/p_Position is a required field'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_Super OrdinatePosition is a required fi_3e1055'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/p_Position is a required field'), 0)

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/p_Position is a required field'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_PositionPosition is a required field'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Modify Structure_jss2705 jss2699 jss2700'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/svg_Collapse Sidebar_jss2708_1'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/button_Details'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/div_Structure of Organization'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/span_Structure of Organization (1)'), 0)

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/path (3)'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/li_Refresh'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/li_Modify'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/li_Delete'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/li_Delete'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/h6_Delete Confirmation'), 0)

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/span_Disagree'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/span_Agree'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/div_Unable to process your submission'), 0)

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/path_1'))

WebUI.setText(findTestObject('Object Repository/OR_ORGANIZATION/input_Organization Structure_jss11654 jss11442'), 'Administrator')

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/svg_Structure Details_jss2708'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/svg_Organization Structure_jss7874 jss7881'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/svg_Organization Structure_jss7874 jss7881_1'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/li_Administrator in Any'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/li_Administrator in Structure ID'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/li_Administrator in Description'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/div_concat(id(, , appbar-more-menu, , )divc_179a10'))

WebUI.setText(findTestObject('Object Repository/OR_ORGANIZATION/input_Organization Structure_jss11654 jss11442'), 'Administrator')

WebUI.sendKeys(findTestObject('Object Repository/OR_ORGANIZATION/input_Organization Structure_jss11654 jss11442'), Keys.chord(
        Keys.ENTER))

