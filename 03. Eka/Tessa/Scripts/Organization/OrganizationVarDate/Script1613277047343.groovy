import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_1'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_2'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_3'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_4'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_5'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_6'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_7'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_8'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_9'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_10'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_11'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_12'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_13'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_14'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_15'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_16'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_17'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_18'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_19'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_20'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_21'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_22'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_23'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_24'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_25'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_26'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_27'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_28'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_29'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_30'))

WebUI.click(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_31'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_Clear'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_Cancel'))

WebUI.rightClick(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/button_OK'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/p_date must be over than today'), 
    0)

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_ORGANIZATION/DateOnly/p_date must be over than start date'), 
    0)

