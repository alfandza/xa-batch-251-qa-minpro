import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://xdev.equine.co.id/apps/tessa/')

WebUI.rightClick(findTestObject('Object Repository/OR_General/span_Login'))

WebUI.click(findTestObject('Object Repository/OR_General/button_Let me in'))

WebUI.switchToWindowTitle('Login - Equine Identity')

WebUI.setText(findTestObject('Object Repository/OR_General/input_Login_Username'), 'della.mutiara@xsis.net')

WebUI.setEncryptedText(findTestObject('Object Repository/OR_General/input_Login_Password'), 'zP4F53vxAWI=')

WebUI.rightClick(findTestObject('Object Repository/OR_General/a_Forgot password'))

WebUI.click(findTestObject('Object Repository/OR_General/button_Login'))

WebUI.switchToWindowTitle('Welcome to New TESSA')

WebUI.rightClick(findTestObject('Object Repository/OR_General/div_Hi DELLA MUTIARAWelcome to TESSA, pleas_d1364a'))

WebUI.click(findTestObject('Object Repository/OR_General/svg_Hi DELLA MUTIARA_jss1681'))

WebUI.click(findTestObject('Object Repository/OR_General/div_Xsis Mitra Utama_jss1813 jss1807 jss1824'))

WebUI.rightClick(findTestObject('Object Repository/OR_General/span_Curriculum Specialist'))

WebUI.rightClick(findTestObject('Object Repository/OR_General/span_Automation Test Submitter'))

