import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://xdev.equine.co.id/apps/tessa/')

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_General/h6_Dashboard'), 0)

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_General/span_XMU - Curriculum Specialist'), 0)

WebUI.click(findTestObject('Object Repository/OR_General/svg_XMU - Curriculum Specialist_jss2708'))

WebUI.click(findTestObject('Object Repository/OR_General/li_Switch Access'))

WebUI.rightClick(findTestObject('Object Repository/OR_General/button_Discard'))

WebUI.click(findTestObject('Object Repository/OR_General/span_Continue'))

WebUI.rightClick(findTestObject('Object Repository/OR_General/div_Hi DELLA MUTIARAWelcome to TESSA, pleas_6a8e97'))

WebUI.click(findTestObject('Object Repository/OR_General/li_Dark Mode'))

WebUI.click(findTestObject('Object Repository/OR_General/button_XMU - Curriculum Specialist_user-nav-option'))

WebUI.click(findTestObject('Object Repository/OR_General/li_Logout'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_General/h6_Logout Confirmation'), 0)

WebUI.rightClick(findTestObject('Object Repository/OR_General/button_No'))

WebUI.rightClick(findTestObject('Object Repository/OR_General/button_Yes'))

WebUI.click(findTestObject('Object Repository/OR_General/button_No'))

WebUI.click(findTestObject('Object Repository/OR_General/button_XMU - Curriculum Specialist_user-nav-option'))

WebUI.click(findTestObject('Object Repository/OR_General/li_Switch Access'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_General/h6_Switching Confirmation'), 0)

WebUI.click(findTestObject('Object Repository/OR_General/button_Discard'))

