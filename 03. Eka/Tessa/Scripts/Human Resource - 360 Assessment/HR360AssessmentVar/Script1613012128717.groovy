import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Object Repository/OR_360ASSESMENT/div_Human Resource'))

WebUI.click(findTestObject('Object Repository/OR_360ASSESMENT/div_360 Assessment'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_360ASSESMENT/h6_360 Assessment'), 0)

WebUI.click(findTestObject('Object Repository/OR_360ASSESMENT/button_Collapse Sidebar_option-filter'))

WebUI.click(findTestObject('Object Repository/OR_360ASSESMENT/span_Year'))

WebUI.rightClick(findTestObject('Object Repository/OR_360ASSESMENT/span_2020'))

WebUI.rightClick(findTestObject('Object Repository/OR_360ASSESMENT/span_2021'))

WebUI.rightClick(findTestObject('Object Repository/OR_360ASSESMENT/span_2022'))

WebUI.click(findTestObject('Object Repository/OR_360ASSESMENT/span_2021'))

WebUI.click(findTestObject('Object Repository/OR_360ASSESMENT/span_Company'))

WebUI.rightClick(findTestObject('Object Repository/OR_360ASSESMENT/span_antar niaga daya'))

WebUI.rightClick(findTestObject('Object Repository/OR_360ASSESMENT/span_Automation Tester'))

WebUI.rightClick(findTestObject('Object Repository/OR_360ASSESMENT/span_Equine Global'))

WebUI.rightClick(findTestObject('Object Repository/OR_360ASSESMENT/span_Niagaprima Paramitra'))

WebUI.rightClick(findTestObject('Object Repository/OR_360ASSESMENT/span_Optima Data Internasional'))

WebUI.rightClick(findTestObject('Object Repository/OR_360ASSESMENT/span_Programmer'))

WebUI.rightClick(findTestObject('Object Repository/OR_360ASSESMENT/span_Test Comp'))

WebUI.rightClick(findTestObject('Object Repository/OR_360ASSESMENT/span_Xsis Mitra Utama'))

WebUI.click(findTestObject('Object Repository/OR_360ASSESMENT/span_Equine Global'))

WebUI.click(findTestObject('Object Repository/OR_360ASSESMENT/input_Assessment Completed_jss12101'))

WebUI.click(findTestObject('Object Repository/OR_360ASSESMENT/input_Assessment Completed_jss12101'))

WebUI.rightClick(findTestObject('Object Repository/OR_360ASSESMENT/button_Reset'))

WebUI.click(findTestObject('Object Repository/OR_360ASSESMENT/button_Apply'))

WebUI.click(findTestObject('Object Repository/OR_360ASSESMENT/button_Collapse Sidebar_option-field'))

WebUI.rightClick(findTestObject('Object Repository/OR_360ASSESMENT/li_ID'))

WebUI.click(findTestObject('Object Repository/OR_360ASSESMENT/li_Last Changes'))

WebUI.click(findTestObject('Object Repository/OR_360ASSESMENT/button_Collapse Sidebar_option-order'))

WebUI.rightClick(findTestObject('Object Repository/OR_360ASSESMENT/li_Descending'))

WebUI.click(findTestObject('Object Repository/OR_360ASSESMENT/li_Ascending'))

WebUI.click(findTestObject('Object Repository/OR_360ASSESMENT/button_Collapse Sidebar_option-size'))

WebUI.rightClick(findTestObject('Object Repository/OR_360ASSESMENT/li_30'))

WebUI.rightClick(findTestObject('Object Repository/OR_360ASSESMENT/li_20'))

WebUI.click(findTestObject('Object Repository/OR_360ASSESMENT/li_10'))

WebUI.click(findTestObject('Object Repository/OR_360ASSESMENT/button_Collapse Sidebar_option-sync'))

WebUI.setText(findTestObject('Object Repository/OR_360ASSESMENT/input_DM_jss11206 jss10994'), '123')

WebUI.click(findTestObject('Object Repository/OR_360ASSESMENT/button_DM_search.fields'))

WebUI.rightClick(findTestObject('Object Repository/OR_360ASSESMENT/li_123 in Any'))

WebUI.rightClick(findTestObject('Object Repository/OR_360ASSESMENT/li_123 in ID'))

WebUI.click(findTestObject('Object Repository/OR_360ASSESMENT/button_DM_jss7625 jss7619 jss10995'))

WebUI.click(findTestObject('Object Repository/OR_360ASSESMENT/button_Details'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_360ASSESMENT/div_AGUSSANTOSOXsis Mitra UtamaDirektur COS_e36576'), 
    0)

WebUI.click(findTestObject('Object Repository/OR_360ASSESMENT/div_AGUSSANTOSOXsis Mitra UtamaDirektur COS_e36576'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_360ASSESMENT/button_Result'), 0)

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_360ASSESMENT/button_Details'), 0)

WebUI.click(findTestObject('Object Repository/OR_360ASSESMENT/button_Result'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_360ASSESMENT/h6_360 Assessment Result'), 0)

WebUI.click(findTestObject('Object Repository/OR_360ASSESMENT/div_AGUSSANTOSOXsis Mitra UtamaDirektur COS_e36576'))

WebUI.click(findTestObject('Object Repository/OR_360ASSESMENT/button_Details'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_360ASSESMENT/h6_360 Assessment Detail'), 0)

