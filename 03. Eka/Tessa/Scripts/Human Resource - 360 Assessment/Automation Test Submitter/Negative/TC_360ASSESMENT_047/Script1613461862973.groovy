import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Human Resource - 360 Assessment/Automation Test Submitter/Positive/TC_360ASSESMENT_029'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.comment('Submit')

WebUI.click(findTestObject('OR_360ASSESMENT/CRUD/button_Submit'))

WebUI.click(findTestObject('OR_360ASSESMENT/CRUD/button_Continue'))

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_360ASSESMENT/CRUD/p_Year is a required field'), 0)

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_360ASSESMENT/CRUD/p_Company is a required field'), 0)

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_360ASSESMENT/CRUD/p_Position is a required field'), 0)

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_360ASSESMENT/CRUD/p_Employee is a required field'), 0)

WebUI.verifyElementPresent(findTestObject('Object Repository/OR_360ASSESMENT/CRUD/span_Must have at least one responder'), 
    0)

