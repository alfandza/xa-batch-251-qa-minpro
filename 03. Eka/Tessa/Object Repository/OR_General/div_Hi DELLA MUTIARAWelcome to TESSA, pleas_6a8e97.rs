<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Hi DELLA MUTIARAWelcome to TESSA, pleas_6a8e97</name>
   <tag></tag>
   <elementGuidId>f247ec18-943d-45ba-a578-5ac276105ea5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.jss6062.jss6065.jss6066.jss6070.jss6072</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div/div/div/div/ul/li/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss6062 jss6065 jss6066 jss6070 jss6072</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Hi DELLA MUTIARAWelcome to TESSA, please select your company access</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss5344&quot;]/div[@class=&quot;jss5363&quot;]/div[@class=&quot;jss5576 jss5599 jss5593&quot;]/div[@class=&quot;jss5577 jss5616 jss5626 jss5638 jss5650&quot;]/div[@class=&quot;jss6031 jss6034 jss5364&quot;]/ul[@class=&quot;jss6058 jss6059&quot;]/li[@class=&quot;jss6063&quot;]/div[@class=&quot;jss6062 jss6065 jss6066 jss6070 jss6072&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div/div/div/div/ul/li/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Xsis Mitra Utama'])[1]/preceding::div[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/div</value>
   </webElementXpaths>
</WebElementEntity>
