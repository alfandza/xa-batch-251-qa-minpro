<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Company is a required field</name>
   <tag></tag>
   <elementGuidId>8e8b878d-ea53-4be6-9b97-baf125ae2e0d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>p.jss9393.jss9394.jss9400</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[2]/div/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss9393 jss9394 jss9400</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Company is a required field</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1732&quot;]/main[@class=&quot;jss2248 jss2250&quot;]/form[1]/div[@class=&quot;jss8726&quot;]/div[@class=&quot;jss8727&quot;]/div[@class=&quot;jss8730&quot;]/div[@class=&quot;jss3511 jss3514 jss9350&quot;]/div[@class=&quot;jss9357&quot;]/div[@class=&quot;css-10nd86i&quot;]/div[@class=&quot;jss9358 jss9359 jss9361&quot;]/p[@class=&quot;jss9393 jss9394 jss9400&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[2]/div/p</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Company is a required field']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/p</value>
   </webElementXpaths>
</WebElementEntity>
