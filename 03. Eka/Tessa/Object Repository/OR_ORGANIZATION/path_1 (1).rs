<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>path_1 (1)</name>
   <tag></tag>
   <elementGuidId>5c0bf899-ea54-4abd-976a-4ef1f6e8cce0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>path</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>d</name>
      <type>Main</type>
      <value>M14 12c0-1.1-.9-2-2-2s-2 .9-2 2 .9 2 2 2 2-.9 2-2zm-2-9c-4.97 0-9 4.03-9 9H0l4 4 4-4H5c0-3.87 3.13-7 7-7s7 3.13 7 7-3.13 7-7 7c-1.51 0-2.91-.49-4.06-1.3l-1.42 1.44C8.04 20.3 9.94 21 12 21c4.97 0 9-4.03 9-9s-4.03-9-9-9z</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;mui-fixed jss3558 jss3544 jss5596&quot;]/div[@class=&quot;jss3547 jss3545&quot;]/div[@class=&quot;jss3511 jss3537 jss3512 jss3548 jss3549 jss3552 jss3557&quot;]/div[@class=&quot;jss7158 jss5638&quot;]/ul[@class=&quot;jss4553 jss4554&quot;]/li[@class=&quot;jss4558&quot;]/div[@class=&quot;jss5057&quot;]/button[@class=&quot;jss2705 jss2699&quot;]/span[@class=&quot;jss2704&quot;]/svg[@class=&quot;jss2708&quot;]/path[2]</value>
   </webElementProperties>
</WebElementEntity>
