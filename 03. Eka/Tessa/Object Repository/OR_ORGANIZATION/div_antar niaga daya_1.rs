<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_antar niaga daya_1</name>
   <tag></tag>
   <elementGuidId>69181b3e-09f8-444a-b312-bf491c6a2c98</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[2]/div/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.jss38111</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss38111</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>antar niaga daya</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss12467&quot;]/main[@class=&quot;jss12983 jss12985&quot;]/form[1]/div[@class=&quot;jss37434&quot;]/div[@class=&quot;jss37435&quot;]/div[@class=&quot;jss37438&quot;]/div[@class=&quot;jss13784 jss13787 jss38058&quot;]/div[@class=&quot;jss38065&quot;]/div[@class=&quot;css-10nd86i&quot;]/div[@class=&quot;jss38066 jss38067 jss38069&quot;]/div[@class=&quot;jss34981 jss38088 jss38092 jss34990 jss38095 jss34983 jss38090 jss34982 jss38089&quot;]/div[@class=&quot;jss34991 jss38096 jss38109&quot;]/div[@class=&quot;jss38111&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[2]/div/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='|'])[1]/preceding::div[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='antar niaga daya'])[2]/preceding::div[8]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
