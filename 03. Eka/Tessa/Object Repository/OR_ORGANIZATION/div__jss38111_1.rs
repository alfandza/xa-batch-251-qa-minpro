<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div__jss38111_1</name>
   <tag></tag>
   <elementGuidId>ff214fcd-249f-4c57-b3de-568251b43c74</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[3]/div/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.jss34981.jss38088.jss38092.jss34990.jss38095.jss34983.jss38090.jss34982.jss38089 > div.jss34991.jss38096.jss38109 > div.jss38111</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss38111</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss12467&quot;]/main[@class=&quot;jss12983 jss12985&quot;]/form[1]/div[@class=&quot;jss37434&quot;]/div[@class=&quot;jss37435&quot;]/div[@class=&quot;jss37438&quot;]/div[@class=&quot;jss13784 jss13787 jss38058&quot;]/div[@class=&quot;jss38065&quot;]/div[@class=&quot;css-10nd86i&quot;]/div[@class=&quot;jss38066 jss38067 jss38069&quot;]/div[@class=&quot;jss34981 jss38088 jss38092 jss34990 jss38095 jss34983 jss38090 jss34982 jss38089&quot;]/div[@class=&quot;jss34991 jss38096 jss38109&quot;]/div[@class=&quot;jss38111&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[3]/div/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New Manager'])[1]/preceding::div[7]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Description'])[1]/preceding::div[11]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[3]/div/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
