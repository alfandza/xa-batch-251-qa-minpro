<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>path (1)</name>
   <tag></tag>
   <elementGuidId>47e9b84a-d000-47dc-b05d-34118c428526</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.material-icons.jss9615 > svg.jss2708 > path</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>path</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>d</name>
      <type>Main</type>
      <value>M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;mui-fixed jss3558 jss3544&quot;]/div[@class=&quot;jss3547 jss3545&quot;]/div[@class=&quot;jss3511 jss3537 jss3512 jss3548 jss9410 jss3549 jss3552&quot;]/div[@class=&quot;jss9604 jss9411&quot;]/div[2]/div[@class=&quot;jss9610&quot;]/button[@class=&quot;jss2705 jss2699 jss9612&quot;]/span[@class=&quot;jss2704&quot;]/span[@class=&quot;material-icons jss9615&quot;]/svg[@class=&quot;jss2708&quot;]/path[1]</value>
   </webElementProperties>
</WebElementEntity>
