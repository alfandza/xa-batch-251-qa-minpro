<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>path (6)</name>
   <tag></tag>
   <elementGuidId>823cc8e1-d205-4d5e-bcc2-fc8329852448</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>path</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>d</name>
      <type>Main</type>
      <value>M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm5 11h-4v4h-2v-4H7v-2h4V7h2v4h4v2z</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss20010&quot;]/header[@class=&quot;jss21327 jss21329 jss22132 jss22133 jss22138 mui-fixed jss20266 jss20296&quot;]/div[@class=&quot;jss20938 jss20940 jss20939&quot;]/button[@class=&quot;jss20983 jss20977 jss20978&quot;]/span[@class=&quot;jss20982&quot;]/svg[@class=&quot;jss20986&quot;]/path[2]</value>
   </webElementProperties>
</WebElementEntity>
