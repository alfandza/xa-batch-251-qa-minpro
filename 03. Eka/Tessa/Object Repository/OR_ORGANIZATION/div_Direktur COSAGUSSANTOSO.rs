<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Direktur COSAGUSSANTOSO</name>
   <tag></tag>
   <elementGuidId>c04fd672-a633-4bb1-86cb-effcdd04b526</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.rst__rowContents.rst__rowContentsDragDisabled</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Expand All'])[1]/following::div[13]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>rst__rowContents rst__rowContentsDragDisabled</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Direktur COSAGUSSANTOSO</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;mui-fixed jss3558 jss3544&quot;]/div[@class=&quot;jss3547 jss3545&quot;]/div[@class=&quot;jss3511 jss3537 jss3512 jss3548 jss3549 jss3554 jss3556&quot;]/div[@class=&quot;jss7161&quot;]/div[1]/div[@class=&quot;rst__tree jss5509&quot;]/div[1]/div[@class=&quot;ReactVirtualized__Grid ReactVirtualized__List rst__virtualScrollOverride&quot;]/div[@class=&quot;ReactVirtualized__Grid__innerScrollContainer&quot;]/div[@class=&quot;rst__node&quot;]/div[@class=&quot;rst__nodeContent&quot;]/div[1]/div[@class=&quot;rst__rowWrapper&quot;]/div[@class=&quot;rst__row&quot;]/div[@class=&quot;rst__rowContents rst__rowContentsDragDisabled&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Expand All'])[1]/following::div[13]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Organization Chart'])[1]/following::div[14]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div[2]/div/div[2]/div/div</value>
   </webElementXpaths>
</WebElementEntity>
