<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>path_1</name>
   <tag></tag>
   <elementGuidId>8265fc1f-657c-4301-8f8b-9da5c32a9a3c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.jss2705.jss2699.jss2702 > span.jss2704 > svg.jss2708 > path</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>path</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>d</name>
      <type>Main</type>
      <value>M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1732&quot;]/div[@class=&quot;jss3560 jss3562&quot;]/div[@class=&quot;jss2664 jss2672 jss3511 jss3519 jss15055&quot;]/div[@class=&quot;jss15057&quot;]/button[@class=&quot;jss2705 jss2699 jss2702&quot;]/span[@class=&quot;jss2704&quot;]/svg[@class=&quot;jss2708&quot;]/path[1]</value>
   </webElementProperties>
</WebElementEntity>
