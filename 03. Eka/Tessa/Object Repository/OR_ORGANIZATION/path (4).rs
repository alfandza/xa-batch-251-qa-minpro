<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>path (4)</name>
   <tag></tag>
   <elementGuidId>1ab9f77f-2625-4d6c-b3d2-a0f25bca281b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>header.jss3511.jss3513.jss3567.jss3568.jss3573.mui-fixed.jss5568 > div.jss2660.jss2662.jss2661 > button.jss2705.jss2699.jss2700 > span.jss2704 > svg.jss2708 > path</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>path</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>d</name>
      <type>Main</type>
      <value>M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;mui-fixed jss3558 jss3544 jss5596&quot;]/div[@class=&quot;jss3547 jss3545&quot;]/div[@class=&quot;jss3511 jss3537 jss3512 jss3548 jss3549 jss3552 jss3557&quot;]/header[@class=&quot;jss3511 jss3513 jss3567 jss3568 jss3573 mui-fixed jss5568&quot;]/div[@class=&quot;jss2660 jss2662 jss2661&quot;]/button[@class=&quot;jss2705 jss2699 jss2700&quot;]/span[@class=&quot;jss2704&quot;]/svg[@class=&quot;jss2708&quot;]/path[1]</value>
   </webElementProperties>
</WebElementEntity>
