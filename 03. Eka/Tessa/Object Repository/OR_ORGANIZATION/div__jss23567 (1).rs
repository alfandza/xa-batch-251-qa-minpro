<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div__jss23567 (1)</name>
   <tag></tag>
   <elementGuidId>4942f95d-8607-40c1-bb18-d0a2be7a0bfd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[3]/div/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.jss21894.jss23544.jss23548.jss21900.jss23549.jss21903.jss23551.jss21896.jss23546.jss21895.jss23545 > div.jss21904.jss23552.jss23565 > div.jss23567</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss23567</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss12467&quot;]/main[@class=&quot;jss12983 jss12985&quot;]/form[1]/div[@class=&quot;jss22890&quot;]/div[@class=&quot;jss22891&quot;]/div[@class=&quot;jss22894&quot;]/div[@class=&quot;jss13784 jss13787 jss23514&quot;]/div[@class=&quot;jss23521&quot;]/div[@class=&quot;css-10nd86i&quot;]/div[@class=&quot;jss23522 jss23523 jss23525&quot;]/div[@class=&quot;jss21894 jss23544 jss23548 jss21900 jss23549 jss21903 jss23551 jss21896 jss23546 jss21895 jss23545&quot;]/div[@class=&quot;jss21904 jss23552 jss23565&quot;]/div[@class=&quot;jss23567&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[3]/div/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='BS 1 - Business Support Analyst'])[1]/preceding::div[7]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='BS 2 - Business Support Analyst'])[1]/preceding::div[8]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[3]/div/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
