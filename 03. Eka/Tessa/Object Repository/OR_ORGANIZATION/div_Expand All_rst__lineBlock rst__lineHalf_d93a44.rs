<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Expand All_rst__lineBlock rst__lineHalf_d93a44</name>
   <tag></tag>
   <elementGuidId>2273d860-b650-4ede-a832-2da501814a72</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.rst__lineBlock.rst__lineHalfHorizontalRight</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[2]/div/div[2]/div/div/div/div/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>rst__lineBlock rst__lineHalfHorizontalRight</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;mui-fixed jss3558 jss3544&quot;]/div[@class=&quot;jss3547 jss3545&quot;]/div[@class=&quot;jss3511 jss3537 jss3512 jss3548 jss3549 jss3554 jss3556&quot;]/div[@class=&quot;jss7161&quot;]/div[1]/div[@class=&quot;rst__tree jss5509&quot;]/div[1]/div[@class=&quot;ReactVirtualized__Grid ReactVirtualized__List rst__virtualScrollOverride&quot;]/div[@class=&quot;ReactVirtualized__Grid__innerScrollContainer&quot;]/div[@class=&quot;rst__node&quot;]/div[@class=&quot;rst__lineBlock rst__lineHalfHorizontalRight&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/div/div/div/div/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
