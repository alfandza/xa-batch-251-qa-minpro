<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div__jss21393 (1)</name>
   <tag></tag>
   <elementGuidId>9a3b1e34-d13f-4bd2-9059-f3a55d078741</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.jss21435.jss21422.jss21426.jss21444.jss21429.jss21437.jss21424.jss21436.jss21423 > div.jss21445.jss21430.jss21391 > div.jss21393</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[3]/div/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss21393</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1&quot;]/main[@class=&quot;jss5973 jss5975&quot;]/form[1]/div[@class=&quot;jss20759&quot;]/div[@class=&quot;jss20760&quot;]/div[@class=&quot;jss20763&quot;]/div[@class=&quot;jss6216 jss6219 jss21383&quot;]/div[@class=&quot;jss21390&quot;]/div[@class=&quot;css-10nd86i&quot;]/div[@class=&quot;jss21400 jss21401 jss21403&quot;]/div[@class=&quot;jss21435 jss21422 jss21426 jss21444 jss21429 jss21437 jss21424 jss21436 jss21423&quot;]/div[@class=&quot;jss21445 jss21430 jss21391&quot;]/div[@class=&quot;jss21393&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[3]/div/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Business Development Director'])[1]/preceding::div[7]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Curriculum Specialist'])[1]/preceding::div[8]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[3]/div/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
