<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>path (1)</name>
   <tag></tag>
   <elementGuidId>2cddf0ee-b6b9-4897-84cf-1d3ebedaa7b6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.css-1q3fsek > svg.css-19bqh2r > path</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>path</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>d</name>
      <type>Main</type>
      <value>M14.348 14.849c-0.469 0.469-1.229 0.469-1.697 0l-2.651-3.030-2.651 3.029c-0.469 0.469-1.229 0.469-1.697 0-0.469-0.469-0.469-1.229 0-1.697l2.758-3.15-2.759-3.152c-0.469-0.469-0.469-1.228 0-1.697s1.228-0.469 1.697 0l2.652 3.031 2.651-3.031c0.469-0.469 1.228-0.469 1.697 0s0.469 1.229 0 1.697l-2.758 3.152 2.758 3.15c0.469 0.469 0.469 1.229 0 1.698z</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1&quot;]/main[@class=&quot;jss5973 jss5975&quot;]/form[1]/div[@class=&quot;jss20759&quot;]/div[@class=&quot;jss20760&quot;]/div[@class=&quot;jss20763&quot;]/div[@class=&quot;jss6216 jss6219 jss21383&quot;]/div[@class=&quot;jss21390&quot;]/div[@class=&quot;css-10nd86i&quot;]/div[@class=&quot;jss21400 jss21401 jss21403&quot;]/div[@class=&quot;jss21435 jss21422 jss21426 jss21444 jss21429 jss21437 jss21424 jss21436 jss21423&quot;]/div[@class=&quot;jss21445 jss21430 jss21391&quot;]/div[@class=&quot;jss21392&quot;]/div[@class=&quot;css-1q3fsek&quot;]/svg[@class=&quot;css-19bqh2r&quot;]/path[1]</value>
   </webElementProperties>
</WebElementEntity>
