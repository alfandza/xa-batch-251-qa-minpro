<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_360 Assessment</name>
   <tag></tag>
   <elementGuidId>9e89e06e-5f94-4afd-8d3c-e6e4f87acf74</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.jss10218.jss10219 > div.jss10220 > div.jss10221 > div.jss7625.jss10204.jss10207.jss10212.jss10213 > div.jss10198.jss9766.jss9912</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div[2]/div/nav/div[5]/div/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss10198 jss9766 jss9912</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>360 Assessment</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss6652&quot;]/div[@class=&quot;jss8986 jss8987 jss9037 jss9211&quot;]/div[@class=&quot;jss8454 jss8456 jss8988 jss9211 jss8989 jss8993&quot;]/nav[@class=&quot;jss9713&quot;]/div[@class=&quot;jss10218 jss10219&quot;]/div[@class=&quot;jss10220&quot;]/div[@class=&quot;jss10221&quot;]/div[@class=&quot;jss7625 jss10204 jss10207 jss10212 jss10213&quot;]/div[@class=&quot;jss10198 jss9766 jss9912&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/div/nav/div[5]/div/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Human Resource'])[1]/following::div[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Finance Approval'])[1]/following::div[9]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Competencies Standard'])[1]/preceding::div[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Employee Profile'])[1]/preceding::div[7]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
