<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Employee is a required field</name>
   <tag></tag>
   <elementGuidId>35edb0a4-3897-4a65-9f2a-39c561af80b1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>p.jss19117.jss19118.jss19124</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[4]/div/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss19117 jss19118 jss19124</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Employee is a required field</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1&quot;]/main[@class=&quot;jss5973 jss5975&quot;]/form[1]/div[@class=&quot;jss18083&quot;]/div[@class=&quot;jss18084&quot;]/div[@class=&quot;jss18087&quot;]/div[@class=&quot;jss6216 jss6219 jss18707&quot;]/div[@class=&quot;jss18714&quot;]/div[@class=&quot;css-10nd86i&quot;]/div[@class=&quot;jss18724 jss18725 jss18727&quot;]/p[@class=&quot;jss19117 jss19118 jss19124&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div[4]/div/p</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assessor'])[1]/preceding::p[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Must have at least one responder'])[1]/preceding::p[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Employee is a required field']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/p</value>
   </webElementXpaths>
</WebElementEntity>
