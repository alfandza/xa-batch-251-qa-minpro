<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Employee is a required field (1)</name>
   <tag></tag>
   <elementGuidId>f2d88602-fc35-412d-a1f8-58bc5daf1f15</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>p.jss21756.jss21757.jss21763</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/form/div/div[2]/div/div/ul/div/div/div/div[2]/div/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss21756 jss21757 jss21763</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Employee is a required field</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1&quot;]/main[@class=&quot;jss5973 jss5975&quot;]/form[1]/div[@class=&quot;jss20759&quot;]/div[@class=&quot;jss20760&quot;]/div[@class=&quot;jss20763&quot;]/div[@class=&quot;jss6216 jss6219 jss21383&quot;]/ul[@class=&quot;jss6797 jss6798&quot;]/div[@class=&quot;jss7803 jss7804 jss21543&quot;]/div[@class=&quot;jss7805&quot;]/div[@class=&quot;jss7806&quot;]/div[@class=&quot;css-10nd86i&quot;]/div[@class=&quot;jss21400 jss21401 jss21403&quot;]/p[@class=&quot;jss21756 jss21757 jss21763&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div/div[2]/div/div/ul/div/div/div/div[2]/div/p</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assessor'])[2]/preceding::p[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add'])[1]/preceding::p[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Employee is a required field']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/p</value>
   </webElementXpaths>
</WebElementEntity>
