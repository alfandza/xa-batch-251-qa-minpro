<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div__jss9506</name>
   <tag></tag>
   <elementGuidId>955eaefe-7ac0-4b03-bf52-290b4473b9c2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.jss9506</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div/div/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss9506</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1703&quot;]/main[@class=&quot;jss2219 jss2221&quot;]/form[1]/div[@class=&quot;jss8872&quot;]/div[@class=&quot;jss8873&quot;]/div[@class=&quot;jss8876&quot;]/div[@class=&quot;jss3482 jss3485 jss9496&quot;]/div[@class=&quot;jss9503&quot;]/div[@class=&quot;css-10nd86i&quot;]/div[@class=&quot;jss9513 jss9514 jss9516&quot;]/div[@class=&quot;jss7909 jss9535 jss9539 jss7918 jss9542 jss7911 jss9537 jss7910 jss9536&quot;]/div[@class=&quot;jss7919 jss9543 jss9504&quot;]/div[@class=&quot;jss9506&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/main/form/div/div/div/div/div[2]/div/div/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='antar niaga daya'])[1]/preceding::div[7]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Equine Global'])[1]/preceding::div[8]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
