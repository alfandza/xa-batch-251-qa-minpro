<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>path</name>
   <tag></tag>
   <elementGuidId>a58172db-ea73-43f9-aefc-46a9a7d0b776</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.css-1q3fsek > svg.css-19bqh2r > path</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>path</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>d</name>
      <type>Main</type>
      <value>M4.516 7.548c0.436-0.446 1.043-0.481 1.576 0l3.908 3.747 3.908-3.747c0.533-0.481 1.141-0.446 1.574 0 0.436 0.445 0.408 1.197 0 1.615-0.406 0.418-4.695 4.502-4.695 4.502-0.217 0.223-0.502 0.335-0.787 0.335s-0.57-0.112-0.789-0.335c0 0-4.287-4.084-4.695-4.502s-0.436-1.17 0-1.615z</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;jss1732&quot;]/main[@class=&quot;jss2248 jss2250&quot;]/form[1]/div[@class=&quot;jss7456&quot;]/div[@class=&quot;jss7457&quot;]/div[@class=&quot;jss7460&quot;]/div[@class=&quot;jss3511 jss3514 jss8080&quot;]/div[@class=&quot;jss8087&quot;]/div[@class=&quot;css-10nd86i&quot;]/div[@class=&quot;jss8088 jss8089 jss8091&quot;]/div[@class=&quot;jss6480 jss8110 jss8114 jss6489 jss8117 jss6482 jss8112 jss6481 jss8111&quot;]/div[@class=&quot;jss6490 jss8118 jss8131&quot;]/div[@class=&quot;jss8132&quot;]/div[@class=&quot;css-1q3fsek&quot;]/svg[@class=&quot;css-19bqh2r&quot;]/path[1]</value>
   </webElementProperties>
</WebElementEntity>
