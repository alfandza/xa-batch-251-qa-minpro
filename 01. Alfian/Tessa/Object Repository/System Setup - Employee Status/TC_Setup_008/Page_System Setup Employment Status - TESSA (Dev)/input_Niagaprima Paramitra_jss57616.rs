<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Niagaprima Paramitra_jss57616</name>
   <tag></tag>
   <elementGuidId>03d6fa15-e8e0-4e23-9795-a6f21d285180</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//input[@value=''])[7]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>jss57616</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>radio</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;mui-fixed jss6774 jss6760 jss56306&quot;]/div[@class=&quot;jss6763 jss6761&quot;]/div[@class=&quot;jss6218 jss6244 jss6219 jss6764 jss6765 jss6768 jss6773&quot;]/ul[@class=&quot;jss6280 jss6281&quot;]/div[@class=&quot;jss5874 jss6251 jss6254 jss6259 jss6260&quot;]/span[@class=&quot;jss5874 jss5868 jss57613 jss57608 jss57612&quot;]/span[@class=&quot;jss5873&quot;]/input[@class=&quot;jss57616&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@value=''])[7]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/span/span/input</value>
   </webElementXpaths>
</WebElementEntity>
