import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://xdev.equine.co.id/apps/tessa/')

WebUI.click(findTestObject('Object Repository/LOGIN/Page_Welcome to New TESSA/button_Let me in'))

WebUI.switchToWindowTitle('')

WebUI.setText(findTestObject('Object Repository/LOGIN/Page_Login - Equine Identity/input_Login_Username'), 'della.mutiara@xsis.net')

WebUI.setEncryptedText(findTestObject('Object Repository/LOGIN/Page_Login - Equine Identity/input_Login_Password'), 'zP4F53vxAWI=')

WebUI.click(findTestObject('Object Repository/LOGIN/Page_Login - Equine Identity/button_Login'))

WebUI.switchToWindowTitle('Welcome to New TESSA')

WebUI.click(findTestObject('Object Repository/LOGIN/Page_Welcome to New TESSA/div_Xsis Mitra Utama'))

WebUI.click(findTestObject('Object Repository/LOGIN/Page_Welcome to New TESSA/div_Automation Test Submitter'))

