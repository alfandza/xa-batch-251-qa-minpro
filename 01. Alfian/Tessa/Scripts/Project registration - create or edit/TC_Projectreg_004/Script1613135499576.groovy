import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('LOGIN'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_004/Page_Dashboard - TESSA (Dev)/span_Project Registration'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_004/Page_Dashboard - TESSA (Dev)/span_Create  Edit Project'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_004/Page_Project Registrations - TESSA (Dev)/button_Project Registrations_jss2705 jss269_05d654'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_004/Page_New Project - TESSA (Dev)/div__jss8133'))

WebUI.setText(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_003/Page_New Project - TESSA (Dev)/input__react-select-2-input'), 
    'barima')

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_003/Page_New Project - TESSA (Dev)/div_Barima Handal R4jas'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_003/Page_New Project - TESSA (Dev)/div__jss8133'))

WebUI.setText(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_003/Page_New Project - TESSA (Dev)/input__react-select-3-input'), 
    'pre')

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_003/Page_New Project - TESSA (Dev)/div_Pre Sales'))

WebUI.setText(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_004/Page_New Project - TESSA (Dev)/input_POContract No._contractNumber'), 
    '12345432')

WebUI.setText(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_004/Page_New Project - TESSA (Dev)/input__name'), 
    'penjualan mobile')

WebUI.setText(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_004/Page_New Project - TESSA (Dev)/input_Description_description'), 
    'membuat aplikasi penjualan berbasis mobile')

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_004/Page_New Project - TESSA (Dev)/input__start'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_004/Page_New Project - TESSA (Dev)/button_15'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_004/Page_New Project - TESSA (Dev)/span_OK'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_004/Page_New Project - TESSA (Dev)/div_End'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_004/Page_New Project - TESSA (Dev)/button_28'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_004/Page_New Project - TESSA (Dev)/span_OK'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_004/Page_New Project - TESSA (Dev)/div__jss8133'))

WebUI.setText(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_003/Page_New Project - TESSA (Dev)/input__react-select-4-input'), 
    'id')

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_004/Page_New Project - TESSA (Dev)/div_IDR (Rp)'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_004/Page_New Project - TESSA (Dev)/button_Add'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_004/Page_New Project - TESSA (Dev)/button__jss2705 jss2699'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_004/Page_New Project - TESSA (Dev)/span_Submit'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_004/Page_New Project - TESSA (Dev)/span_Continue'))

WebUI.closeBrowser()

