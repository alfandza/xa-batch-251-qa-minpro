import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('LOGIN'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_013/Page_Dashboard - TESSA (Dev)/span_Project Registration'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_013/Page_Dashboard - TESSA (Dev)/span_Create  Edit Project'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_013/Page_Project Registrations - TESSA (Dev)/button_Project Registrations_jss2705 jss269_05d654'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_013/Page_New Project - TESSA (Dev)/div__jss6490 jss8118 jss8131'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_013/Page_New Project - TESSA (Dev)/div_CV. Adi Putra'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_013/Page_New Project - TESSA (Dev)/div__jss8133'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_013/Page_New Project - TESSA (Dev)/div_Pre Sales'))

WebUI.setText(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_013/Page_New Project - TESSA (Dev)/input_POContract No._contractNumber'), 
    '123434543')

WebUI.setText(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_013/Page_New Project - TESSA (Dev)/input__name'), 
    'penjualan mobile')

WebUI.setText(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_013/Page_New Project - TESSA (Dev)/input_Description_description'), 
    'membuat aplikasi')

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_013/Page_New Project - TESSA (Dev)/input__start'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_013/Page_New Project - TESSA (Dev)/button_15'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_013/Page_New Project - TESSA (Dev)/span_OK'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_013/Page_New Project - TESSA (Dev)/input__end'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_013/Page_New Project - TESSA (Dev)/span_28'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_013/Page_New Project - TESSA (Dev)/span_OK'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_013/Page_New Project - TESSA (Dev)/button_Add'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_013/Page_New Project - TESSA (Dev)/div__jss6490 jss8118 jss8131_1'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_013/Page_New Project - TESSA (Dev)/div__jss8133_1'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_013/Page_New Project - TESSA (Dev)/div__jss8133_1'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_013/Page_New Project - TESSA (Dev)/div_ELDI HERWANSYAH'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_013/Page_New Project - TESSA (Dev)/span_Submit'))

WebUI.click(findTestObject('Object Repository/Project registration - create or edit/TC_Projectreg_013/Page_New Project - TESSA (Dev)/span_Continue'))

WebUI.closeBrowser()

