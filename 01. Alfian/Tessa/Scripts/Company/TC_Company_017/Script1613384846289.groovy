import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('LOGIN'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Company/TC_Company_017/Page_Dashboard - TESSA (Dev)/div_Setup'))

WebUI.click(findTestObject('Object Repository/Company/TC_Company_017/Page_Dashboard - TESSA (Dev)/div_Company'))

WebUI.click(findTestObject('Object Repository/Company/TC_Company_017/Page_Company - TESSA (Dev)/div_CP002XMUAutomation TesterAutomation Tes_b45407'))

WebUI.click(findTestObject('Object Repository/Company/TC_Company_017/Page_Company - TESSA (Dev)/span_Modify'))

WebUI.setText(findTestObject('Object Repository/Company/TC_Company_017/Page_Modify Company - TESSA (Dev)/input__code'), 
    '')

WebUI.click(findTestObject('Object Repository/Company/TC_Company_017/Page_Modify Company - TESSA (Dev)/button_Submit'))

WebUI.click(findTestObject('Object Repository/Company/TC_Company_017/Page_Modify Company - TESSA (Dev)/span_Continue'))

WebUI.closeBrowser()

